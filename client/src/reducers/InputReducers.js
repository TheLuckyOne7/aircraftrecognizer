import { createReducer } from '../utils/redux'
import * as R from 'ramda'

const initialState = {
	manual: {
		carrierFrequency: '',
		durationPuls: '',
		widthSpectrumPuls: '',
		modulationRadar: '',
		cycleTimeInPack: '',
		freqPulsesInAPack: '',
		durationPack: '',
		periodPack: '',
		frequencyNumber: '',
		freqPulsesInPack: '',
		durRadioWaves: '',
		periodObjRWaves: '',
	},
	data: []
}

export default createReducer(initialState, {
	MANUAL_INPUT: (currentState, action) => {
		return R.assocPath(['manual', action.data.target.id], action.data.target.value)(currentState)
	},
	MANUAL_CLEAR: (currentState, action) => R.assoc('manual', initialState.manual)(currentState),
	MANUAL_APPLY: (currentState, action) => R.pipe(
		R.assoc('data', R.append({id: action._id, ...currentState.manual}, currentState.data)),
		R.assoc('manual', initialState.manual)
	)(currentState),
	FILE_INPUT: (currentState, action) => {
		let res = action.data.map((e, idx) => R.assoc('id', action._id + idx)(e))
		return R.assoc('data', R.concat(currentState.data, res))(currentState)
	},
	FETCH_SIMULATION_DATA_SUCCESS: (currentState, action)=> {
		return R.assoc('data', R.append({id: action._id, ...action.data}, currentState.data))(currentState)
	},
	CLEAR_RESULT: (currentState) => R.assoc('data', initialState.data)(currentState)
})
