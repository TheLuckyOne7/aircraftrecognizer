import {combineReducers} from 'redux'

import input from './InputReducers'
import result from './ResultReducers'
import loader from './LoaderReducers'

export default combineReducers({
	input,
	result,
	loader
})
