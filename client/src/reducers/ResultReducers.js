import { createReducer } from '../utils/redux'
import * as R from 'ramda'

const initialState = []

export default createReducer(initialState, {
	RECOGNIZE_SUCCES: (state, action) => R.concat(state, action.data),
	CLEAR_RESULT: (state) => initialState
})
