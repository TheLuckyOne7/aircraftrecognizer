import { createReducer } from '../utils/redux'

const initialState = {
	visible: false
}

export default createReducer(initialState, {
	FETCH_SIMULATION_DATA: (currentState, action) => ({visible: true}),
	FETCH_SIMULATION_DATA_SUCCESS: (currentState, action) => ({visible: false}),
	FETCH_SIMULATION_DATA_FAILURE: (currentState, action) => ({visible: false}),
	RECOGNIZE_RE_FACILITIES: (currentState, action) => ({visible: true}),
	RECOGNIZE_SUCCES: (currentState, action) => ({visible: false}),
	RECOGNIZE_FAILURE: (currentState, action) => ({visible: false})
})
