import * as R from 'ramda'

export let resultButtonsSelector = (state) => ({
	isClearDisabled: !state.input.data.length,
	isRecognizeDisabled: !state.input.data.length || state.result.length === state.input.data.length
})

export let resultGridSelector = (state) => ({
	result: R.map(x => {
		let res = R.find(R.propEq('id', x.id))(state.result)
		return res ? R.assoc('result', res.result)(x) : x
	})(state.input.data)
})

export let manualTabSelector = (state) => ({
	carrierFrequency: state.input.manual.carrierFrequency,
	durationPuls: state.input.manual.durationPuls,
	widthSpectrumPuls: state.input.manual.widthSpectrumPuls,
	modulationRadar: state.input.manual.modulationRadar,
	cycleTimeInPack: state.input.manual.cycleTimeInPack,
	freqPulsesInAPack: state.input.manual.freqPulsesInAPack,
	durationPack: state.input.manual.durationPack,
	periodPack: state.input.manual.periodPack,
	frequencyNumber: state.input.manual.frequencyNumber,
	freqPulsesInPack: state.input.manual.freqPulsesInPack,
	durRadioWaves: state.input.manual.durRadioWaves,
	periodObjRWaves: state.input.manual.periodObjRWaves
})
