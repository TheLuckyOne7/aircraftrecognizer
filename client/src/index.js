import React from 'react'
import ReactDOM from 'react-dom'

import { Provider } from 'react-redux'
import {createStore, applyMiddleware} from 'redux'
import thunk from 'redux-thunk'
import reducers from './reducers'

import MuiThemeProvider from 'material-ui/styles/MuiThemeProvider'
import baseTheme from 'material-ui/styles/baseThemes/lightBaseTheme'
import getMuiTheme from 'material-ui/styles/getMuiTheme'

import * as R from 'ramda'

import App from './components/App'

import './index.css'

const store = createStore(reducers, applyMiddleware(thunk))

ReactDOM.render(
	<Provider store={store}>
		<MuiThemeProvider muiTheme={getMuiTheme(baseTheme)}>
			<App />
		</MuiThemeProvider>
	</Provider>,
	document.getElementById('root')
);

window.R = R;
window.store = store;
