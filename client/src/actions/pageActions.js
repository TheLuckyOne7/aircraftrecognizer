import { createActions } from '../utils/redux'
import * as R from 'ramda'
import cuid from 'cuid'

export let resultButtonsActions = createActions ([
	'CLEAR_RESULT',
	{
		type: 'RECOGNIZE_RE_FACILITIES',
		act: function(){
			return (dispatch, getState) => {
				dispatch({type: this.type})
				let input = getState().input.data
				let result = R.pluck('id')(getState().result)
				let payload = R.filter(x => result.indexOf(x.id) === -1)(input)
				fetch('/recognize',
					{
						method: 'POST',
						headers: {Accept: 'application/json', 'Content-Type': 'application/json'},
						body: JSON.stringify(payload)
					}
				)
				.then(res => res.json())
				.then(json => dispatch({type: 'RECOGNIZE_SUCCES', data: json}))
				.catch(err => dispatch({type: 'RECOGNIZE_FAILURE'}))
			}
		}
	}
])

export let manualTabActions = createActions([
	'MANUAL_INPUT',
	'MANUAL_CLEAR',
	'MANUAL_APPLY'
])

export let importTabActions = createActions([
	'FILE_INPUT'
])

export let simulationTabActions = createActions([
	{
		type: 'FETCH_SIMULATION_DATA',
		act: function(){
			return (dispatch, getState) => {
				dispatch({type: this.type})
				fetch('/simulaton-log',
					{
						method: 'GET',
						headers: {Accept: 'application/json'},
					}
				)
				.then(res => res.json())
				.then(json => dispatch({type: 'FETCH_SIMULATION_DATA_SUCCESS', data: json, _id: cuid()}))
				.catch(err => dispatch({type: 'FETCH_SIMULATION_DATA_FAILURE'}))
			}
		}
	}
])
