import React, { Component } from 'react'

import InputDataTabs from './InputDataTabs'
import Result from './Result'

export default class Main extends Component {
	render(){
		return [<InputDataTabs key="input-data-tabs" />, <Result key="result" />]
	}
}
