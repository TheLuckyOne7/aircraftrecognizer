import React, {Component} from 'react'
import {connect} from 'react-redux'

import RaisedButton from 'material-ui/RaisedButton'
import TextField from 'material-ui/TextField'

import {manualTabActions} from '../../../actions/pageActions'
import {manualTabSelector} from '../../../selectors/pageSelector'

export class ManualTab extends Component {
	render(){
		let {props} = this
		console.log('RENDER ManualTab', props)
		return (
			<div>
				<div style={{display: 'flex'}}>
					<TextField
						floatingLabelText="Carrier Frequency"
						id='carrierFrequency'
						style={{margin: '0 15px'}}
						type="number"
						value={props.carrierFrequency}
						onChange={props.MANUAL_INPUT}
					/>
					<TextField
						floatingLabelText="Duration Pulss"
						id='durationPuls'
						style={{margin: '0 15px'}}
						type="number"
						value={props.durationPuls}
						onChange={props.MANUAL_INPUT}
					/>
					<TextField
						floatingLabelText="Width Spectrum Puls"
						id='widthSpectrumPuls'
						style={{margin: '0 15px'}}
						type="number"
						value={props.widthSpectrumPuls}
						onChange={props.MANUAL_INPUT}
					/>
					<TextField
						floatingLabelText="Modulation Radar S"
						id='modulationRadar'
						style={{margin: '0 15px'}}
						type="number"
						value={props.modulationRadar}
						onChange={props.MANUAL_INPUT}
					/>
					<TextField
						floatingLabelText="Cycle Time In Pack"
						id='cycleTimeInPack'
						style={{margin: '0 15px'}}
						type="number"
						value={props.cycleTimeInPack}
						onChange={props.MANUAL_INPUT}
					/>
					<TextField
						floatingLabelText="Freq Pulses In A Pack"
						id='freqPulsesInAPack'
						style={{margin: '0 15px'}}
						type="number"
						value={props.freqPulsesInAPack}
						onChange={props.MANUAL_INPUT}
					/>
				</div>
				<div style={{display: 'flex'}}>
					<TextField
						floatingLabelText="Duration Pack"
						id='durationPack'
						style={{margin: '0 15px'}}
						type="number"
						value={props.durationPack}
						onChange={props.MANUAL_INPUT}
					/>
					<TextField
						floatingLabelText="Period Pack"
						id='periodPack'
						style={{margin: '0 15px'}}
						type="number"
						value={props.periodPack}
						onChange={props.MANUAL_INPUT}
					/>
					<TextField
						floatingLabelText="Frequency Number"
						id='frequencyNumber'
						style={{margin: '0 15px'}}
						type="number"
						value={props.frequencyNumber}
						onChange={props.MANUAL_INPUT}
					/>
					<TextField
						floatingLabelText="Freq Pulses In Pack"
						id='freqPulsesInPack'
						style={{margin: '0 15px'}}
						type="number"
						value={props.freqPulsesInPack}
						onChange={props.MANUAL_INPUT}
					/>
					<TextField
						floatingLabelText="Dur Of R Irradiation Obj"
						id='durRadioWaves'
						style={{margin: '0 15px'}}
						type="number"
						value={props.durRadioWaves}
						onChange={props.MANUAL_INPUT}
					/>
					<TextField
						floatingLabelText="Period Of R Irradiation Obj"
						id='periodObjRWaves'
						style={{margin: '0 15px'}}
						type="number"
						value={props.periodObjRWaves}
						onChange={props.MANUAL_INPUT}
					/>
				</div>
				<div style={{textAlign: 'right', margin: '10px 50px'}}>
					<RaisedButton
						label="Clear"
						style={{margin: '0 15px'}}
						onClick={props.MANUAL_CLEAR}
					/>
					<RaisedButton
						label="Apply"
						style={{margin: '0 15px'}}
						primary={true}
						onClick={props.MANUAL_APPLY}
					/>
				</div>
			</div>
		)
	}
}

export default connect(manualTabSelector, manualTabActions)(ManualTab)
