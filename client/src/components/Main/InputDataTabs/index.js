import React, {Component} from 'react'

import {Tabs, Tab} from 'material-ui/Tabs'

import ManualTab from './ManualTab'
import ImportTab from './ImportTab'
import SimulationTab from './SimulationTab'

export default class InputDataTabs extends Component {
	render(){
		return (
			<Tabs style={{height: '300px'}}>
				<Tab label="Manual input data" value={0} >
					<ManualTab />
				</Tab>
				<Tab label="Input data via file" value={1} >
					<ImportTab />
				</Tab>
				<Tab label="Recognization simulation flight" value={2} >
					<SimulationTab />
				</Tab>
			</Tabs>
		)
	}
}
