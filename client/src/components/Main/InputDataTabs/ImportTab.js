import React, {Component} from 'react'
import {connect} from 'react-redux'

import RaisedButton from 'material-ui/RaisedButton'
import FileUpload from 'material-ui/svg-icons/editor/attach-file';

import {importTabActions} from '../../../actions/pageActions'

export class ImportTab extends Component {
	render(){
		let {props} = this
		return (
			<div>
			<RaisedButton
				containerElement='label'
				icon={<FileUpload />}>
					<input
						type="file"
						style={{display: 'none'}}
						accept=".json"
						onChange={ (e) => {
							let reader = new FileReader();
							reader.onload = function(event) {
								var jsonObj = JSON.parse(event.target.result);
								props.FILE_INPUT(jsonObj)
							}
							reader.readAsText(e.target.files[0]);
						}}
					/>
			</RaisedButton>
			</div>
		)
	}
}

export default connect(null, importTabActions)(ImportTab)
