import React, {Component} from 'react'
import {connect} from 'react-redux'

import RaisedButton from 'material-ui/RaisedButton'

import {simulationTabActions} from '../../../actions/pageActions'

export class SimulationtTab extends Component {
	render(){
		let {props} = this
		return (
			<div>
				<RaisedButton
					label="Fetch simulation data"
					onClick={props.FETCH_SIMULATION_DATA}
				/>
			</div>
		)
	}
}

export default connect(null, simulationTabActions)(SimulationtTab)
