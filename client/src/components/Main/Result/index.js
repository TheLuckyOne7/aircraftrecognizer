import React, { Component } from 'react'

import ResultButtons from './ResultButtons'
import ResultGrid from './ResultGrid'

export default class Result extends Component {
	render(){
		return [<ResultButtons key="result-buttons" />, <ResultGrid key="result-grid" />]
	}
}
