import React, { Component } from 'react'
import {connect} from 'react-redux'

import RaisedButton from 'material-ui/RaisedButton'

import {resultButtonsSelector} from '../../../selectors/pageSelector'
import {resultButtonsActions} from '../../../actions/pageActions'

export class ResultButtons extends Component {
	render(){
		let { props } = this
		return (
			<div>
				<RaisedButton
					label="Clear Result"
					primary={false}
					style={{margin: '15px'}}
					disabled={props.isClearDisabled}
					onClick={props.CLEAR_RESULT}
				/>
				<RaisedButton
					label="Recognize"
					primary={true}
					style={{margin: '15px'}}
					disabled={props.isRecognizeDisabled}
					onClick={props.RECOGNIZE_RE_FACILITIES}
				/>
			</div>
		)
	}
}

export default connect(resultButtonsSelector, resultButtonsActions)(ResultButtons)
