import React, { Component } from 'react'
import {connect} from 'react-redux'

import {Table, TableBody, TableHeader, TableHeaderColumn, TableRow, TableRowColumn} from 'material-ui/Table';

import {resultGridSelector} from '../../../selectors/pageSelector'

export class ResultGrid extends Component {
	render(){
		let { props } = this
		return (
			<Table
				selectable={false}
			>
				<TableHeader adjustForCheckbox={false} displaySelectAll={false} style={{height: '60px'}}>
					<TableRow >
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Carrier Frequency</TableHeaderColumn>
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Duration Puls</TableHeaderColumn>
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Width Spectrum Puls</TableHeaderColumn>
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Modulation Radar S</TableHeaderColumn>
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Cycle Time In Pack</TableHeaderColumn>
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Freq Pulses In A Pack</TableHeaderColumn>
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Duration Pack</TableHeaderColumn>
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Period Pack</TableHeaderColumn>
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Frequency Number</TableHeaderColumn>
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Freq Pulses In Pack</TableHeaderColumn>
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Dur Of R Irradiation Obj</TableHeaderColumn>
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Period Of R Irradiation Obj</TableHeaderColumn>
					<TableHeaderColumn style={{whiteSpace: 'normal', textAlign: 'center'}}>Result</TableHeaderColumn>
					</TableRow>
				</TableHeader>
				<TableBody displayRowCheckbox={false}>
					{props.result.map(res => (
						<TableRow key={res.id}>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.carrierFrequency}</TableRowColumn>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.durationPuls}</TableRowColumn>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.widthSpectrumPuls}</TableRowColumn>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.modulationRadar}</TableRowColumn>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.cycleTimeInPack}</TableRowColumn>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.freqPulsesInAPack}</TableRowColumn>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.durationPack}</TableRowColumn>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.periodPack}</TableRowColumn>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.frequencyNumber}</TableRowColumn>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.freqPulsesInPack}</TableRowColumn>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.durRadioWaves}</TableRowColumn>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.periodObjRWaves}</TableRowColumn>
							<TableRowColumn style={{whiteSpace: 'normal', textAlign: 'center'}} >{res.result}</TableRowColumn>
						</TableRow>
					))}
				</TableBody>
			</Table>
		)
	}
}

export default connect(resultGridSelector)(ResultGrid)
