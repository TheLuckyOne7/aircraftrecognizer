import React, { Component } from 'react'
import { connect } from 'react-redux'

import CircularProgress from 'material-ui/CircularProgress'

import loaderSelector from '../../selectors/loaderSelector'

export class Loader extends Component {
	render(){
		let { props } = this
		return (
			<div
				style={{
					display: props.visible ? 'block' : 'none',
					background: 'rgba(0,0,0,0.2)',
					position: 'absolute',
					width: '100%',
					height: '100%',
					top: '0',
					zIndex: '9999',
					textAlign: 'center',
					paddingTop: '20%'
				}}
			>
				<CircularProgress size={100} thickness={7} />
			</div>
		)
	}
}
export default connect(loaderSelector)(Loader)
