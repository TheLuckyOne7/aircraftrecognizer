import cuid from 'cuid'

export function createActions(actions, defaults) {
	return actions.reduce(function(actions, action) {
		if (typeof action === 'string') action = { type: action }
		action = Object.assign({}, defaults, action)
		if (!action.act) {
			action.act = function(data) {
				return { type: this.type, data, date: new Date(), _id: cuid() }
			}
		}
		var act = function(data) {
			return action.act(data)
		}
		Object.assign(act, action)
		actions[action.type] = act
		return actions
	}, {})
}

export function createReducer(initialState, handlers) {
  	return function reducer(state = initialState, action, ...rest) {
  		const handler = handlers[action.type]
  		return handler ? handler(state, action, ...rest) : state
  	}
}
