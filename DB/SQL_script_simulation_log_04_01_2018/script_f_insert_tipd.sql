﻿CREATE OR REPLACE FUNCTION re_situation.f_insert_tipd(
p_operative_time interval second, -- оперативное время 
p_air_object_id integer -- номер воздушного объекта 

)
RETURNS boolean AS
$BODY$
declare 
tipd_less re_situation.situation; -- для хранения строки таблицы с ближайшим меньшим оперативным временем для заданного ВО 
tipd_more re_situation.situation; -- для хранения строки таблицы с ближайшим большим оперативным временем для заданного ВО
time_1 double precision;
time_2 double precision;
time_dp double precision;
rec_1 record;
rec_2 record;
time_in interval second;


begin 
-- проверим нет ли для заданного ВО строки с заданным оперативным временем 
select * 
into tipd_less
from re_situation.situation
where operative_time=p_operative_time and air_object_id=p_air_object_id ;

-- если такая строка есть, то остановить выполнение программы и выдать сообщение  
if tipd_less.situation_id is not null 
then 
raise notice 'Для заданного объекта точка с оперативным временем % уже есть.
Новая точка изменения параметров движения не вставлена.', tipd_less.operative_time;
return false;  
end if;



-- имеем время и номер объекта 
-- найти ТИПД с ближайшим большим и ближайшим меньшим временем для заданного воздушного объекта 
-- найти ТИПД с ближайшим меньшим временем для заданного воздушного объекта
select * 
into tipd_less
from re_situation.situation
where air_object_id=p_air_object_id and operative_time<p_operative_time
order by operative_time desc
limit 1 ;

raise notice 'tipd_less.situation_id=%', tipd_less.situation_id; 

-- найти ТИПД с ближайшим большим временем для заданного воздушного объекта 
select * 
into tipd_more
from re_situation.situation
where air_object_id=p_air_object_id and operative_time>p_operative_time
order by operative_time asc 
limit 1 ;
raise notice 'tipd_more.situation_id=%', tipd_more.situation_id;

-- возможны четыре ситуации
case
when (tipd_less.situation_id is null ) and (tipd_more.situation_id is null)
--  вставляемая точка первая и единственная в сценарии для заданного ВО
then
-- для заданного объекта вставить точку с координатами по умолчанию и нулевым временем и скоростями  .
insert into re_situation.situation( 
situation_id, operative_time, air_object_id, x_longitude, y_latitude, h_height, course, horizontal_velocity, vertical_velocity)
values (default, '0', p_air_object_id, 121.505, 25.935, 0, 90, 0, 0);
 
when (tipd_less.situation_id is not null) and (tipd_more.situation_id is null) 
-- вставляемая точка последняя в сценарии для заданного объекта 
then 
-- рассчитать координаты соответствующие заданному оперативному времени, вставить точку с этими координатами 
-- скорости и курс взять такие же как у предидущей точки 
time_1:=extract(hours from (p_operative_time))*3600 + extract(minutes from (p_operative_time))*60 + extract(seconds from (p_operative_time));
time_2:=extract(hours from (tipd_less.operative_time))*3600 + extract(minutes from (tipd_less.operative_time))*60 + extract(seconds from (tipd_less.operative_time));
time_dp:=time_2-time_1;

rec_1:=re_situation.f_position_in_time(
tipd_less.x_longitude , tipd_less.y_latitude, tipd_less.h_height, tipd_less.course, 
tipd_less.horizontal_velocity, tipd_less.vertical_velocity, time_dp );

time_in:=''''||time_dp::text||'''';
time_in:=time_in+tipd_less.operative_time;

insert into re_situation.situation( 
situation_id, operative_time, air_object_id, x_longitude, y_latitude, h_height, course, horizontal_velocity, vertical_velocity)
values (default, time_in, p_air_object_id, rec_1.x2, rec_1.y2, rec_1.h2, tipd_less.course, 
tipd_less.horizontal_velocity, tipd_less.vertical_velocity);

when (tipd_less.situation_id is null) and (tipd_more.situation_id is not null)
-- попытка вставить первую точку в уже имеющийся маршрут для заданного ВО 
then 
-- этого делать нельзя. Остановть выполнение подпрограммы и выдать сообщение 
raise notice 'Предпринимается попытка вставить первую точку в уже имеющийся маршрут воздушного объекта с id=% 
Нельзя изменять начало маршрута таким образом.', p_air_object_id ; 
return false;

when (tipd_less.situation_id is not null) and (tipd_more.situation_id is not null)
-- попытка вставить промежуточную точку между двумя уже имеющимися точками маршрута заданного ВО
then 
-- для заданного оперативного времени рассчитать координаты и вставить в таблицу, курс и скорости оставить старыми 
-- рассчитать координаты соответствующие заданному оперативному времени, вставить точку с этими координатами 
-- скорости и курс взять такие же как у предидущей точки 
time_1:=extract(hours from (tipd_less.operative_time))*3600 + extract(minutes from (tipd_less.operative_time))*60 + extract(seconds from (tipd_less.operative_time));
time_2:=extract(hours from (p_operative_time))*3600 + extract(minutes from (p_operative_time))*60 + extract(seconds from (p_operative_time));
time_dp:=time_2-time_1;

rec_1:=re_situation.f_position_in_time(
tipd_less.x_longitude , tipd_less.y_latitude, tipd_less.h_height, tipd_less.course, 
tipd_less.horizontal_velocity, tipd_less.vertical_velocity, time_dp );

time_in:=''''||time_dp::text||'''';
time_in:=time_in+tipd_less.operative_time;

insert into re_situation.situation( 
situation_id, operative_time, air_object_id, x_longitude, y_latitude, h_height, course, horizontal_velocity, vertical_velocity)
values (default, time_in, p_air_object_id, rec_1.x2, rec_1.y2, rec_1.h2, tipd_less.course, 
tipd_less.horizontal_velocity, tipd_less.vertical_velocity);

end case;



 return true;

end;
$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;