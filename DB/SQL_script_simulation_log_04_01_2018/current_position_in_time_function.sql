﻿-- Function: current_position_in_time(real, real, real)
-- Расчет местоположения воздушного объекта для заданного момента времени 
-- При известном начальном положении X, Y, h, v1, v2, curs, time .
-- DROP FUNCTION current_position_in_time(real, real, real);

CREATE OR REPLACE FUNCTION simulation_log.current_position_in_time(
IN X double precision, -- долгота 
in Y double precision, -- широта
in h double precision, -- висота полета 
in V1 double precision, -- горизонтальная скорость полета 
in V2 double precision, -- вертикальная скорость полета 
in Curs double precision, -- курс ЛА
in delta_t double precision, -- время от момента соответствующего в сценприи ТИПД
in air_obj integer -- air_object_id номер воздушного обїекта
)
RETURNS simulation_log.air_situation 
-- TABLE (
-- Xr double precision, -- долгота расчетная на момент времени  "delta_t" 
-- Yr double precision, -- широта расчетная на момент времени  "delta_t" 
-- hr double precision -- высота расчетная на момент времени  "delta_t" 
-- ) 
AS
$BODY$
declare 
new_h double precision;
avg_h double precision;
l_dugy double precision;
R_zemli double precision :=6371;  -- радиус Земли 
fi_dugi double precision;
grad_v_rad double precision :=PI()/180 ; -- в Пи радиан - 180 градусов
rad_v_grad double precision :=180/PI() ; -- в 180 градусах - Пи радиан 
 
 
 
  t_row simulation_log.air_situation;
  

begin
-- определяем расчетную высоту. расчетная высота не может быть меньше "нуля"
if h<30 then h:=30; end if; 
new_h:=h+V2*delta_t;
if new_h<30 then new_h:=30; end if; 


-- определяем среднюю высоту между начальной и средней точками.
avg_h:=(h+(new_h-h)/2)*1e-3;

-- рассчитываем расстояние в километрах которое пролетел ЛА из заданной точки 
-- до момента времени "delta_t"
l_dugy:=V1*delta_t*1e-3;


-- рассчитываем угол "начальная точка"-"центр Земли"-"расчетная точка"
-- при условии, что полет происходит на средней выоте.
fi_dugi:=(l_dugy/(R_zemli+avg_h))*rad_v_grad;
-- fi_dugi:=(l_dugy/R_zemli)*rad_v_grad;


-- рассчитываем угловые координаты новой точки
t_row.x_longitude :=X+fi_dugi*sin(grad_v_rad*Curs);
t_row.y_latitude := Y+fi_dugi*cos(grad_v_rad*Curs);

 
-- заполняем незаполненніе поля структуры "t_row"
t_row.air_situation_id:=1;
t_row.modeling_time:=delta_t;
t_row.air_object_id:=air_obj;
t_row.h_height :=new_h;
 
return t_row;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE
