﻿-- Function: f_curs_v1_v2() рачсет курса вертикальной и горизонтальной скоростей 
-- по заданным начальным, конечным координатам ивысотам ВО и интервалу времени 

CREATE OR REPLACE FUNCTION re_situation.f_curs_v1_v2(
x1 double precision,
y1 double precision, 
h1 double precision, 
x2 double precision,
y2 double precision, 
h2 double precision, 
delta_time double precision)
returns record as
$BODY$
declare
rec_1 record;
avg_h double precision;
l_dugy double precision;
R_zemli double precision :=6371;  -- радиус Земли 
fi_dugy double precision;
grad_v_rad double precision :=PI()/180 ; -- в Пи радиан - 180 градусов
rad_v_grad double precision :=180/PI() ; -- в 180 градусах - Пи радиан 
v1 double precision;  -- переменная для хранения горизонтальной скорости;
v2 double precision;  -- переменная для хранения вертикальной скорости 
curs double precision; -- переменная для хранения курса ВО
delta_x double precision; -- переменная для хранения разности долгот 
delta_y double precision; -- переменна для хранения разности широт 
x22 double precision; 
y22 double precision; 

begin
-- Определяем среднюю высоту полета 
avg_h=h1+((h2-h1)/2);

-- Определяем угол между точками: точка(x1,y1) - центр Земли - (x2, y2)
fi_dugy=(acos(sin(grad_v_rad*y1)*sin(grad_v_rad*y2)+cos(grad_v_rad*y1)*cos(grad_v_rad*y2)*cos(grad_v_rad*(x1-x2))));
raise notice 'fi_dugy=%', fi_dugy;


-- Определяем длинну дуги в километрах
l_dugy=(R_zemli+avg_h*1e-3) * fi_dugy;
raise notice 'l_dugy=%', l_dugy;

-- Определяем горизонтальную скорость 
v1=(l_dugy*1e3)/delta_time;

-- Определяем вертикальную скорость 
v2=(h2-h1)/delta_time;

-- Определяем курс ВО
curs:= re_situation.f_curs_1(x1, y1, x2, y2);

select curs, v1, v2 into rec_1;
return rec_1 ;
end

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;
 