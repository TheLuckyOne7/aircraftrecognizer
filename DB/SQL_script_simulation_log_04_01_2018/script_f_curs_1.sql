﻿-- Function: re_situation.f_curs(...) - вычисление курса по координатам начальной (x1, y1) и конечной точек.


CREATE OR REPLACE FUNCTION re_situation.f_curs_1(
x1 double precision, 
y1 double precision, 
x2 double precision, 
y2 double precision 
)
returns double precision as 
$BODY$

declare 
curs double precision ;
grad_v_rad double precision :=PI()/180 ; -- в Пи радиан - 180 градусов
rad_v_grad double precision :=180/PI() ; -- в 180 градусах - Пи радиан 
delta_x double precision;  
delta_y double precision;  

begin
delta_x:=x2-x1; 
delta_y:=y2-y1;

-- raise notice 'delta_x=%,  delta_y=%', delta_x, delta_y;

if (delta_x=0)and(delta_y=0) then curs:=0;
else
curs:=atan(
sin(grad_v_rad*delta_x)*cos(grad_v_rad*y2)/
(cos(grad_v_rad*y1)*sin(grad_v_rad*y2)- sin(grad_v_rad*y1)*cos(grad_v_rad*y2)*cos(grad_v_rad*delta_x) )
)*rad_v_grad;
end if;

-- raise notice 'curs:=%', curs;

if delta_x>=0 -- начало оператора if delta_x>=0
then -- оператор if delta_x>=0
if delta_y>0 then curs:=curs;
else if delta_y=0 then curs=90; else curs=180+curs; end if;
end if;

else -- оператор if delta_x>=0
if delta_y>0 then curs=360+curs ;
else if delta_y=0 then curs=270; else curs=180+curs ; end if;
end if; 

end if; -- конец оператора if delta_x>=0




return curs ;

end;

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;