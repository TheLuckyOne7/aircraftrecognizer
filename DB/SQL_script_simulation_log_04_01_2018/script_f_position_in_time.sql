﻿-- Function: re_situation.f_position_in_time(...)

CREATE OR REPLACE FUNCTION re_situation.f_position_in_time(
x1 double precision, 
y1 double precision, 
h1 double precision,
curs double precision, 
v1 double precision,
v2 double precision,
delta_time double precision
)
returns record as 
$BODY$

declare 
avg_h double precision;
l_dugy double precision;
R_zemli double precision :=6371;  -- радиус Земли 
fi_dugy double precision;
grad_v_rad double precision :=PI()/180 ; -- в Пи радиан - 180 градусов
rad_v_grad double precision :=180/PI() ; -- в 180 градусах - Пи радиан 
x2 double precision; -- переменная для хранения новой долготы 
y2 double precision; -- переменная для хранения новой широты 
h2 double precision; -- переменные для хранения новой высоты 
rec_1 record; -- переменная для хранения результатов расчетов


begin 
-- определяем расчетную высоту. расчетная высота не может быть меньше "нуля"
if h1<30 then h1:=30; end if; 
h2:=h1+v2*delta_time;
if h2<30 then h2:=30; end if; 


-- определяем среднюю высоту между начальной и средней точками.
avg_h:=(h1+(h2-h1)/2)*1e-3;

-- рассчитываем расстояние в километрах которое пролетел ЛА из заданной точки 
-- до момента времени "delta_time"
l_dugy:=V1*delta_time*1e-3;

-- рассчитываем угол "начальная точка"-"центр Земли"-"расчетная точка"
-- при условии, что полет происходит на средней выоте.
fi_dugy:=(l_dugy/(R_zemli+avg_h)); -- *rad_v_grad;


-- рассчитываем угловые координаты новой точки

-- x2 := x1+fi_dugy*sin(grad_v_rad*curs);
y2 := asin(sin(grad_v_rad*y1)*cos(fi_dugy)+cos(grad_v_rad*y1)*sin(fi_dugy)*cos(grad_v_rad*curs))*rad_v_grad;

x2:=x1+atan((sin(fi_dugy)*sin(grad_v_rad*curs))/
(cos(grad_v_rad*y1)*cos(fi_dugy)-sin(grad_v_rad*y1)*sin(fi_dugy)*cos(grad_v_rad*curs)))*rad_v_grad; 



-- формируем запись для вывода 
select x2, y2, h2 
into rec_1; 

return rec_1 ;

end;

$BODY$
LANGUAGE plpgsql VOLATILE
COST 100;