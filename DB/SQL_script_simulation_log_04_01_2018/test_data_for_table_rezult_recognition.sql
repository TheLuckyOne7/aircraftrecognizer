﻿delete from simulation_log.rezult_recognition;
select setval('simulation_log.rezult_recognition_rezult_recognition_id_seq', 1);


insert into simulation_log.rezult_recognition
(
rezult_recognition_id,  -- integer NOT NULL, -- первичный ключ 
air_situation_id,  -- integer NOT NULL, -- номер строки в таблице air_situation 
      -- внешний ключ 1 на поле air_situation_id таблицы  simulation_log.air_situation
air_object_id, -- integer NOT NULL, -- номер воздушного объекта 
      -- дублирует поле air_object_id таблицы  simulation_log.air_situation
modeling_time, -- interval second  not null, -- модельное время 
      /* дублирует поле modeling_time таблицы  simulation_log.air_situation
         поля modeling_time и air_object_id образуют внешний ключ на 
         поля modeling_time и air_object_id таблицы  simulation_log.air_situation */      
classes_of_aircraft_id, -- integer NOT NULL, -- номер распознанного класса ЛА 
        -- внешний ключ 4 на поле classes_of_aircraft_id таблицы  re_situation.classes_of_aircraft  
"classObj", -- character varying(255), -- Название класса ЛА распознанного воздушного объекта 
"probClassObj", -- double precision NOT NULL, -- Вероятность распознавания класса
type_of_aircraft_id, -- integer NOT NULL, -- номер распознанного типа ЛА 
      -- внешний ключ 5 на поле type_of_aircraft_id таблицы  re_situation.type_of_aircraft
"typeObj", -- character varying(255), -- Название типа ЛА распознанного воздушного объекта     
"probTypeObj", -- double precision NOT NULL, -- Вероятность распознавания типа
types_of_re_facilities_id, -- integer NOT NULL, -- номер распознанного типа РЭС-БРЛС
      -- внешний ключ 6 на поле types_of_re_facilities_id таблицы types_of_re_facilities 
"radarName", -- character varying(255), -- наименование распознанного типа РЭС-БРЛС 
"probRadarName", -- double precision, -- вероятность распознавания типа РЭС-БРЛС
d_m_airborne_radar_id, -- integer not null, -- Номер распознанного режима работы БРЛС
      -- внешний ключ 7 на поле d_m_airborne_radar_id таблицы d_m_airborne_radar 
"radarMode", --  character varying(255), -- Название распознанного режима работы БРЛС
"probRadarMode", -- double precision, -- Вероятность распознавания режима работы БРЛС
cat_of_radio_comm_signals_id, -- integer not null , -- номер распознанной категории сигналла связи 
      -- внешний ключ 8 на поле cat_of_radio_comm_signals_id таблицы cat_of_radio_comm_signals 
"radiationClass", -- character varying(255), -- наименование распознанной категории сигналла связи 
"probRadiationClass", -- double precision, -- Вероятность распознавания категории сигналла связи
"xLongtitude", -- double precision, -- координата X долгота воздушного объекта 
"yLatitude", -- double precision, -- координата Y широта воздушного объекта 
"hHeight", -- double precision, -- высота воздушного объекта 
"hVelocity", -- double precision, -- горизонтальная скорость 
"vVelocity", -- double precision, -- вертикальная скорость
"modeRecognizeID", -- integer NOT NULL, -- номер распознанного состояния объекта 
"modeRecognizeName", -- character varying(255), -- название распознанного состояния 
"probModeRecognize" -- double precision, -- вероятность рапознавания состояния ВО
)
values

-------------------------------------------------------------------------------------
-- ВО - E-3C состояние - "Поиск и сопровождение ВЦ в ИР1 над горизонтом"
-- air_situation_id=2
-- Режим БРЛС - 'Режим Р1'
-- средства связи излучают 'C1 category'
(
default,  -- integer NOT NULL, -- первичный ключ 
2,  -- integer NOT NULL, -- номер строки в таблице air_situation 
      -- внешний ключ 1 на поле air_situation_id таблицы  simulation_log.air_situation
(
select air_object_id from simulation_log.air_situation where air_situation_id=2
), -- integer NOT NULL, -- номер воздушного объекта 
      -- дублирует поле air_object_id таблицы  simulation_log.air_situation
(
select modeling_time from simulation_log.air_situation where air_situation_id=2
), -- interval second  not null, -- модельное время 
      /* дублирует поле modeling_time таблицы  simulation_log.air_situation
         поля modeling_time и air_object_id образуют внешний ключ на 
         поля modeling_time и air_object_id таблицы  simulation_log.air_situation */      
(
select cl_a.classes_of_aircraft_id --, cl_a.cl_of_aircraft_name_e,
 from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2
), -- integer NOT NULL, -- номер распознанного класса ЛА 
        -- внешний ключ 4 на поле classes_of_aircraft_id таблицы  re_situation.classes_of_aircraft  

(
select cl_a.cl_of_aircraft_name_e -- 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2
), -- character varying(255), -- Название класса ЛА распознанного воздушного объекта 

0.85, -- double precision NOT NULL, -- Вероятность распознавания класса

(
select ta.type_of_aircraft_id -- , ta.code_name_e 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2
), -- integer NOT NULL, -- номер распознанного типа ЛА 
      -- внешний ключ 5 на поле type_of_aircraft_id таблицы  re_situation.type_of_aircraft
(
select ta.code_name_e  -- ta.type_of_aircraft_id , 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2
), -- character varying(255), -- Название типа ЛА распознанного воздушного объекта 
    
0.8 , -- double precision NOT NULL, -- Вероятность распознавания типа

(
select t_f.types_of_re_facilities_id --, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2
), -- integer NOT NULL, -- номер распознанного типа РЭС-БРЛС
      -- внешний ключ 6 на поле types_of_re_facilities_id таблицы types_of_re_facilities 


(
select t_f.name_facilities_e -- t_f.types_of_re_facilities_id, , p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2
), -- character varying(255), -- наименование распознанного типа РЭС-БРЛС 

0.8, -- double precision, -- вероятность распознавания типа РЭС-БРЛС

(
select dmar.d_m_airborne_radar_id -- , dmar.abr_name_mode_r, dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2
and dmar.abr_name_mode_r like 'Режим Р1'
), -- integer not null, -- Номер распознанного режима работы БРЛС
      -- внешний ключ 7 на поле d_m_airborne_radar_id таблицы d_m_airborne_radar 
      
(
select dmar.abr_name_mode_r -- , dmar.d_m_airborne_radar_id , dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2
and dmar.abr_name_mode_r like 'Режим Р1'
), --  character varying(255), -- Название распознанного режима работы БРЛС

0.8 , -- double precision, -- Вероятность распознавания режима работы БРЛС

(
select cat_of_radio_comm_signals_id --, name_category_e
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C1 category'
), -- integer not null , -- номер распознанной категории сигналла связи 
      -- внешний ключ 8 на поле cat_of_radio_comm_signals_id таблицы cat_of_radio_comm_signals 
      
(
select name_category_e -- cat_of_radio_comm_signals_id, 
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C1 category'
), -- character varying(255), -- наименование распознанной категории сигналла связи 

0.8 , -- double precision, -- Вероятность распознавания категории сигналла связи

(
select x_longitude --, y_latitude, h_height
FROM simulation_log.air_situation
where air_situation_id=2
), -- double precision, -- координата X долгота воздушного объекта 

(
select  y_latitude -- , h_height, x_longitude,
FROM simulation_log.air_situation
where air_situation_id=2
), -- double precision, -- координата Y широта воздушного объекта 

(
select h_height  -- , , x_longitude, y_latitude
FROM simulation_log.air_situation
where air_situation_id=2
), -- double precision, -- высота воздушного объекта 
null , -- double precision, -- горизонтальная скорость 
null , -- double precision, -- вертикальная скорость
null , -- integer NOT NULL, -- номер распознанного состояния объекта 
null , -- character varying(255), -- название распознанного состояния 
null  -- double precision, -- вероятность рапознавания состояния ВО
),

--------------------------------------------------------------------
-- ВО - E-3C состояние - "Поиск и сопровождение ВЦ в ИР1 над горизонтом"
-- air_situation_id=2
-- Режим БРЛС - 'Режим Р1'
-- средства связи излучают 'C4 category'

(
default,  -- integer NOT NULL, -- первичный ключ 
2,  -- integer NOT NULL, -- номер строки в таблице air_situation 
      -- внешний ключ 1 на поле air_situation_id таблицы  simulation_log.air_situation
(
select air_object_id from simulation_log.air_situation where air_situation_id=2
), -- integer NOT NULL, -- номер воздушного объекта 
      -- дублирует поле air_object_id таблицы  simulation_log.air_situation
(
select modeling_time from simulation_log.air_situation where air_situation_id=2
), -- interval second  not null, -- модельное время 
      /* дублирует поле modeling_time таблицы  simulation_log.air_situation
         поля modeling_time и air_object_id образуют внешний ключ на 
         поля modeling_time и air_object_id таблицы  simulation_log.air_situation */      
(
select cl_a.classes_of_aircraft_id --, cl_a.cl_of_aircraft_name_e,
 from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2
), -- integer NOT NULL, -- номер распознанного класса ЛА 
        -- внешний ключ 4 на поле classes_of_aircraft_id таблицы  re_situation.classes_of_aircraft  

(
select cl_a.cl_of_aircraft_name_e -- 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2
), -- character varying(255), -- Название класса ЛА распознанного воздушного объекта 

0.85, -- double precision NOT NULL, -- Вероятность распознавания класса

(
select ta.type_of_aircraft_id -- , ta.code_name_e 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2
), -- integer NOT NULL, -- номер распознанного типа ЛА 
      -- внешний ключ 5 на поле type_of_aircraft_id таблицы  re_situation.type_of_aircraft
(
select ta.code_name_e  -- ta.type_of_aircraft_id , 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2
), -- character varying(255), -- Название типа ЛА распознанного воздушного объекта 
    
0.8 , -- double precision NOT NULL, -- Вероятность распознавания типа

(
select t_f.types_of_re_facilities_id --, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2
), -- integer NOT NULL, -- номер распознанного типа РЭС-БРЛС
      -- внешний ключ 6 на поле types_of_re_facilities_id таблицы types_of_re_facilities 


(
select t_f.name_facilities_e -- t_f.types_of_re_facilities_id, , p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2
), -- character varying(255), -- наименование распознанного типа РЭС-БРЛС 

0.8, -- double precision, -- вероятность распознавания типа РЭС-БРЛС

(
select dmar.d_m_airborne_radar_id -- , dmar.abr_name_mode_r, dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2
and dmar.abr_name_mode_r like 'Режим Р1'
), -- integer not null, -- Номер распознанного режима работы БРЛС
      -- внешний ключ 7 на поле d_m_airborne_radar_id таблицы d_m_airborne_radar 
      
(
select dmar.abr_name_mode_r -- , dmar.d_m_airborne_radar_id , dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2
and dmar.abr_name_mode_r like 'Режим Р1'
), --  character varying(255), -- Название распознанного режима работы БРЛС

0.8 , -- double precision, -- Вероятность распознавания режима работы БРЛС

(
select cat_of_radio_comm_signals_id --, name_category_e
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C4 category'
), -- integer not null , -- номер распознанной категории сигналла связи 
      -- внешний ключ 8 на поле cat_of_radio_comm_signals_id таблицы cat_of_radio_comm_signals 
      
(
select name_category_e -- cat_of_radio_comm_signals_id, 
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C4 category'
), -- character varying(255), -- наименование распознанной категории сигналла связи 

0.8 , -- double precision, -- Вероятность распознавания категории сигналла связи

(
select x_longitude --, y_latitude, h_height
FROM simulation_log.air_situation
where air_situation_id=2
), -- double precision, -- координата X долгота воздушного объекта 

(
select  y_latitude -- , h_height, x_longitude,
FROM simulation_log.air_situation
where air_situation_id=2
), -- double precision, -- координата Y широта воздушного объекта 

(
select h_height  -- , , x_longitude, y_latitude
FROM simulation_log.air_situation
where air_situation_id=2
), -- double precision, -- высота воздушного объекта 
null , -- double precision, -- горизонтальная скорость 
null , -- double precision, -- вертикальная скорость
null , -- integer NOT NULL, -- номер распознанного состояния объекта 
null , -- character varying(255), -- название распознанного состояния 
null  -- double precision, -- вероятность рапознавания состояния ВО
),

--------------------------------------------------------------------
-- ВО - E-3C состояние - "Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- air_situation_id=2000
-- Режим БРЛС - 'Режим Р1'
-- средства связи излучают <rrr'C1 category'rrr>, 'C2 category', 4 раза по 'C6 category', 'C9 category'

(
default,  -- integer NOT NULL, -- первичный ключ 
2,  -- integer NOT NULL, -- номер строки в таблице air_situation 
      -- внешний ключ 1 на поле air_situation_id таблицы  simulation_log.air_situation
(
select air_object_id from simulation_log.air_situation where air_situation_id=2000
), -- integer NOT NULL, -- номер воздушного объекта 
      -- дублирует поле air_object_id таблицы  simulation_log.air_situation
(
select modeling_time from simulation_log.air_situation where air_situation_id=2000
), -- interval second  not null, -- модельное время 
      /* дублирует поле modeling_time таблицы  simulation_log.air_situation
         поля modeling_time и air_object_id образуют внешний ключ на 
         поля modeling_time и air_object_id таблицы  simulation_log.air_situation */      
(
select cl_a.classes_of_aircraft_id --, cl_a.cl_of_aircraft_name_e,
 from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного класса ЛА 
        -- внешний ключ 4 на поле classes_of_aircraft_id таблицы  re_situation.classes_of_aircraft  

(
select cl_a.cl_of_aircraft_name_e -- 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название класса ЛА распознанного воздушного объекта 

0.85, -- double precision NOT NULL, -- Вероятность распознавания класса

(
select ta.type_of_aircraft_id -- , ta.code_name_e 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа ЛА 
      -- внешний ключ 5 на поле type_of_aircraft_id таблицы  re_situation.type_of_aircraft
(
select ta.code_name_e  -- ta.type_of_aircraft_id , 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название типа ЛА распознанного воздушного объекта 
    
0.8 , -- double precision NOT NULL, -- Вероятность распознавания типа

(
select t_f.types_of_re_facilities_id --, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа РЭС-БРЛС
      -- внешний ключ 6 на поле types_of_re_facilities_id таблицы types_of_re_facilities 


(
select t_f.name_facilities_e -- t_f.types_of_re_facilities_id, , p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- character varying(255), -- наименование распознанного типа РЭС-БРЛС 

0.8, -- double precision, -- вероятность распознавания типа РЭС-БРЛС

(
select dmar.d_m_airborne_radar_id -- , dmar.abr_name_mode_r, dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), -- integer not null, -- Номер распознанного режима работы БРЛС
      -- внешний ключ 7 на поле d_m_airborne_radar_id таблицы d_m_airborne_radar 
      
(
select dmar.abr_name_mode_r -- , dmar.d_m_airborne_radar_id , dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), --  character varying(255), -- Название распознанного режима работы БРЛС

0.8 , -- double precision, -- Вероятность распознавания режима работы БРЛС

(
select cat_of_radio_comm_signals_id --, name_category_e
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C1 category'
), -- integer not null , -- номер распознанной категории сигналла связи 
      -- внешний ключ 8 на поле cat_of_radio_comm_signals_id таблицы cat_of_radio_comm_signals 
      
(
select name_category_e -- cat_of_radio_comm_signals_id, 
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C1 category'
), -- character varying(255), -- наименование распознанной категории сигналла связи 

0.8 , -- double precision, -- Вероятность распознавания категории сигналла связи

(
select x_longitude --, y_latitude, h_height
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата X долгота воздушного объекта 

(
select  y_latitude -- , h_height, x_longitude,
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата Y широта воздушного объекта 

(
select h_height  -- , , x_longitude, y_latitude
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- высота воздушного объекта 
null , -- double precision, -- горизонтальная скорость 
null , -- double precision, -- вертикальная скорость
null , -- integer NOT NULL, -- номер распознанного состояния объекта 
null , -- character varying(255), -- название распознанного состояния 
null  -- double precision, -- вероятность рапознавания состояния ВО
),

--------------------------------------------------------------------
-- ВО - E-3C состояние - "Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- air_situation_id=2000
-- Режим БРЛС - 'Режим Р1'
-- средства связи излучают 'C1 category', <rrr'C2 category'rrr>, 4 раза по 'C6 category', 'C9 category'

(
default,  -- integer NOT NULL, -- первичный ключ 
2,  -- integer NOT NULL, -- номер строки в таблице air_situation 
      -- внешний ключ 1 на поле air_situation_id таблицы  simulation_log.air_situation
(
select air_object_id from simulation_log.air_situation where air_situation_id=2000
), -- integer NOT NULL, -- номер воздушного объекта 
      -- дублирует поле air_object_id таблицы  simulation_log.air_situation
(
select modeling_time from simulation_log.air_situation where air_situation_id=2000
), -- interval second  not null, -- модельное время 
      /* дублирует поле modeling_time таблицы  simulation_log.air_situation
         поля modeling_time и air_object_id образуют внешний ключ на 
         поля modeling_time и air_object_id таблицы  simulation_log.air_situation */      
(
select cl_a.classes_of_aircraft_id --, cl_a.cl_of_aircraft_name_e,
 from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного класса ЛА 
        -- внешний ключ 4 на поле classes_of_aircraft_id таблицы  re_situation.classes_of_aircraft  

(
select cl_a.cl_of_aircraft_name_e -- 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название класса ЛА распознанного воздушного объекта 

0.85, -- double precision NOT NULL, -- Вероятность распознавания класса

(
select ta.type_of_aircraft_id -- , ta.code_name_e 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа ЛА 
      -- внешний ключ 5 на поле type_of_aircraft_id таблицы  re_situation.type_of_aircraft
(
select ta.code_name_e  -- ta.type_of_aircraft_id , 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название типа ЛА распознанного воздушного объекта 
    
0.8 , -- double precision NOT NULL, -- Вероятность распознавания типа

(
select t_f.types_of_re_facilities_id --, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа РЭС-БРЛС
      -- внешний ключ 6 на поле types_of_re_facilities_id таблицы types_of_re_facilities 


(
select t_f.name_facilities_e -- t_f.types_of_re_facilities_id, , p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- character varying(255), -- наименование распознанного типа РЭС-БРЛС 

0.8, -- double precision, -- вероятность распознавания типа РЭС-БРЛС

(
select dmar.d_m_airborne_radar_id -- , dmar.abr_name_mode_r, dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), -- integer not null, -- Номер распознанного режима работы БРЛС
      -- внешний ключ 7 на поле d_m_airborne_radar_id таблицы d_m_airborne_radar 
      
(
select dmar.abr_name_mode_r -- , dmar.d_m_airborne_radar_id , dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), --  character varying(255), -- Название распознанного режима работы БРЛС

0.8 , -- double precision, -- Вероятность распознавания режима работы БРЛС

(
select cat_of_radio_comm_signals_id --, name_category_e
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C2 category'
), -- integer not null , -- номер распознанной категории сигналла связи 
      -- внешний ключ 8 на поле cat_of_radio_comm_signals_id таблицы cat_of_radio_comm_signals 
      
(
select name_category_e -- cat_of_radio_comm_signals_id, 
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C2 category'
), -- character varying(255), -- наименование распознанной категории сигналла связи 

0.8 , -- double precision, -- Вероятность распознавания категории сигналла связи

(
select x_longitude --, y_latitude, h_height
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата X долгота воздушного объекта 

(
select  y_latitude -- , h_height, x_longitude,
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата Y широта воздушного объекта 

(
select h_height  -- , , x_longitude, y_latitude
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- высота воздушного объекта 
null , -- double precision, -- горизонтальная скорость 
null , -- double precision, -- вертикальная скорость
null , -- integer NOT NULL, -- номер распознанного состояния объекта 
null , -- character varying(255), -- название распознанного состояния 
null  -- double precision, -- вероятность рапознавания состояния ВО
),

--------------------------------------------------------------------
-- ВО - E-3C состояние - "Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- air_situation_id=2000
-- Режим БРЛС - 'Режим Р1'
-- средства связи излучают 'C1 category', 'C2 category', 4 раза по <rrr'C6 category' 1 раз из 4 rrr>, 'C9 category'

(
default,  -- integer NOT NULL, -- первичный ключ 
2,  -- integer NOT NULL, -- номер строки в таблице air_situation 
      -- внешний ключ 1 на поле air_situation_id таблицы  simulation_log.air_situation
(
select air_object_id from simulation_log.air_situation where air_situation_id=2000
), -- integer NOT NULL, -- номер воздушного объекта 
      -- дублирует поле air_object_id таблицы  simulation_log.air_situation
(
select modeling_time from simulation_log.air_situation where air_situation_id=2000
), -- interval second  not null, -- модельное время 
      /* дублирует поле modeling_time таблицы  simulation_log.air_situation
         поля modeling_time и air_object_id образуют внешний ключ на 
         поля modeling_time и air_object_id таблицы  simulation_log.air_situation */      
(
select cl_a.classes_of_aircraft_id --, cl_a.cl_of_aircraft_name_e,
 from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного класса ЛА 
        -- внешний ключ 4 на поле classes_of_aircraft_id таблицы  re_situation.classes_of_aircraft  

(
select cl_a.cl_of_aircraft_name_e -- 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название класса ЛА распознанного воздушного объекта 

0.85, -- double precision NOT NULL, -- Вероятность распознавания класса

(
select ta.type_of_aircraft_id -- , ta.code_name_e 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа ЛА 
      -- внешний ключ 5 на поле type_of_aircraft_id таблицы  re_situation.type_of_aircraft
(
select ta.code_name_e  -- ta.type_of_aircraft_id , 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название типа ЛА распознанного воздушного объекта 
    
0.8 , -- double precision NOT NULL, -- Вероятность распознавания типа

(
select t_f.types_of_re_facilities_id --, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа РЭС-БРЛС
      -- внешний ключ 6 на поле types_of_re_facilities_id таблицы types_of_re_facilities 


(
select t_f.name_facilities_e -- t_f.types_of_re_facilities_id, , p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- character varying(255), -- наименование распознанного типа РЭС-БРЛС 

0.8, -- double precision, -- вероятность распознавания типа РЭС-БРЛС

(
select dmar.d_m_airborne_radar_id -- , dmar.abr_name_mode_r, dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), -- integer not null, -- Номер распознанного режима работы БРЛС
      -- внешний ключ 7 на поле d_m_airborne_radar_id таблицы d_m_airborne_radar 
      
(
select dmar.abr_name_mode_r -- , dmar.d_m_airborne_radar_id , dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), --  character varying(255), -- Название распознанного режима работы БРЛС

0.8 , -- double precision, -- Вероятность распознавания режима работы БРЛС

(
select cat_of_radio_comm_signals_id --, name_category_e
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C6 category'
), -- integer not null , -- номер распознанной категории сигналла связи 
      -- внешний ключ 8 на поле cat_of_radio_comm_signals_id таблицы cat_of_radio_comm_signals 
      
(
select name_category_e -- cat_of_radio_comm_signals_id, 
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C6 category'
), -- character varying(255), -- наименование распознанной категории сигналла связи 

0.8 , -- double precision, -- Вероятность распознавания категории сигналла связи

(
select x_longitude --, y_latitude, h_height
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата X долгота воздушного объекта 

(
select  y_latitude -- , h_height, x_longitude,
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата Y широта воздушного объекта 

(
select h_height  -- , , x_longitude, y_latitude
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- высота воздушного объекта 
null , -- double precision, -- горизонтальная скорость 
null , -- double precision, -- вертикальная скорость
null , -- integer NOT NULL, -- номер распознанного состояния объекта 
null , -- character varying(255), -- название распознанного состояния 
null  -- double precision, -- вероятность рапознавания состояния ВО
),

--------------------------------------------------------------------
-- ВО - E-3C состояние - "Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- air_situation_id=2000
-- Режим БРЛС - 'Режим Р1'
-- средства связи излучают 'C1 category', 'C2 category', 4 раза по <rrr'C6 category' 2 раз из 4 rrr>, 'C9 category'

(
default,  -- integer NOT NULL, -- первичный ключ 
2,  -- integer NOT NULL, -- номер строки в таблице air_situation 
      -- внешний ключ 1 на поле air_situation_id таблицы  simulation_log.air_situation
(
select air_object_id from simulation_log.air_situation where air_situation_id=2000
), -- integer NOT NULL, -- номер воздушного объекта 
      -- дублирует поле air_object_id таблицы  simulation_log.air_situation
(
select modeling_time from simulation_log.air_situation where air_situation_id=2000
), -- interval second  not null, -- модельное время 
      /* дублирует поле modeling_time таблицы  simulation_log.air_situation
         поля modeling_time и air_object_id образуют внешний ключ на 
         поля modeling_time и air_object_id таблицы  simulation_log.air_situation */      
(
select cl_a.classes_of_aircraft_id --, cl_a.cl_of_aircraft_name_e,
 from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного класса ЛА 
        -- внешний ключ 4 на поле classes_of_aircraft_id таблицы  re_situation.classes_of_aircraft  

(
select cl_a.cl_of_aircraft_name_e -- 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название класса ЛА распознанного воздушного объекта 

0.85, -- double precision NOT NULL, -- Вероятность распознавания класса

(
select ta.type_of_aircraft_id -- , ta.code_name_e 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа ЛА 
      -- внешний ключ 5 на поле type_of_aircraft_id таблицы  re_situation.type_of_aircraft
(
select ta.code_name_e  -- ta.type_of_aircraft_id , 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название типа ЛА распознанного воздушного объекта 
    
0.8 , -- double precision NOT NULL, -- Вероятность распознавания типа

(
select t_f.types_of_re_facilities_id --, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа РЭС-БРЛС
      -- внешний ключ 6 на поле types_of_re_facilities_id таблицы types_of_re_facilities 


(
select t_f.name_facilities_e -- t_f.types_of_re_facilities_id, , p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- character varying(255), -- наименование распознанного типа РЭС-БРЛС 

0.8, -- double precision, -- вероятность распознавания типа РЭС-БРЛС

(
select dmar.d_m_airborne_radar_id -- , dmar.abr_name_mode_r, dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), -- integer not null, -- Номер распознанного режима работы БРЛС
      -- внешний ключ 7 на поле d_m_airborne_radar_id таблицы d_m_airborne_radar 
      
(
select dmar.abr_name_mode_r -- , dmar.d_m_airborne_radar_id , dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), --  character varying(255), -- Название распознанного режима работы БРЛС

0.8 , -- double precision, -- Вероятность распознавания режима работы БРЛС

(
select cat_of_radio_comm_signals_id --, name_category_e
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C6 category'
), -- integer not null , -- номер распознанной категории сигналла связи 
      -- внешний ключ 8 на поле cat_of_radio_comm_signals_id таблицы cat_of_radio_comm_signals 
      
(
select name_category_e -- cat_of_radio_comm_signals_id, 
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C6 category'
), -- character varying(255), -- наименование распознанной категории сигналла связи 

0.8 , -- double precision, -- Вероятность распознавания категории сигналла связи

(
select x_longitude --, y_latitude, h_height
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата X долгота воздушного объекта 

(
select  y_latitude -- , h_height, x_longitude,
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата Y широта воздушного объекта 

(
select h_height  -- , , x_longitude, y_latitude
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- высота воздушного объекта 
null , -- double precision, -- горизонтальная скорость 
null , -- double precision, -- вертикальная скорость
null , -- integer NOT NULL, -- номер распознанного состояния объекта 
null , -- character varying(255), -- название распознанного состояния 
null  -- double precision, -- вероятность рапознавания состояния ВО
),

--------------------------------------------------------------------
-- ВО - E-3C состояние - "Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- air_situation_id=2000
-- Режим БРЛС - 'Режим Р1'
-- средства связи излучают 'C1 category', 'C2 category', 4 раза по <rrr'C6 category' 3 раз из 4 rrr>, 'C9 category'

(
default,  -- integer NOT NULL, -- первичный ключ 
2,  -- integer NOT NULL, -- номер строки в таблице air_situation 
      -- внешний ключ 1 на поле air_situation_id таблицы  simulation_log.air_situation
(
select air_object_id from simulation_log.air_situation where air_situation_id=2000
), -- integer NOT NULL, -- номер воздушного объекта 
      -- дублирует поле air_object_id таблицы  simulation_log.air_situation
(
select modeling_time from simulation_log.air_situation where air_situation_id=2000
), -- interval second  not null, -- модельное время 
      /* дублирует поле modeling_time таблицы  simulation_log.air_situation
         поля modeling_time и air_object_id образуют внешний ключ на 
         поля modeling_time и air_object_id таблицы  simulation_log.air_situation */      
(
select cl_a.classes_of_aircraft_id --, cl_a.cl_of_aircraft_name_e,
 from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного класса ЛА 
        -- внешний ключ 4 на поле classes_of_aircraft_id таблицы  re_situation.classes_of_aircraft  

(
select cl_a.cl_of_aircraft_name_e -- 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название класса ЛА распознанного воздушного объекта 

0.85, -- double precision NOT NULL, -- Вероятность распознавания класса

(
select ta.type_of_aircraft_id -- , ta.code_name_e 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа ЛА 
      -- внешний ключ 5 на поле type_of_aircraft_id таблицы  re_situation.type_of_aircraft
(
select ta.code_name_e  -- ta.type_of_aircraft_id , 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название типа ЛА распознанного воздушного объекта 
    
0.8 , -- double precision NOT NULL, -- Вероятность распознавания типа

(
select t_f.types_of_re_facilities_id --, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа РЭС-БРЛС
      -- внешний ключ 6 на поле types_of_re_facilities_id таблицы types_of_re_facilities 


(
select t_f.name_facilities_e -- t_f.types_of_re_facilities_id, , p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- character varying(255), -- наименование распознанного типа РЭС-БРЛС 

0.8, -- double precision, -- вероятность распознавания типа РЭС-БРЛС

(
select dmar.d_m_airborne_radar_id -- , dmar.abr_name_mode_r, dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), -- integer not null, -- Номер распознанного режима работы БРЛС
      -- внешний ключ 7 на поле d_m_airborne_radar_id таблицы d_m_airborne_radar 
      
(
select dmar.abr_name_mode_r -- , dmar.d_m_airborne_radar_id , dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), --  character varying(255), -- Название распознанного режима работы БРЛС

0.8 , -- double precision, -- Вероятность распознавания режима работы БРЛС

(
select cat_of_radio_comm_signals_id --, name_category_e
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C6 category'
), -- integer not null , -- номер распознанной категории сигналла связи 
      -- внешний ключ 8 на поле cat_of_radio_comm_signals_id таблицы cat_of_radio_comm_signals 
      
(
select name_category_e -- cat_of_radio_comm_signals_id, 
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C6 category'
), -- character varying(255), -- наименование распознанной категории сигналла связи 

0.8 , -- double precision, -- Вероятность распознавания категории сигналла связи

(
select x_longitude --, y_latitude, h_height
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата X долгота воздушного объекта 

(
select  y_latitude -- , h_height, x_longitude,
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата Y широта воздушного объекта 

(
select h_height  -- , , x_longitude, y_latitude
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- высота воздушного объекта 
null , -- double precision, -- горизонтальная скорость 
null , -- double precision, -- вертикальная скорость
null , -- integer NOT NULL, -- номер распознанного состояния объекта 
null , -- character varying(255), -- название распознанного состояния 
null  -- double precision, -- вероятность рапознавания состояния ВО
), 

--------------------------------------------------------------------
-- ВО - E-3C состояние - "Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- air_situation_id=2000
-- Режим БРЛС - 'Режим Р1'
-- средства связи излучают 'C1 category', 'C2 category', 4 раза по <rrr'C6 category' 4 раз из 4 rrr>, 'C9 category'

(
default,  -- integer NOT NULL, -- первичный ключ 
2,  -- integer NOT NULL, -- номер строки в таблице air_situation 
      -- внешний ключ 1 на поле air_situation_id таблицы  simulation_log.air_situation
(
select air_object_id from simulation_log.air_situation where air_situation_id=2000
), -- integer NOT NULL, -- номер воздушного объекта 
      -- дублирует поле air_object_id таблицы  simulation_log.air_situation
(
select modeling_time from simulation_log.air_situation where air_situation_id=2000
), -- interval second  not null, -- модельное время 
      /* дублирует поле modeling_time таблицы  simulation_log.air_situation
         поля modeling_time и air_object_id образуют внешний ключ на 
         поля modeling_time и air_object_id таблицы  simulation_log.air_situation */      
(
select cl_a.classes_of_aircraft_id --, cl_a.cl_of_aircraft_name_e,
 from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного класса ЛА 
        -- внешний ключ 4 на поле classes_of_aircraft_id таблицы  re_situation.classes_of_aircraft  

(
select cl_a.cl_of_aircraft_name_e -- 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название класса ЛА распознанного воздушного объекта 

0.85, -- double precision NOT NULL, -- Вероятность распознавания класса

(
select ta.type_of_aircraft_id -- , ta.code_name_e 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа ЛА 
      -- внешний ключ 5 на поле type_of_aircraft_id таблицы  re_situation.type_of_aircraft
(
select ta.code_name_e  -- ta.type_of_aircraft_id , 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название типа ЛА распознанного воздушного объекта 
    
0.8 , -- double precision NOT NULL, -- Вероятность распознавания типа

(
select t_f.types_of_re_facilities_id --, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа РЭС-БРЛС
      -- внешний ключ 6 на поле types_of_re_facilities_id таблицы types_of_re_facilities 


(
select t_f.name_facilities_e -- t_f.types_of_re_facilities_id, , p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- character varying(255), -- наименование распознанного типа РЭС-БРЛС 

0.8, -- double precision, -- вероятность распознавания типа РЭС-БРЛС

(
select dmar.d_m_airborne_radar_id -- , dmar.abr_name_mode_r, dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), -- integer not null, -- Номер распознанного режима работы БРЛС
      -- внешний ключ 7 на поле d_m_airborne_radar_id таблицы d_m_airborne_radar 
      
(
select dmar.abr_name_mode_r -- , dmar.d_m_airborne_radar_id , dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), --  character varying(255), -- Название распознанного режима работы БРЛС

0.8 , -- double precision, -- Вероятность распознавания режима работы БРЛС

(
select cat_of_radio_comm_signals_id --, name_category_e
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C6 category'
), -- integer not null , -- номер распознанной категории сигналла связи 
      -- внешний ключ 8 на поле cat_of_radio_comm_signals_id таблицы cat_of_radio_comm_signals 
      
(
select name_category_e -- cat_of_radio_comm_signals_id, 
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C6 category'
), -- character varying(255), -- наименование распознанной категории сигналла связи 

0.8 , -- double precision, -- Вероятность распознавания категории сигналла связи

(
select x_longitude --, y_latitude, h_height
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата X долгота воздушного объекта 

(
select  y_latitude -- , h_height, x_longitude,
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата Y широта воздушного объекта 

(
select h_height  -- , , x_longitude, y_latitude
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- высота воздушного объекта 
null , -- double precision, -- горизонтальная скорость 
null , -- double precision, -- вертикальная скорость
null , -- integer NOT NULL, -- номер распознанного состояния объекта 
null , -- character varying(255), -- название распознанного состояния 
null  -- double precision, -- вероятность рапознавания состояния ВО
),

--------------------------------------------------------------------
-- ВО - E-3C состояние - "Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- air_situation_id=2000
-- Режим БРЛС - 'Режим Р1'
-- средства связи излучают 'C1 category', 'C2 category', 4 раза по 'C6 category' , <rrr'C9 category'rrr>

(
default,  -- integer NOT NULL, -- первичный ключ 
2,  -- integer NOT NULL, -- номер строки в таблице air_situation 
      -- внешний ключ 1 на поле air_situation_id таблицы  simulation_log.air_situation
(
select air_object_id from simulation_log.air_situation where air_situation_id=2000
), -- integer NOT NULL, -- номер воздушного объекта 
      -- дублирует поле air_object_id таблицы  simulation_log.air_situation
(
select modeling_time from simulation_log.air_situation where air_situation_id=2000
), -- interval second  not null, -- модельное время 
      /* дублирует поле modeling_time таблицы  simulation_log.air_situation
         поля modeling_time и air_object_id образуют внешний ключ на 
         поля modeling_time и air_object_id таблицы  simulation_log.air_situation */      
(
select cl_a.classes_of_aircraft_id --, cl_a.cl_of_aircraft_name_e,
 from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного класса ЛА 
        -- внешний ключ 4 на поле classes_of_aircraft_id таблицы  re_situation.classes_of_aircraft  

(
select cl_a.cl_of_aircraft_name_e -- 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.classes_of_aircraft as cl_a
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and t_a.classes_of_aircraft_id=cl_a.classes_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название класса ЛА распознанного воздушного объекта 

0.85, -- double precision NOT NULL, -- Вероятность распознавания класса

(
select ta.type_of_aircraft_id -- , ta.code_name_e 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа ЛА 
      -- внешний ключ 5 на поле type_of_aircraft_id таблицы  re_situation.type_of_aircraft
(
select ta.code_name_e  -- ta.type_of_aircraft_id , 
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as ta 
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=ta.type_of_aircraft_id
and air_situation_id=2000
), -- character varying(255), -- Название типа ЛА распознанного воздушного объекта 
    
0.8 , -- double precision NOT NULL, -- Вероятность распознавания типа

(
select t_f.types_of_re_facilities_id --, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- integer NOT NULL, -- номер распознанного типа РЭС-БРЛС
      -- внешний ключ 6 на поле types_of_re_facilities_id таблицы types_of_re_facilities 


(
select t_f.name_facilities_e -- t_f.types_of_re_facilities_id, , p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and t_f.categoryes_facilities_id=1
and air_situation_id=2000
), -- character varying(255), -- наименование распознанного типа РЭС-БРЛС 

0.8, -- double precision, -- вероятность распознавания типа РЭС-БРЛС

(
select dmar.d_m_airborne_radar_id -- , dmar.abr_name_mode_r, dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), -- integer not null, -- Номер распознанного режима работы БРЛС
      -- внешний ключ 7 на поле d_m_airborne_radar_id таблицы d_m_airborne_radar 
      
(
select dmar.abr_name_mode_r -- , dmar.d_m_airborne_radar_id , dmar.name_mode_r, 
-- t_f.types_of_re_facilities_id, t_f.name_facilities_e, p_a.code_name
from simulation_log.air_situation as air_s,
re_situation.air_object as air_o, 
re_situation.type_of_aircraft as t_a, 
re_situation.placing_f_t_aircraft as p_a,
re_situation.types_of_re_facilities as t_f,
re_situation.d_m_airborne_radar as dmar
where air_o.air_object_id=air_s.air_object_id
and air_o.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.type_of_aircraft_id=t_a.type_of_aircraft_id
and p_a.types_of_re_facilities_id=t_f.types_of_re_facilities_id
and dmar.types_of_re_facilities_id=t_f.types_of_re_facilities_id

and t_f.categoryes_facilities_id=1
and air_situation_id=2000
and dmar.abr_name_mode_r like 'Режим Р1'
), --  character varying(255), -- Название распознанного режима работы БРЛС

0.8 , -- double precision, -- Вероятность распознавания режима работы БРЛС

(
select cat_of_radio_comm_signals_id --, name_category_e
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C9 category'
), -- integer not null , -- номер распознанной категории сигналла связи 
      -- внешний ключ 8 на поле cat_of_radio_comm_signals_id таблицы cat_of_radio_comm_signals 
      
(
select name_category_e -- cat_of_radio_comm_signals_id, 
FROM re_situation.cat_of_radio_comm_signals
where name_category_e like 'C9 category'
), -- character varying(255), -- наименование распознанной категории сигналла связи 

0.8 , -- double precision, -- Вероятность распознавания категории сигналла связи

(
select x_longitude --, y_latitude, h_height
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата X долгота воздушного объекта 

(
select  y_latitude -- , h_height, x_longitude,
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- координата Y широта воздушного объекта 

(
select h_height  -- , , x_longitude, y_latitude
FROM simulation_log.air_situation
where air_situation_id=2000
), -- double precision, -- высота воздушного объекта 
null , -- double precision, -- горизонтальная скорость 
null , -- double precision, -- вертикальная скорость
null , -- integer NOT NULL, -- номер распознанного состояния объекта 
null , -- character varying(255), -- название распознанного состояния 
null  -- double precision, -- вероятность рапознавания состояния ВО
);