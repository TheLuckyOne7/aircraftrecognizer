﻿-- Function: re_situation.change_param_of_motion(...)


CREATE OR REPLACE FUNCTION re_situation.change_param_of_motion
(
p_situation_id integer, -- 
p_operative_time interval second,
p_air_object_id integer,
p_x_longitude double precision ,
p_y_latitude double precision ,
p_h_height double precision ,
p_course double precision ,
p_horizontal_velocity double precision ,
p_vertical_velocity double precision 
)
RETURNS boolean AS
$BODY$
declare 
f_operative_time interval second;
tipd_tm1 re_situation.situation; -- строка таблицы "re_situation.situation" соответчтвующая точке tm1
tipd_tp1 re_situation.situation; -- строка таблицы "re_situation.situation" соответчтвующая точке tp1
tipd_tp2 re_situation.situation; -- строка таблицы "re_situation.situation" соответчтвующая точке tp1
tipd_oper record;
tipd_oper2 record;
new_h double precision;
avg_h double precision;
l_dugy double precision;
R_zemli double precision :=6371;  -- радиус Земли 
-- fi_dugi double precision;
grad_v_rad double precision :=PI()/180 ; -- в Пи радиан - 180 градусов
rad_v_grad double precision :=180/PI() ; -- в 180 градусах - Пи радиан 
horizontal_velocity double precision;
vertical_velocity double precision;
time_1 double precision;
time_2 double precision;
time_ double precision;
 
 
 
--  t_row simulation_log.air_situation;
  

begin
-- проверить чтобы оперативное время p_operative_time должно быть в пределах::
-- tm1.operative_time<tp1.f_operative_time
-- определить оперативное время точек tm1, tp1
-- определить оперативное время для изменяемой точки записанной в базе 
select operative_time 
into f_operative_time
from re_situation.situation
where situation_id=p_situation_id;

-- raise notice 'situation_id=%,  operative_time=% ', p_situation_id, f_operative_time;

-- текущая ТИПД - t
-- определить предидущую для t - tm1 ТИПД
select * 
into tipd_tm1
from re_situation.situation
where air_object_id=p_air_object_id
and operative_time<f_operative_time
order by operative_time desc
limit 1 ;

raise notice 
'
точка tm1:
tipd_tm1.situation_id=% ', tipd_tm1.situation_id ; 


-- следующая ТИПД tp1
-- определить следующую tp1 ТИПД
select * 
into tipd_tp1
from re_situation.situation
where air_object_id=p_air_object_id
and operative_time>f_operative_time
order by operative_time
limit 1 ;

raise notice 
'
точка tp1:
tipd_tp1.situation_id=% ', tipd_tp1.situation_id; 

-- определить tp2 ТИПД (следующую за следующей) ТИПД
select * 
into tipd_tp2
from re_situation.situation
where air_object_id=p_air_object_id
and operative_time>tipd_tp1.operative_time
order by operative_time
limit 1 ;

raise notice 
'точка tp2:
tipd_tp2.situation_id=% ', tipd_tp2.situation_id; 


-- проверка чтобы заданное оперативное время небыло меньше времени предидущей (tm1) ТИПД
if tipd_tm1.operative_time is not null 
-- начало оператора if tipd_tm1.operative_time is not null 

then
if p_operative_time<=tipd_tm1.operative_time 
-- начало оператора if operative_time<tipd_tm1.operative_time 
then 
raise notice '
Ошибка задания оперативного времени.
Заданное оперативное время - %.
Для изменяемой ТИПД оперативное время должно быть больше % и меньще %',
p_operative_time, tipd_tm1.operative_time , tipd_tp1.operative_time; 
return false;
end if; -- конец оператора if operative_time<tipd_tm1.operative_time 


end if;-- конец оператора if tipd_tm1.operative_time is not null 
---------------------------------------------------------------------
-- проверка чтобы заданное оперативное время небыло больше времени следующей (tp1) ТИПД
if tipd_tp1.operative_time is not null 
-- начало оператора if tipd_tp1.operative_time is not null 
then

if p_operative_time>=tipd_tp1.operative_time 
-- начало оператора if operative_time>tipd_tp1.operative_time 
then 
raise notice '
Ошибка задания оперативного времени.
Заданное оперативное время - %.
Для изменяемой ТИПД оперативное время должно быть больше % и меньще %',
p_operative_time, tipd_tm1.operative_time , tipd_tp1.operative_time; 
return false;
end if; -- конец оператора if operative_time>tipd_tp1.operative_time 

end if;-- конец оператора if tipd_tp1.operative_time is not null 
----------------------------------------------------------------------

-- программа оказывается в этой точке когда tipd_tm1.operative_time<p_operative_time<tipd_tp1.operative_time
-- скорректировать параметры движения в точке tm1 таким образом, 
-- чтобы двигаясь из tm1 ВО в момент оперативного времени t оказался 
-- в точке координатами (p_x_longitude, p_y_latitude, p_h_height)

-- рассчитываем время между точками tipd_tm1 и tipd_t
time_1:=extract(hours from (tipd_tm1.operative_time))*3600 + extract(minutes from (tipd_tm1.operative_time))*60 + extract(seconds from (tipd_tm1.operative_time));
time_2:=extract(hours from (p_operative_time))*3600 + extract(minutes from (p_operative_time))*60 + extract(seconds from (p_operative_time));
time_:=time_2-time_1;

-- заносим в таблицу изменения по предидущую точку tipd_tm1
if tipd_tm1.situation_id is not null 
then 
tipd_oper:=re_situation.f_curs_v1_v2(
tipd_tm1.x_longitude, tipd_tm1.y_latitude, tipd_tm1.h_height, p_x_longitude, p_y_latitude, p_h_height, time_);

UPDATE re_situation.situation 
SET course = tipd_oper.curs , horizontal_velocity = tipd_oper.v1, vertical_velocity = tipd_oper.v2 
WHERE situation_id=tipd_tm1.situation_id;

else
-- здесь еще что-то должно быть 
end if;

-- заносим в таблицу текущую точку 
if tipd_tm1.situation_id is null 
then 
UPDATE re_situation.situation 
SET operative_time = '0' , air_object_id = p_air_object_id , 
x_longitude = p_x_longitude, y_latitude = p_y_latitude, h_height = p_h_height , course = p_course,
horizontal_velocity = p_horizontal_velocity , vertical_velocity = p_vertical_velocity 
WHERE situation_id=p_situation_id;
else 
UPDATE re_situation.situation 
SET operative_time = p_operative_time , air_object_id = p_air_object_id , 
x_longitude = p_x_longitude, y_latitude = p_y_latitude, h_height = p_h_height , course = p_course,
horizontal_velocity = p_horizontal_velocity , vertical_velocity = p_vertical_velocity 
WHERE situation_id=p_situation_id;

end if;

-- рассчитываем и заносим в таблицу параметры точки tp1 если она есть 
if tipd_tp1.situation_id is not null 
then
-- рассчитываем новое местоположение tp1
time_1:=extract(hours from (p_operative_time))*3600 + extract(minutes from (p_operative_time))*60 + extract(seconds from (p_operative_time));
time_2:=extract(hours from (tipd_tp1.operative_time))*3600 + extract(minutes from (tipd_tp1.operative_time))*60 + extract(seconds from (tipd_tp1.operative_time));
time_:=time_2-time_1;
tipd_oper := re_situation.f_position_in_time( 
p_x_longitude, p_y_latitude, p_h_height, p_course, p_horizontal_velocity, p_vertical_velocity, time_ );
-- заносим рассчитанные параметры в таблицу для точки tp1 
UPDATE re_situation.situation 
SET x_longitude = tipd_oper.x2, y_latitude = tipd_oper.y2, h_height = tipd_oper.h2
WHERE situation_id=tipd_tp1.situation_id;

-- если есть точка tp2 то 
if tipd_tp2.situation_id is not null 
then
-- рассчитываем курс и скорости таким образом, чтобы из нового положения tp1 попасть в старую точку tp2
time_1:=extract(hours from (tipd_tp1.operative_time))*3600 + extract(minutes from (tipd_tp1.operative_time))*60 + extract(seconds from (tipd_tp1.operative_time));
time_2:=extract(hours from (tipd_tp2.operative_time))*3600 + extract(minutes from (tipd_tp2.operative_time))*60 + extract(seconds from (tipd_tp2.operative_time));
time_:=time_2-time_1;

tipd_oper2:=re_situation.f_curs_v1_v2(
tipd_oper.x2, tipd_oper.y2, tipd_oper.h2, tipd_tp2.x_longitude, tipd_tp2.y_latitude, tipd_tp2.h_height, time_);

-- заносим в таблицу курс горизонтальную и вертикальную скорости для точки tp2
UPDATE re_situation.situation 
SET course = tipd_oper2.curs, horizontal_velocity = tipd_oper2.v1, vertical_velocity = tipd_oper2.v2
WHERE situation_id=tipd_tp1.situation_id;



end if; -- если есть точка tp2 конец if 


end if; 



return true;

end;
$BODY$
LANGUAGE plpgsql VOLATILE
--  COST 100;
-- ALTER FUNCTION simulation_log.current_position_in_time(double precision, double precision, double precision, double precision, double precision, double precision, double precision, integer)
--   OWNER TO postgres;
