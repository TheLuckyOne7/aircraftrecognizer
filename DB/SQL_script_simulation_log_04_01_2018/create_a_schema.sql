﻿-- Schema: re_situation
DROP SCHEMA if exists   simulation_log cascade ;
CREATE SCHEMA simulation_log AUTHORIZATION maximrepey;

-- Table: air_situation - Воздушная обстановка
-- drop table if exists simulation_log.air_situation;
-- /*
create table simulation_log.air_situation (

air_situation_id serial not null primary key, -- искуственый первичный ключ
modeling_time interval second  not null , -- время с начала процесса моделирования
air_object_id integer not null, -- внешний ключ на поле "air_object_id" таблицы "re_situation.air_object"
x_longitude double precision, -- географическа долгота
y_latitude double precision, -- географическа шиота
h_height double precision, -- высота полета
aircraft_condition_id  integer, -- номер состояния воздушного объекта внешний на поле "aircraft_condition_id" таблицы "aircraft_condition"


-- алтернативный ключ
constraint air_situation_UNIQUE
	unique(air_object_id, modeling_time),
-- в каждый момент времени одному воздушному объекту соответсвует одни точка в пространстве

-- реализация внешнего ключа на поле "air_object_id" таблицы "re_situation.air_object"
constraint air_object_id_re_situation_air_object_id_FK
	foreign key (air_object_id)
	references  re_situation.air_object(air_object_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа на поле "aircraft_condition_id" таблицы "aircraft_condition"
CONSTRAINT simulation_log_aircraft_condition_id_fk
	FOREIGN KEY (aircraft_condition_id)
	REFERENCES re_situation.aircraft_condition (aircraft_condition_id)
		ON UPDATE CASCADE ON DELETE RESTRICT
);
--*/


-- Table: freq_band_air_radars - ЭМО в полосе частот БРЛС
-- drop table if exists simulation_log.freq_band_air_radars;
-- /*
create table simulation_log.freq_band_air_radars(
freq_band_air_radars_id serial not null primary key, -- искуственый первичный ключ
air_situation_id integer not null, -- внешний ключ 1 на поле "air_situation_id" таблицы "simulation_log.air_situation"
m_airborne_radar_in_air_obj_id integer not null, -- внешний ключ 2 на таблицу "re_situation.m_airborne_radar_in_air_obj"
carrier_frequency double precision, -- несущая частота
duration_puls double precision, -- длительность зондирующего импульса, мкс
width_spectrum_puls double precision, -- Ширина спектра зондирующего импульса, МГц
modulation_radar_s_id integer not null, -- Вид модуляции зондирующего сигнала. Внешний ключ 3
cycle_time_in_pack double precision, -- Период повторения зондирующих импульсов в пачке, мкс
freq_pulses_in_a_pack double precision, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
duration_pack double precision , -- Длительность КГПИ, мс
period_pack double precision , -- Мин. Период следования КГПИ, мс
q_of_carrier_freq_in_pack integer, -- Количество несущих ча-стот в пачке, шт
r_ch_freq_of_pack_in_overview double precision, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
dur_of_r_irradiation_obj double precision , -- Длительность облучения объ-екта , мс
period_of_r_irradiation_obj double precision, -- Период облучения объекта (цикл скани-рования), с



-- алтернативный ключ
constraint freq_band_air_radars_UNIQUE
	unique(air_situation_id, m_airborne_radar_in_air_obj_id),
-- в каждый момент времени на воздущном объекте БРЛС работает в одном режиме

-- реализация внешнего ключа 1
constraint air_situation_air_situation_id_FK
	foreign key (air_situation_id)
	references  simulation_log.air_situation(air_situation_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 2
constraint re_situation_m_airborne_radar_in_air_obj_id_FK
	foreign key (m_airborne_radar_in_air_obj_id)
	references  re_situation.m_airborne_radar_in_air_obj(m_airborne_radar_in_air_obj_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 3
constraint situation_log_freq_band_air_radar_modulation_radar_s_id_FK
	foreign key (modulation_radar_s_id)
	references  re_situation.modulation_radar_s(modulation_radar_s_id)
		on delete cascade on update cascade
);
--*/

-- Table: freq_band_fas_com - ЭМО в полосе частот средств связи
-- drop table if exists simulation_log.freq_band_fas_com;
-- /*
create table simulation_log.freq_band_fas_com(
freq_band_fas_com_id serial not null primary key, -- искуственый первичный ключ
air_situation_id integer not null, -- внешний ключ 1 на поле "air_situation_id" таблицы "simulation_log.air_situation"
radio_network_id integer not null, -- внешний ключ 2 на таблицу "re_situation.radio_network_id"
carrier_frequency double precision, -- несущая частота
frequency_hopping_mode boolean, -- Признак режима с ППРЧ
freq_of_range_fhm double precision, -- мин. Ширина диапазона рабочих частот ППРЧ
while_single_fr_of_use double precision, -- длительность скачка частоты
width_signal_spectrum double precision, -- ширина спектра сигнала
modulation_fc_id integer not null, -- вид модуляции. Внешний ключ 3
digital_signal_indication boolean, -- признак цифровой/аналоговый сигналы
length_of_frame double precision, -- длительность кадра
length_of_cycle double precision, -- длительность цикла
quantity_frame_in_cykle integer, -- количество кадров в цикле
speed_of_data_transfer double precision, -- скорость передачи данных
kind_of_work_id integer, -- вид передачи (телефон/телеграф/передача данных). Внешний ключ 4
div_multiplexing_id integer, -- способ разделения каналов. Внешний ключ 5

-- алтернативный ключ
constraint freq_band_fas_com_UNIQUE
	unique(air_situation_id, radio_network_id),


-- реализация внешнего ключа 1
constraint freq_band_fas_com_air_situation_id_FK
	foreign key (air_situation_id)
	references  simulation_log.air_situation(air_situation_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 2
constraint freq_band_fas_com_radio_network_id_FK
	foreign key (radio_network_id)
	references  re_situation.radio_network(radio_network_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 3
constraint freq_band_fas_com_modulation_fc_id_FK
	foreign key (modulation_fc_id)
	references re_situation.modulation_fc(modulation_fc_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 4
constraint freq_band_fas_com_kind_of_workid_id_FK
	foreign key (kind_of_work_id)
	references re_situation.kind_of_work(kind_of_work_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 5
constraint freq_band_fas_com_multiplexing_id_FK
	foreign key (div_multiplexing_id)
	references re_situation.div_multiplexing(div_multiplexing_id)
		on delete cascade on update cascade
);
--*/

-- Table: monitoring_station_radars - Векторы измеренных параметров излучения БРЛС.
-- drop table if exists simulation_log.monitoring_station_radars;
-- /*
create table simulation_log.monitoring_station_radars (

freq_band_air_radars_id integer not null primary key, -- первичный ключ, внешний ключ 1
carrier_frequency double precision, -- несущая частота
duration_puls double precision, -- длительность зондирующего импульса, мкс
width_spectrum_puls double precision, -- Ширина спектра зондирующего импульса, МГц
modulation_radar_s double precision, -- Вид модуляции зондирующего сигнала. Внешний ключ 2
cycle_time_in_pack double precision, -- Период повторения зондирующих импульсов в пачке, мкс
freq_pulses_in_a_pack double precision, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
duration_pack double precision , -- Длительность КГПИ, мс
period_pack double precision , -- Мин. Период следования КГПИ, мс
q_of_carrier_freq_in_pack double precision, -- Количество несущих ча-стот в пачке, шт
r_ch_freq_of_pack_in_overview double precision, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
dur_of_r_irradiation_obj double precision , -- Длительность облучения объ-екта , мс
period_of_r_irradiation_obj double precision, -- Период облучения объекта (цикл скани-рования), с

-- реализация внешнего ключа 1
constraint simulation_log_freq_band_air_radars_id_FK
	foreign key (freq_band_air_radars_id)
	references  simulation_log.freq_band_air_radars(freq_band_air_radars_id)
		on delete cascade on update cascade
);
-- */


-- Table: monitoring_station_fas_com - Векторы измеренных значений радиоизлучений средств связи.
-- drop table if exists simulation_log.monitoring_station_fas_com;
-- /*
create table simulation_log.monitoring_station_fas_com (
freq_band_fas_com_id integer not null primary key, -- первичный ключ, внешний ключ 1
carrier_frequency double precision, -- несущая частота
frequency_hopping_mode double precision, -- Признак режима с ППРЧ
freq_of_range_fhm double precision, -- мин. Ширина диапазона рабочих частот ППРЧ
while_single_fr_of_use double precision, -- длительность скачка частоты
width_signal_spectrum double precision, -- ширина спектра сигнала
modulation_fc double precision, -- вид модуляции. Внешний ключ 2
digital_signal_indication double precision, -- признак цифровой/аналоговый сигналы
length_of_frame double precision, -- длительность кадра
length_of_cycle double precision, -- длительность цикла
quantity_frame_in_cykle double precision, -- количество кадров в цикле
speed_of_data_transfer double precision, -- скорость передачи данных
kind_of_work double precision, -- вид передачи (телефон/телеграф/передача данных). Внешний ключ 3
div_multiplexing double precision, -- способ разделения каналов. . Внешний ключ 4

--реализация внешнего ключа 1
constraint simulation_log_freq_band_fas_com_id_FK
	foreign key (freq_band_fas_com_id)
	references simulation_log.freq_band_fas_com(freq_band_fas_com_id)
		on delete cascade on update cascade
);
-- */


-- Table: decision_station_radars - Результаты распознавания излучений БРЛС.
-- drop table if exists simulation_log.decision_station_radars;
-- /*
create table simulation_log.decision_station_radars(
freq_band_air_radars_id integer not null primary key, -- первичный ключ, внешний ключ 1
alg_1_mod_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 2
alg_2_mod_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 3
alg_3_mod_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 4
alg_4_mod_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 5
alg_5_mod_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 6
с_alg_1_mod_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 7
с_alg_2_mod_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 8
alg_1_type_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 9
alg_2_type_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 10
alg_3_type_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 11
alg_4_type_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 12
alg_5_type_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 13
с_alg_1_type_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 14
с_alg_2_type_a_radar integer, -- результат распознавания статистический алгоритм внешний ключ 15
alg_1_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 16
alg_2_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 17
alg_3_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 18
alg_4_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 19
alg_5_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 20
с_alg_1_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 21
с_alg_2_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 22


--реализация внешнего ключа 1
constraint s__log_decision_station_radars_freq_band_air_radars_id_FK
	foreign key (freq_band_air_radars_id)
	references simulation_log.freq_band_air_radars(freq_band_air_radars_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 2
constraint simulation_log_decision_station_radars_alg_1_mod_a_radar_FK
	foreign key (alg_1_mod_a_radar)
	references re_situation.d_m_airborne_radar(d_m_airborne_radar_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 3
constraint simulation_log_decision_station_radars_alg_2_mod_a_radar_FK
	foreign key (alg_2_mod_a_radar)
	references re_situation.d_m_airborne_radar(d_m_airborne_radar_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 4
constraint simulation_log_decision_station_radars_alg_3_mod_a_radar_FK
	foreign key (alg_3_mod_a_radar)
	references re_situation.d_m_airborne_radar(d_m_airborne_radar_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 5
constraint simulation_log_decision_station_radars_alg_4_mod_a_radar_FK
	foreign key (alg_4_mod_a_radar)
	references re_situation.d_m_airborne_radar(d_m_airborne_radar_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 6
constraint simulation_log_decision_station_radars_alg_5_mod_a_radar_FK
	foreign key (alg_5_mod_a_radar)
	references re_situation.d_m_airborne_radar(d_m_airborne_radar_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 7
constraint simulation_log_decision_station_radars_с_alg_1_mod_a_radar_FK
	foreign key (с_alg_1_mod_a_radar)
	references re_situation.d_m_airborne_radar(d_m_airborne_radar_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 8
constraint simulation_log_decision_station_radars_с_alg_2_mod_a_radar_FK
	foreign key (с_alg_2_mod_a_radar)
	references re_situation.d_m_airborne_radar(d_m_airborne_radar_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 9
constraint simulation_log_decision_station_radars_alg_1_type_a_radar_FK
	foreign key (alg_1_type_a_radar)
	references re_situation.types_of_re_facilities(types_of_re_facilities_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 10
constraint simulation_log_decision_station_radars_alg_2_type_a_radar_FK
	foreign key (alg_2_type_a_radar)
	references re_situation.types_of_re_facilities(types_of_re_facilities_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 11
constraint simulation_log_decision_station_radars_alg_3_type_a_radar_FK
	foreign key (alg_3_type_a_radar)
	references re_situation.types_of_re_facilities(types_of_re_facilities_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 12
constraint simulation_log_decision_station_radars_alg_4_type_a_radar_FK
	foreign key (alg_4_type_a_radar)
	references re_situation.types_of_re_facilities(types_of_re_facilities_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 13
constraint simulation_log_decision_station_radars_alg_5_type_a_radar_FK
	foreign key (alg_5_type_a_radar)
	references re_situation.types_of_re_facilities(types_of_re_facilities_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 14
constraint simulation_log_decision_station_radars_с_alg_1_type_a_radar_FK
	foreign key (с_alg_1_type_a_radar)
	references re_situation.types_of_re_facilities(types_of_re_facilities_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 15
constraint simulation_log_decision_station_radars_с_alg_2_type_a_radar_FK
	foreign key (с_alg_2_type_a_radar)
	references re_situation.types_of_re_facilities(types_of_re_facilities_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 16
constraint sim__log_decision_station_radars_alg_1_type_of_aircraft_FK
	foreign key (alg_1_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 17
constraint sim__log_decision_station_radars_alg_2_type_of_aircraft_FK
	foreign key (alg_2_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 18
constraint sim__log_decision_station_radars_alg_3_type_of_aircraft_FK
	foreign key (alg_3_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 19
constraint sim__log_decision_station_radars_alg_4_type_of_aircraft_FK
	foreign key (alg_4_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 20
constraint sim__log_decision_station_radars_alg_5_type_of_aircraft_FK
	foreign key (alg_5_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 21
constraint sim__log_decision_station_radars_с_alg_1_type_of_aircraft_FK
	foreign key (с_alg_1_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 22
constraint sim__log_decision_station_radars_с_alg_2_type_of_aircraft_FK
	foreign key (с_alg_2_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade
);
-- */


-- Table: decision_station_fac_com - Результаты распознавания радиоизлучений средств связи.
-- drop table if exists simulation_log.decision_station_fas_com;
-- /*
create table simulation_log.decision_station_fas_com(
freq_band_fas_com_id integer not null primary key, -- первичный ключ, внешний ключ 1
alg_1_mod_fas_com integer, -- результат распознавания статистический алгоритм внешний ключ 2
alg_2_mod_fas_com integer, -- результат распознавания статистический алгоритм внешний ключ 3
alg_3_mod_fas_com integer, -- результат распознавания статистический алгоритм внешний ключ 4
alg_4_mod_fas_com integer, -- результат распознавания статистический алгоритм внешний ключ 5
alg_5_mod_fas_com integer, -- результат распознавания статистический алгоритм внешний ключ 6
c_alg_1_mod_fas_com integer, -- результат распознавания статистический алгоритм внешний ключ 7
c_alg_2_mod_fas_com integer, -- результат распознавания статистический алгоритм внешний ключ 8

--реализация внешнего ключа 1
constraint s__log_decision_station_fas_com_freq_band_fas_com_id_FK
	foreign key (freq_band_fas_com_id)
	references simulation_log.freq_band_fas_com(freq_band_fas_com_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 2
constraint simulation_log_decision_station_fas_com_alg_1_mod_fas_com_FK
	foreign key (alg_1_mod_fas_com)
	references re_situation.cat_of_radio_comm_signals(cat_of_radio_comm_signals_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 3
constraint simulation_log_decision_station_fas_com_alg_2_mod_fas_com_FK
	foreign key (alg_2_mod_fas_com)
	references re_situation.cat_of_radio_comm_signals(cat_of_radio_comm_signals_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 4
constraint simulation_log_decision_station_fas_com_alg_3_mod_fas_com_FK
	foreign key (alg_3_mod_fas_com)
	references re_situation.cat_of_radio_comm_signals(cat_of_radio_comm_signals_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 5
constraint simulation_log_decision_station_fas_com_alg_4_mod_fas_com_FK
	foreign key (alg_4_mod_fas_com)
	references re_situation.cat_of_radio_comm_signals(cat_of_radio_comm_signals_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 6
constraint simulation_log_decision_station_fas_com_alg_5_mod_fas_com_FK
	foreign key (alg_5_mod_fas_com)
	references re_situation.cat_of_radio_comm_signals(cat_of_radio_comm_signals_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 7
constraint simulation_log_decision_station_fas_com_c_alg_1_mod_fas_com_FK
	foreign key (c_alg_1_mod_fas_com)
	references re_situation.cat_of_radio_comm_signals(cat_of_radio_comm_signals_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 8
constraint simulation_log_decision_station_fas_com_c_alg_2_mod_fas_com_FK
	foreign key (c_alg_2_mod_fas_com)
	references re_situation.cat_of_radio_comm_signals(cat_of_radio_comm_signals_id)
		on delete cascade on update cascade
);
-- */


-- Table: decision_complex - Результаты комплексного распознавания ЛА по излучениям их бортовых РЭС.
-- drop table if exists simulation_log.decision_complex;
-- /*
create table simulation_log.decision_complex(
freq_band_air_radars_id integer not null, -- первичный ключ, внешний ключ 1
freq_band_fas_com_id integer not null, -- первичный ключ, внешний ключ 2
alg_1_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 3
alg_2_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 4
alg_3_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 5
alg_4_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 6
alg_5_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 7
c_alg_1_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 8
c_alg_2_type_of_aircraft integer, -- результат распознавания статистический алгоритм внешний ключ 9

constraint freq_band_air_radars_id_freq_band_fas_com_id_PK
primary key(freq_band_air_radars_id, freq_band_fas_com_id),

--реализация внешнего ключа 1
constraint s__log_decision_complex_freq_band_air_radars_id_FK
	foreign key (freq_band_air_radars_id)
	references simulation_log.freq_band_air_radars(freq_band_air_radars_id)
		on delete cascade on update cascade,

--реализация внешнего ключа 2
constraint s__log_decision_complex_freq_band_fas_com_id_FK
	foreign key (freq_band_fas_com_id)
	references simulation_log.freq_band_fas_com(freq_band_fas_com_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 3
constraint simulation_log_decision_station_fas_com_alg_1_mod_fas_com_FK
	foreign key (alg_1_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 4
constraint simulation_log_decision_station_fas_com_alg_2_mod_fas_com_FK
	foreign key (alg_2_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 5
constraint simulation_log_decision_station_fas_com_alg_3_mod_fas_com_FK
	foreign key (alg_3_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 6
constraint simulation_log_decision_station_fas_com_alg_4_mod_fas_com_FK
	foreign key (alg_4_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 7
constraint simulation_log_decision_station_fas_com_alg_5_mod_fas_com_FK
	foreign key (alg_5_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 8
constraint simulation_log_decision_station_fas_com_c_alg_1_mod_fas_com_FK
	foreign key (c_alg_1_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- реализация внешнего ключа 9
constraint simulation_log_decision_station_fas_com_c_alg_2_mod_fas_com_FK
	foreign key (c_alg_2_type_of_aircraft)
	references re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade
);
-- */

-- Table: monitoring_station - станция мониторига.
-- drop table if exists simulation_log.monitoring_station;
-- /*
create table simulation_log.monitoring_station(
monitoring_station_id serial not null primary key, -- искуственый первичный ключ
ms_name varchar (80) not null unique,
x_longitude double precision, -- географическа долгота
y_latitude double precision, -- географическа шиота
h_height double precision, -- высота подъема антены
azimut_start double precision, -- начальный азимут зоны действия
azimut_stop double precision, -- конечный азимут зоны действия
distance double precision -- дальность действия
);
-- */

-- Table: simulation_log.rezult_recognition
-- DROP TABLE if exists simulation_log.rezult_recognition;

CREATE TABLE simulation_log.rezult_recognition
(
  rezult_recognition_id serial NOT NULL, -- первичный ключ
  air_situation_id integer NOT NULL, -- номер строки в таблице air_situation
      -- внешний ключ 1 на поле air_situation_id таблицы  simulation_log.air_situation
  air_object_id integer NOT NULL, -- номер воздушного объекта
      -- дублирует поле air_object_id таблицы  simulation_log.air_situation
  modeling_time interval second  not null, -- модельное время
      /* дублирует поле modeling_time таблицы  simulation_log.air_situation
         поля modeling_time и air_object_id образуют внешний ключ на
         поля modeling_time и air_object_id таблицы  simulation_log.air_situation */
  classes_of_aircraft_id integer NOT NULL, -- номер распознанного класса ЛА
        -- внешний ключ 4 на поле classes_of_aircraft_id таблицы  re_situation.classes_of_aircraft
  "classObj" character varying(255), -- Название класса ЛА распознанного воздушного объекта
  "probClassObj" double precision NOT NULL, -- Вероятность распознавания класса
  type_of_aircraft_id integer NOT NULL, -- номер распознанного типа ЛА
      -- внешний ключ 5 на поле type_of_aircraft_id таблицы  re_situation.type_of_aircraft
  "typeObj" character varying(255), -- Название типа ЛА распознанного воздушного объекта
  "probTypeObj" double precision NOT NULL, -- Вероятность распознавания типа
  types_of_re_facilities_id integer NOT NULL, -- номер распознанного типа РЭС-БРЛС
      -- внешний ключ 6 на поле types_of_re_facilities_id таблицы types_of_re_facilities
  "radarName" character varying(255), -- наименование распознанного типа РЭС-БРЛС
  "probRadarName" double precision, -- вероятность распознавания типа РЭС-БРЛС
  d_m_airborne_radar_id integer not null, -- Номер распознанного режима работы БРЛС
      -- внешний ключ 7 на поле d_m_airborne_radar_id таблицы d_m_airborne_radar
  "radarMode" character varying(255), -- Название распознанного режима работы БРЛС
  "probRadarMode" double precision, -- Вероятность распознавания режима работы БРЛС
  cat_of_radio_comm_signals_id integer not null , -- номер распознанной категории сигналла связи
      -- внешний ключ 8 на поле cat_of_radio_comm_signals_id таблицы cat_of_radio_comm_signals
  "radiationClass" character varying(255), -- наименование распознанной категории сигналла связи
  "probRadiationClass" double precision, -- Вероятность распознавания категории сигналла связи
  "xLongtitude" double precision, -- координата X долгота воздушного объекта
  "yLatitude" double precision, -- координата Y широта воздушного объекта
  "hHeight" double precision, -- высота воздушного объекта
  "hVelocity" double precision, -- горизонтальная скорость
  "vVelocity" double precision, -- вертикальная скорость
  "modeRecognizeID" integer, -- номер распознанного состояния объекта
  "modeRecognizeName" character varying(255), -- название распознанного состояния
  "probModeRecognize" double precision, -- вероятность рапознавания состояния ВО

  -- реализация первичного ключа
  CONSTRAINT rezult_recognition_pkey PRIMARY KEY (rezult_recognition_id),

  -- реализация внешненго ключа 1 на поле air_situation_id таблицы air_situation
-- /*
  constraint rezult_recognition_air_situation_id_fk foreign key (air_situation_id)
      references simulation_log.air_situation(air_situation_id) match simple
      on update cascade on delete cascade,
-- */

-- реализация составного внешненго ключа 2 и 3 на поля modeling_time, air_object_id
-- таблицы simulation_log.air_situation
-- /*
  constraint rezult_recognition_modelinng_time_air_object_id_fk
      foreign key (modeling_time, air_object_id)
      references simulation_log.air_situation(modeling_time, air_object_id) match simple
      on update cascade on delete cascade,
-- */

-- реализация внешненго ключа 4 на поле classes_of_aircraft_id таблицы  classes_of_aircraft
-- /*
  constraint rezult_recognition_classes_of_aircraft_id_fk foreign key (classes_of_aircraft_id)
      references re_situation.classes_of_aircraft(classes_of_aircraft_id) match simple
      on update cascade on delete cascade,
-- */

-- реализация внешненго ключа 5 на поле type_of_aircraft_id таблицы types_of_re_facilities
-- /*
  constraint rezult_recognition_type_of_aircraft_id_fk foreign key (type_of_aircraft_id)
      references re_situation.type_of_aircraft(type_of_aircraft_id)match simple
      on update cascade on delete cascade,
-- */

-- реализация внешненго ключа 6 на поле types_of_re_facilities_id таблицы types_of_re_facilities
-- /*
  constraint rezult_recognition_types_of_re_facilities_id_fk foreign key (types_of_re_facilities_id)
      references  re_situation.types_of_re_facilities(types_of_re_facilities_id) match simple
      on update cascade on delete cascade,
-- */

-- реализация внешненго ключа 7 на поле d_m_airborne_radar_id таблицы d_m_airborne_radar
-- d_m_airborne_radar_id
-- /*
  constraint rezult_recognition_d_m_airborne_radar_id_fk foreign key (d_m_airborne_radar_id)
      references re_situation.d_m_airborne_radar(d_m_airborne_radar_id) match simple
      on update cascade on delete cascade,
-- */

-- реализация внешненго ключа 8 на поле cat_of_radio_comm_signals_id таблицы cat_of_radio_comm_signals
-- cat_of_radio_comm_signals_id
-- /*
  constraint rezult_recognition_cat_of_radio_comm_signals_id_fk foreign key (cat_of_radio_comm_signals_id)
      references re_situation.cat_of_radio_comm_signals(cat_of_radio_comm_signals_id) match simple
      on update cascade on delete cascade
-- */

)
