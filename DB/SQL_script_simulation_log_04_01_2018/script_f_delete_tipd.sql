﻿-- функуия re_situation.f_delete_tipd(situation_id integer)

CREATE OR REPLACE FUNCTION re_situation.f_delete_tipd(p_situation_id integer)
RETURNS boolean AS
$BODY$
declare 
tipd_less re_situation.situation; -- для хранения строки таблицы с ближайшим меньшим оперативным временем для заданного ВО 
tipd_more re_situation.situation; -- для хранения строки таблицы с ближайшим большим оперативным временем для заданного ВО
tipd_del re_situation.situation; -- для хранения удаляемой строки таблицы 
time_1 double precision;
time_2 double precision;
time_dp double precision;
rec_1 record;
rec_2 record;
time_in interval second;


begin 
-- необходимо удалить ТИПД c номером p_situation_id
-- определить operative_time и air_object_id для 
select * into tipd_del from re_situation.situation where situation_id=p_situation_id;
if tipd_del.situation_id is null 
then 
raise notice 'в сценарии отсутствует строка с номером %.
Строка не удалена', p_situation_id;
return false;
end if;

-- определить ТИПД с ближайшим меньшим оперативным временем для заданного ВО
select * 
into tipd_less
from re_situation.situation
where air_object_id=tipd_del.air_object_id and operative_time<tipd_del.operative_time
order by operative_time desc
limit 1 ;

-- найти ТИПД с ближайшим большим временем для заданного воздушного объекта 
select * 
into tipd_more
from re_situation.situation
where air_object_id=tipd_del.air_object_id and operative_time>tipd_del.operative_time
order by operative_time asc 
limit 1 ;

-- возможны четыре ситуации
case
when (tipd_less.situation_id is null) and (tipd_more.situation_id is null)
--  удаляемая точка первая и единственная в сценарии для заданного ВО
then
-- удалить соответстующую ей строку из таблицы и все
DELETE FROM re_situation.situation WHERE situation_id = tipd_del.situation_id;

when (tipd_less.situation_id is not null) and (tipd_more.situation_id is null) 
-- удаляемая точка последняя в сценарии для заданного объекта 
then
-- удалить соответстующую ей строку из таблицы и все
DELETE FROM re_situation.situation WHERE situation_id = tipd_del.situation_id;


when (tipd_less.situation_id is null) and (tipd_more.situation_id is not null)
-- попытка удалить первую точку из уже имеющегося маршрут для заданного ВО 
then 
-- первую точку удалять нельзя вывести сообщение и прекратить работу функции
raise notice 'Предпринимается попытка удалить первую точку из уже имеющегося маршрута воздушного объекта с id=% 
Нельзя изменять начало маршрута таким образом.', tipd_del.air_object_id ; 
return false;

when (tipd_less.situation_id is not null) and (tipd_more.situation_id is not null)
-- попытка удалить промежуточную точку между двумя уже имеющимися точками маршрута заданного ВО
then 
-- удалить строку из таблицы соответствующую этой ТИПД
DELETE FROM re_situation.situation WHERE situation_id = tipd_del.situation_id;
-- определить временной интервал в формате "double precision"
time_1:=extract(hours from (tipd_less.operative_time))*3600 + extract(minutes from (tipd_less.operative_time))*60 + extract(seconds from (tipd_less.operative_time));
time_2:=extract(hours from (tipd_more.operative_time))*3600 + extract(minutes from (tipd_more.operative_time))*60 + extract(seconds from (tipd_more.operative_time));
time_dp:=time_2-time_1;

-- вычислить курс, горизонтальную и вертикальную скорости из tipd_less в tipd_more
rec_1=re_situation.f_curs_v1_v2(tipd_less.x_longitude, tipd_less.y_latitude, tipd_less.h_height,
tipd_more.x_longitude, tipd_more.y_latitude, tipd_more.h_height, time_dp);
-- обновитть соответствующие поля в строке таблицы "tipd_less" 
UPDATE re_situation.situation SET course = rec_1.curs, horizontal_velocity = rec_1.v1 , vertical_velocity = rec_1.v2 
WHERE situation_id=tipd_less.situation_id;

end case;

return true;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE
  COST 100;