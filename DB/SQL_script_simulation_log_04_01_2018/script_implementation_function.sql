﻿-- Function: script implementation(real, real, real)
-- Расчет местоположения воздушного объекта для заданного момента времени 
-- При известном начальном положении X, Y, h, v1, v2, curs, time .
-- DROP FUNCTION current_position_in_time(real, real, real);

CREATE OR REPLACE FUNCTION simulation_log.script_implementation(
in delta_t double precision -- интервал времени моделирования
)
RETURNS double precision  
-- TABLE (
-- Xr double precision, -- долгота расчетная на момент времени  "delta_t" 
-- Yr double precision, -- широта расчетная на момент времени  "delta_t" 
-- hr double precision -- высота расчетная на момент времени  "delta_t" 
-- ) 
AS
$BODY$
declare 

rez integer ;
List_object record; -- список воздушных объектов имеющихся в сценарии 
max_time double precision ; -- длительность сценария double precision
time_interval  interval ; -- временная переменная типа interval
model_time double precision ; -- текущее время в реализации сценария
oper_time double precision ; -- оперативное время сценария
situation_row re_situation.situation ; -- строка таблицы "re_situation.situation" 
air_situation_row simulation_log.air_situation ; -- строка таблицы "situation_log.air_situation"
m_airborne_radar_in_air_obj_idf integer; -- номер строки в таблице работы БРЛС
air_sutuation_idf integer; --  
m_airborne_radar_in_air_obj_row re_situation.m_airborne_radar_in_air_obj; -- строка таблицы "re_situation.m_airborne_radar_in_air_obj"
modulation_idf integer; -- переменная для хранения вида модуляции в промежуточны расчетах 
oper_freq double precision; -- опретивная частота 
use_networks_air_objects_idf integer; -- номер строки в таблице работы сетей 
fas_com_sign_parameters record ; -- набор параметров для полкчения и записи в таблицу "freq_band_fas_com"

begin
-- Очиститть журнал от педидущей реализации 
delete from simulation_log.decision_complex;
delete from simulation_log.decision_station_fas_com;
delete from simulation_log.decision_station_radars;
delete from simulation_log.monitoring_station_fas_com;
delete from simulation_log.monitoring_station_radars;

delete from simulation_log.freq_band_air_radars;
select setval('simulation_log.freq_band_air_radars_freq_band_air_radars_id_seq', 1) into rez;

delete from simulation_log.freq_band_fas_com;
select setval('simulation_log.freq_band_fas_com_freq_band_fas_com_id_seq', 1) into rez;

delete from simulation_log.air_situation;
select setval('simulation_log.air_situation_air_situation_id_seq', 1) into rez;

-- Определить длительность сценария 
select max(operative_time)
into time_interval 
from re_situation.situation ; 
-- raise notice 'max_time_interval=%' , max_time_interval;

max_time=extract(hours from (max(time_interval)))*3600 + extract(minutes from (max(time_interval)))*60 + extract(seconds from (max(time_interval))); 
-- raise notice 'max_time=%' , max_time;





-- Цикл по списку объектов присутствующих в сценарии
for list_object in 
select distinct air_object_id from re_situation.situation 
loop
-- raise notice 'цикл по list_object, list_object.air_object_id=%',  list_object.air_object_id ;


-- Определить начальное время сценария 
execute 
'select min(operative_time)
from re_situation.situation
where air_object_id=$1'
into time_interval 
using list_object.air_object_id;
model_time=extract(hours from (max(time_interval)))*3600 + extract(minutes from (max(time_interval)))*60 + extract(seconds from (max(time_interval))); 



-- Цикл по времени исполнения сценария для текущего объекта 
while model_time<=max_time loop
-- raise notice 'air_object_id=% , model_time=%' , list_object.air_object_id , model_time;

-- получить данные о ТИПД соответствующей текущим ВО и модельному времени 
--сторку таблицы "re_situation.situation" 
execute 'select * from re_situation.situation 
where operative_time<='''||model_time::text||''' and air_object_id=$1
order by operative_time desc limit 1'
into situation_row
using list_object.air_object_id;

-- raise notice 'situation_row.operative_time = %, model_time=%', situation_row.operative_time, model_time ;
-- raise notice 'situation_row.operative_time = %, model_time=%', situation_row.operative_time, model_time ;


-- рассчитать положение объекта для текущего модельного времени 
oper_time:=extract(hours from situation_row.operative_time)*3600 +
	extract(minutes from situation_row.operative_time)*60 + 
	extract (seconds from situation_row.operative_time);

air_situation_row:=simulation_log.current_position_in_time(
situation_row.x_longitude , -- долгота 
situation_row.y_latitude , -- широта
situation_row.h_height , -- висота полета 
situation_row.horizontal_velocity , -- горизонтальная скорость полета 
situation_row.vertical_velocity , -- вертикальная скорость полета 
situation_row.course , -- курс ЛА
model_time-oper_time, -- время от момента соответствующего в сценприи ТИПД
list_object.air_object_id -- air_object_id номер воздушного обїекта
);

air_situation_row.aircraft_condition_id=situation_row.aircraft_condition_id;

-- записать положение объекта в таблицу "simulation_log.air_situation"

execute '
INSERT INTO simulation_log.air_situation(
air_situation_id, modeling_time, air_object_id, x_longitude, y_latitude, h_height, aircraft_condition_id) 
VALUES (default, '''||model_time::text||''', $1, $2, $3, $4, $5)'
using list_object.air_object_id, air_situation_row.x_longitude, air_situation_row.y_latitude, 
air_situation_row.h_height, air_situation_row.aircraft_condition_id;

-- определить только что записанное значение air_situation_id 
execute '
select air_situation_id 
from simulation_log.air_situation
where modeling_time='''||model_time::text||''' and air_object_id=$1'
into air_sutuation_idf 
using list_object.air_object_id;

-- raise notice 'air_sutuation_idf=%', air_sutuation_idf;

-- опреределить режим работы БРЛС, включенной в текущий момент времени 
-- ????? select m_airborne_radar_in_air_obj_id
execute '
select m_airborne_radar_in_air_obj_id
from re_situation.work_radar_on_air_objects
where situation_id=$1'
into m_airborne_radar_in_air_obj_idf
using situation_row.situation_id;

-- raise notice 'm_airborne_radar_in_air_obj_idf=%', m_airborne_radar_in_air_obj_idf;

-- если есть работающая БРЛС то записать параметры режима работы в таблицу "simulation_log.freq_band_air_radars"
if m_airborne_radar_in_air_obj_idf is not null
then 
-- возможно здесь нужен цикл 
-- получить строку таблицы "re_situation.m_airborne_radar_in_air_obj"
execute '
select * from re_situation.m_airborne_radar_in_air_obj
where m_airborne_radar_in_air_obj_id=$1'
into m_airborne_radar_in_air_obj_row
using m_airborne_radar_in_air_obj_idf;

--raise notice '???????  m_airborne_radar_in_air_obj_row=%', 
--m_airborne_radar_in_air_obj_row.m_airborne_radar_in_air_obj_id; 

-- определить первую из рабочих частот режима работы БРЛС
execute '
select operating_frequency
from re_situation.oper_freq_air_objects
where m_airborne_radar_in_air_obj_id=$1
limit 1'
into oper_freq
using m_airborne_radar_in_air_obj_idf;

-- определить номер вида модуляции
execute '
select modulation_radar_s_id from re_situation.d_m_airborne_radar
where d_m_airborne_radar_id= $1'
into modulation_idf
using m_airborne_radar_in_air_obj_row.d_m_airborne_radar_id;
-- raise notice 'modulation_idf=%',  modulation_idf;

-- запись в таблицу 
execute '
insert into simulation_log.freq_band_air_radars(
freq_band_air_radars_id, air_situation_id, m_airborne_radar_in_air_obj_id ,
carrier_frequency, duration_puls, width_spectrum_puls, modulation_radar_s_id,
cycle_time_in_pack, freq_pulses_in_a_pack, duration_pack,
period_pack, q_of_carrier_freq_in_pack, r_ch_freq_of_pack_in_overview,
dur_of_r_irradiation_obj, period_of_r_irradiation_obj)
values ( default, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14)'
using
air_sutuation_idf, -- $1 air_situation_id
m_airborne_radar_in_air_obj_idf, -- $2 m_airborne_radar_in_air_obj_id
oper_freq, -- $3 carrier_frequency
m_airborne_radar_in_air_obj_row.duration_puls, -- $4 duration_puls
m_airborne_radar_in_air_obj_row.width_spectrum_puls, -- $5 width_spectrum_puls
modulation_idf, -- $6 modulation_radar_s_id
m_airborne_radar_in_air_obj_row.cycle_time_in_pack, -- $7 cycle_time_in_pack
m_airborne_radar_in_air_obj_row.freq_pulses_in_a_pack, -- $8 freq_pulses_in_a_pack
m_airborne_radar_in_air_obj_row.duration_pack, -- $9 duration_pack
m_airborne_radar_in_air_obj_row.period_pack, -- $10 period_pack
m_airborne_radar_in_air_obj_row.q_of_carrier_freq_in_pack, -- $11 q_of_carrier_freq_in_pack
m_airborne_radar_in_air_obj_row.r_ch_freq_of_pack_in_overview, -- $12 r_ch_freq_of_pack_in_overview
m_airborne_radar_in_air_obj_row.dur_of_r_irradiation_obj, -- $13 dur_of_r_irradiation_obj
m_airborne_radar_in_air_obj_row.period_of_r_irradiation_obj; -- $14 period_of_r_irradiation_obj

end if;

-- определить средство средство связи и класс и параметры сигнала включенное в данный момент времени 
-- сначала подсчитаем колисество связных сигналов в данный момент времени от данного объекта 
execute 'select count (use_networks_air_objects_id)
from re_situation.work_fc_on_air_object
where situation_id=$1'
into use_networks_air_objects_idf
using situation_row.situation_id;
-- raise notice 'кол-во излучений средств связи - %', use_networks_air_objects_idf ;


--Начало цикла по средствам связи 
for use_networks_air_objects_idf in
execute 'select use_networks_air_objects_id
from re_situation.work_fc_on_air_object
where situation_id=$1'
using situation_row.situation_id
loop
-- raise notice 'use_networks_air_objects_idf=%', use_networks_air_objects_idf;

-- получить данные для таблицы "simulation_log.freq_band_fas_com"
execute '
select 
rn.radio_network_id,
ds.frequency_hopping_mode ,
rn.freq_of_range_fhm ,
rn.while_single_fr_of_use ,
rn.width_signal_spectrum ,
ds.modulation_fc_id ,
ds.digital_signal_indication ,
rn.length_of_frame ,
rn.length_of_cycle ,
rn.quantity_frame_in_cykle ,
rn.speed_of_data_transfer ,
ds.kind_of_work_id ,
ds.div_multiplexing_id 
from 
re_situation.radio_network as rn,
re_situation.descr_cat_of_r_com_signals as ds,
re_situation.use_networks_air_objects as uo
where 
rn.descr_cat_of_r_com_signals_id=ds.descr_cat_of_r_com_signals_id
and uo.radio_network_id=rn.radio_network_id
and use_networks_air_objects_id=$1'
into fas_com_sign_parameters
using use_networks_air_objects_idf;

-- одну из рабочих частот радиосети.
execute '
select operating_frequency
from re_situation.operating_frequencies as f,
re_situation.use_networks_air_objects as u
where u.radio_network_id=f.radio_network_id
and use_networks_air_objects_id=$1
limit 1 '
into oper_freq
using use_networks_air_objects_idf;

-- записать полученные данные в таблицу "simulation_log.freq_band_fas_com"
execute '
insert into simulation_log.freq_band_fas_com
(
freq_band_fas_com_id,
air_situation_id,
radio_network_id,
carrier_frequency,
frequency_hopping_mode,
freq_of_range_fhm,
while_single_fr_of_use,
width_signal_spectrum,
modulation_fc_id,
digital_signal_indication,
length_of_frame,
length_of_cycle,
quantity_frame_in_cykle,
speed_of_data_transfer,
kind_of_work_id,
div_multiplexing_id)
values (default, $1, $2, $3, $4, $5, $6, $7, $8, $9, $10, $11, $12, $13, $14, $15)'
using 
air_sutuation_idf ,
fas_com_sign_parameters.radio_network_id,
oper_freq,
fas_com_sign_parameters.frequency_hopping_mode ,
fas_com_sign_parameters.freq_of_range_fhm ,
fas_com_sign_parameters.while_single_fr_of_use ,
fas_com_sign_parameters.width_signal_spectrum ,
fas_com_sign_parameters.modulation_fc_id ,
fas_com_sign_parameters.digital_signal_indication ,
fas_com_sign_parameters.length_of_frame ,
fas_com_sign_parameters.length_of_cycle ,
fas_com_sign_parameters.quantity_frame_in_cykle ,
fas_com_sign_parameters.speed_of_data_transfer ,
fas_com_sign_parameters.kind_of_work_id ,
fas_com_sign_parameters.div_multiplexing_id;

end loop;
--Конец цикла по средствам связи 

model_time:=model_time+delta_t;
end loop;
-- Конец цикла по времени исполнения сценария для текущего объекта 

end loop;
-- Конец цикла по списку объектов присутствующих в сценарии


 
return max_time;

end;
$BODY$
  LANGUAGE plpgsql VOLATILE
