﻿-- /*
delete from re_situation.categoryes_facilities;

-- */
-- /*
INSERT INTO re_situation.categoryes_facilities 
(categoryes_facilities_id, categoryes_name_r, abr_categoryes_name_r, categoryes_name_e, abr_categoryes_name_e) 
VALUES 
( 1, 'Бортовая радиолокационная станция', 'БРЛС', 'airborne radar', 'AR'),
( 2, 'Средство радиосвязи и передачи данных',  'СРС ПД' , 'Radio communications and data transmission', 'RCDT'),
(3, 'Бортовая радионавигационная система', 'РНС', 'On-board navigation system', 'NS'),
(4, 'Бортовая система опознавания и определения государственной принадлежности ЛА', 'БСОГП' , 
'On-board identification system and the definition of state aircraft accessories', 'IDSDS');

select setval ('re_situation.categoryes_facilities_categoryes_facilities_id_seq', 5);
--*/