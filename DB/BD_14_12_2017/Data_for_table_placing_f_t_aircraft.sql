﻿delete from re_situation.placing_f_t_aircraft;

insert into re_situation.placing_f_t_aircraft(
placing_f_t_aircraft_id,  -- Алтьтернативный первичный ключ 
type_of_aircraft_id, -- Внешний ключ на таблицу type_of_aircraft(тип Л.А)
types_of_re_facilities_id, -- Внешний ключ на таблицу types_of_re_facilities(типы РЭС)
code_name --Кодовое наименование средства 
)
values 
( 
1, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'B-52H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APQ-166'), 
'ЛА-"В-52Н" БРЛС №1-"AN/APQ-166" №1'
),

( 
2, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'B-1B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APQ-164'), 
'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
),

( 
3, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'B-2B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APQ-181'), 
'ЛА-"В-2В" БРЛС -"AN/APQ-181" №1'
),

( 
4, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'RC-135'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APQ-122'), 
'ЛА-"RC-135" БРЛС-"AN/APQ-122" №1' 
),

( 
5, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'C-130'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APN-59'), 
'ЛА-"C-130" БРЛС-"AN/APN-59" №1' 
),

( 
6 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'MC-130H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APQ-170'),
'ЛА-"MC-130H" БРЛС-"AN/APQ-170" №1' 
 ),
 
( 
7 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'RC-130E'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APQ-175'),
'ЛА-"С-130Е" БРЛС-"AN/APQ-175" №1' 
 ),
 
( 
8 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'EC-130H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APS-128'),
'ЛА-"ЕС-130H" БРЛС-"AN/APS-128" №1' 
 ),

( 
9 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'C-5A'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='РЛС С-5А'),
'ЛА-"С-5А" БРЛС-"РЛС С-5А" №1' 
 ),
 
( 
10 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APY-2'),
'ЛА-"Е-3C" БРЛС-"AN/APY-2" №1' 
 ),

( 
11 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'F-16F'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APG-80'),
'ЛА-"F-16F" БРЛС-"AN/APG-80" №1' 
 ),

( 
12 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'F/A-18F'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APG-79'),
'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1' 
 ),
 
( 
13 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'F-22'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APG-77'),
'ЛА-"F-22" БРЛС-"AN/APG-77" №1' 
 ),

( 
14 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'F/A-18D'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APG-73'),
'ЛА-"F/A-18C/D" БРЛС-"AN/APG-73" №1' 
 ),

( 
15 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'F-15E'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APG-70'),
'ЛА-"F-15Е" БРЛС-"AN/APG-70" №1' 
 ),

( 
16 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'F-16D'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APG-68'),
'ЛА-"F-16D" БРЛС-"AN/APG-68" №1' 
 ),

( 
17 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'F/A-18B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APG-65'),
'ЛА-"F/A-18А/В" БРЛС-"AN/APG-65" №1' 
 ),

( 
18 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'F-15B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APG-63'),
'ЛА-"F-15В" БРЛС-"AN/APG-63" №1' 
 ),

( 
19 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'F-15D'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APG-63(v)2'),
'ЛА-"F-15D" БРЛС-"AN/APG-63(v)1/(v)2" №1' 
 ),

( 
20 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'C-17A'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APS-133'),
'ЛА-"C-17A" БРЛС-"AN/APS-133" №1' 
 ),

( 
21 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'E-2J'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APS-145'),
'ЛА-"Е-2J" БРЛС-"AN/APS-145" №1' 
 ),

( 
22 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'E-2C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APS-145'),
'ЛА-"E-2C" БРЛС-"AN/APS-145" №1' 
 ),

( 
23 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'AP-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APS-503F'),
'ЛА-"AP-3C" БРЛС-"AN/APS-503F" №1' 
 ),

( 
24 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'P-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APS-137В'),
'ЛА-"P-3C" БРЛС-"AN/APS-137В" №1' 
 ),

( 
25 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'C-18'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='JSTARS'),
'ЛА-"С-18" БРЛС-"AN/APS-80" №1' 
 ),

 ( 
26 ,
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r = 'KC-135'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r='AN/APN-59'),
'ЛА-"KС-135" БРЛС-"AN/APN-59" №1' 
 );
--*/


select setval('re_situation.placing_f_t_aircraft_placing_f_t_aircraft_id_seq', 26);
