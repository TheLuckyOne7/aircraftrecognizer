﻿--/*
delete from re_situation.categoryes_a_radar;
-- */

-- /*
INSERT INTO re_situation.categoryes_a_radar
(categoryes_a_radar_id, categoryes_name_r, abr_categoryes_name_r, categoryes_name_e, abr_categoryes_name_e)
VALUES 
(1, 'Многофункциональная БРЛС', 'МФ БРЛС', 'Multi-function аirborne radar', 'MFAR'),
(2, 'БРЛС обзора земной поверхности',  'БРЛС ОЗП', 'Airborne radar survey the Earth''s surface', 'AR'),
(3, 'Метеонавигационная БРЛС', 'МН БРЛС', 'Weather and navigation airborne radar', 'WNAR' ),
(4, 'БРЛС управления оружием', 'БРРС УО', 'Onboard fire control radar', 'FCR'),
(5, 'БРЛС обеспечения полета на малых высотах', 'БРЛС ОПМВ', 'Radar ensure flight at low altitudes', 'EFLA'),
(6, 'БРЛС дальнего радиолокационного обнаружения воздушных и надводных целей', 'БРЛС ДРЛО', 
'AWACS radar aircraft and surface targets', 'AWACSR'),
(7, 'БРЛС обнаружения надводных целей', 'БРЛС ОНВЦ', 'Radar detection of surface targets', 'RDST'),
(8, 'БРЛС бокового обзора', 'БРЛС БО', 'Side-looking radar', 'SLR');

select setval ('re_situation.categoryes_a_radar_categoryes_a_radar_id_seq', 9);
--*/