#!/bin/bash
cd ~/Work/Diplom/AircraftRecognizer/DB/BD_14_12_2017/
dropdb 'Simulator'
createdb 'Simulator'
psql -U maximrepey -d Simulator -a -f classes_of_aircraft.sql
psql -U maximrepey -d Simulator -a -f data_for_table_classes_of_aircraft.sql
psql -U maximrepey -d Simulator -a -f data_for_table_manufacturer_country.sql
psql -U maximrepey -d Simulator -a -f data_for_table_categoryes_facilities.sql
psql -U maximrepey -d Simulator -a -f data_for_table_categoryes_a_radar.sql
psql -U maximrepey -d Simulator -a -f data_for_table_type_of_aircraft.sql
psql -U maximrepey -d Simulator -a -f Data_for_table_type_of_re_facilities_radars.sql
psql -U maximrepey -d Simulator -a -f Data_for_table_modulation_radar_s.sql
psql -U maximrepey -d Simulator -a -f data_for_table_d_m_airborne_radar.sql
psql -U maximrepey -d Simulator -a -f Data_for_table_placing_f_t_aircraft.sql
psql -U maximrepey -d Simulator -a -f data_for_table_kind_of_work.sql
psql -U maximrepey -d Simulator -a -f data_for_table_modulation_fc.sql
psql -U maximrepey -d Simulator -a -f data_for_table_div_multiplexing.sql
psql -U maximrepey -d Simulator -a -f Data_for_table_cat_of_radio_comm_signals.sql
psql -U maximrepey -d Simulator -a -f data_for_table_descr_cat_of_r_com_signals.sql
psql -U maximrepey -d Simulator -a -f data_for_table_type_of_re_facilities_comunication.sql
psql -U maximrepey -d Simulator -a -f Data_for_table_placing_f_t_aircraft_f_com.sql
psql -U maximrepey -d Simulator -a -f Data_for_table_work_mode_in_fc.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_condition.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_cond_mode_radar_F-16D.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_cond_mode_radar_F-18F.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_cond_mode_radar_B-1B.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_cond_mode_radar_KC-135.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_cond_mode_radar_E-3C.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_cond_mode_radar_E-2C.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_fc_signals_F-18F.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_fc_signals_F-16D.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_fc_signals_B-1B.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_fc_signals_KC-135.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_fc_signals_E-3C.sql
psql -U maximrepey -d Simulator -a -f data_for_table_aircraft_fc_signals_E-2C.sql
