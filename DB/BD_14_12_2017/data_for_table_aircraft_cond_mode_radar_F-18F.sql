﻿insert into re_situation.aircraft_cond_mode_radar
(
  aircraft_condition_id , -- номер состояния воздушного объекта 
  d_m_airborne_radar_id  -- номер режима работы БРЛС воздушного объекта
)

values 
-------------------------------
-- самолет - F/A-18F
-- состояние ЛА - 18 Навигация по радиолокационным маякам
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Навигация по радиолокационным маякам' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р1'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - F/A-18F
-- состояние ЛА - 19 Навигация по радиолокационным ориентирам
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р2
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Навигация по радиолокационным ориентирам' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р2'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 20 Полет на малой высоте c огибанием рельефа местности
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р3
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Полет на малой высоте c огибанием рельефа местности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р3'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - F/A-18F
-- состояние ЛА - 21 Полет на малой высоте c заданным превышением над максимальной точкой маршрута
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р4
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Полет на малой высоте c заданным превышением над максимальной точкой маршрута' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р4'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 22 Поиск наземных движущихся целей в секторе (+/-) 10 градусов
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р5
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе (+/-) 10 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р5'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - F/A-18F
-- состояние ЛА - 23 Поиск наземных движущихся целей в секторе +/- 30 градусов
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р6
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе (+/-) 30 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р6'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 24 Поиск наземных движущихся целей в секторе +/- 60 градусов
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р7
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе (+/-) 60 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р7'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - F/A-18F
-- состояние ЛА - 25 Сопровождение наземной движущейся цели
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р8
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Сопровождение наземной движущейся цели' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р8'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 26 Поиск ВЦ на большой дальности при использовании ВЧПИ
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р9
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Поиск ВЦ на большой дальности при использовании ВЧПИ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р9'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 27 Сопровождение ВЦ на большой дальности в режиме ВЧПИ
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р10
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Сопровождение ВЦ на большой дальности в режиме ВЧПИ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р10'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 28 Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 10 градусов
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р11
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 10 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р11'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 29 Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 30 градусов
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р12
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 30 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р12'
) -- номер режима работы БРЛС воздушного объекта	
),

-- самолет - F/A-18F
-- состояние ЛА - 30 Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 60 градусов
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р13
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 60 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р13'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 31 Сопровождение ВЦ в верхней полусфере на средней дальности в режиме СЧПИ
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р14
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Сопровождение ВЦ в верхней полусфере на средней дальности в режиме СЧПИ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р14'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 32 Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 10 градусов
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р15
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 10 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р15'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 33 Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 30 градусов
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р16
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 30 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р16'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 34 Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 60 градусов
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р17
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 60 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р17'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 35 Сопровождение ВЦ в нижней полусфере на средней дальности в режиме СЧПИ
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р18
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Сопровождение ВЦ в нижней полусфере на средней дальности в режиме СЧПИ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р18'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 36 Поиск и сопровождение ВЦ в ближней зоне на проходе
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р19
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в ближней зоне на проходе' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р19'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 37 Поиск ВЦ на малой дальности в секторе (+/-) 10 градусов
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р20
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Поиск ВЦ на малой дальности в секторе (+/-) 10 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р20'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F/A-18F
-- состояние ЛА - 38 Сопровождение ВЦ на малой дальности
-- БРЛС AN/APG-79
-- Режим работы БРЛС - Режим Р21
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F/A-18F'
and ac.condition_name_r like 'Сопровождение ВЦ на малой дальности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-79'
and dmar.abr_name_mode_r like 'Режим Р21'
) -- номер режима работы БРЛС воздушного объекта
);
-- */