﻿delete from re_situation.work_mode_in_fc;


insert into re_situation.work_mode_in_fc 
(
work_mode_in_fc_id, 
types_of_re_facilities_id, 
cat_of_radio_comm_signals_id
)
values 
( 
1 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-190(V)' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C1' )
),

(
2 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-210' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C2' )
),

(
3 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-210' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C6' )
),

(
4 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-211' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C1' )
),

(
5 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-215' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C2' )
),

(
6 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-215' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C3' )
),

(
7 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-215' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C4' )
),

(
8 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-215' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C5' )
),

(
9 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-215' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C6' )
),

(
10 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-215' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C7' )
),

(
11 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-215' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C8' )
),


(
12 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-186' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C2' )
),

(
13 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-186' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C6' )
),

(
14 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-181' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C9' )
),

(
15 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-164' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C2' )
),

(
16 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-164' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C3' )
),

(
17 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-164' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C4' )
),

(
18 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-164' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C6' )
),

(
19 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-194' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C1' )
),

(20 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-182' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C2' )
),

(
21 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-182' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C3' )
),

(
22 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-182' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C4' )
),

(23 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-182' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C5' )
),

(
24 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-182' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C6' )
),

(
25 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-192' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C7' )
),

(
26 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-192' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C8' )
),

(
27 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-193' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C4' )
),

(
28 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/URS-107' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C9' )
),

(
29 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-222' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C2' )
),

(
30 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-222' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C3' )
),

(
31 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-222' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C6' )
),

(
32 ,
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r like 'AN/ARC-201' ),
(select cat_of_radio_comm_signals_id from re_situation.cat_of_radio_comm_signals where abr_name_category_r like 'C5' )
);

-- */


select setval('re_situation.work_mode_in_fc_work_mode_in_fc_id_seq', 32);