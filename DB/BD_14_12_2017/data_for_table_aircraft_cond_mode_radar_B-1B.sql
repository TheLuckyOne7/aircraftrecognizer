﻿-- вспомогательные запросы 
/*
select ac.aircraft_condition_id, ac.condition_name_r, ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' -- 'F/A-18F' --  'F-16D' 
-- and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе (+/-) 10 градусов' 
-- */

/*
select d_m_airborne_radar_id, dmar.abr_name_mode_r, f.name_facilities_e
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APQ-164'
-- and dmar.abr_name_mode_r like 'Режим Р1'
-- */



-- Вставка данных 

insert into re_situation.aircraft_cond_mode_radar
(
  aircraft_condition_id , -- номер состояния воздушного объекта 
  d_m_airborne_radar_id  -- номер режима работы БРЛС воздушного объекта
)

values 
-------------------------------
-- самолет - B-1B B-1B
-- состояние ЛА - 39 Навигация по радиолокационным ориентирам
-- БРЛС AN/APQ-164 "AN/APQ-164"
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'B-1B'
and ac.condition_name_r like 'Навигация по радиолокационным ориентирам' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APQ-164'
and dmar.abr_name_mode_r like 'Режим Р1'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - B-1B
-- состояние ЛА - 40 Разведка погоды на маршруте полета
-- БРЛС AN/APQ-164
-- Режим работы БРЛС - Режим Р2
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'B-1B'
and ac.condition_name_r like 'Разведка погоды на маршруте полета' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APQ-164'
and dmar.abr_name_mode_r like 'Режим Р2'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - B-1B
-- состояние ЛА 41 Работа с воздушным маяком-ответчиком топливозаправщика
-- БРЛС AN/APQ-164
-- Режим работы БРЛС - Режим Р3
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'B-1B'
and ac.condition_name_r like 'Работа с воздушным маяком-ответчиком топливозаправщика' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APQ-164'
and dmar.abr_name_mode_r like 'Режим Р3'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - B-1B
-- состояние ЛА - 42 Встреча с топливозаправщиком в зоне дозаправки
-- БРЛС AN/APQ-164
-- Режим работы БРЛС - Режим Р4
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'B-1B'
and ac.condition_name_r like 'Встреча с топливозаправщиком в зоне дозаправки' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APQ-164'
and dmar.abr_name_mode_r like 'Режим Р4'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - B-1B
-- состояние ЛА - 43 Маловысотный полет (преодоление системы ПВО)
-- БРЛС AN/APQ-164
-- Режим работы БРЛС - Режим Р5
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'B-1B'
and ac.condition_name_r like 'Маловысотный полет (преодоление системы ПВО)' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APQ-164'
and dmar.abr_name_mode_r like 'Режим Р5'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - B-1B
-- состояние ЛА - 44 Коррекция ИНС
-- БРЛС AN/APQ-164
-- Режим работы БРЛС - Режим Р6
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'B-1B'
and ac.condition_name_r like 'Коррекция ИНС' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APQ-164'
and dmar.abr_name_mode_r like 'Режим Р6'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - B-1B
-- состояние ЛА - 45 Поиск наземных объектов
-- БРЛС AN/APQ-164
-- Режим работы БРЛС - Режим Р7
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'B-1B'
and ac.condition_name_r like 'Поиск наземных объектов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APQ-164'
and dmar.abr_name_mode_r like 'Режим Р7'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - B-1B
-- состояние ЛА - 46 Сопровождение наземных объектов
-- БРЛС AN/APQ-164
-- Режим работы БРЛС - Режим Р8
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'B-1B'
and ac.condition_name_r like 'Сопровождение наземных объектов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APQ-164'
and dmar.abr_name_mode_r like 'Режим Р8'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - B-1B
-- состояние ЛА - 47 Прицеливание по наземному объекту
-- БРЛС AN/APQ-164
-- Режим работы БРЛС - Режим Р9
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'B-1B'
and ac.condition_name_r like 'Прицеливание по наземному объекту' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APQ-164'
and dmar.abr_name_mode_r like 'Режим Р9'
) -- номер режима работы БРЛС воздушного объекта
);

