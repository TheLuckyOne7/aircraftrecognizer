﻿/*
delete from re_situation.aircraft_condition;
select setval('re_situation.aircraft_condition_aircraft_condition_id_seq', 1);
-- */

insert into re_situation.aircraft_condition
(
  aircraft_condition_id , -- искуственный первичный ключ 
  type_of_aircraft_id, -- тип объекта
  condition_name_r , -- название состояния объекта на русском 
  condition_name_e ,  -- название состояния объекта на английском 
  nsr_esrc_id -- номер состояния в базе данных прохорова 
   
)

values 

---------------------------------------
-- F-16D
-- 1) навигация по радиолокационным маякам 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Навигация по радиолокационным маякам',
null,
31 -- номер состояния в базе данных прохорова 
), 
-- 2) навигация по радиолокационным ориентирам
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Навигация по радиолокационным ориентирам',
null, 32
), 

-- 3) полет на малой высоте c огибанием рельефа местности
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Полет на малой высоте c огибанием рельефа местности',
null,
33
), 

-- 4) полет на малой высоте c заданным превышением над максимальной точкой маршрута
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Полет на малой высоте c заданным превышением над максимальной точкой маршрута',
null,
34
), 

-- 5) поиск наземных движущихся целей в секторе 10о
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Поиск наземных движущихся целей в секторе +/- 10 градусов',
null,
35
), 

-- 6) поиск наземных движущихся целей в секторе 30о
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Поиск наземных движущихся целей в секторе +/- 30 градусов',
null,
36
), 

-- 7) поиск наземных движущихся целей в секторе 60о
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Поиск наземных движущихся целей в секторе +/- 60 градусов',
null,
37
), 

-- 8) сопровождение наземной движущейся цели 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Сопровождение наземной движущейся цели',
null,
38
), 

-- 9) поиск ВЦ по скорости на большой дальности
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Поиск ВЦ по скорости на большой дальности',
null,
39
), 

-- 10) сопровождение ВЦ по скорости на большой дальности 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Сопровождение ВЦ по скорости на большой дальности',
null,
40
), 

-- 11) поиск ВЦ в верхней полусфере на средней дальности 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Поиск ВЦ в верхней полусфере на средней дальности',
null, 
41
), 

-- 12) сопровождение ВЦ в верхней полусфере на средней дальности 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Сопровождение ВЦ в верхней полусфере на средней дальности',
null, 
42
), 

-- 13) поиск ВЦ в нижней полусфере на средней дальности 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Поиск ВЦ в нижней полусфере на средней дальности',
null,
43
), 

-- 14) сопровождение ВЦ в нижней полусфере на средней дальности
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Сопровождение ВЦ в нижней полусфере на средней дальности',
null, 
44
), 

-- 15) поиск и сопровождение ВЦ на проходе
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Поиск и сопровождение ВЦ на проходе',
null, 45
), 

-- 16) поиск ВЦ на малой дальности
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Поиск ВЦ на малой дальности',
null, 46
), 

-- 17) сопровождение ВЦ на малой дальности 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F-16D'), -- тип объекта
'Сопровождение ВЦ на малой дальности',
null, 47
), 

-------------------------------------------
-- F/A-18F
-- 1) навигация по радиолокационным маякам 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Навигация по радиолокационным маякам',
null, 1
), 

-- 2) навигация по радиолокационным ориентирам
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Навигация по радиолокационным ориентирам',
null, 2
), 

-- 3) полет на малой высоте c огибанием рельефа местности
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Полет на малой высоте c огибанием рельефа местности',
null, 3
), 

-- 4) полет на малой высоте c заданным превышением над максимальной точкой маршрута
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Полет на малой высоте c заданным превышением над максимальной точкой маршрута',
null, 4
), 

-- 5) поиск наземных движущихся целей в секторе 10о
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Поиск наземных движущихся целей в секторе (+/-) 10 градусов',
null, 5
), 

-- 6) поиск наземных движущихся целей в секторе 30о
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Поиск наземных движущихся целей в секторе (+/-) 30 градусов',
null, 6
), 

-- 7) поиск наземных движущихся целей в секторе 60о
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Поиск наземных движущихся целей в секторе (+/-) 60 градусов',
null, 7
), 

-- 8) сопровождение наземной движущейся цели 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Сопровождение наземной движущейся цели',
null, 8
), 

-- 9) поиск ВЦ на большой дальности при использовании ВЧПИ
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Поиск ВЦ на большой дальности при использовании ВЧПИ',
null, 9
), 

-- 10) сопровождение ВЦ на большой дальности в режиме ВЧПИ
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Сопровождение ВЦ на большой дальности в режиме ВЧПИ',
null, 10
), 

-- 11) поиск ВЦ в верхней полусфере на средней дальности в секторе  10о 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 10 градусов',
null, 11
), 

-- 12) поиск ВЦ в верхней полусфере на средней дальности в секторе 30о 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 30 градусов',
null, 12
), 

-- 13) поиск ВЦ в верхней полусфере на средней дальности в секторе 60о 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 60 градусов',
null, 13
), 

-- 14) сопровождение ВЦ в верхней полусфере на средней дальности в режиме СЧПИ;
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Сопровождение ВЦ в верхней полусфере на средней дальности в режиме СЧПИ',
null, 14
), 

-- 15) поиск ВЦ в нижней полусфере на средней дальности в секторе 10о 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 10 градусов',
null, 15
), 

-- 16) поиск ВЦ в нижней полусфере на средней дальности в секторе 30о 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 30 градусов',
null, 16
), 

-- 17) поиск ВЦ в нижней полусфере на средней дальности в секторе 60о 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 60 градусов',
null, 17
), 

-- 18) сопровождение ВЦ в нижней полусфере на средней дальности в режиме СЧПИ;
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Сопровождение ВЦ в нижней полусфере на средней дальности в режиме СЧПИ',
null, 18
), 

-- 19) поиск и сопровождение ВЦ в ближней зоне на проходе
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Поиск и сопровождение ВЦ в ближней зоне на проходе',
null, 19
), 

-- 20) поиск ВЦ на малой дальности в секторе 10о
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Поиск ВЦ на малой дальности в секторе (+/-) 10 градусов',
null, 20
), 

-- 21) сопровождение ВЦ на МД 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'F/A-18F'), -- тип объекта
'Сопровождение ВЦ на малой дальности',
null, 21
),

-------------------------------------------
-- B-1B
-- 1 навигация по радиолокационным ориентирам
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'B-1B'), -- тип объекта
'Навигация по радиолокационным ориентирам',
null, 51
), 

-- 2 разведка погоды на маршруте полета
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'B-1B'), -- тип объекта
'Разведка погоды на маршруте полета',
null, 52
), 

-- 3 работа с воздушным маяком-ответчиком топливозаправщика
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'B-1B'), -- тип объекта
'Работа с воздушным маяком-ответчиком топливозаправщика',
null, 53
), 

-- 4 встреча с топливозаправщиком в зоне дозаправки
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'B-1B'), -- тип объекта
'Маловысотный полет (преодоление системы ПВО)',
null, 54
), 

-- 5 маловысотный полет (преодоление системы ПВО)
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'B-1B'), -- тип объекта
'Встреча с топливозаправщиком в зоне дозаправки',
null, 55
), 

-- 6 коррекция ИНС
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'B-1B'), -- тип объекта
'Коррекция ИНС',
null, 56
), 

-- 7 поиск наземных объектов 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'B-1B'), -- тип объекта
'Поиск наземных объектов',
null, 57
), 

-- 8 сопровождение наземных объектов
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'B-1B'), -- тип объекта
'Сопровождение наземных объектов',
null, 58
), 

-- 9 прицеливание по наземному объекту
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'B-1B'), -- тип объекта
'Прицеливание по наземному объекту',
null, 59
), 

-------------------------------------------
-- KC-135
-- 1.Навигация по радиолокационным маякам
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'KC-135'), -- тип объекта
'Навигация по радиолокационным маякам',
null, 131
), 

-- 2. Навигация по радиолокационным ориентирам
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'KC-135'), -- тип объекта
'Навигация по радиолокационным ориентирам',
null, 132
), 

-- 3.Разведка погоды на маршруте полета
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'KC-135'), -- тип объекта
'Разведка погоды на маршруте полета',
null, 133
), 
-- 4.	Картографирование зоны поиска реальным лучом в режиме кругового обзора
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'KC-135'), -- тип объекта
'Картографирование зоны поиска реальным лучом в режиме кругового обзора',
null, 134
), 
-- 5.	Картографирование зоны поиска в режиме РСА (коррекция ИНС)
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'KC-135'), -- тип объекта
'Картографирование зоны поиска в режиме РСА (коррекция ИНС)',
null, 135
), 
-- 6.	Поиск воздушной цели
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'KC-135'), -- тип объекта
'Поиск воздушной цели',
null, 136
), 
-- 7.	Сопровождение воздушной цели
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'KC-135'), -- тип объекта
'Сопровождение воздушной цели',
null, 137
), 
-- 8.	Встреча с заправляемым самолетом
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'KC-135'), -- тип объекта
'Встреча с заправляемым самолетом',
null, 138
), 

-------------------------------------------
-- E-3C
-- 101.	Поиск и сопровождение ВЦ в ИР1 над горизонтом
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Поиск и сопровождение ВЦ в ИР1 над горизонтом',
null, 101
), 
-- 102.	Поиск и сопровождение ВЦ в КР1 над горизонтом
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Поиск и сопровождение ВЦ в КР1 над горизонтом',
null, 102
),
-- 103.	Поиск и сопровождение ВЦ в ИДР1 без измерения НЦ
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Поиск и сопровождение ВЦ в ИДР1 без измерения НЦ',
null, 103
),
-- 104.	Поиск и сопровождение ВЦ в КР2 без измерения НЦ
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Поиск и сопровождение ВЦ в КР2 без измерения НЦ',
null, 104
),
-- 105.	Поиск и сопровождение ВЦ в ИДР2 с измерением НЦ
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Поиск и сопровождение ВЦ в ИДР2 с измерением НЦ',
null, 105
),
-- 106.	Поиск и сопровождение ВЦ в КР3 с измерением НЦ
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Поиск и сопровождение ВЦ в КР3 с измерением НЦ',
null, 106
),
-- 107.	Поиск и сопровождение НВЦ в ИР2 (морском режиме)
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Поиск и сопровождение НВЦ в ИР2 (морском режиме)',
null, 107
),
-- 108.	Поиск и сопровождение НВЦ в КР4 на фоне морской поверхности
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Поиск и сопровождение НВЦ в КР4 на фоне морской поверхности',
null, 108
),
-- 109.	Оповещение о ВО по результатам поиска в ИР1 над горизонтом
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Оповещение о ВО по результатам поиска в ИР1 над горизонтом',
null, 109
),
-- 110.	Оповещение о ВО по результатам поиска в КР1 над горизонтом 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Оповещение о ВО по результатам поиска в КР1 над горизонтом',
null, 110
),
-- 111.	Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ',
null, 111
),
-- 112.	Оповещение о ВО по результатам поиска в КР2 без измерения НЦ
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Оповещение о ВО по результатам поиска в КР2 без измерения НЦ',
null, 112
),
-- 113.	Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ',
null, 113
),
-- 114.	Оповещение о ВО по результатам поиска в КР3 с измерением НЦ
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Оповещение о ВО по результатам поиска в КР3 с измерением НЦ',
null, 114
),
-- 115.	Оповещение о НВО по результатам поиска в ИР2 (морской  режим)
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)',
null, 115
),
-- 116.	Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.',
null, 116
),
-- 117.	Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом',
null, 117
),
-- 118.	Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ',
null, 118
),
-- 119.	Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ',
null, 119
),
-- 120.	Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме) 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)',
null, 120
),
-- 121.	Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-3C'), -- тип объекта
'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП',
null, 121
),

-------------------------------------------
-- E-2C
-- 71.	Поиск ВЦ над горизонтом
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Поиск ВЦ над горизонтом',
null, 71
), 

-- 72.	Поиск надводных целей
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Поиск надводных целей',
null, 72
), 

-- 73.	Поиск ВЦ на фоне земной поверхности
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Поиск ВЦ на фоне земной поверхности',
null, 73
), 

-- 74.	Поиск ВЦ на фоне морской поверхности
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Поиск ВЦ на фоне морской поверхности',
null, 74
),

-- 75.	Поиск целей в комбинированном режиме
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Поиск целей в комбинированном режиме',
null, 75
),
-- 76.	Оповещение о ВО над горизонтом
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Оповещение о ВО над горизонтом',
null, 76
),
-- 77.	Оповещение о НВО по результатам поиска НвЦ
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Оповещение о НВО по результатам поиска НвЦ',
null, 77
),
-- 78.	Оповещение о ВО по результатам поиска ВЦ на фоне земн. поверхн.
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Оповещение о ВО по результатам поиска ВЦ на фоне земн. поверхн.',
null, 78
),
-- 79.	Оповещение о НВО по результатам поиска ВЦ на фоне морской поверхности 
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Оповещение о НВО по результатам поиска ВЦ на фоне морской поверхности',
null, 79
),
-- 80.	Оповещение о ВО по результатам поиска целей в комбинир. режиме.
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Оповещение о ВО по результатам поиска целей в комбинир. режиме.',
null, 80
),
-- 81.	Сопровождение ВЦ и наведение истребителей на ВЦ над горизонтом
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Сопровождение ВЦ и наведение истребителей на ВЦ над горизонтом',
null, 81
),
-- 82.	Сопровождение надводных целей и наведение штурмовиков на надводные цели
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Сопровождение надводных целей и наведение штурмовиков на надводные цели',
null, 82
),
-- 83.	Сопровождение и наведение истребителей на цели на фоне земной поверхности
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Сопровождение и наведение истребителей на цели на фоне земной поверхности',
null, 83
),
-- 84.	Сопровождение и наведение истребителей на цели на фоне морской поверхности
(
default, -- искуственный первичный ключ 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r like 'E-2C'), -- тип объекта
'Сопровождение и наведение истребителей на цели на фоне морской поверхности',
null, 84
);

