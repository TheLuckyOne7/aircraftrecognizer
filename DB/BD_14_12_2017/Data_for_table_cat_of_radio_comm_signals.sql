﻿delete from re_situation.cat_of_radio_comm_signals ;

insert into re_situation.cat_of_radio_comm_signals 
(
cat_of_radio_comm_signals_id ,-- Искуственный первичный ключ
name_category_r , -- название класса излучения руский  
abr_name_category_r , -- сокращенное название класса излучения Русский 
name_category_e , -- название класса излучения English
abr_name_category_e ,  -- сокращенное название класса излучения English 
description_r , -- Описание русский 
description_e  -- Описание Englich
)
values 
(
1,-- Искуственный первичный ключ
'категория С1', -- название класса излучения руский  
'C1' , -- сокращенное название класса излучения Русский 
'C1 category' , -- название класса излучения English
'C1' ,  -- сокращенное название класса излучения English 
null , -- Описание русский 
null  -- Описание Englich
),

(
2,-- Искуственный первичный ключ
'категория С2', -- название класса излучения руский  
'C2' , -- сокращенное название класса излучения Русский 
'C2 category' , -- название класса излучения English
'C2' ,  -- сокращенное название класса излучения English 
null , -- Описание русский 
null  -- Описание Englich
),

(
3,-- Искуственный первичный ключ
'категория С3', -- название класса излучения руский  
'C3' , -- сокращенное название класса излучения Русский 
'C3 category' , -- название класса излучения English
'C3' ,  -- сокращенное название класса излучения English 
null , -- Описание русский 
null  -- Описание Englich
),

(
4,-- Искуственный первичный ключ
'категория С4', -- название класса излучения руский  
'C4' , -- сокращенное название класса излучения Русский 
'C4 category' , -- название класса излучения English
'C4' ,  -- сокращенное название класса излучения English 
null , -- Описание русский 
null  -- Описание Englich
),

(
5,-- Искуственный первичный ключ
'категория С5', -- название класса излучения руский  
'C5' , -- сокращенное название класса излучения Русский 
'C5 category' , -- название класса излучения English
'C5' ,  -- сокращенное название класса излучения English 
null , -- Описание русский 
null  -- Описание Englich
),

(
6,-- Искуственный первичный ключ
'категория С6', -- название класса излучения руский  
'C6' , -- сокращенное название класса излучения Русский 
'C6 category' , -- название класса излучения English
'C6' ,  -- сокращенное название класса излучения English 
null , -- Описание русский 
null  -- Описание Englich
),

(
7,-- Искуственный первичный ключ
'категория С7', -- название класса излучения руский  
'C7' , -- сокращенное название класса излучения Русский 
'C7 category' , -- название класса излучения English
'C7' ,  -- сокращенное название класса излучения English 
null , -- Описание русский 
null  -- Описание Englich
),

(
8,-- Искуственный первичный ключ
'категория С8', -- название класса излучения руский  
'C8' , -- сокращенное название класса излучения Русский 
'C8 category' , -- название класса излучения English
'C8' ,  -- сокращенное название класса излучения English 
null , -- Описание русский 
null  -- Описание Englich
),


(
9,-- Искуственный первичный ключ
'категория С9', -- название класса излучения руский  
'C9' , -- сокращенное название класса излучения Русский 
'C9 category' , -- название класса излучения English
'C9' ,  -- сокращенное название класса излучения English 
null , -- Описание русский 
null  -- Описание Englich
);

select setval ('re_situation.cat_of_radio_comm_signals_cat_of_radio_comm_signals_id_seq', 10);