﻿-- вспомогательные запросы 
/*
select ac.aircraft_condition_id, ac.condition_name_r, ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
-- and ac.condition_name_r like 'Полет в зону дозаправки' 
-- */

/*
select d_m_airborne_radar_id, dmar.abr_name_mode_r, f.name_facilities_e
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APN-59'
-- and dmar.abr_name_mode_r like 'Режим Р1'
-- */


-- Вставка данных 

insert into re_situation.aircraft_cond_mode_radar
(
  aircraft_condition_id , -- номер состояния воздушного объекта 
  d_m_airborne_radar_id  -- номер режима работы БРЛС воздушного объекта
)

values 
-------------------------------
-- самолет - KC-135 KC-135
-- состояние ЛА - 48 Навигация по радиолокационным маякам
-- БРЛС AN/APN-59 "AN/APN-59"
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'KC-135'
and ac.condition_name_r like 'Навигация по радиолокационным маякам' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APN-59'
and dmar.abr_name_mode_r like 'Режим Р1'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - KC-135
-- состояние ЛА - 49 Навигация по радиолокационным ориентирам
-- БРЛС AN/APN-59
-- Режим работы БРЛС - Режим Р2
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'KC-135'
and ac.condition_name_r like 'Навигация по радиолокационным ориентирам' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APN-59'
and dmar.abr_name_mode_r like 'Режим Р2'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - KC-135
-- состояние ЛА 50;"Разведка погоды на маршруте полета"
-- БРЛС AN/APN-59
-- Режим работы БРЛС - Режим Р3
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'KC-135'
and ac.condition_name_r like 'Разведка погоды на маршруте полета' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APN-59'
and dmar.abr_name_mode_r like 'Режим Р3'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - KC-135
-- состояние ЛА - 51;"Картографирование зоны поиска реальным лучом в режиме кругового обзора"
-- БРЛС AN/APN-59
-- Режим работы БРЛС - Режим Р4
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'KC-135'
and ac.condition_name_r like 'Картографирование зоны поиска реальным лучом в режиме кругового обзора' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APN-59'
and dmar.abr_name_mode_r like 'Режим Р4'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - KC-135
-- состояние ЛА - 52;"Картографирование зоны поиска в режиме РСА (коррекция ИНС)"
-- БРЛС AN/APN-59
-- Режим работы БРЛС - Режим Р5
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'KC-135'
and ac.condition_name_r like 'Картографирование зоны поиска в режиме РСА (коррекция ИНС)' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APN-59'
and dmar.abr_name_mode_r like 'Режим Р5'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - KC-135
-- состояние ЛА - 53;"Поиск воздушной цели"
-- БРЛС AN/APN-59
-- Режим работы БРЛС - Режим Р6
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'KC-135'
and ac.condition_name_r like 'Поиск воздушной цели' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APN-59'
and dmar.abr_name_mode_r like 'Режим Р6'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - KC-135
-- состояние ЛА - 54;"Сопровождение воздушной цели"
-- БРЛС AN/APN-59
-- Режим работы БРЛС - Режим Р7
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'KC-135'
and ac.condition_name_r like 'Сопровождение воздушной цели' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APN-59'
and dmar.abr_name_mode_r like 'Режим Р7'
) -- номер режима работы БРЛС воздушного объекта
);  
  