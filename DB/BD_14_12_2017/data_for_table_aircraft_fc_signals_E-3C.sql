﻿-- вспомогательные запросы 
-- Запрос: выбрать все состояния для заданного типа самолета 
/*
select ac.aircraft_condition_id, ac.condition_name_r, ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
-- and ac.condition_name_r like 'Поиск и сопровождение воздушных (надводных) целей'
-- */

-- РЭС в списке средств, имеющихся на ЛА 
/*
select placing_f_t_aircraft_id , tf.name_facilities_e, pa.code_name 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
-- and tf.name_facilities_e like 'AN/ARC-194'
-- and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N1'
-- */

-- Категории сигналов в средстве связи 
/*
select tf.name_facilities_e, cs.name_category_e
from re_situation.types_of_re_facilities as tf, re_situation.work_mode_in_fc as wm_fc,
re_situation.cat_of_radio_comm_signals as cs 
where tf.types_of_re_facilities_id=wm_fc.types_of_re_facilities_id
and cs.cat_of_radio_comm_signals_id=wm_fc.cat_of_radio_comm_signals_id
order by tf.name_facilities_e

-- */ 

-- Вставка данных 

insert into re_situation.aircraft_fc_signals
(
  aircraft_condition_id , -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition
  placing_f_t_aircraft_id , -- номер средства в списке средств имеющихся на ЛА 
  cat_of_radio_comm_signals_id -- номер категории сигнала связи
)
values 
-- самолет E-3C
-- состояние 56;"Поиск и сопровождение ВЦ в ИР1 над горизонтом"
-- средство саязи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА р.ст. AN/ARC-194 на л.а. E-3C N1
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 57;"Поиск и сопровождение ВЦ в КР1 над горизонтом"
-- средство саязи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА р.ст. AN/ARC-194 на л.а. E-3C N1
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в КР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 58;"Поиск и сопровождение ВЦ в ИДР1 без измерения НЦ"
-- средство саязи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА р.ст. AN/ARC-194 на л.а. E-3C N1
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
), 

-- самолет E-3C
-- состояние 59;"Поиск и сопровождение ВЦ в КР2 без измерения НЦ"
-- средство связи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА р.ст. AN/ARC-194 на л.а. E-3C N1
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в КР2 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 60;"Поиск и сопровождение ВЦ в ИДР2 с измерением НЦ"
-- средство связи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА р.ст. AN/ARC-194 на л.а. E-3C N1
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 61;"Поиск и сопровождение ВЦ в КР3 с измерением НЦ"
-- средство связи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА р.ст. AN/ARC-194 на л.а. E-3C N1
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в КР3 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 62;"Поиск и сопровождение НВЦ в ИР2 (морском режиме)"
-- средство связи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА р.ст. AN/ARC-194 на л.а. E-3C N1
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение НВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 63;"Поиск и сопровождение НВЦ в КР4 на фоне морской поверхности"
-- средство связи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА р.ст. AN/ARC-194 на л.а. E-3C N1
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение НВЦ в КР4 на фоне морской поверхности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-----------------------------------------
-- самолет E-3C
-- состояние 64;"Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- средство связи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-194 на л.а. E-3C N2"
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 65;"Оповещение о ВО по результатам поиска в КР1 над горизонтом"
-- средство связи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-194 на л.а. E-3C N2"
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 66;"Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ"
-- средство связи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-194 на л.а. E-3C N2"
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 67;"Оповещение о ВО по результатам поиска в КР2 без измерения НЦ"
-- средство связи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-194 на л.а. E-3C N2"
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР2 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 68;"Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ"
-- средство связи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-194 на л.а. E-3C N2"
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 69;"Оповещение о ВО по результатам поиска в КР3 с измерением НЦ"
-- средство связи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-194 на л.а. E-3C N2"
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР3 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 64;"Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- средство саязи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-194 на л.а. E-3C N3"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 65;"Оповещение о ВО по результатам поиска в КР1 над горизонтом"
-- средство саязи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-194 на л.а. E-3C N3"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 66;"Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ"
-- средство саязи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-194 на л.а. E-3C N3"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 67;"Оповещение о ВО по результатам поиска в КР2 без измерения НЦ"
-- средство саязи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-194 на л.а. E-3C N3"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР2 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 68;"Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ"
-- средство саязи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-194 на л.а. E-3C N3"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 69;"Оповещение о ВО по результатам поиска в КР3 с измерением НЦ"
-- средство саязи AN/ARC-194
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-194 на л.а. E-3C N3"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР3 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-194'
and pa.code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 56;"Поиск и сопровождение ВЦ в ИР1 над горизонтом"
-- средство саязи AN/ARC-193
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-3C"
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-3C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 57;"Поиск и сопровождение ВЦ в КР1 над горизонтом"
-- средство саязи AN/ARC-193
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-3C"
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в КР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-3C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 58;"Поиск и сопровождение ВЦ в ИДР1 без измерения НЦ"
-- средство саязи AN/ARC-193
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-3C"
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-3C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
), 

-- самолет E-3C
-- состояние 59;"Поиск и сопровождение ВЦ в КР2 без измерения НЦ"
-- средство связи AN/ARC-193
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-3C"
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в КР2 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-3C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 60;"Поиск и сопровождение ВЦ в ИДР2 с измерением НЦ"
-- средство связи AN/ARC-193
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-3C"
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-3C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 61;"Поиск и сопровождение ВЦ в КР3 с измерением НЦ"
-- средство связи AN/ARC-193
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-3C"
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в КР3 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-3C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 62;"Поиск и сопровождение НВЦ в ИР2 (морском режиме)"
-- средство связи AN/ARC-193
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-3C"
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение НВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-3C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 63;"Поиск и сопровождение НВЦ в КР4 на фоне морской поверхности"
-- средство связи AN/ARC-193
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-3C"
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Поиск и сопровождение НВЦ в КР4 на фоне морской поверхности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-3C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 64;"Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N1"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 65;"Оповещение о ВО по результатам поиска в КР1 над горизонтом"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N1"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 66;"Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N1"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 67;"Оповещение о ВО по результатам поиска в КР2 без измерения НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N1"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР2 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 68;"Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N1"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 69;"Оповещение о ВО по результатам поиска в КР3 с измерением НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N1"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР3 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),
--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 64;"Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N2"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 65;"Оповещение о ВО по результатам поиска в КР1 над горизонтом"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N2"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 66;"Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N2"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 67;"Оповещение о ВО по результатам поиска в КР2 без измерения НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N2"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР2 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 68;"Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N2"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 69;"Оповещение о ВО по результатам поиска в КР3 с измерением НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N2"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР3 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 64;"Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N3"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 65;"Оповещение о ВО по результатам поиска в КР1 над горизонтом"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N3"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 66;"Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N3"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 67;"Оповещение о ВО по результатам поиска в КР2 без измерения НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N3"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР2 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 68;"Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N3"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 69;"Оповещение о ВО по результатам поиска в КР3 с измерением НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N3"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР3 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 64;"Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N4"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 65;"Оповещение о ВО по результатам поиска в КР1 над горизонтом"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N4"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 66;"Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N4"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 67;"Оповещение о ВО по результатам поиска в КР2 без измерения НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N4"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР2 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 68;"Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N4"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 69;"Оповещение о ВО по результатам поиска в КР3 с измерением НЦ"
-- средство саязи AN/ARC-192
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-192 на л.а. E-3С N4"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР3 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-192'
and pa.code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N1"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N1"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N1"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N1"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N1"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N1"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N1"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N1'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N2"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N2"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N2"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N2"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N2"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N2"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N2"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N2'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N3"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N3"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N3"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N3"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N3"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N3"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N3"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N3'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N4"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N4"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N4"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N4"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N4"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N4"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N4"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N4'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N5"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N5'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N5"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N5'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N5"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N5'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N5"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N5'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N5"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N5'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N5"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N5'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N5"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N5'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N6"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N6'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N6"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N6'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N6"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N6'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N6"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N6'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N6"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N6'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N6"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N6'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N6"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N6'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N7"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N7'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N7"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N7'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N7"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N7'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N7"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N7'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N7"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N7'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N7"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N7'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N7"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N7'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N8"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N8'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N8"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N8'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N8"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N8'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N8"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N8'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N8"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N8'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N8"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N8'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N8"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N8'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N9"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N9'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N9"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N9'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N9"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N9'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N9"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N9'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N9"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N9'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N9"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N9'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N9"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N9'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N10"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N10'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N10"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N10'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N10"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N10'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N10"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N10'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N10"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N10'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N10"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N10'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N10"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N10'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N11"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N11'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N11"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N11'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N11"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N11'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N11"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N11'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N11"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N11'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N11"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N11'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N11"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N11'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N12"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N12'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N12"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N12'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N12"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N12'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N12"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N12'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N12"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N12'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N12"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N12'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N12"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N12'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
), 

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N13"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N13'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N13"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N13'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N13"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N13'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N13"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N13'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N13"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N13'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N13"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N13'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N13"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N13'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N14"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N14'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N14"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N14'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N14"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N14'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 73;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N14"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N14'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N14"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N14'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N14"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N14'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- средство саязи "AN/ARC-186"
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-186 на л.а. E-3С N14"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/ARC-186'
and pa.code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N14'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет E-3C
-- состояние 64;"Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- средство саязи AN/URS-107
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/URS-107 на л.а. E-3С"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. E-3С'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 65;"Оповещение о ВО по результатам поиска в КР1 над горизонтом"
-- средство саязи AN/URS-107
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/URS-107 на л.а. E-3С N4"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР1 над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. E-3С'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 66;"Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ"
-- средство саязи AN/URS-107
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/URS-107 на л.а. E-3С N4"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. E-3С'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 67;"Оповещение о ВО по результатам поиска в КР2 без измерения НЦ"
-- средство саязи AN/URS-107
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/URS-107 на л.а. E-3С N4"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР2 без измерения НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. E-3С'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 68;"Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ"
-- средство саязи AN/URS-107
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/URS-107 на л.а. E-3С N4"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. E-3С'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет E-3C
-- состояние 69;"Оповещение о ВО по результатам поиска в КР3 с измерением НЦ"
-- средство саязи AN/URS-107
-- имя РЭС в списке РЭС имеющихся на ЛА "р.ст. AN/URS-107 на л.а. E-3С N4"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР3 с измерением НЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id   
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-3C' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. E-3С'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
);





--------------------------------------------------------------------------
