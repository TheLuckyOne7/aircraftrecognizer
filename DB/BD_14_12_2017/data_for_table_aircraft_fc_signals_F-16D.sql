﻿-- вспомогательные запросы 
-- Запрос: выбрать все состояния для заданного типа самолета 
/*
select ac.aircraft_condition_id, ac.condition_name_r, ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
-- and ac.condition_name_r like 'Поиск и сопровождение воздушных (надводных) целей'
-- */

--типы РЭС на ЛА 
/*
sselect placing_f_t_aircraft_id,  ta.code_name_e, tf.name_facilities_e, pa.code_name 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-222'
and pa.code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
-- */

-- Категории сигналов в средстве связи 
/*
select tf.name_facilities_e, cs.name_category_e
from re_situation.types_of_re_facilities as tf, re_situation.work_mode_in_fc as wm_fc,
re_situation.cat_of_radio_comm_signals as cs 
where tf.types_of_re_facilities_id=wm_fc.types_of_re_facilities_id
and cs.cat_of_radio_comm_signals_id=wm_fc.cat_of_radio_comm_signals_id
order by tf.name_facilities_e

-- */ 

-- Вставка данных 

insert into re_situation.aircraft_fc_signals
(
  aircraft_condition_id , -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition
  placing_f_t_aircraft_id , -- номер средства связи в списке средств связи на самолете F-16D  
  cat_of_radio_comm_signals_id -- номер категории сигнала связи
)
values 
-- самолет F-16D
-- состояние 1;"Навигация по радиолокационным маякам"
-- средство саязи AN/ARC-222
-- условное обозначение средства связи на самолете 'р.ст. AN/ARC-222 на л.а. F-16D'
-- категория сигналла 'C3 category'

(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Навигация по радиолокационным маякам'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-222'
and pa.code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 3;"Полет на малой высоте c огибанием рельефа местности"
-- средство саязи AN/ARC-222
-- условное обозначение средства связи на самолете 'р.ст. AN/ARC-222 на л.а. F-16D'
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Полет на малой высоте c огибанием рельефа местности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-222'
and pa.code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 4;"Полет на малой высоте c заданным превышением над максимальной точкой маршрута"
-- средство саязи AN/ARC-222
-- условное обозначение средства связи на самолете 'р.ст. AN/ARC-222 на л.а. F-16D'
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 
'Полет на малой высоте c заданным превышением над максимальной точкой маршрута'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-222'
and pa.code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 2;"Навигация по радиолокационным ориентирам"
-- средство саязи AN/ARC-222
-- условное обозначение средства связи на самолете 'р.ст. AN/ARC-222 на л.а. F-16D'
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Навигация по радиолокационным ориентирам'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-222'
and pa.code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
), 

-- самолет F-16D
-- состояние 3;"Полет на малой высоте c огибанием рельефа местности"
-- средство связи AN/ARC-222
-- условное обозначение средства связи на самолете 'р.ст. AN/ARC-222 на л.а. F-16D'
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Полет на малой высоте c огибанием рельефа местности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-222'
and pa.code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 8;"Сопровождение наземной движущейся цели"
-- средство связи AN/ARC-222
-- условное обозначение средства связи на самолете 'р.ст. AN/ARC-222 на л.а. F-16D'
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Сопровождение наземной движущейся цели'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-222'
and pa.code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 10;"Сопровождение ВЦ по скорости на большой дальности"
-- средство связи AN/ARC-222
-- условное обозначение средства связи на самолете 'р.ст. AN/ARC-222 на л.а. F-16D'
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Сопровождение ВЦ по скорости на большой дальности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-222'
and pa.code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-----------------------------------------------
-- самолет F-16D
-- состояние 2;"Навигация по радиолокационным ориентирам"
-- средство связи AN/ARC-164
-- условное обозначение средства связи на самолете "р.ст. AN/ARC-164 на л.а. F-16D"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Навигация по радиолокационным ориентирам'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-164'
and pa.code_name like 'р.ст. AN/ARC-164 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 5;"Поиск наземных движущихся целей в секторе +/- 10 градусов"
-- средство связи AN/ARC-164
-- условное обозначение средства связи на самолете "р.ст. AN/ARC-164 на л.а. F-16D"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе +/- 10 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-164'
and pa.code_name like 'р.ст. AN/ARC-164 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 6;"Поиск наземных движущихся целей в секторе +/- 30 градусов"
-- средство связи AN/ARC-164
-- условное обозначение средства связи на самолете "р.ст. AN/ARC-164 на л.а. F-16D"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе +/- 30 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-164'
and pa.code_name like 'р.ст. AN/ARC-164 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 7;"Поиск наземных движущихся целей в секторе +/- 60 градусов"
-- средство связи AN/ARC-164
-- условное обозначение средства связи на самолете "р.ст. AN/ARC-164 на л.а. F-16D"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе +/- 60 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-164'
and pa.code_name like 'р.ст. AN/ARC-164 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 9;"Поиск ВЦ по скорости на большой дальности"
-- средство связи AN/ARC-164
-- условное обозначение средства связи на самолете "р.ст. AN/ARC-164 на л.а. F-16D"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Поиск ВЦ по скорости на большой дальности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-164'
and pa.code_name like 'р.ст. AN/ARC-164 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 11;"Поиск ВЦ в верхней полусфере на средней дальности"
-- средство связи AN/ARC-164
-- условное обозначение средства связи на самолете "р.ст. AN/ARC-164 на л.а. F-16D"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Поиск ВЦ в верхней полусфере на средней дальности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-164'
and pa.code_name like 'р.ст. AN/ARC-164 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 12;"Сопровождение ВЦ в верхней полусфере на средней дальности"
-- средство связи AN/ARC-164
-- условное обозначение средства связи на самолете "р.ст. AN/ARC-164 на л.а. F-16D"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Сопровождение ВЦ в верхней полусфере на средней дальности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-164'
and pa.code_name like 'р.ст. AN/ARC-164 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 15;"Поиск и сопровождение ВЦ на проходе"
-- средство связи AN/ARC-164
-- условное обозначение средства связи на самолете "р.ст. AN/ARC-164 на л.а. F-16D"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ на проходе'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/ARC-164'
and pa.code_name like 'р.ст. AN/ARC-164 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-----------------------------------------------
-- самолет F-16D
-- состояние 2;"Навигация по радиолокационным ориентирам"
-- средство связи AN/URS-107 "AN/URS-107"
-- условное обозначение средства связи на самолете "р.ст. AN/URS-107 на л.а. F-16D"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Навигация по радиолокационным ориентирам'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 5;"Поиск наземных движущихся целей в секторе +/- 10 градусов"
-- средство связи AN/URS-107
-- условное обозначение средства связи на самолете "р.ст. AN/URS-107 на л.а. F-16D"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе +/- 10 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 6;"Поиск наземных движущихся целей в секторе +/- 30 градусов"
-- средство связи AN/URS-107
-- условное обозначение средства связи на самолете "р.ст. AN/URS-107 на л.а. F-16D"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе +/- 30 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 7;"Поиск наземных движущихся целей в секторе +/- 60 градусов"
-- средство связи AN/URS-107
-- условное обозначение средства связи на самолете
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе +/- 60 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 9;"Поиск ВЦ по скорости на большой дальности"
-- средство связи AN/URS-107
-- условное обозначение средства связи на самолете "р.ст. AN/URS-107 на л.а. F-16D"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Поиск ВЦ по скорости на большой дальности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 11;"Поиск ВЦ в верхней полусфере на средней дальности"
-- средство связи AN/URS-107
-- условное обозначение средства связи на самолете "р.ст. AN/URS-107 на л.а. F-16D"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Поиск ВЦ в верхней полусфере на средней дальности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 12;"Сопровождение ВЦ в верхней полусфере на средней дальности"
-- средство связи AN/URS-107
-- условное обозначение средства связи на самолете "р.ст. AN/URS-107 на л.а. F-16D"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Сопровождение ВЦ в верхней полусфере на средней дальности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 15;"Поиск и сопровождение ВЦ на проходе"
-- средство связи AN/URS-107
-- условное обозначение средства связи на самолете "р.ст. AN/URS-107 на л.а. F-16D"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ на проходе'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F-16D
-- состояние 17;"Сопровождение ВЦ на малой дальности"
-- средство связи AN/URS-107
-- условное обозначение средства связи на самолете "р.ст. AN/URS-107 на л.а. F-16D"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F-16D' 
and ac.condition_name_r like 'Сопровождение ВЦ на малой дальности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F-16D'
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
);