﻿delete from re_situation.placing_f_t_aircraft where placing_f_t_aircraft_id > 30;

select setval('re_situation.placing_f_t_aircraft_placing_f_t_aircraft_id_seq', 30);
-- /*
insert into re_situation.placing_f_t_aircraft
(
placing_f_t_aircraft_id, 
type_of_aircraft_id, 
types_of_re_facilities_id, 
code_name
)

values 
---------------------------------------------------------------------------------------------------
-- 'B-52H'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'B-52H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. B-52H'
),

-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'B-52H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. B-52H'
),

-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'B-52H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. B-52H'
),

---------------------------------------------------------------------------------------------------
-- 'B-1B'
-- 'AN/ARC-190'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'B-1B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. B-1B'
),
-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'B-1B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. B-1B'
),
-- 'AN/URS-107'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'B-1B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/URS-107'), 
'р.ст. AN/URS-107 на л.а. B-1B'
),

---------------------------------------------------------------------------------------------------
-- 'B-2B'
-- 'AN/ARC-211'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'B-2B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-211'), 
'р.ст. AN/ARC-211 на л.а. B-2B'
),

-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'B-2B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. B-2B'
),

-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'B-2B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. B-2B'
),

---------------------------------------------------------------------------------------------------
-- 'RC-135'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'RC-135'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. RC-135'
),
-- 'AN/ARC-210'
(
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'RC-135'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. RC-135'
),
-- 'AN/ARC-181'
(
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'RC-135'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. RC-135'
),

---------------------------------------------------------------------------------------------------
-- 'C-130'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'C-130'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. C-130'
),

-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'C-130'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. C-130'
),

-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'C-130'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. C-130'
),

---------------------------------------------------------------------------------------------------
-- 'MC-130H'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'MC-130H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V на л.а. MC-130H'
),

-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'MC-130H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. MC-130H'
),

-- 'AN/ARC-192'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'MC-130H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-192'), 
'р.ст. AN/ARC-192 на л.а. MC-130H'
),

-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'MC-130H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. MC-130H'
),

---------------------------------------------------------------------------------------------------
-- 'RC-130E'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'RC-130E'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. RC-130E'
),


-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'RC-130E'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. RC-130E'
),


---------------------------------------------------------------------------------------------------
-- 'EC-130H'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'EC-130H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. EC-130H'
),


-- 'AN/ARC-215'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'EC-130H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-215'), 
'р.ст. AN/ARC-215 на л.а. EC-130H'
),


-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'EC-130H'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. EC-130H'
),

---------------------------------------------------------------------------------------------------
-- 'C-5A'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'C-5A'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. C-5A'
),

-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'C-5A'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. C-5A'
),

-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'C-5A'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. C-5A'
),


---------------------------------------------------------------------------------------------------
-- 'E-3C'
-- 'AN/ARC-194' N1
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-194'), 
'р.ст. AN/ARC-194 на л.а. E-3C N1'
),

-- 'AN/ARC-194' N2
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-194'), 
'р.ст. AN/ARC-194 на л.а. E-3C N2'
),

-- 'AN/ARC-194' N3
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-194'), 
'р.ст. AN/ARC-194 на л.а. E-3C N3'
),

-- 'AN/ARC-193'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-193'), 
'р.ст. AN/ARC-193 на л.а. E-3C'
),


-- 'AN/ARC-192' n1
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-192'), 
'р.ст. AN/ARC-192 на л.а. E-3С N1'
),

-- 'AN/ARC-192' n2
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-192'), 
'р.ст. AN/ARC-192 на л.а. E-3С N2'
),

-- 'AN/ARC-192' n3
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-192'), 
'р.ст. AN/ARC-192 на л.а. E-3С N3'
),

-- 'AN/ARC-192' n4
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-192'), 
'р.ст. AN/ARC-192 на л.а. E-3С N4'
),

-- 'AN/ARC-186' n1
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N1'
),

-- 'AN/ARC-186' n2
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N2'
),

-- 'AN/ARC-186' n3
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N3'
),

-- 'AN/ARC-186' n4
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N4'
),

-- 'AN/ARC-186' n5
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N5'
),

-- 'AN/ARC-186' n6
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N6'
),

-- 'AN/ARC-186' n7
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N7'
),

-- 'AN/ARC-186' n8
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N8'
),

-- 'AN/ARC-186' n9
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N9'
),

-- 'AN/ARC-186' n10
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N10'
),

-- 'AN/ARC-186' n11
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N11'
),

-- 'AN/ARC-186' n12
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N12'
),

-- 'AN/ARC-186' n13
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N13'
),

-- 'AN/ARC-186' n14
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-186'), 
'р.ст. AN/ARC-186 на л.а. E-3С N14'
),


-- 'AN/URS-107'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/URS-107'), 
'р.ст. AN/URS-107 на л.а. E-3С'
), 

---------------------------------------------------------------------------------------------------
-- 'F-16F'
-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-16F'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. F-16F'
),


-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-16F'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. F-16F'
),

---------------------------------------------------------------------------------------------------

-- 'F/A-18F'
-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F/A-18F'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. F/A-18F'
),


-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F/A-18F'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/URS-107'), 
'р.ст. AN/URS-107 на л.а. F/A-18F'
),


---------------------------------------------------------------------------------------------------
-- 'F-22'
-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-22'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. F-22'
),

-- 'AN/ARC-192'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-22'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-192'), 
'р.ст. AN/ARC-192 на л.а. F-22'
),

-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-22'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. F-22'
),


---------------------------------------------------------------------------------------------------
-- 'F/A-18D'
-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F/A-18D'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. F/A-18D'
),


-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F/A-18D'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. F/A-18D'
),

---------------------------------------------------------------------------------------------------
-- 'F-15E'
-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-15E'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. F-15E'
),


-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-15E'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. F-15E'
),


---------------------------------------------------------------------------------------------------
-- 'F-16D'
-- 'AN/ARC-222' 
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-16D'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-222'), 
'р.ст. AN/ARC-222 на л.а. F-16D'
),


-- 'AN/ARC-164'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-16D'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-164'), 
'р.ст. AN/ARC-164 на л.а. F-16D'
),

-- 'AN/URS-107'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-16D'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/URS-107'), 
'р.ст. AN/URS-107 на л.а. F-16D'
),

---------------------------------------------------------------------------------------------------
-- 'F/A-18B'
-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F/A-18B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. F/A-18B'
),

-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F/A-18B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. F/A-18B'
),


---------------------------------------------------------------------------------------------------
-- 'F-15B'
-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-15B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. F-15B'
),

-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-15B'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. F-15B'
),


---------------------------------------------------------------------------------------------------
-- 'F-15D'
-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-15D'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. F-15D'
),

-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'F-15D'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. F-15D'
),

---------------------------------------------------------------------------------------------------
-- 'C-17A'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'C-17A'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. C-17A'
),

-- 'AN/ARC-222'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'C-17A'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-222'), 
'р.ст. AN/ARC-222 на л.а. C-17A'
),


---------------------------------------------------------------------------------------------------
-- 'E-2J'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-2J'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. E-2J'
),


-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-2J'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. E-2J'
),

-- 'AN/URS-107'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-2J'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/URS-107'), 
'р.ст. AN/URS-107 на л.а. E-2J'
),


---------------------------------------------------------------------------------------------------
-- 'E-2C'
-- 'AN/ARC-193'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-2C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-193'), 
'р.ст. AN/ARC-193 на л.а. E-2C'
),

-- 'AN/ARC-182'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-2C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-182'), 
'р.ст. AN/ARC-182 на л.а. E-2C'
),

-- 'AN/ARC-201'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-2C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-201'), 
'р.ст. AN/ARC-201 на л.а. E-2C'
),

-- 'AN/ARC-164'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-2C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-164'), 
'р.ст. AN/ARC-164 на л.а. E-2C'
),

-- 'AN/ARC-181'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'E-2C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-181'), 
'р.ст. AN/ARC-181 на л.а. E-2C'
),


---------------------------------------------------------------------------------------------------
-- 'AP-3C'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'AP-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. AP-3C'
),

-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'AP-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. AP-3C'
),

-- 'AN/URS-107'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'AP-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/URS-107'), 
'р.ст. AN/URS-107 на л.а. AP-3C'
),


---------------------------------------------------------------------------------------------------
-- 'P-3C'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'P-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. P-3C'
),

-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'P-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. P-3C'
),

-- 'AN/URS-107'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'P-3C'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/URS-107'), 
'р.ст. AN/URS-107 на л.а. P-3C'
),
---------------------------------------------------------------------------------------------------
-- 'C-18'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'C-18'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. C-18'
),


-- 'AN/ARC-215'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'C-18'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-215'), 
'р.ст. AN/ARC-215 на л.а. C-18'
),

-- 'AN/URS-107'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'C-18'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/URS-107'), 
'р.ст. AN/URS-107 на л.а. C-18'
),

---------------------------------------------------------------------------------------------------
-- 'KC-135'
-- 'AN/ARC-190(V)'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'KC-135'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-190(V)'), 
'р.ст. AN/ARC-190(V) на л.а. KC-135'
),


-- 'AN/ARC-210'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'KC-135'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/ARC-210'), 
'р.ст. AN/ARC-210 на л.а. KC-135'
),

-- 'AN/URS-107'
( 
default, 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_r= 'KC-135'), 
(select types_of_re_facilities_id from re_situation.types_of_re_facilities where name_facilities_r = 'AN/URS-107'), 
'р.ст. AN/URS-107 на л.а. KC-135'
);


-- */
