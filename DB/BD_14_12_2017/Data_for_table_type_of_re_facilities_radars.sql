﻿delete from re_situation.types_of_re_facilities;

insert into re_situation.types_of_re_facilities
(types_of_re_facilities_id, name_facilities_r, name_facilities_e, manufacturer_country_id, categoryes_facilities_id, 
description_r, description_e, frequency_min, frequency_max, categoryes_a_radar_id)
values

(1, 'AN/APQ-166', 'AN/APQ-166', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 8.9, 9.3, 1 ),
(2, 'AN/APQ-164', 'AN/APQ-164', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 9.5, 16.8, 1 ),
(3, 'AN/APQ-181', 'AN/APQ-181', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 13.4, 16.3, 1 ),
(4, 'AN/APQ-122', 'AN/APQ-122', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 9.3, 9.5, 1 ),
(5, 'AN/APN-59', 'AN/APN-59', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 9.3, 9.5, 1 ),
(6, 'AN/APQ-170', 'AN/APQ-170', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 8.0, 9.9, 1 ),
(7, 'AN/APQ-175', 'AN/APQ-175', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 8.5, 9.6, 1 ),
(8, 'AN/APS-128', 'AN/APS-128', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 8.5, 9.6, 2 ),
(9, 'РЛС С-5А', 'РЛС С-5А', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 9.2, 9.5, 1 ),
(10, 'AN/APY-2', 'AN/APY-2', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 3.1, 3.5, 6 ),
(11, 'AN/APG-80', 'AN/APG-80', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 8.5, 9.9, 1 ),
(12, 'AN/APG-79', 'AN/APG-79', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 8.5, 9.9, 1 ),
(13, 'AN/APG-77', 'AN/APG-77', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 8.5, 9.9, 1 ),
(14, 'AN/APG-73', 'AN/APG-73', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 8.7, 9.5, 1 ),
(15, 'AN/APG-70', 'AN/APG-70', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 8.9, 9.1, 1 ),
(16, 'AN/APG-68', 'AN/APG-68', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 8.5, 9.9, 1 ),
(17, 'AN/APG-65', 'AN/APG-65', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 9.0, 9.8, 1 ),
(18, 'AN/APG-63', 'AN/APG-63', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 8.5, 9.7, 1 ),
(19, 'AN/APG-63(v)2', 'AN/APG-63(v)2', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 8.5, 9.7, 1 ),
(20, 'AN/APS-133', 'AN/APS-133', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 9.3, 9.4, 2 ),
(21, 'AN/APS-145', 'AN/APS-145', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 0.410, 0.460, 6 ),
(22, 'AN/APS-139', 'AN/APS-139', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 0.410, 0.460, 6 ),
(23, 'AN/APS-503F', 'AN/APS-503F', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 9.375, 9.775 , 1 ),
(24, 'AN/APS-137В', 'AN/APS-137В', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 9.3, 9.8 , 1 ),
(25, 'JSTARS', 'JSTARS', (select manufacturer_country_id from re_situation.manufacturer_country where abr_name_of_country_r='США'), 1, null, null, 9.0, 10.0, 8 );



select setval('re_situation.types_of_re_facilities_types_of_re_facilities_id_seq', 25);