﻿-- Schema: re_situation
DROP SCHEMA if exists   re_situation cascade ;
CREATE SCHEMA re_situation AUTHORIZATION maximrepey;


-- Table: classes_of_aircraft - алфавит классов летательных аппаратов
-- drop table if exists re_situation.classes_of_aircraft;
-- /*
create table re_situation.classes_of_aircraft (
classes_of_aircraft_id serial not null primary key,
cl_of_aircraft_name_r varchar(80) not null unique ,
cl_abbreviation_r varchar(10),
cl_of_aircraft_name_e varchar(80),
cl_abbreviation_e varchar(10)
);
--*/

--Table: manufacturer_country -  страны производители летательных аппаратов и РЭО
-- drop table if exists re_situation.manufacturer_country  ;
--  /*
create table re_situation.manufacturer_country (
manufacturer_country_id serial not null primary key,
name_of_country_r varchar (80) not null unique,
abr_name_of_country_r varchar(20) not null unique,
name_of_country_e varchar(80),
abr_name_of_country_e varchar(20)
);
-- */

-- Table: categoryes_facilities - словарь групп Радиоэлектронных средств
-- drop table if exists re_situation.categoryes_facilities;
-- /*
create table re_situation.categoryes_facilities (
categoryes_facilities_id serial not null primary key,
categoryes_name_r varchar(80) not null unique ,
abr_categoryes_name_r varchar(10) not null unique,
categoryes_name_e varchar(80),
abr_categoryes_name_e varchar(10)
);
--*/

-- Table: categoryes_a_radar - словарь классов бортовых радиолокационных станций
-- drop table if exists re_situation.categoryes_a_radar;
-- /*
create table re_situation.categoryes_a_radar (
categoryes_a_radar_id  serial not null primary key,
categoryes_name_r varchar (80) not null unique,
abr_categoryes_name_r varchar (10) not null unique,
categoryes_name_e varchar (80) not null ,
abr_categoryes_name_e varchar (10) not null
);
--*/

-- Table: type_of_aircraft - ТТХ летательных аппаратов конкретных типов (моделей).
-- drop table if exists re_situation.type_of_aircraft;
-- /*
create table re_situation.type_of_aircraft (
type_of_aircraft_id serial not null primary key ,
type_of_aircraft_name_r varchar (80) not null unique ,
code_name_r varchar (20) not null unique ,
classes_of_aircraft_id integer not null ,
type_of_aircraft_name_e varchar (80) ,
code_name_e varchar (20),
manufacturer_country_id integer not null,
number_of_crew_members integer ,
length double precision,
height double precision,
wingspan double precision,
wing_area double precision,
empty_weight double precision,
max_take_off_weight double precision,
max_payload_mass double precision,
required_length_runway double precision,
max_flight_speed double precision not null,
min_flight_speed double precision not null,
increment_speed double precision not null,
max_altitude double precision not null,
min_altitude double precision not null,
min_tactical_radius_of double precision ,
max_tactical_radius_of double precision ,
ferry_range double precision ,
engine_type varchar (80),
number_of_engines integer,
max_thrust_of_the_engine double precision ,

constraint re_situation_type_of_aircraft_classes_of_aircraft_id_FK
foreign key (classes_of_aircraft_id)
references  re_situation.classes_of_aircraft(classes_of_aircraft_id)
on delete restrict on update cascade,

constraint re_situation_type_of_aircraft_manufacturer_country_id_FK
	foreign key (manufacturer_country_id)
	references  re_situation.manufacturer_country(manufacturer_country_id)
		on delete restrict on update cascade

);
-- */

-- Table: types_of_re_facilities - типы радиоэлектронных средств.
-- drop table if exists re_situation.types_of_re_facilities;
-- /*
create table re_situation.types_of_re_facilities (
types_of_re_facilities_id serial not null primary key ,
name_facilities_r varchar(20) unique,
name_facilities_e varchar(20) unique,
manufacturer_country_id integer not null ,
categoryes_facilities_id integer not null ,
description_r varchar(80) ,
description_e varchar(80) ,
frequency_min double precision ,
frequency_max double precision ,
categoryes_a_radar_id integer,

constraint re_situation_types_of_re_facilities_manufacturer_country_id_FK
	foreign key (manufacturer_country_id)
	references  re_situation.manufacturer_country(manufacturer_country_id)
		on delete restrict on update cascade,

constraint re_situation_types_of_re_facilities_categoryes_facilities_id_FK
	foreign key (categoryes_facilities_id)
	references  re_situation.categoryes_facilities(categoryes_facilities_id)
		on delete restrict on update cascade,

constraint re_situation_types_of_re_facilities_categoryes_a_radar_id_FK
	foreign key (categoryes_a_radar_id)
	references  re_situation.categoryes_a_radar(categoryes_a_radar_id)
		on delete restrict on update cascade

);
-- */

-- Table: placing_f_t_aircraft - размещение радиоэлектронных средств на типах летальных аппаратов .
-- drop table if exists re_situation.placing_f_t_aircraft;
-- /*
create table re_situation.placing_f_t_aircraft (
placing_f_t_aircraft_id serial not null primary key ,
type_of_aircraft_id integer not null ,
types_of_re_facilities_id integer not null ,
code_name varchar(60),

constraint re_situation_placing_f_t_aircrafttype_of_aircraft_id_FK
	foreign key (type_of_aircraft_id)
	references  re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete restrict on update cascade,

constraint re_situation_placing_f_t_aircraft_types_of_re_facilities_id_FK
	foreign key (types_of_re_facilities_id)
	references  re_situation.types_of_re_facilities(types_of_re_facilities_id)
		on delete restrict on update cascade,

constraint re_situation_placing_f_t_aircraft_AK_UNIQUE
	unique (type_of_aircraft_id, types_of_re_facilities_id, code_name)
);
-- */

-- Table: modulation_fc - вид модуляции сигналов радиосвязи.
-- drop table if exists re_situation.modulation_fc;
-- /*
create table re_situation.modulation_fc (
modulation_fc_id serial not null primary key ,
name_modulation_r varchar(80),
abr_modulation_r varchar(20) not null unique ,
name_modulation_e varchar(80) ,
abr_modulation_e varchar(20) not null unique,
description_r text,
description_e text
);
-- */

-- Table: kind_of_work - виды работы средств связи.
-- drop table if exists re_situation.kind_of_work;
-- /*
create table re_situation.kind_of_work (
kind_of_work_id  serial not null primary key ,
name_kind_of_work_r varchar(80),
abr_name_kw_r varchar(20) not null unique ,
name_kind_of_work_e varchar(80) ,
abr_name_kw_e varchar(20) not null unique
);
-- */

-- Table: div_multiplexing - способы разделения каналов средствами связи.
-- drop table if exists re_situation.div_multiplexing;
-- /*
create table re_situation.div_multiplexing (
div_multiplexing_id  serial not null primary key ,
name_div_mu_r varchar(80),
abr_div_mu_r varchar(20) not null unique ,
name_div_mu_e varchar(80) ,
abr_div_mu_e varchar(20) not null unique
);
-- */

-- Table: mode_indication - режимы работы БРЛС.
-- drop table if exists re_situation.mode_indication;
 /*
create table re_situation.mode_indication (
mode_indication_id serial not null primary key ,
name_mode_indication_r varchar(80),
abr_mode_indication_r varchar(20) not null unique ,
name_mode_indication_e varchar(80) ,
abr_mode_indication_e varchar(20) not null unique,
description_r text,
description_e text
);
-- */



-- Table: modulation_radar_s - виды модуляции зондирующих сигналов РЛС .
-- drop table if exists re_situation.modulation_radar_s;
-- /*
create table re_situation.modulation_radar_s (
modulation_radar_s_id  serial not null primary key ,
name_modulation_r varchar(80),
abr_name_modulation_r varchar(20) not null unique ,
name_modulation_e varchar(80) ,
abr_name_modulation_e varchar(20) not null unique,
description_r text,
description_e text
);
-- */

-- Table: d_m_airborne_radar - режимы работы БРЛС .
-- drop table if exists re_situation.d_m_airborne_radar;
-- /*
create table re_situation.d_m_airborne_radar
(
d_m_airborne_radar_id  serial not null primary key ,
name_mode_r varchar(80),
abr_name_mode_r varchar(20) not null ,
name_mode_e varchar(80) ,
abr_name_mode_e varchar(20) ,
description_r text,
description_e text,
types_of_re_facilities_id integer not null,
frequency_min double precision, -- Мин частота диапазона ра-бочих частот, ГГц
frequency_max double precision, -- Макс частота диапазона ра-бочих частот, ГГц
duration_puls_min double precision, -- Мин Длительность импульса, мкс
duration_puls_max double precision, -- Макс Длительность импульса, мкс
width_spectrum_puls_min double precision, -- мин. Ширина спектра им-пульса, МГц
width_spectrum_puls_max double precision, -- макс Ширина спектра им-пульса, МГц
modulation_radar_s_id integer not null, -- Вид модуляции им-пульса
cycle_time_in_pack_min double precision, -- Мин. Период повторения импульсов, мкс
cycle_time_in_pack_max double precision, -- Макс. Период повторения импульсов, мкс
freq_pulses_in_a_pack_min double precision, -- Мин. частота диапазона перестройки несущей частоты в пачке,МГц
freq_pulses_in_a_pack_max double precision, -- Макс. частота диапазона перестройки несущей частоты в пачке,МГц
duration_pack_min double precision , -- Мин. Длительность КГПИ, мс
duration_pack_max double precision , -- Макс. Длительность КГПИ, мс
period_pack_min double precision , -- Мин. Период следования КГПИ (Тп = 60/w, мс
period_pack_max double precision , -- Макс. Период следования КГПИ (Тп = 60/w, мс
c_frequency_number_min integer, -- Мин. Количество несущих ча-стот в пачке, шт
c_frequency_number_max integer, -- Макс. Количество несущих ча-стот в пачке,шт
freq_pulses_pack_min double precision, -- Мин. частота диапазона перестройки частоты КГПИ в об-зоре, шт
freq_pulses_pack_max double precision, -- Макс. частота диапазона перестройки частоты КГПИ в об-зоре, шт
dur_to_radio_waves_min double precision , -- Мин Длительность облучения объ-екта (1000/360), мс
dur_to_radio_waves_max double precision , -- Макс. Длительность облучения объ-екта (1000/360), мс
period_obj_r_waves_min double precision, -- Мин. Период облу-чения объекта (цикл скани-рования), с
period_obj_r_waves_max double precision, -- Макс. Период облу-чения объекта (цикл скани-рования), с

constraint d_m_airborne_radar_types_of_re_facilities_id_FK
	foreign key (types_of_re_facilities_id)
	references  re_situation.types_of_re_facilities(types_of_re_facilities_id)
		on delete cascade on update cascade,

constraint d_m_airborne_radar_modulation_radar_s_id_FK
	foreign key (modulation_radar_s_id)
	references re_situation.modulation_radar_s(modulation_radar_s_id)
		on delete restrict on update cascade,


CONSTRAINT d_m_airborne_radar_abr_name_mode_r_and_types_of_refacilities_id
	UNIQUE (abr_name_mode_r, types_of_re_facilities_id )
);

--/*
-- Table: cat_of_radio_comm_signals - категории сигналов радиосвязи
-- drop table if exists re_situation.cat_of_radio_comm_signals

create table re_situation.cat_of_radio_comm_signals (
cat_of_radio_comm_signals_id  serial not null primary key ,-- Искуственный первичный ключ
name_category_r varchar(80), -- название класса излучения руский
abr_name_category_r varchar(20) not null unique , -- сокращенное название класса излучения Русский
name_category_e varchar(80) , -- название класса излучения English
abr_name_category_e varchar(20) not null unique,  -- сокращенное название класса излучения English
description_r text, -- Описание русский
description_e text, -- Описание Englich

constraint cat_of_radio_comm_signals_name_category_r_UNIQUE -- альтернативный ключ на поле name_mode_r
	unique(name_category_r) , -- название класса излучения руский

constraint cat_of_radio_comm_signals_abr_name_category_r_UNIQUE -- алтернативный ключ на поле abr_name_mode_r
	unique(abr_name_category_r) ,

constraint cat_of_radio_comm_signals_name_category_e_UNIQUE -- альтернативный ключ на поле name_mode_e
	unique(name_category_e),

constraint cat_of_radio_comm_signals_abr_name_category_e_UNIQUE -- альтернативный ключ на поле abr_name_mode_e
	unique(abr_name_category_e)
);
--*/


--*/
-- Table: descr_cat_of_r_com_signals  - описание сатегорий сигналов радиосвязи.
-- drop table if exists re_situation.descr_cat_of_r_com_signals;
create table re_situation.descr_cat_of_r_com_signals (
descr_cat_of_r_com_signals_id serial not null primary key , -- искуственный первичный ключ
cat_of_radio_comm_signals_id integer not null, -- Номер названия сатегории сигнала средств радиосвязи внешний ключ 1
frequency_min double precision, -- Мин. частота диапазона рабочих частот
frequency_max double precision, -- Макс. частота диапазона рабочих частот
frequency_hopping_mode boolean, -- Признак режима ППРЧ
min_freq_of_range_fhm double precision, -- мин. частота диапазона рабочих частот ППРЧ
max_freq_of_range_fhm double precision, -- макс. частота диапазона рабочих частот ППРЧ
min_while_single_fr_of_use double precision, -- мин длительность скачка частоты
max_while_single_fr_of_use double precision, -- макс. длительность скачка частоты
min_width_signal_spectrum double precision, -- мин. ширина спектра сигнала
max_width_signal_spectrum double precision, -- макс. ширина спектра сигнала
modulation_fc_id integer not null, -- Внешний ключ 2 на вид модуляции
digital_signal_indication boolean, -- Признак цифрового сигнала 1- цифровой 0-аналоговый сигнал
min_length_of_frame double precision, -- мин. длительность кадра
max_length_of_frame double precision, -- макс. длительность кадра
min_length_of_cycle double precision, -- мин. длительность цикла
max_length_of_cycle double precision, -- макс. длительность цикла
min_quantity_frame_in_cykle integer, -- мин. количество кадров в цикле
max_quantity_frame_in_cykle integer, -- макс. количество кадров в цикле
min_speed_of_data_transfer double precision, -- мин. скорость передачи данных
max_speed_of_data_transfer double precision, -- макс. скорость передачи данных
kind_of_work_id integer not null, -- вид передачи внешний ключ 3 на таблицу kind_of_work
div_multiplexing_id integer not null, -- вид разделения каналов внешний ключ 4 (способ многостанционного доступа)

-- реализация внешнего ключа 1
constraint descr_cat_of_r_com_signals_cat_of_radio_comm_signals_id_FC
	foreign key (cat_of_radio_comm_signals_id)
	references re_situation.cat_of_radio_comm_signals(cat_of_radio_comm_signals_id)
		on delete restrict on update cascade,

--реализация внешнего ключа 2
constraint descr_cat_of_r_com_signals_modulation_fc_id_FK
	foreign key (modulation_fc_id)
	references  re_situation.modulation_fc(modulation_fc_id)
		on delete restrict on update cascade,

--реализация внешнего ключа 3
constraint descr_cat_of_r_com_signals_kind_of_work_id_FK
	foreign key (kind_of_work_id)
	references re_situation.kind_of_work(kind_of_work_id)
		on delete restrict on update cascade,

--реализация внешнего ключа 4
constraint descr_cat_of_r_com_signals_div_multiplexing_id_FK
	foreign key (div_multiplexing_id)
	references re_situation.div_multiplexing(div_multiplexing_id)
		on delete restrict on update cascade,



-- реализация альтернативного ключа
constraint wm_fac_of_com_UNIQUE
	unique(
frequency_min , -- Мин. частота диапазона рабочих частот
frequency_max , -- Макс. частота диапазона рабочих частот
frequency_hopping_mode , -- Признак режима ППРЧ
min_freq_of_range_fhm , -- мин. частота диапазона рабочих частот ППРЧ
max_freq_of_range_fhm , -- макс. частота диапазона рабочих частот ППРЧ
min_while_single_fr_of_use , -- мин длительность скачка частоты
max_while_single_fr_of_use , -- макс. длительность скачка частоты
min_width_signal_spectrum , -- мин. ширина спектра сигнала
max_width_signal_spectrum , -- макс. ширина спектра сигнала
modulation_fc_id , -- Внешний ключ 1 на вид модуляции
digital_signal_indication , -- Признак цифрового сигнала 1- цифровой 0-аналоговый сигнал
min_length_of_frame , -- мин. длительность кадра
max_length_of_frame , -- макс. длительность кадра
min_length_of_cycle , -- мин. длительность цикла
max_length_of_cycle , -- макс. длительность цикла
min_quantity_frame_in_cykle , -- мин. количество кадров в цикле
max_quantity_frame_in_cykle , -- макс. количество кадров в цикле
min_speed_of_data_transfer , -- мин. скорость передачи данных
max_speed_of_data_transfer , -- макс. скорость передачи данных
kind_of_work_id , -- вид передачи внешний ключ 2 на таблицу kind_of_work
div_multiplexing_id  -- вид разделения каналов внешний ключ 3 (способ многостанционного доступа)
)
);
-- */


-- Table: work_mode_in_fc -реализация режима работы в средстве связи.
-- drop table if exists re_situation.work_mode_in_fc;
-- /*
create table re_situation.work_mode_in_fc (
work_mode_in_fc_id  serial not null primary key ,
types_of_re_facilities_id integer not null, -- Внешний ключ 1
cat_of_radio_comm_signals_id integer not null, -- Внешний ключ 2

--реализация внешнего ключа 1
constraint work_mode_in_fc_types_of_re_facilities_id_FK
	foreign key (types_of_re_facilities_id)
	references  re_situation.types_of_re_facilities(types_of_re_facilities_id)
		on delete cascade on update cascade,

--реализация внешнего ключа 2
constraint work_mode_in_fc_cat_of_radio_comm_signals_id_FK
	foreign key (cat_of_radio_comm_signals_id)
	references  re_situation.cat_of_radio_comm_signals(cat_of_radio_comm_signals_id)
		on delete cascade on update cascade,

-- Реализация альтернативного ключа:
-- "В одном и том же средстве связи - не может быть более одного одноименного режима работы"
constraint work_mode_in_fc_UNIQUE
	unique (types_of_re_facilities_id, cat_of_radio_comm_signals_id)

);
-- */




-- Table: radio_network - Радиосети.
-- drop table if exists re_situation.radio_network;
-- /*
create table re_situation.radio_network (
radio_network_id  serial not null primary key ,
destination_r varchar(100) not null unique ,
destination_e varchar(100) not null unique ,
description_r text,
description_e text,
descr_cat_of_r_com_signals_id integer not null, -- Внешний ключ 1
freq_of_range_fhm double precision, -- мин. Ширина диапазона рабочих частот ППРЧ
while_single_fr_of_use double precision, -- длительность скачка частоты
width_signal_spectrum double precision, -- ширина спектра сигнала
length_of_frame double precision, -- длительность кадра
length_of_cycle double precision, -- длительность цикла
quantity_frame_in_cykle integer, -- количество кадров в цикле
speed_of_data_transfer double precision, -- скорость передачи данных


--реализация внешнего ключа 1
constraint radio_network_descr_cat_of_r_com_signals_id_FK
	foreign key (descr_cat_of_r_com_signals_id)
	references re_situation.descr_cat_of_r_com_signals(descr_cat_of_r_com_signals_id)
		on delete cascade on update cascade
);
-- */

-- Table: operating_frequencies Рабочие частоты радиосетей
-- drop table if exists re_situation.operating_frequencies;
-- /*
create table re_situation.operating_frequencies(
operating_frequencies_id  serial not null primary key ,
radio_network_id integer not null,  -- Внешний ключ 1
operating_frequency double precision not null,

--реализация внешнего ключа 1
constraint operating_frequencies_radio_network_id_FK
	foreign key (radio_network_id)
	references  re_situation.radio_network(radio_network_id)
		on delete cascade on update cascade,
-- Реализация альтернативного ключа:
-- "В одной радиосети не может быть двух и более одинаковых рабочих частот"
constraint operating_frequencies_UNIQUE
	unique (radio_network_id, operating_frequency)

);
-- */


-- Table: message_structure Структура сообщения в радиосети
-- drop table if exists re_situation.message_structure;
-- /*
create table re_situation.message_structure(
radio_network_id integer not null primary key ,  -- Внешний ключ 1
discrete_length_mod_code double precision ,
number_of_items_in_c_mod integer ,
duration_of_synchronization double precision ,
duration_of_target_group double precision ,
duration_of_inform_package double precision ,
duration_of_verification_code double precision ,

--реализация внешнего ключа 1
constraint message_structure_radio_network_id_FK
	foreign key (radio_network_id)
	references  re_situation.radio_network(radio_network_id)
		on delete cascade on update cascade

);
-- */

-- Table: air_object - Воздушный объект
-- drop table if exists re_situation.air_object;
-- /*
create table re_situation.air_object(
air_object_id serial not null primary key ,
air_object_name varchar(20) unique ,
type_of_aircraft_id integer not null,  -- внешний ключ 1

--реализация внешнего ключа 1
constraint air_object_type_of_aircraft_id_FK
	foreign key (type_of_aircraft_id)
	references  re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete restrict on update cascade

);
-- */

-- Table: m_airborne_radar_in_air_obj - параметры режимы работы экземпляра БРЛС на конкретном воздушном объекте.
-- drop table if exists re_situation.m_airborne_radar_in_air_obj;
-- /*
create table re_situation.m_airborne_radar_in_air_obj (
m_airborne_radar_in_air_obj_id  serial not null primary key , -- Искуственный первичный ключ
air_object_id integer not null, -- внешний ключ на воздушный объект
placing_f_t_aircraft_id integer not null, -- внешний ключ на табл. размещение РЭС на типе ЛА
d_m_airborne_radar_id integer not null, -- внешний ключ на описание режима работы
duration_puls double precision, -- длительность зондирующего импульса, мкс
width_spectrum_puls double precision, -- Ширина спектра зондирующего импульса, МГц
cycle_time_in_pack double precision, -- Период повторения зондирующих импульсов, мкс
freq_pulses_in_a_pack double precision, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
duration_pack double precision , -- Длительность КГПИ, мс
period_pack double precision , -- Мин. Период следования КГПИ, мс
q_of_carrier_freq_in_pack integer, -- Количество несущих ча-стот в пачке, шт
r_ch_freq_of_pack_in_overview double precision, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
dur_of_r_irradiation_obj double precision , -- Длительность облучения объ-екта , мс
period_of_r_irradiation_obj double precision, -- Период облу-чения объекта (цикл скани-рования), с


constraint air_object_air_object_id_FK
	foreign key (air_object_id)
	references  re_situation.air_object(air_object_id)
		on delete cascade on update cascade,

constraint placing_f_t_aircraft_placing_f_t_aircraft_id_FK
	foreign key (placing_f_t_aircraft_id)
	references  re_situation.placing_f_t_aircraft(placing_f_t_aircraft_id)
		on delete cascade on update cascade,


constraint d_m_airborne_radar_d_m_airborne_radar_id_FK
	foreign key (d_m_airborne_radar_id)
	references  re_situation.d_m_airborne_radar(d_m_airborne_radar_id)
		on delete cascade on update cascade,

constraint air_object_id_placing_f_t_aircraft_id_d_m_airborne_radar_id_UNI
	unique(air_object_id, placing_f_t_aircraft_id, d_m_airborne_radar_id)

);
-- */


-- Table:use_networks_air_objects - Использование РС воздушными объектами
-- drop table if exists re_situation.use_networks_air_objects;
-- /*
create table re_situation.use_networks_air_objects(
use_networks_air_objects_id serial not null primary key ,
air_object_id integer not null,  -- внешний ключ 1
placing_f_t_aircraft_id integer not null,  -- внешний ключ 2
radio_network_id integer not null,  -- внешний ключ 3

--реализация внешнего ключа 1
constraint use_networks_air_objects_air_object_id_FK
	foreign key (air_object_id)
	references  re_situation.air_object(air_object_id)
		on delete cascade on update cascade,

--реализация внешнего ключа 2
constraint use_networks_air_objects_placing_f_t_aircraft_id_FK
	foreign key (placing_f_t_aircraft_id)
	references  re_situation.placing_f_t_aircraft(placing_f_t_aircraft_id)
		on delete cascade on update cascade,

--реализация внешнего ключа 3
constraint use_networks_air_objects_radio_network_id_FK
	foreign key (radio_network_id)
	references  re_situation.radio_network(radio_network_id)
		on delete restrict on update cascade,

-- Реализация альтернативного ключа:
-- "На одном воздушном объекте одним и тем-же средством одна радиосеть
-- не может быть использовано два и более раза"
constraint use_networks_air_objects_UNIQUE
	unique (air_object_id, placing_f_t_aircraft_id, radio_network_id)

);
-- */

-- Table: oper_freq_air_objects - Рабочие частоты БРЛС воздушных объектов
-- drop table if exists re_situation.oper_freq_air_objects;
-- /*
create table re_situation.oper_freq_air_objects(

m_airborne_radar_in_air_obj_id integer not null,  -- внешний ключ
operating_frequency double precision not null,

--реализация внешнего ключа
constraint oper_freq_air_objects_m_airborne_radar_in_air_obj_id_FK
	foreign key (m_airborne_radar_in_air_obj_id)
	references  re_situation.m_airborne_radar_in_air_obj(m_airborne_radar_in_air_obj_id)
		on delete cascade on update cascade,

-- Реализация альтернативного ключа:
-- ""
constraint oper_freq_air_objects_UNIQUE
	unique (m_airborne_radar_in_air_obj_id, operating_frequency)
);
-- */

-------------------------------------------------------------------------
-- Table: aircraft_condition - состояние Воздушного объекта
-- drop table if exists re_situation.aircraft_condition;
-- /*
create table re_situation.aircraft_condition(
aircraft_condition_id serial not null primary key,  -- искуственный первичный ключ
condition_name_r character varying(80), -- название состояния воздушного объекта руский
condition_name_e character varying(80), -- название состояния воздушного объекта английский
type_of_aircraft_id integer not null, -- номер типа воздушного объекта,
--  внешний ключ на таблицу re_situation.type_of_aircraft
nsr_esrc_id integer,  -- аргумент "NSR" первичного предиката COCT REC RIO(...) в експертной системе
-- первичный ключ в БД Прохоровых.

--реализация внешнего ключа
constraint aircraft_condition_type_of_aircraft_id_FK
	foreign key (type_of_aircraft_id)
	references  re_situation.type_of_aircraft(type_of_aircraft_id)
		on delete cascade on update cascade,

-- Реализация альтернативного ключа:
constraint aircraft_condition_condition_name_r_UNIQUE
	unique (type_of_aircraft_id, condition_name_r),

-- требование уникальности для поля nsr_esrc_id (дубль из БД Прохоровых)
constraint aircraft_condition_nsr_esrc_id_UNIQUE
	unique (nsr_esrc_id)

);
-- */

-- Table: aircraft_cond_mode_radar - состояние Воздушного объекта
-- drop table if exists re_situation.aircraft_cond_mode_radar;
-- /*
create table re_situation.aircraft_cond_mode_radar(
aircraft_condition_id integer not null ,  -- номер состояния воздушного объекта
-- внешний ключ 1 на таблицу re_situation.aircraft_condition
d_m_airborne_radar_id integer not null , -- номер режима работы БРЛС воздушного объекта
-- внешний ключ 2 на таблицу re_situation.d_m_airborne_radar


--реализация внешнего ключа 1
constraint aircraft_cond_mode_radar_aircraft_condition_id_FK
	foreign key (aircraft_condition_id)
	references  re_situation.aircraft_condition(aircraft_condition_id)
		on delete cascade on update cascade,

-- Реализация внешнего ключа 2
constraint aircraft_cond_mode_radar_d_m_airborne_radar_id_FK
	foreign key (d_m_airborne_radar_id)
	references  re_situation.d_m_airborne_radar(d_m_airborne_radar_id)
		on delete cascade on update cascade,

-- реализация первичного ключа
constraint aircraft_cond_mode_radar_pkey
	primary key(aircraft_condition_id, d_m_airborne_radar_id)

);
-- */

-- Table: aircraft_fc_signals - состояние Воздушного объекта
-- drop table if exists re_situation.aircraft_fc_signals;
-- /*
create table re_situation.aircraft_fc_signals(
aircraft_condition_id integer not null ,  -- номер состояния воздушного объекта
-- внешний ключ 1 на таблицу re_situation.aircraft_condition
placing_f_t_aircraft_id integer not null , -- номер средства связи воздушного объекта
-- внешний ключ 2 на таблицу re_situation.types_of_re_facilities
cat_of_radio_comm_signals_id integer not null , -- номер типа излучения
-- внешний ключ 3 на таблицу re_situation.cat_of_radio_comm_signals



--реализация внешнего ключа 1
constraint aircraft_fc_signals_aircraft_condition_id_FK
	foreign key (aircraft_condition_id)
	references  re_situation.aircraft_condition(aircraft_condition_id)
		on delete cascade on update cascade,

-- Реализация внешнего ключа 2
constraint aircraft_fc_signals_placing_f_t_aircraft_id_FK
	foreign key (placing_f_t_aircraft_id )
	references  re_situation.placing_f_t_aircraft(placing_f_t_aircraft_id)
		on delete cascade on update cascade,

-- Реализация внешнего ключа 3
constraint aircraft_fc_signals_cat_of_radio_comm_signals_id_FK
	foreign key (cat_of_radio_comm_signals_id)
	references  re_situation.cat_of_radio_comm_signals(cat_of_radio_comm_signals_id)
		on delete cascade on update cascade,


-- реализация первичного ключа
constraint aircraft_fc_signals_pkey
	primary key(aircraft_condition_id, placing_f_t_aircraft_id,
		cat_of_radio_comm_signals_id)

);
-- */


-------------------------------------------------------------------------
-- Table: situation - Обстановка
-- drop table if exists re_situation.situation;
-- /*
create table re_situation.situation(
situation_id serial not null primary key ,
operative_time interval second  not null ,
air_object_id integer not null,  -- внешний ключ 1
x_longitude double precision not null,
y_latitude double precision not null,
h_height double precision not null,
course double precision not null,
horizontal_velocity double precision not null,
vertical_velocity double precision not null,
aircraft_condition_id integer, -- внешний ключ 2 номер состония объекта

--реализация внешнего ключа 1
constraint situation_air_objects_air_object_id_FK
	foreign key (air_object_id)
	references  re_situation.air_object(air_object_id)
		on delete restrict on update cascade,

-- Реализация альтернативного ключа:
-- "В каждый момент времени один воздушный объект находится в одной точке пространства"
constraint situation_UNIQUE
	unique (operative_time, air_object_id),

-- реализация внешнего ключа 2
constraint situation_aircraft_condition_id_FK
	foreign key (aircraft_condition_id)
	references  re_situation.aircraft_condition(aircraft_condition_id)
		on delete restrict on update cascade

);
-- */


-- Table: work_fc_on_air_object - Работа Средств связи на воздушном объекте
-- drop table if exists re_situation.work_fc_on_air_object;
-- /*
create table re_situation.work_fc_on_air_object(
use_networks_air_objects_id integer not null,  -- внешний ключ 1
situation_id integer not null,  -- внешний ключ 2

--реализация внешнего ключа 1
constraint work_fc_on_air_object_use_networks_air_objects_id_FK
	foreign key (use_networks_air_objects_id)
	references  re_situation.use_networks_air_objects(use_networks_air_objects_id)
		on delete cascade on update cascade,

--реализация внешнего ключа 2
constraint work_fc_on_air_object_situation_id_FK
	foreign key (situation_id)
	references  re_situation.situation(situation_id)
		on delete cascade on update cascade,


-- Реализация альтернативного ключа:
-- "В каждый момент времени один воздушный объект находится в одной точке пространства"
constraint work_fc_on_air_object_pk
	primary key (use_networks_air_objects_id, situation_id)
);
-- */

-- Table: work_radar_on_air_objects - Работа Бортовых РЛС на Воздушном объекте
-- drop table if exists re_situation.work_radar_on_air_objects;
-- /*
create table re_situation.work_radar_on_air_objects(
m_airborne_radar_in_air_obj_id integer not null,  -- внешний ключ 1
situation_id integer not null,  -- внешний ключ 2

--реализация внешнего ключа 1
constraint work_radar_on_air_objects_m_airborne_radar_in_air_obj_id_FK
	foreign key (m_airborne_radar_in_air_obj_id)
	references  re_situation.m_airborne_radar_in_air_obj(m_airborne_radar_in_air_obj_id)
		on delete cascade on update cascade,

--реализация внешнего ключа 2
constraint work_radar_on_air_objects_situation_id_FK
	foreign key (situation_id)
	references  re_situation.situation(situation_id)
		on delete cascade on update cascade,


-- Реализация альтернативного ключа:
-- "В каждый момент времени один воздушный объект находится в одной точке пространства"
constraint work_radar_on_air_objects_pk
	primary key (m_airborne_radar_in_air_obj_id, situation_id)
);
-- */
