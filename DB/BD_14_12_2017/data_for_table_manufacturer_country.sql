﻿delete from re_situation.manufacturer_country ;
--/*
Insert into re_situation.manufacturer_country(
manufacturer_country_id, name_of_country_r, abr_name_of_country_r, name_of_country_e, abr_name_of_country_e) 
values

(1,'Соединенные Штаты Америки','США','United States of America','USA'),
(2,'Соединённое Королевство Великобритании и Северной Ирландии','Великобритания',
'The United Kingdom of Great Britain and Northern Ireland','UK'),
(3,'Французская Республика','Франция','French Republic','French'),
(4,'Федеративная Республика Германия','ФРГ','The Federal Republic of Germany','German'),
(5,'Франция и Великобритания','French&UK', 'The Federal Republic of Germany', 'German'),
(6,'Европейское сотрудничество','ЕЭС', 'The Federal Republic of Germany', 'German'),
(7,'Япония','Япония', 'Japan', 'Japan');

-- */

select setval('re_situation.manufacturer_country_manufacturer_country_id_seq', 8);
 