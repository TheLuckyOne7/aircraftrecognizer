﻿-- вспомогательные запросы 
-- Запрос: выбрать все состояния для заданного типа самолета 
/*
select ac.aircraft_condition_id, ac.condition_name_r, ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
-- and ac.condition_name_r like 'Поиск и сопровождение воздушных (надводных) целей'
-- */

--типы РЭС на ЛА 
/*
select placing_f_t_aircraft_id,  ta.code_name_e, tf.name_facilities_e, pa.code_name 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
--and tf.name_facilities_e like 'AN/ARC-210'
--and pa.code_name like 'р.ст. AN/ARC-210 на л.а. B-1B'
-- */

-- Категории сигналов в средстве связи 
/*
select tf.name_facilities_e, cs.name_category_e
from re_situation.types_of_re_facilities as tf, re_situation.work_mode_in_fc as wm_fc,
re_situation.cat_of_radio_comm_signals as cs 
where tf.types_of_re_facilities_id=wm_fc.types_of_re_facilities_id
and cs.cat_of_radio_comm_signals_id=wm_fc.cat_of_radio_comm_signals_id
order by tf.name_facilities_e

-- */ 

-- Вставка данных 

insert into re_situation.aircraft_fc_signals
(
  aircraft_condition_id , -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition
  placing_f_t_aircraft_id , -- номер средства в табл. placing_f_t_aircraft 
  cat_of_radio_comm_signals_id -- номер категории сигнала связи
)
values 
-- самолет B-1B
-- состояние 43;"Встреча с топливозаправщиком в зоне дозаправки"
-- средство саязи AN/ARC-190(V)
-- наименование средства связи в "р.ст. AN/ARC-190(V) на л.а. B-1B"
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Встреча с топливозаправщиком в зоне дозаправки'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/ARC-190(V)'
and pa.code_name like 'р.ст. AN/ARC-190(V) на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет B-1B
-- состояние 45;"Поиск наземных объектов"
-- средство саязи AN/ARC-190(V)
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Поиск наземных объектов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/ARC-190(V)'
and pa.code_name like 'р.ст. AN/ARC-190(V) на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет B-1B
-- состояние 46;"Сопровождение наземных объектов"
-- средство саязи AN/ARC-190(V)
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Сопровождение наземных объектов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/ARC-190(V)'
and pa.code_name like 'р.ст. AN/ARC-190(V) на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
), 

-- самолет B-1B
-- состояние 47;"Прицеливание по наземному объекту"
-- средство связи AN/ARC-190(V)
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Прицеливание по наземному объекту'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/ARC-190(V)'
and pa.code_name like 'р.ст. AN/ARC-190(V) на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет B-1B
-- состояние 43;"Встреча с топливозаправщиком в зоне дозаправки"
-- средство саязи AN/ARC-210
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Встреча с топливозаправщиком в зоне дозаправки'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. B-1B'), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет B-1B
-- состояние 45;"Поиск наземных объектов"
-- средство саязи AN/ARC-210
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Поиск наземных объектов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет B-1B
-- состояние 46;"Сопровождение наземных объектов"
-- средство саязи AN/ARC-210
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Сопровождение наземных объектов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
), 

-- самолет B-1B
-- состояние 47;"Прицеливание по наземному объекту"
-- средство связи AN/ARC-210
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Прицеливание по наземному объекту'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),
--------------------------------------------------------------------------
-- самолет B-1B
-- состояние 40;"Разведка погоды на маршруте полета"
-- средство саязи "AN/ARC-210"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Разведка погоды на маршруте полета'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет B-1B
-- состояние 41;"Работа с воздушным маяком-ответчиком топливозаправщика"
-- средство саязи AN/ARC-210
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Работа с воздушным маяком-ответчиком топливозаправщика'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
), 

-- самолет B-1B
-- состояние 42;"Маловысотный полет (преодоление системы ПВО)"
-- средство связи AN/ARC-210
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Маловысотный полет (преодоление системы ПВО)'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет B-1B
-- состояние 45;"Поиск наземных объектов"
-- средство саязи AN/URS-107
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Поиск наземных объектов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет B-1B
-- состояние 46;"Сопровождение наземных объектов"
-- средство саязи AN/URS-107
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Сопровождение наземных объектов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
), 

-- самолет B-1B
-- состояние 47;"Прицеливание по наземному объекту"
-- средство связи AN/ARC-190(V)
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'B-1B' 
and ac.condition_name_r like 'Прицеливание по наземному объекту'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'B-1B'
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. B-1B'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
);