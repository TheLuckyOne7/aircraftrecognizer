﻿delete from  re_situation.types_of_re_facilities where types_of_re_facilities_id > 25;
 
insert into re_situation.types_of_re_facilities(
types_of_re_facilities_id,
name_facilities_r,
name_facilities_e,
manufacturer_country_id,
categoryes_facilities_id,
description_r,
description_e,
frequency_min,
frequency_max,
categoryes_a_radar_id)
values 
(36 , 'AN/ARC-190(V)' , 'AN/ARC-190(V)' , 1 , 2 , null , null , null , null , null ),
(37 , 'AN/ARC-210' , 'AN/ARC-210' , 1 , 2 , null , null , null , null , null ),
(38 , 'AN/ARC-211' , 'AN/ARC-211' , 1 , 2 , null , null , null , null , null ),
(39 , 'AN/ARC-215' , 'AN/ARC-215' , 1 , 2 , null , null , null , null , null ),
(40 , 'AN/ARC-186' , 'AN/ARC-186' , 1 , 2 , null , null , null , null , null ),
(41 , 'AN/ARC-181' , 'AN/ARC-181' , 1 , 2 , null , null , null , null , null ),
(42 , 'AN/ARC-164' , 'AN/ARC-164' , 1 , 2 , null , null , null , null , null ),
(43 , 'AN/ARC-194' , 'AN/ARC-194' , 1 , 2 , null , null , null , null , null ),
(44 , 'AN/ARC-182' , 'AN/ARC-182' , 1 , 2 , null , null , null , null , null ),
(45 , 'AN/ARC-192' , 'AN/ARC-192' , 1 , 2 , null , null , null , null , null ),
(46 , 'AN/ARC-193' , 'AN/ARC-193' , 1 , 2 , null , null , null , null , null ),
(47 , 'AN/URS-107' , 'AN/URS-107' , 1 , 2 , null , null , null , null , null ),
(48 , 'AN/ARC-222' , 'AN/ARC-222' , 1 , 2 , null , null , null , null , null ),
(49 , 'AN/ARC-201' , 'AN/ARC-201' , 1 , 2 , null , null , null , null , null );

select setval('re_situation.types_of_re_facilities_types_of_re_facilities_id_seq', 49);
