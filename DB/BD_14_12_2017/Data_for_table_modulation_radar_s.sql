﻿delete from re_situation.modulation_radar_s;

insert into re_situation.modulation_radar_s(
modulation_radar_s_id, name_modulation_r, abr_name_modulation_r, name_modulation_e, 
abr_name_modulation_e, description_r, description_e) 
values 
(1, 'Простой радиоимпульс', 'И', 'Simple RF pulse', 'SRFP', 'Простой радиоимпульс', null),
(2, 'Фазо-кодовая модуляция', 'ФКМ', 'The phase-code modulation', 'FCM', 'Фазо-кодовая модуляция', null),
(3, 'Ступенчатая частотная модуляция', 'СЧМ', 'Stepped Frequency Modulation', 'SFM', 'Ступенчатая частотная модуляция', null),
(4, 'Линейная частотная модуляция', 'ЛЧМ', 'Linear frequency modulation', 'LFM', 'Линейная частотная модуляция', null),
(5, 'Комбинация простых радиоимпульсов и ЛЧМ импульсов', 'И-ЛЧМ', 
	'The combination of simple radio pulses and Linear frequency modulation', 'SRFP&LFM',
	'Комбинация простых радиоимпульсов и ЛЧМ импульсов', null);

select setval('re_situation.modulation_radar_s_modulation_radar_s_id_seq', 6); 