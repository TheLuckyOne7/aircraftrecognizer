﻿-- вспомогательные запросы 
-- Запрос: выбрать все состояния для заданного типа самолета 
/*
select ac.aircraft_condition_id, ac.condition_name_r, ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
-- and ac.condition_name_r like 'Поиск и сопровождение воздушных (надводных) целей'
-- */

-- Номер РЄС в списке РЄС имеющихсвя На ЛА  
/*
select placing_f_t_aircraft_id,  ta.code_name_e, tf.name_facilities_e, pa.code_name 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
-- */

-- Категории сигналов в средстве связи 
/*
select tf.name_facilities_e, cs.name_category_e
from re_situation.types_of_re_facilities as tf, re_situation.work_mode_in_fc as wm_fc,
re_situation.cat_of_radio_comm_signals as cs 
where tf.types_of_re_facilities_id=wm_fc.types_of_re_facilities_id
and cs.cat_of_radio_comm_signals_id=wm_fc.cat_of_radio_comm_signals_id
order by tf.name_facilities_e

-- */ 

-- Вставка данных 

insert into re_situation.aircraft_fc_signals
(
  aircraft_condition_id , -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition
  placing_f_t_aircraft_id , -- Номер РЄС в списке РЄС имеющихсвя На ЛА
  cat_of_radio_comm_signals_id -- номер категории сигнала связи
)
values 
-- самолет F/A-18F
-- состояние 18;"Навигация по радиолокационным маякам";"F/A-18F"
-- средство саязи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C2 category'

(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Навигация по радиолокационным маякам'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 19;"Навигация по радиолокационным ориентирам";"F/A-18F"
-- средство саязи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Навигация по радиолокационным ориентирам'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 20;"Полет на малой высоте c огибанием рельефа местности";"F/A-18F"
-- средство саязи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Полет на малой высоте c огибанием рельефа местности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 21;"Полет на малой высоте c заданным превышением над максимальной точкой маршрута";"F/A-18F"
-- средство саязи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 
'Полет на малой высоте c заданным превышением над максимальной точкой маршрута'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
), 

-- самолет F/A-18F
-- состояние 19;"Навигация по радиолокационным ориентирам";"F/A-18F"
-- средство связи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Навигация по радиолокационным ориентирам'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 25;"Сопровождение наземной движущейся цели";"F/A-18F"
-- средство связи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Сопровождение наземной движущейся цели'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 27;"Сопровождение ВЦ на большой дальности в режиме ВЧПИ";"F/A-18F"
-- средство связи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Сопровождение ВЦ на большой дальности в режиме ВЧПИ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 31;"Сопровождение ВЦ в верхней полусфере на средней дальности в режиме СЧПИ";"F/A-18F"
-- средство связи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Сопровождение ВЦ в верхней полусфере на средней дальности в режиме СЧПИ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 35;"Сопровождение ВЦ в нижней полусфере на средней дальности в режиме СЧПИ";"F/A-18F"
-- средство связи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Сопровождение ВЦ в нижней полусфере на средней дальности в режиме СЧПИ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 38;"Сопровождение ВЦ на малой дальности";"F/A-18F"
-- средство связи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Сопровождение ВЦ на малой дальности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),
------------------------------
-- самолет F/A-18F
-- состояние 25;"Сопровождение наземной движущейся цели";"F/A-18F"
-- средство связи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Сопровождение наземной движущейся цели'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 27;"Сопровождение ВЦ на большой дальности в режиме ВЧПИ";"F/A-18F"
-- средство связи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Сопровождение ВЦ на большой дальности в режиме ВЧПИ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 31;"Сопровождение ВЦ в верхней полусфере на средней дальности в режиме СЧПИ";"F/A-18F"
-- средство связи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Сопровождение ВЦ в верхней полусфере на средней дальности в режиме СЧПИ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 35;"Сопровождение ВЦ в нижней полусфере на средней дальности в режиме СЧПИ";"F/A-18F"
-- средство связи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Сопровождение ВЦ в нижней полусфере на средней дальности в режиме СЧПИ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 38;"Сопровождение ВЦ на малой дальности";"F/A-18F"
-- средство связи AN/ARC-210
-- имя средства связи в списке средств, имеющихся на ЛА р.ст. AN/ARC-210 на л.а. F/A-18F
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Сопровождение ВЦ на малой дальности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-------------------------------------
-- самолет F/A-18F
-- состояние 19;"Навигация по радиолокационным ориентирам";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Навигация по радиолокационным ориентирам'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 22;"Поиск наземных движущихся целей в секторе (+/-) 10 градусов";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе (+/-) 10 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 23;"Поиск наземных движущихся целей в секторе (+/-) 30 градусов";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе (+/-) 30 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 24;"Поиск наземных движущихся целей в секторе (+/-) 60 градусов";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе (+/-) 60 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 26;"Поиск ВЦ на большой дальности при использовании ВЧПИ";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Поиск ВЦ на большой дальности при использовании ВЧПИ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 28;"Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 10 градусов";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 10 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 29;"Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 30 градусов";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 30 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 30;"Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 60 градусов";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Поиск ВЦ в верхней полусфере на средней дальности в секторе (+/-) 60 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 32;"Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 10 градусов";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 10 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 33;"Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 30 градусов";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 30 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 34;"Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 60 градусов";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Поиск ВЦ в нижней полусфере на средней дальности в секторе (+/-) 60 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 36;"Поиск и сопровождение ВЦ в ближней зоне на проходе";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в ближней зоне на проходе'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет F/A-18F
-- состояние 37;"Поиск ВЦ на малой дальности в секторе (+/-) 10 градусов";"F/A-18F"
-- средство связи AN/URS-107
-- имя средства связи в списке средств, имеющихся на ЛА "р.ст. AN/URS-107 на л.а. F/A-18F"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'F/A-18F' 
and ac.condition_name_r like 'Поиск ВЦ на малой дальности в секторе (+/-) 10 градусов'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'F/A-18F' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
);
-- */