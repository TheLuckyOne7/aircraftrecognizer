﻿--  data_for_table_div_multiplexing

delete from re_situation.div_multiplexing;

insert into re_situation.div_multiplexing
(
  div_multiplexing_id, -- serial NOT NULL,
  name_div_mu_r, -- character varying(80),
  abr_div_mu_r, -- character varying(20) NOT NULL,
  name_div_mu_e, -- character varying(80),
  abr_div_mu_e -- character varying(20) NOT NULL,
)

values 
(1, 'Временное разделение каналов',  'В', 'Time Division Multiplexing', 'Т'),
(2, 'Чстотное разделение каналов', 'Ч','Frequency Division Multiplexing', 'F');

select setval('re_situation.div_multiplexing_div_multiplexing_id_seq', 3);

