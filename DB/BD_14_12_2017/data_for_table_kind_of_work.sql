﻿delete from re_situation.kind_of_work;

insert into re_situation.kind_of_work
(
kind_of_work_id,
name_kind_of_work_r,
abr_name_kw_r,
name_kind_of_work_e,
abr_name_kw_e
)
values 
(1, 'Телефония','Тлф', 'Telephony', 'TLF'), 
(2, 'Передача данных','ПД', 'Data transfer', 'DT'),
(3, 'Телеграфия','Тлг', 'Telegraphy', 'TLG'),
(4, 'Передача изображений', 'ПИ', 'Transferring images', 'TI' );

select setval('re_situation.kind_of_work_kind_of_work_id_seq', 5);