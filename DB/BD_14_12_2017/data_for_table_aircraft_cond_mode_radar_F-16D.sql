﻿insert into re_situation.aircraft_cond_mode_radar
(
  aircraft_condition_id , -- номер состояния воздушного объекта 
  d_m_airborne_radar_id  -- номер режима работы БРЛС воздушного объекта
)

values 
-------------------------------
-- самолет - F-16D
-- состояние ЛА - Навигация по радиолокационным маякам
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Навигация по радиолокационным маякам' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р1'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - F-16D
-- состояние ЛА - Навигация по радиолокационным ориентирам
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Навигация по радиолокационным ориентирам' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р2'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F-16D
-- состояние ЛА - Полет на малой высоте c огибанием рельефа местности
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Полет на малой высоте c огибанием рельефа местности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р3'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - F-16D
-- состояние ЛА - Полет на малой высоте c заданным превышением над максимальной точкой маршрута
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Полет на малой высоте c заданным превышением над максимальной точкой маршрута' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р4'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F-16D
-- состояние ЛА - Поиск наземных движущихся целей в секторе +/- 10 градусов
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе +/- 10 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р5'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - F-16D
-- состояние ЛА - Поиск наземных движущихся целей в секторе +/- 30 градусов
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе +/- 30 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р6'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F-16D
-- состояние ЛА - Поиск наземных движущихся целей в секторе +/- 60 градусов
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Поиск наземных движущихся целей в секторе +/- 60 градусов' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р7'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - F-16D
-- состояние ЛА - Сопровождение наземной движущейся цели
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Сопровождение наземной движущейся цели' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р8'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F-16D
-- состояние ЛА - Поиск ВЦ по скорости на большой дальности
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Поиск ВЦ по скорости на большой дальности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р9'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F-16D
-- состояние ЛА - Сопровождение ВЦ по скорости на большой дальности
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Сопровождение ВЦ по скорости на большой дальности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р10'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F-16D
-- состояние ЛА - Поиск ВЦ в верхней полусфере на средней дальности
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Поиск ВЦ в верхней полусфере на средней дальности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р11'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F-16D
-- состояние ЛА - Сопровождение ВЦ в верхней полусфере на средней дальности
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Сопровождение ВЦ в верхней полусфере на средней дальности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р12'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F-16D
-- состояние ЛА - Поиск ВЦ в нижней полусфере на средней дальности
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Поиск ВЦ в нижней полусфере на средней дальности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р13'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F-16D
-- состояние ЛА - Сопровождение ВЦ в нижней полусфере на средней дальности
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Сопровождение ВЦ в нижней полусфере на средней дальности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р14'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F-16D
-- состояние ЛА - Поиск и сопровождение ВЦ на проходе
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Поиск и сопровождение ВЦ на проходе' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р15'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F-16D
-- состояние ЛА - Поиск ВЦ на малой дальности
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Поиск ВЦ на малой дальности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р16'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - F-16D
-- состояние ЛА - Сопровождение ВЦ на малой дальности
-- БРЛС AN/APG-68
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'F-16D'
and ac.condition_name_r like 'Сопровождение ВЦ на малой дальности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APG-68'
and dmar.abr_name_mode_r like 'Режим Р17'
) -- номер режима работы БРЛС воздушного объекта
);