﻿-- вспомогательные запросы 
/*
select ac.aircraft_condition_id, ac.condition_name_r, ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
-- and ac.condition_name_r like 'Поиск и сопровождение воздушных (надводных) целей'
-- */

/*
select d_m_airborne_radar_id, dmar.abr_name_mode_r, f.name_facilities_e
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
-- and dmar.abr_name_mode_r like 'Режим Р1'
-- */

-- Вставка данных 

insert into re_situation.aircraft_cond_mode_radar
(
  aircraft_condition_id , -- номер состояния воздушного объекта 
  d_m_airborne_radar_id  -- номер режима работы БРЛС воздушного объекта
)

values 
-------------------------------
-- самолет - E-2C
-- состояние ЛА - 77;"Поиск ВЦ над горизонтом"
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Поиск ВЦ над горизонтом' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р1'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - E-2C
-- состояние ЛА - 82;"Оповещение о ВО над горизонтом"
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Оповещение о ВО над горизонтом' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р1'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - E-2C
-- состояние ЛА 87;"Сопровождение ВЦ и наведение истребителей на ВЦ над горизонтом"
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р2
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Сопровождение ВЦ и наведение истребителей на ВЦ над горизонтом' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р2'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - E-2C
-- состояние ЛА 78;"Поиск надводных целей"
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р3
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Поиск надводных целей' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р3'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - E-2C
-- состояние ЛА 83;"Оповещение о НВО по результатам поиска НвЦ"
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р3
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска НвЦ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р3'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - E-2C
-- состояние ЛА 88;"Сопровождение надводных целей и наведение штурмовиков на надводные цели"
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р4
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Сопровождение надводных целей и наведение штурмовиков на надводные цели' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р4'
) -- номер режима работы БРЛС воздушного объекта
), 


-- самолет - E-2C
-- состояние ЛА - 79;"Поиск ВЦ на фоне земной поверхности"
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р5
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Поиск ВЦ на фоне земной поверхности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р5'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-2C
-- состояние ЛА - 84;"Оповещение о ВО по результатам поиска ВЦ на фоне земн. поверхн."
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р5
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска ВЦ на фоне земн. поверхн.' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р5'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-2C
-- состояние ЛА - 89;"Сопровождение и наведение истребителей на цели на фоне земной поверхности"
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р6
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Сопровождение и наведение истребителей на цели на фоне земной поверхности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р6'
) -- номер режима работы БРЛС воздушного объекта
),


-- самолет - E-2C
-- состояние ЛА - 80;"Поиск ВЦ на фоне морской поверхности"
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р7
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Поиск ВЦ на фоне морской поверхности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р7'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-2C
-- состояние ЛА - 85;"Оповещение о НВО по результатам поиска ВЦ на фоне морской поверхности"
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р7
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска ВЦ на фоне морской поверхности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р7'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-2C
-- состояние ЛА - 90;"Сопровождение и наведение истребителей на цели на фоне морской поверхности"
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р8
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Сопровождение и наведение истребителей на цели на фоне морской поверхности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р8'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-2C
-- состояние ЛА - 81;"Поиск целей в комбинированном режиме"
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р9
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Поиск целей в комбинированном режиме' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р9'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-2C
-- состояние ЛА - 86;"Оповещение о ВО по результатам поиска целей в комбинир. режиме."
-- БРЛС AN/APS-145
-- Режим работы БРЛС - Режим Р9
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-2C'
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска целей в комбинир. режиме.' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APS-145'
and dmar.abr_name_mode_r like 'Режим Р9'
) -- номер режима работы БРЛС воздушного объекта
);

