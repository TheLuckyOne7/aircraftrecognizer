﻿-- вспомогательные запросы 
-- Запрос: выбрать все состояния для заданного типа самолета 
/*
select ac.aircraft_condition_id, ac.condition_name_r, ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
-- and ac.condition_name_r like 'Поиск и сопровождение воздушных (надводных) целей'
-- */

-- РЭС на ЛА 
/*
select placing_f_t_aircraft_id,  ta.code_name_e, tf.name_facilities_e, pa.code_name 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'KC-135' 
and tf.name_facilities_e like 'AN/ARC-190(V)'
and pa.code_name like 'р.ст. AN/ARC-190(V) на л.а. KC-135'
-- */

-- Категории сигналов в средстве связи 
/*
select tf.name_facilities_e, cs.name_category_e
from re_situation.types_of_re_facilities as tf, re_situation.work_mode_in_fc as wm_fc,
re_situation.cat_of_radio_comm_signals as cs 
where tf.types_of_re_facilities_id=wm_fc.types_of_re_facilities_id
and cs.cat_of_radio_comm_signals_id=wm_fc.cat_of_radio_comm_signals_id
order by tf.name_facilities_e

-- */ 

-- Вставка данных 

insert into re_situation.aircraft_fc_signals
(
  aircraft_condition_id , -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition
  placing_f_t_aircraft_id , -- номер средства связи в списке средств, имеющихся На ЛА  
  cat_of_radio_comm_signals_id -- номер категории сигнала связи
)
values 
-- самолет KC-135
-- состояние 48;"Навигация по радиолокационным маякам"
-- средство саязи AN/ARC-190(V)
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-190(V) на л.а. KC-135"
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
and ac.condition_name_r like 'Навигация по радиолокационным маякам'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'KC-135' 
and tf.name_facilities_e like 'AN/ARC-190(V)'
and pa.code_name like 'р.ст. AN/ARC-190(V) на л.а. KC-135'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет KC-135
-- состояние 49;"Навигация по радиолокационным ориентирам"
-- средство саязи AN/ARC-190(V)
-- имя средства связи в списке РЭС имеющихся на ЛА
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
and ac.condition_name_r like 'Навигация по радиолокационным ориентирам'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'KC-135' 
and tf.name_facilities_e like 'AN/ARC-190(V)'
and pa.code_name like 'р.ст. AN/ARC-190(V) на л.а. KC-135'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

-- самолет KC-135
-- состояние 50;"Разведка погоды на маршруте полета"
-- средство саязи AN/ARC-190(V)
-- имя средства связи в списке РЭС имеющихся на ЛА
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
and ac.condition_name_r like 'Разведка погоды на маршруте полета'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'KC-135' 
and tf.name_facilities_e like 'AN/ARC-190(V)'
and pa.code_name like 'р.ст. AN/ARC-190(V) на л.а. KC-135'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
), 

-- самолет KC-135
-- состояние 55;"Встреча с заправляемым самолетом"
-- средство связи AN/ARC-190(V)
-- имя средства связи в списке РЭС имеющихся на ЛА
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
and ac.condition_name_r like 'Встреча с заправляемым самолетом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'KC-135' 
and tf.name_facilities_e like 'AN/ARC-190(V)'
and pa.code_name like 'р.ст. AN/ARC-190(V) на л.а. KC-135'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C1 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет KC-135
-- состояние 53;"Поиск воздушной цели"
-- средство саязи AN/ARC-210
-- имя средства связи в списке РЭС имеющихся на ЛА
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
and ac.condition_name_r like 'Поиск воздушной цели'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'KC-135' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. KC-135'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет KC-135
-- состояние 54;"Сопровождение воздушной цели"
-- средство саязи AN/ARC-210
-- имя средства связи в списке РЭС имеющихся на ЛА
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
and ac.condition_name_r like 'Сопровождение воздушной цели'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'KC-135' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. KC-135'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------------------------
-- самолет KC-135
-- состояние 53;"Поиск воздушной цели"
-- средство саязи "AN/ARC-210"
-- имя средства связи в списке РЭС имеющихся на ЛА
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
and ac.condition_name_r like 'Поиск воздушной цели'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'KC-135' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. KC-135'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет KC-135
-- состояние 54;"Сопровождение воздушной цели"
-- средство саязи AN/ARC-210
-- имя средства связи в списке РЭС имеющихся на ЛА
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
and ac.condition_name_r like 'Сопровождение воздушной цели'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'KC-135' 
and tf.name_facilities_e like 'AN/ARC-210'
and pa.code_name like 'р.ст. AN/ARC-210 на л.а. KC-135'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
), 

--------------------------------------------------------------------------
-- самолет KC-135
-- состояние 48;"Навигация по радиолокационным маякам"
-- средство саязи AN/URS-107
-- имя средства связи в списке РЭС имеющихся на ЛА
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
and ac.condition_name_r like 'Навигация по радиолокационным маякам'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'KC-135' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. KC-135'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет KC-135
-- состояние 49;"Навигация по радиолокационным ориентирам"
-- средство саязи AN/URS-107
-- имя средства связи в списке РЭС имеющихся на ЛА
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
and ac.condition_name_r like 'Навигация по радиолокационным ориентирам'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'KC-135' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. KC-135'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
), 

-- самолет KC-135
-- состояние 55;"Встреча с заправляемым самолетом"
-- средство связи AN/URS-107
-- имя средства связи в списке РЭС имеющихся на ЛА
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'KC-135' 
and ac.condition_name_r like 'Встреча с заправляемым самолетом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'KC-135' 
and tf.name_facilities_e like 'AN/URS-107'
and pa.code_name like 'р.ст. AN/URS-107 на л.а. KC-135'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
);