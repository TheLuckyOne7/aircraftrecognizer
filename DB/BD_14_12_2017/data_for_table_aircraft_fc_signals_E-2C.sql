﻿-- вспомогательные запросы 
-- Запрос: выбрать все состояния для заданного типа самолета 
/*
select ac.aircraft_condition_id, ac.condition_name_r -- , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Поиск ВЦ над горизонтом'
-- */

-- РЭС на ЛА 
/*
select placing_f_t_aircraft_id,  ta.code_name_e, tf.name_facilities_e, pa.code_name 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-2C'
-- */

-- Категории сигналов в средстве связи 
/*
select tf.name_facilities_e, cs.name_category_e
from re_situation.types_of_re_facilities as tf, re_situation.work_mode_in_fc as wm_fc,
re_situation.cat_of_radio_comm_signals as cs 
where tf.types_of_re_facilities_id=wm_fc.types_of_re_facilities_id
and cs.cat_of_radio_comm_signals_id=wm_fc.cat_of_radio_comm_signals_id
order by tf.name_facilities_e

-- */ 

-- Вставка данных 

insert into re_situation.aircraft_fc_signals
(
  aircraft_condition_id , -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition
  placing_f_t_aircraft_id , -- номер средства связи в списке средств, имеющихся На ЛА  
  cat_of_radio_comm_signals_id -- номер категории сигнала связи
)
values 
---------------------------------------------------------------
-- самолет E-2C
-- состояние 77;"Поиск ВЦ над горизонтом"
-- средство саязи AN/ARC-193
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-2C"
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Поиск ВЦ над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 78;"Поиск надводных целей"
-- средство саязи AN/ARC-193
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-2C"
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Поиск надводных целей'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 79;"Поиск ВЦ на фоне земной поверхности"
-- средство саязи AN/ARC-193
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-2C"
-- категория сигналла 'C1 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Поиск ВЦ на фоне земной поверхности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 80;"Поиск ВЦ на фоне морской поверхности"
-- средство саязи AN/ARC-193
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-2C"
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Поиск ВЦ на фоне морской поверхности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
), 

-- самолет E-2C
-- состояние 81;"Поиск целей в комбинированном режиме"
-- средство саязи AN/ARC-193
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-193 на л.а. E-2C"
-- категория сигналла 'C4 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Поиск целей в комбинированном режиме'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-193'
and pa.code_name like 'р.ст. AN/ARC-193 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C4 category'
) -- номер категории сигнала связи
),

-----------------------------------------------
-- самолет E-2C
-- состояние 82;"Оповещение о ВО над горизонтом"
-- средство саязи AN/ARC-182
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-182 на л.а. E-2C"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о ВО над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-182'
and pa.code_name like 'р.ст. AN/ARC-182 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 83;"Оповещение о НВО по результатам поиска НвЦ"
-- средство саязи AN/ARC-182
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-182 на л.а. E-2C"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска НвЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-182'
and pa.code_name like 'р.ст. AN/ARC-182 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 84;"Оповещение о ВО по результатам поиска ВЦ на фоне земн. поверхн."
-- средство саязи AN/ARC-182
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-182 на л.а. E-2C"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска ВЦ на фоне земн. поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-182'
and pa.code_name like 'р.ст. AN/ARC-182 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 85;"Оповещение о НВО по результатам поиска ВЦ на фоне морской поверхности"
-- средство саязи AN/ARC-182
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-182 на л.а. E-2C"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска ВЦ на фоне морской поверхности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-182'
and pa.code_name like 'р.ст. AN/ARC-182 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 86;"Оповещение о ВО по результатам поиска целей в комбинир. режиме."
-- средство саязи AN/ARC-182
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-182 на л.а. E-2C"
-- категория сигналла 'C2 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска целей в комбинир. режиме.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-182'
and pa.code_name like 'р.ст. AN/ARC-182 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C2 category'
) -- номер категории сигнала связи
),

-----------------------------------------------
-- самолет E-2C
-- состояние 82;"Оповещение о ВО над горизонтом"
-- средство саязи AN/ARC-182
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-182 на л.а. E-2C"
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о ВО над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-182'
and pa.code_name like 'р.ст. AN/ARC-182 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 83;"Оповещение о НВО по результатам поиска НвЦ"
-- средство саязи AN/ARC-182
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-182 на л.а. E-2C"
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска НвЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-182'
and pa.code_name like 'р.ст. AN/ARC-182 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 84;"Оповещение о ВО по результатам поиска ВЦ на фоне земн. поверхн."
-- средство саязи AN/ARC-182
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-182 на л.а. E-2C"
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска ВЦ на фоне земн. поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-182'
and pa.code_name like 'р.ст. AN/ARC-182 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 85;"Оповещение о НВО по результатам поиска ВЦ на фоне морской поверхности"
-- средство саязи AN/ARC-182
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-182 на л.а. E-2C"
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска ВЦ на фоне морской поверхности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-182'
and pa.code_name like 'р.ст. AN/ARC-182 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 86;"Оповещение о ВО по результатам поиска целей в комбинир. режиме."
-- средство саязи AN/ARC-182
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-182 на л.а. E-2C"
-- категория сигналла 'C3 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска целей в комбинир. режиме.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-182'
and pa.code_name like 'р.ст. AN/ARC-182 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C3 category'
) -- номер категории сигнала связи
),
--------------------------------------------------------
-- самолет E-2C
-- состояние 87;"Сопровождение ВЦ и наведение истребителей на ВЦ над горизонтом"
-- средство саязи AN/ARC-201
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-201 на л.а. E-2C"
-- категория сигналла 'C5 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Сопровождение ВЦ и наведение истребителей на ВЦ над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-201'
and pa.code_name like 'р.ст. AN/ARC-201 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C5 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 88;"Сопровождение надводных целей и наведение штурмовиков на надводные цели"
-- средство саязи AN/ARC-201
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-201 на л.а. E-2C"
-- категория сигналла 'C5 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Сопровождение надводных целей и наведение штурмовиков на надводные цели'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-201'
and pa.code_name like 'р.ст. AN/ARC-201 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C5 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 89;"Сопровождение и наведение истребителей на цели на фоне земной поверхности"
-- средство саязи AN/ARC-201
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-201 на л.а. E-2C"
-- категория сигналла 'C5 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Сопровождение и наведение истребителей на цели на фоне земной поверхности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-201'
and pa.code_name like 'р.ст. AN/ARC-201 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C5 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 90;"Сопровождение и наведение истребителей на цели на фоне морской поверхности"
-- средство саязи AN/ARC-201
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-201 на л.а. E-2C"
-- категория сигналла 'C5 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Сопровождение и наведение истребителей на цели на фоне морской поверхности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-201'
and pa.code_name like 'р.ст. AN/ARC-201 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C5 category'
) -- номер категории сигнала связи
),

--------------------------------------------------------
-- самолет E-2C
-- состояние 87;"Сопровождение ВЦ и наведение истребителей на ВЦ над горизонтом"
-- средство саязи AN/ARC-164
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-164 на л.а. E-2C"
-- категория сигналла 'C8 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Сопровождение ВЦ и наведение истребителей на ВЦ над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-164'
and pa.code_name like 'р.ст. AN/ARC-164 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 88;"Сопровождение надводных целей и наведение штурмовиков на надводные цели"
-- средство саязи AN/ARC-164
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-164 на л.а. E-2C"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Сопровождение надводных целей и наведение штурмовиков на надводные цели'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-164'
and pa.code_name like 'р.ст. AN/ARC-164 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 89;"Сопровождение и наведение истребителей на цели на фоне земной поверхности"
-- средство саязи AN/ARC-164
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-164 на л.а. E-2C"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Сопровождение и наведение истребителей на цели на фоне земной поверхности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-164'
and pa.code_name like 'р.ст. AN/ARC-164 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 90;"Сопровождение и наведение истребителей на цели на фоне морской поверхности"
-- средство саязи AN/ARC-164
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-164 на л.а. E-2C"
-- категория сигналла 'C6 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Сопровождение и наведение истребителей на цели на фоне морской поверхности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-164'
and pa.code_name like 'р.ст. AN/ARC-164 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C6 category'
) -- номер категории сигнала связи
),

-----------------------------------------------
-- самолет E-2C
-- состояние 82;"Оповещение о ВО над горизонтом"
-- средство саязи AN/ARC-181
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-181 на л.а. E-2C"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о ВО над горизонтом'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-181'
and pa.code_name like 'р.ст. AN/ARC-181 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 83;"Оповещение о НВО по результатам поиска НвЦ"
-- средство саязи AN/ARC-181
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-181 на л.а. E-2C"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска НвЦ'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-181'
and pa.code_name like 'р.ст. AN/ARC-181 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 84;"Оповещение о ВО по результатам поиска ВЦ на фоне земн. поверхн."
-- средство саязи AN/ARC-181
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-181 на л.а. E-2C"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска ВЦ на фоне земн. поверхн.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-181'
and pa.code_name like 'р.ст. AN/ARC-181 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 85;"Оповещение о НВО по результатам поиска ВЦ на фоне морской поверхности"
-- средство саязи AN/ARC-181
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-181 на л.а. E-2C"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска ВЦ на фоне морской поверхности'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-181'
and pa.code_name like 'р.ст. AN/ARC-181 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
),

-- самолет E-2C
-- состояние 86;"Оповещение о ВО по результатам поиска целей в комбинир. режиме."
-- средство саязи AN/ARC-181
-- имя средства связи в списке РЭС имеющихся на ЛА "р.ст. AN/ARC-181 на л.а. E-2C"
-- категория сигналла 'C9 category'
(
(
select ac.aircraft_condition_id  --, ac.condition_name_r , ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-2C' 
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска целей в комбинир. режиме.'
), -- внешний ключ на поле aircraft_condition_id таблицы aircraft_condition

(
select placing_f_t_aircraft_id 
from re_situation.type_of_aircraft as ta, re_situation.placing_f_t_aircraft as pa,
re_situation.types_of_re_facilities as tf 
where tf.types_of_re_facilities_id=pa.types_of_re_facilities_id
and ta.type_of_aircraft_id = pa.type_of_aircraft_id
and tf.categoryes_facilities_id=2
and ta.code_name_e like 'E-2C' 
and tf.name_facilities_e like 'AN/ARC-181'
and pa.code_name like 'р.ст. AN/ARC-181 на л.а. E-2C'
), -- номер средства связи  

(
select cat_of_radio_comm_signals_id
from re_situation.cat_of_radio_comm_signals 
where name_category_e like 'C9 category'
) -- номер категории сигнала связи
);

