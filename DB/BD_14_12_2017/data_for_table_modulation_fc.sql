﻿delete from re_situation.modulation_fc;

insert into re_situation.modulation_fc
(
modulation_fc_id, 
name_modulation_r,
abr_modulation_r,
name_modulation_e,
abr_modulation_e,
description_r,
description_e
)
values 

(1, 'Амплитудная модуляция', 'АМ', 'Amplitude modulation', 'AM', null, null ),
(2, 'Частотная модуляция', 'ЧМ', 'Frequency modulation', 'FM', null, null ),
(3, 'Aмплитудная манипуляция', 'АМн', 'Amplitude-shift keying', 'ASK', null, null ),
(4, 'Частотная манипуляция', 'ЧМн', 'Frequency Shift Keying', 'FSK', null, null ),
(5, 'Фазовая манипуляция', 'ФМн', 'Phase-shift keying', 'PSK', null, null );


select setval('re_situation.modulation_fc_modulation_fc_id_seq',6);
