﻿-- вспомогательные запросы 
/*
select ac.aircraft_condition_id, ac.condition_name_r, ta.code_name_e 
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like   'E-3C' 
-- and ac.condition_name_r like 'Поиск и сопровождение воздушных (надводных) целей'
-- */

/*
select d_m_airborne_radar_id, dmar.abr_name_mode_r, f.name_facilities_e
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
-- and dmar.abr_name_mode_r like 'Режим Р1'
-- */


-- Вставка данных 

insert into re_situation.aircraft_cond_mode_radar
(
  aircraft_condition_id , -- номер состояния воздушного объекта 
  d_m_airborne_radar_id  -- номер режима работы БРЛС воздушного объекта
)

values 
-------------------------------
-- самолет - E-3C
-- состояние ЛА - 56 Поиск и сопровождение ВЦ в ИР1 над горизонтом
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в ИР1 над горизонтом' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р1'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - E-3C
-- состояние ЛА - 64;"Оповещение о ВО по результатам поиска в ИР1 над горизонтом"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИР1 над горизонтом' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р1'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - E-3C
-- состояние ЛА - 72;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р1
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР1 над горизонтом' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р1'
) -- номер режима работы БРЛС воздушного объекта
), 


-- самолет - E-3C
-- состояние ЛА - 57;"Поиск и сопровождение ВЦ в КР1 над горизонтом"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р2
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в КР1 над горизонтом' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р2'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-3C
-- состояние ЛА - 65;"Оповещение о ВО по результатам поиска в КР1 над горизонтом"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р2
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР1 над горизонтом' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р2'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-3C
-- состояние ЛА 58;"Поиск и сопровождение ВЦ в ИДР1 без измерения НЦ"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р3
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в ИДР1 без измерения НЦ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р3'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - E-3C
-- состояние ЛА 66;"Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р3
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР1 без измерения НЦ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р3'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - E-3C
-- состояние ЛА 73; Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р3
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР1 без измерения НЦ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р3'
) -- номер режима работы БРЛС воздушного объекта
), 

-- самолет - E-3C
-- состояние ЛА - 59;"Поиск и сопровождение ВЦ в КР2 без измерения НЦ"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р4
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в КР2 без измерения НЦ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р4'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-3C
-- состояние ЛА - 67;"Оповещение о ВО по результатам поиска в КР2 без измерения НЦ"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р4
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР2 без измерения НЦ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р4'
) -- номер режима работы БРЛС воздушного объекта
),


-- самолет - E-3C
-- состояние ЛА - 60;"Поиск и сопровождение ВЦ в ИДР2 с измерением НЦ"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р5
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в ИДР2 с измерением НЦ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р5'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-3C
-- состояние ЛА - 68;"Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р5
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в ИДР2 с измерением НЦ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р5'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-3C
-- состояние ЛА - 74;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р5
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИДР2 с измерением НЦ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р5'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-3C
-- состояние ЛА - 61;"Поиск и сопровождение ВЦ в КР3 с измерением НЦ"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р6
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Поиск и сопровождение ВЦ в КР3 с измерением НЦ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р6'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-3C
-- состояние ЛА - 69;"Оповещение о ВО по результатам поиска в КР3 с измерением НЦ"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р6
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Оповещение о ВО по результатам поиска в КР3 с измерением НЦ' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р6'
) -- номер режима работы БРЛС воздушного объекта
),


-- самолет - E-3C
-- состояние ЛА - 62;"Поиск и сопровождение НВЦ в ИР2 (морском режиме)"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р7
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Поиск и сопровождение НВЦ в ИР2 (морском режиме)' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р7'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-3C
-- состояние ЛА - 70;"Оповещение о НВО по результатам поиска в ИР2 (морской  режим)"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р7
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в ИР2 (морской  режим)' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р7'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-3C
-- состояние ЛА - 75;"Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р7
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в ИР2 (морском режиме)' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р7'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-3C
-- состояние ЛА - 63;"Поиск и сопровождение НВЦ в КР4 на фоне морской поверхности"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р8
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Поиск и сопровождение НВЦ в КР4 на фоне морской поверхности' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р8'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-3C
-- состояние ЛА - 71;"Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн."
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р8
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Оповещение о НВО по результатам поиска в КР4 на фоне морской  поверхн.' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р8'
) -- номер режима работы БРЛС воздушного объекта
),

-- самолет - E-3C
-- состояние ЛА - 76;"Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП"
-- БРЛС AN/APY-2
-- Режим работы БРЛС - Режим Р8
(
(
select ac.aircraft_condition_id
from re_situation.aircraft_condition as ac, re_situation.type_of_aircraft as ta 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id
and ta.code_name_e like 'E-3C'
and ac.condition_name_r like 'Наведение истребителей ПВО на сопровождаемые ВЦ в КР4 на  фоне МП' 
),  -- номер состояния воздушного объекта
(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dmar, re_situation.types_of_re_facilities as f
where dmar.types_of_re_facilities_id=f.types_of_re_facilities_id
and f.name_facilities_e like 'AN/APY-2'
and dmar.abr_name_mode_r like 'Режим Р8'
) -- номер режима работы БРЛС воздушного объекта
); 

