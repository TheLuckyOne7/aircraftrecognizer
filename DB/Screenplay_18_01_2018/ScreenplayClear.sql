﻿ delete from re_situation.work_radar_on_air_objects;
 delete from re_situation.work_fc_on_air_object;

 delete from re_situation.situation;
 select setval('re_situation.situation_situation_id_seq',1);

 delete from re_situation.use_networks_air_objects;
 select setval('re_situation.use_networks_air_objects_use_networks_air_objects_id_seq',1);

 delete from re_situation.oper_freq_air_objects;
 
 delete from re_situation.operating_frequencies;
 select setval('re_situation.operating_frequencies_operating_frequencies_id_seq',1);

 delete from re_situation.message_structure;
 
 delete from re_situation.radio_network;
 select setval('re_situation.radio_network_radio_network_id_seq',1);

 delete from re_situation.m_airborne_radar_in_air_obj;
 select setval('re_situation.m_airborne_radar_in_air_obj_m_airborne_radar_in_air_obj_id_seq',1);

 delete from re_situation.air_object;
 select setval('re_situation.air_object_air_object_id_seq', 1); 