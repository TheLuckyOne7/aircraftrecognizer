#!/bin/bash
cd ~/Work/Diplom/AircraftRecognizer/DB/Screenplay_18_01_2018/
psql -U maximrepey -d Simulator -a -f ScreenplayClear.sql
psql -U maximrepey -d Simulator -a -f 'registration of air objects.sql'
psql -U maximrepey -d Simulator -a -f 'data_Aairborne_Radar_B-1B master.sql'
psql -U maximrepey -d Simulator -a -f 'data_Aairborne_Radar_B-1B slave.sql'
psql -U maximrepey -d Simulator -a -f data_Aairborne_Radar_KC-135_N1.sql
psql -U maximrepey -d Simulator -a -f data_Aairborne_Radar_KC-135_N2.sql
psql -U maximrepey -d Simulator -a -f data_Aairborne_Radar_E-3C_AWACS.sql
psql -U maximrepey -d Simulator -a -f 'data_Aairborne_Radar_F-16D_N1_guard E-3C.sql'
psql -U maximrepey -d Simulator -a -f 'data_Aairborne_Radar_F-16D_N2_guard E-3C.sql'
psql -U maximrepey -d Simulator -a -f 'data_Aairborne_Radar_F-16D_N3_guard E-3C.sql'
psql -U maximrepey -d Simulator -a -f 'data_Aairborne_Radar_F-16D_N4_guard E-3C.sql'
psql -U maximrepey -d Simulator -a -f data_Aairborne_Radar_E-2C_Hawkeye.sql
psql -U maximrepey -d Simulator -a -f 'data_Aairborne_Radar_FA-18F_N1_g-d E-2C.sql'
psql -U maximrepey -d Simulator -a -f 'data_Aairborne_Radar_FA-18F_N2_g-d E-2C.sql'
psql -U maximrepey -d Simulator -a -f 'data_Aairborne_Radar_FA-18F_N3_g-d E-2C.sql'
psql -U maximrepey -d Simulator -a -f 'data_Aairborne_Radar_FA-18F_N4_g-d E-2C.sql'
psql -U maximrepey -d Simulator -a -f data_Aairborne_Radar_FA-18F_N1_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_Aairborne_Radar_FA-18F_N2_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_Aairborne_Radar_FA-18F_N3_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_Aairborne_Radar_FA-18F_N4_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_Aairborne_Radar_FA-18F_N5_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_Aairborne_Radar_FA-18F_N6_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_Aairborne_Radar_FA-18F_N7_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_Aairborne_Radar_FA-18F_N8_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_network.sql
psql -U maximrepey -d Simulator -a -f data_operating_frequences.sql
psql -U maximrepey -d Simulator -a -f data_for_table_use_network_air_objects.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_B_1B_master.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_B_1B_slave.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_KC-135_N1.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_KC-135_N2.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_E-3C_AWACS.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_F-16D_N1_guard_E-3C.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_F-16D_N2_guard_E-3C.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_F-16D_N3_guard_E-3C.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_F-16D_N4_guard_E-3C.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_E-2C_Hawkeye.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_of_FA-18F_guard_E-2C_N1.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_of_FA-18F_guard_E-2C_N2.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_of_FA-18F_guard_E-2C_N3.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_of_FA-18F_guard_E-2C_N4.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_of_FA-18F_N1_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_of_FA-18F_N2_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_of_FA-18F_N3_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_of_FA-18F_N4_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_of_FA-18F_N5_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_of_FA-18F_N6_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_of_FA-18F_N7_ADSG.sql
psql -U maximrepey -d Simulator -a -f data_situation_route_of_FA-18F_N8_ADSG.sql
psql -U maximrepey -d Simulator -a -f Data_for_table_work_radar_on_air_objects.sql
psql -U maximrepey -d Simulator -a -f data_for_table_work_fc_on_air_object_2.sql
