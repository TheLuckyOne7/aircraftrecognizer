﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)
values 
/* точка P0 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'0'         ,	125.01986 , 28.67674	,	0	,	358.960948668194	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P1 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'330'         ,	125.01986 , 28.67674	,	0	,	358.960948668194	,	0	,	2.519301748,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P2 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'726.9353813' ,	125.00696 , 29.39054	,	1000	,	0.989953414387777	,	1000	,	3.093372089,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P3 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'1050.207193' ,	125.02278 , 30.18196	,	2000	,	357.273435030041	,	2000	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
),
/* точка P4 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'1678.872199' ,	124.92781 , 31.87559	,	2000	,	329.900562196318	,	2000	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
),
/* точка P5 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'1984.527596' ,	124.43713 , 32.58787	,	2000	,	318.444004165865	,	2000	,	-3.766720598,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
),
/* точка P6 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'2449.122714' ,	123.43995 , 33.52174	,	250	,	316.508510553609	,	250	,	-0.401658939,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=9 ) 
),
/* точка P7 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'2822.573883' ,	122.60105 , 34.24985	,	100	,	305.071591077945	,	100	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=9 ) 
),
/* точка P8 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'3234.200251' ,	121.49306 , 34.88298	,	100	,	292.76171623912	,	100	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=5 ) 
),
/* точка P9 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'3525.397871' ,	120.60667 , 35.18372	,	100	,	307.396004452451	,	100	,	4.29978802,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=6 ) 
),
/* точка P10 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'3734.710516' ,	120.14765 , 35.46863	,	1000	,	331.631570201894	,	1000	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=7 ) 
),
/* точка P11 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'3929.150634' ,	119.89040 , 35.85251	,	1000	,	5.93331974714167	,	1000	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=7 ) 
),
/* точка P12 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'4148.612392' ,	119.95371 , 36.34319	,	1000	,	63.0184788388521	,	1000	,	-5.249585565,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
),
/* точка P13 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'4320.054497' ,	120.38108 , 36.5173	,	100	,	138.771149772717	,	100	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=8 ) 
),
/* точка P14 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'4501.239432' ,	120.76096 , 36.24822	,	100	,	136.221648060045	,	100	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=13 ) 
),
/* точка P15 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'4825.728796' ,	121.40992 , 35.74171	,	100	,	133.869859443751	,	100	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=13 ) 
),
/* точка P16 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'5418.192088' ,	122.53373 , 34.77618	,	100	,	127.476355994218	,	100	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=12 ) 
),
/* точка P17 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'5966.332546' ,	123.43595 , 33.79482	,	100	,	132.176873812428	,	100	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=11 ) 
),
/* точка P18 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'6467.852032' ,	124.33816 , 32.95592	,	100	,	126.554477870695	,	100	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=10 ) 
),
/* точка P19 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'6948.606668' ,	125.09793 , 32.08536	,	100	,	112.792403807343	,	100	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=10 ) 
),
/* точка P20 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'7368.269023' ,	125.52529 , 31.2148	,	100	,	93.1387675908787	,	100	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=10 ) 
),
/* точка P21 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'7925.272859' ,	125.60443 , 29.96436	,	100	,	178.678612629221	,	100	,	3.549566797,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=10 ) 
),
/* точка P22 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'8460.549446' ,	125.57278 , 28.76141	,	2000	,	98.9698089262294	,	2000	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P23 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'9014.150994' ,	125.77855 , 27.60594	,	2000	,	141.676899407043	,	2000	,	-6.232060391,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P24 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'9174.61157' ,	125.52929 , 27.32103	,	1000	,	294.32162394832	,	1000	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P25 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'9311.757471' ,	125.21272 , 27.44765	,	1000	,	332.879006848254	,	1000	,	-3.334849731,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P26 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'9461.689264' ,	125.03661 , 27.74639	,	500	,	359.095013662815	,	500	,	-0.966489134,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P27 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №2 ADSG'), 	'9979.025655' ,	125.01986 , 28.67674	,	0	,	359.095013662815	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 )
);

