﻿-- /*
insert into re_situation.work_radar_on_air_objects( situation_id, m_airborne_radar_in_air_obj_id)
(
select s1.situation_id , z1.mar_ar_radar_id
from  re_situation.situation as s1, 
(
select ac.aircraft_condition_id as ac_cond_id, mar.m_airborne_radar_in_air_obj_id as mar_ar_radar_id ,
ao.air_object_id as ao_id  
-- ao.air_object_name, ta.code_name_e,  ac.type_of_aircraft_id,  ac.condition_name_r,  
-- dmar.abr_name_mode_r  

from re_situation.aircraft_condition as ac,  re_situation.type_of_aircraft as ta,
re_situation.aircraft_cond_mode_radar as acmr, re_situation.d_m_airborne_radar as dmar,
re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao  
 
where ta.type_of_aircraft_id=ac.type_of_aircraft_id and ac.aircraft_condition_id=acmr.aircraft_condition_id
and dmar.d_m_airborne_radar_id=acmr.d_m_airborne_radar_id and dmar.d_m_airborne_radar_id=mar.d_m_airborne_radar_id 
and mar.air_object_id=ao.air_object_id
order by air_object_name, dmar.abr_name_mode_r
) as z1
where s1.aircraft_condition_id=z1.ac_cond_id and z1.ao_id=s1.air_object_id
)
-- */