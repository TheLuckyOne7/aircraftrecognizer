﻿-----------------------------------------------------------------------------------------
-- вывести Список ЛА с РЭС размещенными на каждом из ЛА
-- /* 
select ta.code_name_r, tf.name_facilities_r, p.code_name, cf.abr_categoryes_name_r, cf.abr_categoryes_name_r 

from 
re_situation.type_of_aircraft as ta, 
re_situation.types_of_re_facilities as tf, 
re_situation.placing_f_t_aircraft as p,
re_situation.categoryes_facilities as cf

where ta.type_of_aircraft_id=p.type_of_aircraft_id
and tf.types_of_re_facilities_id=p.types_of_re_facilities_id
and tf.categoryes_facilities_id = cf.categoryes_facilities_id
-- and cf.abr_categoryes_name_r='БРЛС'
order by ta.code_name_r,  cf.abr_categoryes_name_r
-- */
-----------------------------------------------------------------------------------------
-- удалить данные из таблицы "re_situation.placing_f_t_aircraft"

/*
delete from re_situation.placing_f_t_aircraft 
-- */

-----------------------------------------------------------------------------------------
-- категории сигналов и их описание 
/*
select distinct
cs.name_category_r, dcs.frequency_min, dcs.frequency_max

from re_situation.cat_of_radio_comm_signals as cs, 
re_situation.descr_cat_of_r_com_signals as dcs

where 
cs.cat_of_radio_comm_signals_id=dcs.cat_of_radio_comm_signals_id
order by 1
--*/
-----------------------------------------------------------------------------------------
-- налисие категорий сигналов в средстве связи 
/*
select tf.name_facilities_e, cs.name_category_r
from 
re_situation.work_mode_in_fc as w,
re_situation.types_of_re_facilities  as tf, 
re_situation.cat_of_radio_comm_signals as cs 

where w.types_of_re_facilities_id=tf.types_of_re_facilities_id 
and cs.cat_of_radio_comm_signals_id=w.cat_of_radio_comm_signals_id 
order by 1, 2
-- */

-----------------------------------------------------------------------------------------
-- Описание режимов работы в типе БРЛС
/*
select tf.name_facilities_e, dmar.name_mode_r, dmar.abr_name_mode_r, dmar.description_r
from re_situation.types_of_re_facilities as tf, re_situation.d_m_airborne_radar as dmar

where tf.types_of_re_facilities_id = dmar.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79' 

-- */

