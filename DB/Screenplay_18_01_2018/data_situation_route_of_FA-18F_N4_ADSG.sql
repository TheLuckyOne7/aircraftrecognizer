﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)
values 
/* точка P0 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'0'     	,	125.01986, 28.67674	,	0	,	358.960948668194	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P1 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'420'   	,	125.01986, 28.67674	,	0	,	358.960948668194	,	200	,	2.512160215,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P2 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'818.0637835'	,	125.00496, 29.39254	,	1000	,	1.11792221174914	,	300.59	,	3.422913051,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P3 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'1110.212601'	,	125.02278, 30.18196	,	2000	,	357.273375929323	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
),
/* точка P4 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'1738.877606'	,	124.92781, 31.87559	,	2000	,	329.900006375835	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
),
/* точка P5 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'2044.533004'	,	124.43713, 32.58787	,	2000	,	318.443342769538	,	300	,	-3.766720598,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=9 ) 
),
/* точка P6 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'2509.128122'	,	123.43995, 33.52174	,	250	,	316.507827425498	,	300	,	-0.401658939,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=9 ) 
),
/* точка P7 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'2882.57929'	,	122.60105, 34.24985	,	100	,	305.070929714296	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=10 ) 
),
/* точка P8 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'3294.205658'	,	121.49306, 34.88298	,	100	,	292.761207696523	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=11 ) 
),
/* точка P9 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'3585.403279'	,	120.60667, 35.18372	,	100	,	269.813033253214	,	250	,	3.814668473,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=5 ) 
),
/* точка P10 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'3821.33466'	,	119.95771, 35.18372	,	1000	,	308.352961329346	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=6 ) 
),
/* точка P11 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'4086.164334'	,	119.38389, 35.55177	,	1000	,	10.7646951510419	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=7 ) 
),
/* точка P12 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'4329.887285'	,	119.51052, 36.08994	,	1000	,	54.5604069511502	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=8 ) 
),
/* точка P13 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'4524.802812'	,	119.95371, 36.34319	,	1000	,	63.0178779454552	,	250	,	-5.249585565,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=8 ) 
),
/* точка P14 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'4696.244917'	,	120.38108, 36.5173	,	100	,	138.770424336763	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=14 ) 
),
/* точка P15 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'4877.429852'	,	120.76096, 36.24822	,	100	,	136.22093131252 	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
),
/* точка P16 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'5201.919216'	,	121.40992, 35.74171	,	100	,	133.869169433858	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=11 ) 
),
/* точка P17 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'5794.382509'	,	122.53373, 34.77618	,	100	,	127.475712222196	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=12 ) 
),
/* точка P18 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'6342.522966'	,	123.43595, 33.79482	,	100	,	132.176232008564	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=13 ) 
),
/* точка P19 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'6844.042452'	,	124.33816, 32.95592	,	100	,	126.5538803428  	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=14 ) 
),
/* точка P20 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'7324.797088'	,	125.09793, 32.08536	,	100	,	112.791971575821	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=15 ) 
),
/* точка P21 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'7744.459443'	,	125.52529, 31.2148	,	100	,	93.138704556885 	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=16 ) 
),
/* точка P22 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'8301.46328'	,	125.60443, 29.96436	,	100	,	178.678637935842	,	250	,	3.549566797,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=17 ) 
),
/* точка P23 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'8836.739867'	,	125.57278, 28.76141	,	2000	,	99.0401887435251	,	236	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=18 ) 
),
/* точка P24 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'9389.056241'	,	125.78055, 27.60394	,	2000	,	141.676899407043	,	250	,	-6.194452922,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P25 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'9550.490998'	,	125.52729, 27.31903	,	1000	,	294.32162394832 	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P26 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'9687.63896'	,	125.21072, 27.44565	,	1000	,	332.879006848254	,	250	,	-3.325360744,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P27 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'9837.998586'	,	125.03661, 27.74639	,	500	,	359.095013662815	,	200	,	-0.966489134,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 ADSG'), 	'10355.33498'	,	125.01986, 28.67674	,	0	,	359.095013662815	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
);


