﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния 
)
values
-- Точка P0 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '0', 132.00858,29.48751, 10000, 289.337117511479, 0, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- Точка P1 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '846.8759643', 132.00858,29.48751, 10000, 289.337117511479, 250, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- Точка P2 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '1565.134005', 130.25163, 30.00985, 10000, 298.563656273834, 250, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=53 )
),
-- Точка P3 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '2282.543117', 128.60548, 30.76961, 10000, 299.910298950744, 250, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=53 )
),
-- Точка P4 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '2955.228822', 127.07013, 31.51354, 10000, 303.124648467232, 250, -16.18050596,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=55 )
),
-- Точка P5 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '3567.076191', 125.70889, 32.25747, 100, 298.850339790011, 250, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- Точка P6 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '4428.205109', 123.68286, 33.17552, 100, 322.869491427116, 250, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- Точка P7 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '5102.05001', 122.57488, 34.37847, 100, 313.657677459348, 250, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- Точка P8 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '5532.27949', 121.72014, 35.04326, 100, 286.866983582284, 250, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=57 )
),
-- Точка P9 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '5952.493096', 120.61216, 35.31235, 100, 259.359940591497, 250, 3.014040219,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=58 )
),
-- Точка P10 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '6251.095615', 119.80491, 35.18572, 1000, 248.521188832136, 250, 4.032107748,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=59 )
),
-- Точка P11 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '6499.10486', 119.17178, 34.97995, 2000, 194.405674424335, 250, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=54 )
),
-- Точка P12 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '6615.426573', 119.09264, 34.72670, 2000, 126.554584783877, 250, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=54 )
),
-- Точка P13 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '6703.052945', 119.23509, 34.56841, 2000, 85.6411906373467, 250, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=54 )
),
-- Точка P14 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '6894.903042', 119.75743, 34.60007, 2000, 58.5634993924522, 250, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=54 )
),
-- Точка P15 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '7084.625354', 120.20062, 34.82167, 2000, 75.784710862885, 250, -8.182602132,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=54 )
),
-- Точка P16 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '7316.825328', 120.81793, 34.94829, 100, 179.006602148825, 200, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=51 )
),
-- Точка P17 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '7720.906606', 121.70432, 34.93247, 100, 134.397038777522, 200, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=51 )
),
-- Точка P18 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '8419.35727', 122.76482, 34.03025, 100, 139.593488621629, 200, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- Точка P19 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '8959.685032', 123.65120, 33.39711, 100, 143.673222183601, 200, 9.230201416,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- Точка P20 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '10032.25101', 125.48729, 32.24164, 10000, 149.040881940138, 300, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- Точка P21 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '10428.73826', 126.56362, 31.68765, 10000, 150.901278637011, 300, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- Точка P22 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '10906.89778', 127.87738, 31.05452, 10000, 152.138550504802, 300, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- Точка P23 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '11428.62043', 129.31776, 30.38973, 10000, 152.820337914969, 300, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=55 )
),
-- Точка P24 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '11760.47456', 130.23580, 29.97819, 10000, 163.147320786454, 300, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=53 )
),
-- Точка P25 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '12353.58939', 131.99275, 29.50334, 10000, 156.217898296665, 300, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- Точка P26 
( default , (select air_object_id from re_situation.air_object where air_object_name like 'B-1B master') , '12426.29809', 132.19852, 29.42420, 10000, 156.217898296665, 300, 0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=51 )
);




