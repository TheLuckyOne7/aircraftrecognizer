﻿-- /*
delete from re_situation.air_object;
select setval('re_situation.air_object_air_object_id_seq',1);
-- */

-- /*
insert into re_situation.air_object
(
air_object_id , -- Первичный ключ 
air_object_name, --Условное наименование ВО
type_of_aircraft_id -- внешний ключ на тип ЛА
)
values 
-- Ударная группа B-1B 
-- ведущий самолет ударной группы B-1B
(
default, 'B-1B master', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'B-1B')
),

-- ведомый самолет ударной группы B-1B
(
default, 'B-1B slave', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'B-1B')
),

-- группа самолетов заправщиков KC-135
-- самолет заправщик KC-135 №1
(
default, 'КС-135 №1', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'KC-135')
),

-- самолет заправщик KC-135 №2
(
default, 'КС-135 №2', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'KC-135')
),

-- самолет ДРЛО E-3C 
(
default, 'E-3C AWACS', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'E-3C')
),


-- Группа охраны самолета ДРЛО E-3C четыре самолета F-16C
-- самолет F-16D №1 (звено охраны E-3C)    
(
default, 'F-16D №1 guard E-3C', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F-16D')
),

-- самолет F-16D №2 (звено охраны E-3C)    
(
default, 'F-16D №2 guard E-3C', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F-16D')
),

-- самолет F-16D №3 (звено охраны E-3C)    
(
default, 'F-16D №3 guard E-3C', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F-16D')
),

-- самолет F-16D №4 (звено охраны E-3C)    
(
default, 'F-16D №4 guard E-3C', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F-16D')
),

-- самолет ДРЛО E-2C Hawkeye
(
default, 'E-2C Hawkeye', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'E-2C')
),

-- группа самолетов охраны самолета ДРЛО E-2C Hawkeye четыре самолета F/A-18F
-- самолет F/A-18F №1 (звено охраны E-2C)    
(
default, 'F/A-18F №1 g-d E-2C', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F/A-18F')
),

-- самолет F/A-18F №2 (звено охраны E-2C)    
(
default, 'F/A-18F №2 g-d E-2C', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F/A-18F')
),

-- самолет F/A-18F №3 (звено охраны E-2C)    
(
default, 'F/A-18F №3 g-d E-2C', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F/A-18F')
),

-- самолет F/A-18F №4 (звено охраны E-2C)    
(
default, 'F/A-18F №4 g-d E-2C', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F/A-18F')
),

-- Группа подавления системы ПВО (восемь самолетов F/A-18F)
-- группа подавления системы ПВО - air defense suppression group (ADSG) - перевод google 

-- самолет F/A-18F №1 (группа прорыва системы ПВО - ADSG)  
(
default, 'F/A-18F №1 ADSG', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F/A-18F')
),

-- самолет F/A-18F №2 (группа прорыва системы ПВО - ADSG)  
(
default, 'F/A-18F №2 ADSG', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F/A-18F')
),

-- самолет F/A-18F №3 (группа прорыва системы ПВО - ADSG)  
(
default, 'F/A-18F №3 ADSG', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F/A-18F')
),

-- самолет F/A-18F №4 (группа прорыва системы ПВО - ADSG)  
(
default, 'F/A-18F №4 ADSG', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F/A-18F')
),

-- самолет F/A-18F №5 (группа прорыва системы ПВО - ADSG)  
(
default, 'F/A-18F №5 ADSG', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F/A-18F')
),

-- самолет F/A-18F №6 (группа прорыва системы ПВО - ADSG)  
(
default, 'F/A-18F №6 ADSG', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F/A-18F')
),

-- самолет F/A-18F №7 (группа прорыва системы ПВО - ADSG)  
(
default, 'F/A-18F №7 ADSG', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F/A-18F')
),

-- самолет F/A-18F №8 (группа прорыва системы ПВО - ADSG)  
(
default, 'F/A-18F №8 ADSG', 
(select type_of_aircraft_id from re_situation.type_of_aircraft where code_name_e like 'F/A-18F')
);
-- */

-- select setval('re_situation.air_object_air_object_id_seq',7);

/*
select ao.air_object_id, ao.air_object_name, ao.type_of_aircraft_id, ta.code_name_e 
from re_situation.air_object as ao, re_situation.type_of_aircraft as ta 
where ao.type_of_aircraft_id = ta.type_of_aircraft_id
order by ao.air_object_id  

-- */

