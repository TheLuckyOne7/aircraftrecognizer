﻿-- delete from re_situation.radio_network ;
-- select setval('re_situation.radio_network_radio_network_id_seq',1);

insert into re_situation.radio_network
(
  radio_network_id, -- первичный ключ
  destination_r, -- назначение по русски
  destination_e, -- назначение по английски 
  description_r, -- Описание по русски,
  description_e, -- описание по английски 
  descr_cat_of_r_com_signals_id, -- внешний ключ на таблицу descr_cat_of_r_com_signals
  -- описание категорий сигналов связи 
  freq_of_range_fhm , -- ширина диапазона частот ????
  while_single_fr_of_use , -- длительность сигналла на одной частоте 
  width_signal_spectrum , -- ширина спектра сигналла 
  length_of_frame , -- длительность кадра
  length_of_cycle , -- длительность цикла 
  quantity_frame_in_cykle,  -- количество кадров в цикле ,
  speed_of_data_transfer  -- скорость передачи данных
) 

values 
-- Р.сеть N1
(
default, -- radio_network_id, -- первичный ключ
'Управления ударной группой на маршруте', --  destination_r, -- назначение по русски
'The network of the shock group management on the route of fly',  -- destination_e, -- назначение по английски 
'Управления ударной группой на маршруте', --  description_r, -- Описание по русски,
'The network of the shock group management on the route of fly', -- description_e, -- описание по английски 
1, --  descr_cat_of_r_com_signals_id, -- описание категорий сигналов связи 
0.002, --   freq_of_range_fhm , -- ширина диапазона частот ????
1.05, --  while_single_fr_of_use , -- длительность сигналла на одной частоте 
3.4, --  width_signal_spectrum , -- ширина спектра сигналла 
0.05, --  length_of_frame , -- длительность кадра
0.05, --  length_of_cycle , -- длительность цикла 
0, --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
1.364 --  speed_of_data_transfer  -- скорость передачи данных
),

-- Р.сеть N2
(
default , -- первичный ключ
'Взаимодействия ударной группы с группой дозаправки',   -- destination_r, -- назначение по русски
'Interaction of the shock group with the refueling group',  -- destination_e, -- назначение по английски 
'Взаимодействия ударной группы с группой дозаправки' , -- description_r, -- Описание по русски,
'Interaction of the shock group with the refueling group' , --  description_e, -- описание по английски 
1, --  descr_cat_of_r_com_signals_id,  -- описание категорий сигналов связи 
0.002, --   freq_of_range_fhm , -- ширина диапазона частот ????
1.05, --  while_single_fr_of_use , -- длительность сигналла на одной частоте 
3.4, --  width_signal_spectrum , -- ширина спектра сигналла 
0.05, --  length_of_frame , -- длительность кадра
0.05, --  length_of_cycle , -- длительность цикла 
0, --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
1.364 --  speed_of_data_transfer  -- скорость передачи данных
),

-- Р.сеть N3 (C2)
(
default, -- radio_network_id, -- первичный ключ
'Взаимодействия с сухопутными войсками', -- назначение по русски
'Interactions with land forces', --  destination_e, -- назначение по английски 
'Взаимодействия с сухопутными войсками', --  description_r, -- Описание по русски,
'Interactions with land forces', --  description_e, -- описание по английски 
5, --  descr_cat_of_r_com_signals_id, -- внешний ключ на таблицу descr_cat_of_r_com_signals
0.002, --  freq_of_range_fhm , -- ширина диапазона частот ????
1.05, --  while_single_fr_of_use , -- длительность сигналла на одной частоте 
20, --  width_signal_spectrum , -- ширина спектра сигналла 
0.05, --  length_of_frame , -- длительность кадра
0.05, --  length_of_cycle , -- длительность цикла 
0, --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
19.6 --  speed_of_data_transfer  -- скорость передачи данных
), 

-- Р.сеть N4 (C6)
(
default, -- radio_network_id, -- первичный ключ
'Управления ударной группой', --  destination_r, -- назначение по русски
'Management of the shock group', --   destination_e, -- назначение по английски 
'Управления ударной группой', --   description_r, -- Описание по русски,
'Management of the shock group', --  description_e, -- описание по английски 
14, --   descr_cat_of_r_com_signals_id, -- внешний ключ на таблицу descr_cat_of_r_com_signals
85, --   freq_of_range_fhm , -- ширина диапазона частот ????
0.0015, -- while_single_fr_of_use , -- длительность сигналла на одной частоте 
7, --  width_signal_spectrum , -- ширина спектра сигналла 
0.05, --  length_of_frame , -- длительность кадра
0.05, --  length_of_cycle , -- длительность цикла 
0 , --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
16.2 --  speed_of_data_transfer  -- скорость передачи данных
),
 
-- Р.сеть N5 (C6)
(
default, -- radio_network_id, -- первичный ключ
'Управления группами прорыва ПВО', --  destination_r, -- назначение по русски
'Managing Air Defense Breakout Groups', --  destination_e, -- назначение по английски 
'Управления группами прорыва ПВО', --  description_r, -- Описание по русски,
'Managing Air Defense Breakout Groups', --  description_e, -- описание по английски 
14, --   descr_cat_of_r_com_signals_id, -- внешний ключ на таблицу descr_cat_of_r_com_signals
85, --   freq_of_range_fhm , -- ширина диапазона частот ????
0.0015, -- while_single_fr_of_use , -- длительность сигналла на одной частоте 
7, --  width_signal_spectrum , -- ширина спектра сигналла 
0.05, --  length_of_frame , -- длительность кадра
0.05, --  length_of_cycle , -- длительность цикла 
0 , --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
16.2 --  speed_of_data_transfer  -- скорость передачи данных
), 

-- Р.сеть N6 (C6)
(
default, -- radio_network_id, -- первичный ключ
'Управления группой охраны E-2C', --  destination_r, -- назначение по русски
'Management of the guard group E-2C',  -- destination_e, -- назначение по английски 
'Управления группой охраны E-2C', --  description_r, -- Описание по русски,
'Management of the guard group E-2C', --  description_e, -- описание по английски 
15, --   descr_cat_of_r_com_signals_id, -- внешний ключ на таблицу descr_cat_of_r_com_signals
0.002, --   freq_of_range_fhm , -- ширина диапазона частот ????
1.1, -- while_single_fr_of_use , -- длительность сигналла на одной частоте 
7, --  width_signal_spectrum , -- ширина спектра сигналла 
0.05, --  length_of_frame , -- длительность кадра
0.05, --  length_of_cycle , -- длительность цикла 
0 , --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
16.2 --  speed_of_data_transfer  -- скорость передачи данных
),

-- Р.сеть N7 (C6)
(
default, -- radio_network_id, -- первичный ключ
'Управления группой охраны E-3C', --  destination_r, -- назначение по русски
'Management of the guard group E-3C',  -- destination_e, -- назначение по английски 
'Управления группой охраны E-3C', --  description_r, -- Описание по русски,
'Management of the guard group E-3C', --  description_e, -- описание по английски 
15, --   descr_cat_of_r_com_signals_id, -- внешний ключ на таблицу descr_cat_of_r_com_signals
0.002, --   freq_of_range_fhm , -- ширина диапазона частот ????
1.1, -- while_single_fr_of_use , -- длительность сигналла на одной частоте 
7, --  width_signal_spectrum , -- ширина спектра сигналла 
0.05, --  length_of_frame , -- длительность кадра
0.05, --  length_of_cycle , -- длительность цикла 
0 , --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
16.2 --  speed_of_data_transfer  -- скорость передачи данных
),

-- Р.сеть N8 (C6)
(
default, -- radio_network_id, -- первичный ключ
'Наведения ДРЛО E-3C N1', --  destination_r, -- назначение по русски
'The guidance of the aircraft E-3C N1',  -- destination_e, -- назначение по английски 
'Наведения ДРЛО E-3C N1', --  description_r, -- Описание по русски,
'The guidance of the aircraft E-3C N1', --  description_e, -- описание по английски 
15, --   descr_cat_of_r_com_signals_id, -- внешний ключ на таблицу descr_cat_of_r_com_signals
0.002, --   freq_of_range_fhm , -- ширина диапазона частот ????
1.1, -- while_single_fr_of_use , -- длительность сигналла на одной частоте 
7, --  width_signal_spectrum , -- ширина спектра сигналла 
0.05, --  length_of_frame , -- длительность кадра
0.05, --  length_of_cycle , -- длительность цикла 
0 , --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
16.2 --  speed_of_data_transfer  -- скорость передачи данных
),

-- Р.сеть N9 (С6)
(
default, -- radio_network_id, -- первичный ключ
'Наведения ДРЛО E-2C N1', --  destination_r, -- назначение по русски
'The guidance of the aircraft E-2C N1',  -- destination_e, -- назначение по английски 
'Наведения ДРЛО E-2C N1', --  description_r, -- Описание по русски,
'The guidance of the aircraft E-2C N1', --  description_e, -- описание по английски 
15, --   descr_cat_of_r_com_signals_id, -- внешний ключ на таблицу descr_cat_of_r_com_signals
0.002, --   freq_of_range_fhm , -- ширина диапазона частот ????
1.1, -- while_single_fr_of_use , -- длительность сигналла на одной частоте 
7, --  width_signal_spectrum , -- ширина спектра сигналла 
0.05, --  length_of_frame , -- длительность кадра
0.05, --  length_of_cycle , -- длительность цикла 
0 , --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
16.2 --  speed_of_data_transfer  -- скорость передачи данных
),

-- Р.сеть N10 (C9)
(
default, -- radio_network_id, -- первичный ключ
'Передачи данных JTIDS', --  destination_r, -- назначение по русски
'JTIDS Data Communications',  -- destination_e, -- назначение по английски 
'Передачи данных JTIDS', --  description_r, -- Описание по русски,
'JTIDS Data Communications', --  description_e, -- описание по английски 
21, --   descr_cat_of_r_com_signals_id, -- внешний ключ на таблицу descr_cat_of_r_com_signals
255, --   freq_of_range_fhm , -- ширина диапазона частот ????
6.4e-6, -- while_single_fr_of_use , -- длительность сигналла на одной частоте 
3500, --  width_signal_spectrum , -- ширина спектра сигналла 
7.812e-6, --  length_of_frame , -- длительность кадра
12, --  length_of_cycle , -- длительность цикла 
1535 , --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
156 --  speed_of_data_transfer  -- скорость передачи данных
), 

-- Р.сеть N11 (C3) 
(
default, -- radio_network_id, -- первичный ключ
'Взаимодействия с воздушными маяками УВД', --  destination_r, -- назначение по русски
'Interactions with air traffic control beacons',  -- destination_e, -- назначение по английски 
'Взаимодействия с воздушными маяками УВД', --  description_r, -- Описание по русски,
'Interactions with air traffic control beacons', --  description_e, -- описание по английски 
11, --   descr_cat_of_r_com_signals_id, -- внешний ключ на таблицу descr_cat_of_r_com_signals
0.002, --   freq_of_range_fhm , -- ширина диапазона частот ????
1.05, -- while_single_fr_of_use , -- длительность сигналла на одной частоте 
7, --  width_signal_spectrum , -- ширина спектра сигналла 
0.05, --  length_of_frame , -- длительность кадра
0.05, --  length_of_cycle , -- длительность цикла 
0 , --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
16.2 --  speed_of_data_transfer  -- скорость передачи данных
),

-- Р.сеть N12 (С4)
(
default, -- radio_network_id, -- первичный ключ
'Управления воздушным движением', --  destination_r, -- назначение по русски
'Air Traffic Control',  -- destination_e, -- назначение по английски 
'Управления воздушным движением', --  description_r, -- Описание по русски,
'Air Traffic Control', --  description_e, -- описание по английски 
12, --   descr_cat_of_r_com_signals_id, -- внешний ключ на таблицу descr_cat_of_r_com_signals
0.002, --   freq_of_range_fhm , -- ширина диапазона частот ????
1.05, -- while_single_fr_of_use , -- длительность сигналла на одной частоте 
7, --  width_signal_spectrum , -- ширина спектра сигналла 
0.05, --  length_of_frame , -- длительность кадра
0.05, --  length_of_cycle , -- длительность цикла 
0 , --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
0.05 --  speed_of_data_transfer  -- скорость передачи данных
),

-- Р.сеть N13 (С5) 
(
default, -- radio_network_id, -- первичный ключ
'Взаимодействия с кораблями УАНГ', --  destination_r, -- назначение по русски
'Interactions with the ships of the shock carrier group',  -- destination_e, -- назначение по английски 
'Взаимодействия с кораблями УАНГ', --  description_r, -- Описание по русски,
'Interactions with the ships of the shock carrier group', --  description_e, -- описание по английски 
13, --   descr_cat_of_r_com_signals_id, -- внешний ключ на таблицу descr_cat_of_r_com_signals
0.002, --   freq_of_range_fhm , -- ширина диапазона частот ????
1.05, -- while_single_fr_of_use , -- длительность сигналла на одной частоте 
5, --  width_signal_spectrum , -- ширина спектра сигналла 
0.05, --  length_of_frame , -- длительность кадра
0.05, --  length_of_cycle , -- длительность цикла 
0 , --  quantity_frame_in_cykle,  -- количество кадров в цикле ,
0.05 --  speed_of_data_transfer  -- скорость передачи данных
);