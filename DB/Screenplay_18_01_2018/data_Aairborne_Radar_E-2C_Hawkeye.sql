﻿-- Описание режимов работы БРЛС воздушного объекта  "E-2C Hawkeye" 
-- Самолет ДРЛО E-2C Hawkeye

--------------------------------------------------------------------------------- 
Insert into re_situation.m_airborne_radar_in_air_obj
(
m_airborne_radar_in_air_obj_id , -- Искуственный первичный ключ 
air_object_id , -- внешний ключ на воздушный объект 
placing_f_t_aircraft_id , -- внешний ключ на табл. размещение РЭС на типе ЛА
d_m_airborne_radar_id, -- внешний ключ на описание режима работы 
duration_puls, -- длительность зондирующего импульса, мкс 
width_spectrum_puls, -- Ширина спектра зондирующего импульса, МГц 
cycle_time_in_pack, -- Период повторения зондирующих импульсов, мкс
freq_pulses_in_a_pack, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
duration_pack, -- Длительность КГПИ, мс
period_pack, -- Мин. Период следования КГПИ, мс
q_of_carrier_freq_in_pack, -- Количество несущих ча-стот в пачке, шт
r_ch_freq_of_pack_in_overview, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
dur_of_r_irradiation_obj, -- Длительность облучения объ-екта , мс
period_of_r_irradiation_obj -- Период облу-чения объекта (цикл скани-рования), с
 )
 values 
--------------------------------------------------------------------------------- 
-- описание режимов работы БРЛС воздушного объекта  "E-2C Hawkeye"
-- Самолет ДРЛО E-2C Hawkeye
--------------------------
-- БРЛС типа "AN/APS-145" условное наименование "ЛА-"E-2C" БРЛС-"AN/APS-145" №1"
-- режим работы "Режим Р1"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'
), -- внешний ключ на воздушный объект 'E-2C Hawkeye'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"E-2C" БРЛС-"AN/APS-145" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APS-145'
and abr_name_mode_r like 'Режим Р1'
), -- внешний ключ на описание режима работы "Режим Р1" БРЛC "AN/APS-145" 
19.0, -- длительность зондирующего импульса, мкс 
1.3, -- Ширина спектра зондирующего импульса, МГц 
4600, -- Период повторения зондирующих импульсов, мкс
4.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
230.0, -- Длительность КГПИ, мс
12000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
230.0, -- Длительность облучения объ-екта , мс
12.0 -- Период облучения объекта (цикл скани-рования), с
),

--------------------------
-- БРЛС типа "AN/APS-145" условное наименование "ЛА-"E-2C" БРЛС-"AN/APS-145" №1"
-- режим работы "Режим Р2"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'
), -- внешний ключ на воздушный объект 'E-2C Hawkeye'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"E-2C" БРЛС-"AN/APS-145" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APS-145'
and abr_name_mode_r like 'Режим Р2'
), -- внешний ключ на описание режима работы "Режим Р1" БРЛC "AN/APS-145" 
19.0, -- длительность зондирующего импульса, мкс 
1.3, -- Ширина спектра зондирующего импульса, МГц 
4600, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
395.0, -- Длительность КГПИ, мс
20000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
395.0, -- Длительность облучения объ-екта , мс
20.0 -- Период облучения объекта (цикл скани-рования), с
),

--------------------------
-- БРЛС типа "AN/APS-145" условное наименование "ЛА-"E-2C" БРЛС-"AN/APS-145" №1"
-- режим работы "Режим Р3"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'
), -- внешний ключ на воздушный объект 'E-2C Hawkeye'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"E-2C" БРЛС-"AN/APS-145" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APS-145'
and abr_name_mode_r like 'Режим Р3'
), -- внешний ключ на описание режима работы "Режим Р1" БРЛC "AN/APS-145" 
16.0, -- длительность зондирующего импульса, мкс 
0.95, -- Ширина спектра зондирующего импульса, МГц 
3800, -- Период повторения зондирующих импульсов, мкс
12.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
195.0, -- Длительность КГПИ, мс
10000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
195.0, -- Длительность облучения объ-екта , мс
10.0 -- Период облучения объекта (цикл скани-рования), с
),

--------------------------
-- БРЛС типа "AN/APS-145" условное наименование "ЛА-"E-2C" БРЛС-"AN/APS-145" №1"
-- режим работы "Режим Р4"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'
), -- внешний ключ на воздушный объект 'E-2C Hawkeye'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"E-2C" БРЛС-"AN/APS-145" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APS-145'
and abr_name_mode_r like 'Режим Р4'
), -- внешний ключ на описание режима работы "Режим Р4" БРЛC "AN/APS-145" 
15.0, -- длительность зондирующего импульса, мкс 
0.95, -- Ширина спектра зондирующего импульса, МГц 
3800, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
395.0, -- Длительность КГПИ, мс
20000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
395.0, -- Длительность облучения объ-екта , мс
20.0 -- Период облучения объекта (цикл скани-рования), с
),

--------------------------
-- БРЛС типа "AN/APS-145" условное наименование "ЛА-"E-2C" БРЛС-"AN/APS-145" №1"
-- режим работы "Режим Р5"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'
), -- внешний ключ на воздушный объект 'E-2C Hawkeye'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"E-2C" БРЛС-"AN/APS-145" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APS-145'
and abr_name_mode_r like 'Режим Р5'
), -- внешний ключ на описание режима работы "Режим Р5" БРЛC "AN/APS-145" 
13.0, -- длительность зондирующего импульса, мкс 
0.95, -- Ширина спектра зондирующего импульса, МГц 
3600, -- Период повторения зондирующих импульсов, мкс
8.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
230.0, -- Длительность КГПИ, мс
12000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
230.0, -- Длительность облучения объ-екта , мс
12.0 -- Период облучения объекта (цикл скани-рования), с
),

--------------------------
-- БРЛС типа "AN/APS-145" условное наименование "ЛА-"E-2C" БРЛС-"AN/APS-145" №1"
-- режим работы "Режим Р6"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'
), -- внешний ключ на воздушный объект 'E-2C Hawkeye'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"E-2C" БРЛС-"AN/APS-145" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APS-145'
and abr_name_mode_r like 'Режим Р6'
), -- внешний ключ на описание режима работы "Режим Р6" БРЛC "AN/APS-145" 
11.0, -- длительность зондирующего импульса, мкс 
1.10, -- Ширина спектра зондирующего импульса, МГц 
3600, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
395.0, -- Длительность КГПИ, мс
20000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
400.0, -- Длительность облучения объ-екта , мс
20.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APS-145" условное наименование "ЛА-"E-2C" БРЛС-"AN/APS-145" №1"
-- режим работы "Режим Р7"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'
), -- внешний ключ на воздушный объект 'E-2C Hawkeye'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"E-2C" БРЛС-"AN/APS-145" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APS-145'
and abr_name_mode_r like 'Режим Р7'
), -- внешний ключ на описание режима работы "Режим Р7" БРЛC "AN/APS-145" 
9.0, -- длительность зондирующего импульса, мкс 
1.250, -- Ширина спектра зондирующего импульса, МГц 
4000, -- Период повторения зондирующих импульсов, мкс
10.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
195.0, -- Длительность КГПИ, мс
10000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
195.0, -- Длительность облучения объ-екта , мс
10.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APS-145" условное наименование "ЛА-"E-2C" БРЛС-"AN/APS-145" №1"
-- режим работы "Режим Р8"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'
), -- внешний ключ на воздушный объект 'E-2C Hawkeye'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"E-2C" БРЛС-"AN/APS-145" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APS-145'
and abr_name_mode_r like 'Режим Р8'
), -- внешний ключ на описание режима работы "Режим Р8" БРЛC "AN/APS-145" 
7.0, -- длительность зондирующего импульса, мкс 
1.450, -- Ширина спектра зондирующего импульса, МГц 
4000, -- Период повторения зондирующих импульсов, мкс
10.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
395.0, -- Длительность КГПИ, мс
20000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
395.0, -- Длительность облучения объ-екта , мс
20.0 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------
-- БРЛС типа "AN/APS-145" условное наименование "ЛА-"E-2C" БРЛС-"AN/APS-145" №1"
-- режим работы "Режим Р9"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'
), -- внешний ключ на воздушный объект 'E-2C Hawkeye'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"E-2C" БРЛС-"AN/APS-145" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APS-145'
and abr_name_mode_r like 'Режим Р9'
), -- внешний ключ на описание режима работы "Режим Р9" БРЛC "AN/APS-145" 
6.0, -- длительность зондирующего импульса, мкс 
1.10, -- Ширина спектра зондирующего импульса, МГц 
4000, -- Период повторения зондирующих импульсов, мкс
14.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
300.0, -- Длительность КГПИ, мс
19000.0, -- Период следования КГПИ, мс
3, -- Количество несущих ча-стот в пачке, шт
3, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
300.0, -- Длительность облучения объ-екта , мс
19.0 -- Период облучения объекта (цикл сканирования), с
); 


--------------------------------------------------------------------------------- 
-- Задание рабочих частот для БРЛС "ЛА-"E-2C" БРЛС-"AN/APS-145" №1" 
-- воздушного объекта "E-2C Hawkeye"

insert into re_situation.oper_freq_air_objects
(
m_airborne_radar_in_air_obj_id , -- Внешний ключ 
operating_frequency  -- значение рабочей частоты 
)
values 
-- Рабочая частота для режима "Режим Р1"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'E-2C Hawkeye'
and pfta.code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
and dmar.abr_name_mode_r like 'Режим Р1'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
0.43 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р2"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'E-2C Hawkeye'
and pfta.code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
and dmar.abr_name_mode_r like 'Режим Р2'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
0.43 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р3"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'E-2C Hawkeye'
and pfta.code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
and dmar.abr_name_mode_r like 'Режим Р3'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
0.43 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р4"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'E-2C Hawkeye'
and pfta.code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
and dmar.abr_name_mode_r like 'Режим Р4'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
0.43 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р5"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'E-2C Hawkeye'
and pfta.code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
and dmar.abr_name_mode_r like 'Режим Р5'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
0.43 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р6"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'E-2C Hawkeye'
and pfta.code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
and dmar.abr_name_mode_r like 'Режим Р6'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
0.43 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р7"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'E-2C Hawkeye'
and pfta.code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
and dmar.abr_name_mode_r like 'Режим Р7'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
0.43 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р8"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'E-2C Hawkeye'
and pfta.code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
and dmar.abr_name_mode_r like 'Режим Р8'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
0.43 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р9"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'E-2C Hawkeye'
and pfta.code_name like 'ЛА-"E-2C" БРЛС-"AN/APS-145" №1'
and dmar.abr_name_mode_r like 'Режим Р9'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
0.44 -- значение рабочей частоты 
);

  