﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния 
)
values
-- точка P1	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'0',		131.30681, 31.48096	,	0	,	358.822644043231	,	80	,	7.99520677,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P2	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'125.0749391',	131.30464, 31.57092	,	1000	,	321.949799632081	,	100	,	7.154476592,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P3	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'264.8475678',	131.21363, 31.66985	,	2000	,	276.425674875807	,	120	,	7.579802898,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P4	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'396.7771197',	131.04743, 31.68567	,	3000	,	219.487274898466	,	140	,	8.184473652,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P5	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'518.9596867',	130.93268, 31.56696	, 	4000	,	93.4078072912647	,	160	,	8.435856533,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P6	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'637.5012949',	130.94455, 31.39681	,	5000	,	119.996413843878	,	180	,	8.85171027,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P7	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'750.4738131',	131.05139, 31.23852	,	6000	,	108.756849525408	,	200	,	8.113681757,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P8	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'873.7224233',	131.13449, 31.02880	,	7000	,	98.5692414459127	,	220	,	8.66364774,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P9	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'989.1472455',	131.17406, 30.80324	,	8000	,	187.819439271829	,	250	,	8.147122,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P10	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'1111.889976',	131.13053, 30.53020	,	9000	,	207.545657604577	,	250	,	10.48226402,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P11	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'1207.289214',	131.01577, 30.34026	,	10000	,	208.902116389516	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=136 )
),
-- точка P12	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'1301.899299',	130.89706, 30.15428	,	10000	,	242.496790616587	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=136 )
),
-- точка P13	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'1408.563413',	130.65172, 30.04348	,	10000	,	261.115815487687	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=138 )
),
-- точка P14	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'1510.536876',	130.39055, 30.00787	,	10000	,	270.977696058689	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=138 )
),
-- точка P15	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'1564.134005',	130.25163, 30.00985	,	10000	,	298.563656273834	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=138 )
),
-- точка P16	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'2281.543117',	128.60548, 30.76961	,	10000	,	299.910298950744	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=138 )
),
-- точка P17	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'2954.228822',	127.07013, 31.51354	,	10000	,	336.7923541755  	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=138 )
),
-- точка P18	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'3026.163733',	126.99537, 31.66193	,	10000	,	5.64304402779314	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P19	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'3102.336067',	127.01516, 31.83209	,	10000	,	63.3366052959042	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P20	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'3181.03358',	127.20114, 31.91123	,	10000	,	173.719553823733	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P21	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'3292.465998',	127.49396, 31.88353	,	10000	,	159.196959721751	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P22	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'3484.937012',	127.96882, 31.72920	,	10000	,	148.848328847915	,	250	,	-4.908458631,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P23	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'3688.666956',	128.42784, 31.49178	,	9000	,	151.257876658535	,	240	,	-4.695658736,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P24	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'3901.629621',	128.89873, 31.27018	,	8000	,	153.263731199934	,	220	,	-4.339028503,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P25	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'4132.095957',	129.37358, 31.06441	,	7000	,	153.053748221108	,	200	,	-3.626400789,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P26	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'4407.851467',	129.88800, 30.83886	,	6000	,	87.4643775195948	,	180	,	-4.324084612,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P27	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'4639.114287',	130.32328, 30.85468	,	5000	,	82.0163512322184	,	160	,	-4.992199778,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P28	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'4839.426784',	130.65568, 30.89426	,	4000	,	65.838202726581 	,	140	,	-3.602307352,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P29	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'5117.026639',	131.02765, 31.03671	,	3000	,	50.2767517481495	,	120	,	-3.95438606,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P30	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'5369.910395',	131.27298, 31.21082	,	2000	,	13.5406339861055	,	100	,	-5.66368732,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P31	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'5546.473814',	131.31651, 31.36515	,	1000	,	355.914319196458	,	80	,	-6.196099596,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P32	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'), 	'5707.865668',	131.30681, 31.48096	,	0	,	355.914319196458	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
);
