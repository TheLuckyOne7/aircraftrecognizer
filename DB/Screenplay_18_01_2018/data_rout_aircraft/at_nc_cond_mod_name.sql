﻿-- воздушные объекты категории сигналов средств связи, соответствующие состоянию типа ЛА  
/*
select ao.air_object_name, ta.code_name_e, ac.condition_name_r,  ac.aircraft_condition_id, crcs.name_category_e
from re_situation.air_object as ao
join re_situation.type_of_aircraft as ta on ao.type_of_aircraft_id=ta.type_of_aircraft_id
join re_situation.aircraft_condition as ac on ta.type_of_aircraft_id=ac.type_of_aircraft_id
join re_situation.aircraft_fc_signals as afcs on ac.aircraft_condition_id=afcs.aircraft_condition_id
join re_situation.cat_of_radio_comm_signals as crcs on crcs.cat_of_radio_comm_signals_id=afcs.cat_of_radio_comm_signals_id
order by 1, 4, 5 
 
-- where ac.aircraft_condition_id=18
-- */

-- Номер состояния в ТИПД и РР РЭС на маршрутах полета
/*
select s1.situation_id, s1.operative_time, s1.air_object_id, ao.air_object_name, s1.aircraft_condition_id
from re_situation.situation as s1
join re_situation.air_object as ao on ao.air_object_id=s1.air_object_id
-- */

-- список радиосетей и категорий сигналлов в них 
/*
select rn.destination_r, crs.name_category_e,  crs.cat_of_radio_comm_signals_id
from re_situation.radio_network as rn
join re_situation.descr_cat_of_r_com_signals as dccs on rn.descr_cat_of_r_com_signals_id=dccs.descr_cat_of_r_com_signals_id
join re_situation.cat_of_radio_comm_signals as crs on crs.cat_of_radio_comm_signals_id=dccs.cat_of_radio_comm_signals_id
-- */

---------------------------------------------------------------------------------
-- использование радиосетей воздушными объектами, категории сигналов в радиосетях 
-- План радиосвязи 
/*
select unwao.use_networks_air_objects_id, ao.air_object_name, ao.air_object_id, 
ta.code_name_e,  ta.type_of_aircraft_id, pfa.code_name, rnw.destination_r, 
dccs.descr_cat_of_r_com_signals_id, crcs.name_category_e
from re_situation.use_networks_air_objects as unwao 
join re_situation.air_object as ao on ao.air_object_id=unwao.air_object_id
join re_situation.type_of_aircraft as ta on ta.type_of_aircraft_id=ao.type_of_aircraft_id
join re_situation.placing_f_t_aircraft as pfa on pfa.placing_f_t_aircraft_id=unwao.placing_f_t_aircraft_id
join re_situation.radio_network as rnw on rnw.radio_network_id=unwao.radio_network_id
join re_situation.descr_cat_of_r_com_signals as dccs on dccs.descr_cat_of_r_com_signals_id=rnw.descr_cat_of_r_com_signals_id
join re_situation.cat_of_radio_comm_signals as crcs on crcs.cat_of_radio_comm_signals_id=dccs.cat_of_radio_comm_signals_id
--*/

-----------------------------------------------------------------------
-- категории сигналов средств связи, соответствующие состоянию типа ЛА  
/*
select ta.type_of_aircraft_id, ta.code_name_e, ac.aircraft_condition_id, ac.condition_name_r, 
crcs.cat_of_radio_comm_signals_id, crcs.name_category_e 
from re_situation.aircraft_fc_signals as afcs 
join re_situation.aircraft_condition as ac on ac.aircraft_condition_id=afcs.aircraft_condition_id
join re_situation.type_of_aircraft as ta on ta.type_of_aircraft_id=ac.type_of_aircraft_id
join re_situation.cat_of_radio_comm_signals as crcs on crcs.cat_of_radio_comm_signals_id=afcs.cat_of_radio_comm_signals_id

-- */


-- /*
select '( '|| z1.situation_id ||', '|| z3.use_networks_air_objects_id || ' ),  --  ', 
z1.operative_time, z1.air_object_id, z1.air_object_name, z1.aircraft_condition_id 
, z2.condition_name_r, z2.name_category_e
,  z3.pfa_code_name, z3.destination_r
from 
(
select s1.situation_id as situation_id , s1.operative_time as operative_time , 
s1.air_object_id as air_object_id, ao.air_object_name as air_object_name, s1.aircraft_condition_id as aircraft_condition_id 
from re_situation.situation as s1
join re_situation.air_object as ao on ao.air_object_id=s1.air_object_id
) as z1
join 
(
select ta.type_of_aircraft_id as type_of_aircraft_id , ta.code_name_e as code_name_e, 
ac.aircraft_condition_id as aircraft_condition_id , ac.condition_name_r as  condition_name_r,
crcs.cat_of_radio_comm_signals_id as cat_of_radio_comm_signals_id , crcs.name_category_e as name_category_e
from re_situation.aircraft_fc_signals as afcs 
join re_situation.aircraft_condition as ac on ac.aircraft_condition_id=afcs.aircraft_condition_id
join re_situation.type_of_aircraft as ta on ta.type_of_aircraft_id=ac.type_of_aircraft_id
join re_situation.cat_of_radio_comm_signals as crcs on crcs.cat_of_radio_comm_signals_id=afcs.cat_of_radio_comm_signals_id
) as z2 
on z2.aircraft_condition_id=z1.aircraft_condition_id
join 
(
select unwao.use_networks_air_objects_id as use_networks_air_objects_id, 
ao.air_object_name as air_object_name, ao.air_object_id as air_object_id, 
ta.code_name_e as code_name_e,  ta.type_of_aircraft_id as type_of_aircraft_id, 
pfa.code_name as pfa_code_name , 
rnw.destination_r as destination_r, 
dccs.descr_cat_of_r_com_signals_id as descr_cat_of_r_com_signals_id, 
crcs.cat_of_radio_comm_signals_id as cat_of_radio_comm_signals_id, crcs.name_category_e as name_category_e
from re_situation.use_networks_air_objects as unwao 
join re_situation.air_object as ao on ao.air_object_id=unwao.air_object_id
join re_situation.type_of_aircraft as ta on ta.type_of_aircraft_id=ao.type_of_aircraft_id
join re_situation.placing_f_t_aircraft as pfa on pfa.placing_f_t_aircraft_id=unwao.placing_f_t_aircraft_id
join re_situation.radio_network as rnw on rnw.radio_network_id=unwao.radio_network_id
join re_situation.descr_cat_of_r_com_signals as dccs on dccs.descr_cat_of_r_com_signals_id=rnw.descr_cat_of_r_com_signals_id
join re_situation.cat_of_radio_comm_signals as crcs on crcs.cat_of_radio_comm_signals_id=dccs.cat_of_radio_comm_signals_id
) as z3 
on z3.cat_of_radio_comm_signals_id=z2.cat_of_radio_comm_signals_id and z3.air_object_id=z1.air_object_id
-- where z1.situation_id=56
order by z1.situation_id, z1.aircraft_condition_id, z2.name_category_e
 

-- */