﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)
values 
/* точка P0 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'0'		,	125.01986, 28.67674	,	0	,	358.960948668194	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P1 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'510'		,	125.01986, 28.67674	,	0	,	358.960948668194	,	200	,	2.512160215,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P2 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'908.0637835'	,	125.00496, 29.39254	,	1000	,	0.867030057127593	,	272.554	,	3.10389559,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P3 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'1230.239569'	,	125.01878, 30.18196	,	2000	,	357.273375929323	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P4 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'1858.904575'	,	124.92381, 31.87559	,	2000	,	329.900006375835	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P5 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'2164.559972'	,	124.43313, 32.58787	,	2000	,	318.443342769538	,	300	,	-3.766720598,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P6 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'2629.15509'	,	123.43595, 33.52174	,	250	,	316.507827425497	,	300	,	-0.401658939,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P7 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'3002.606258'	,	122.59705, 34.24985	,	100	,	305.070929714296	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P8 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'3414.232626'	,	121.48906, 34.88298	,	100	,	292.761207696524	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P9 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'3705.430247'	,	120.60267, 35.18372	,	100	,	116.106406585289	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P10 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'3845.812526'	,	120.1874, 35.01635	,	100	,	127.013336176835	,	300	,	5.510661837,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P11 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'4009.132292'	,	119.75922, 34.75036	,	1000	,	120.750340635112	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P12 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'4172.291469'	,	119.37646, 34.56223	,	1000	,	269.928230671231	,	250	,	-9.710378678,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P13 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'4264.975805'	,	119.12344, 34.56223	,	100	,	322.925072801745	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P14 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'4379.59898'	,	118.89638, 34.80875	,	100	,	359.376267453062	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P15 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'4559.959613'	,	118.88989, 35.29532	,	100	,	26.6827144970877	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P16 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'4751.174387'	,	119.17535, 35.75593	,	100	,	44.6470273090601	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P17 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'4917.116964'	,	119.5646, 36.07382	,	100	,	49.6804642930731	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P18 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'5069.4911'	,	119.95371, 36.33919	,	100	,	63.2324124761334	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P19 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'5213.420596'	,	120.38508, 36.5133	,	100	,	138.771875152727	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P20 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'5364.412419'	,	120.76496, 36.24422	,	100	,	136.222364753389	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P21 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'5634.827325'	,	121.41392, 35.73771	,	100	,	133.870549401849	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P22 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'6128.558301'	,	122.53773, 34.77218	,	100	,	127.476999720865	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P23 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'6585.349971'	,	123.43995, 33.79082	,	100	,	132.177515567837	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P24 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'7003.291458'	,	124.34216, 32.95192	,	100	,	126.555075355495	,	260	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P25 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'7465.562759'	,	125.10193, 32.08136	,	100	,	112.792836011085	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P26 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'7885.227799'	,	125.52929, 31.2108	,	100	,	93.1388306210464	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P27 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'8442.231704'	,	125.60843, 29.96036	,	100	,	178.678587324212	,	250	,	3.549566724,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'8977.508302'	,	125.57678, 28.75741	,	2000	,	98.8991582081584	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P29 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'9496.893799'	,	125.78055, 27.60394	,	2000	,	141.676899407043	,	250	,	-6.194452922,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P30 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'9658.328555'	,	125.52729, 27.31903	,	1000	,	294.32162394832 	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P31 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'9795.476517'	,	125.21072, 27.44565	,	1000	,	332.879006848254	,	250	,	-3.325360744,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P32 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'9945.836144'	,	125.03661, 27.74639	,	500	,	359.095013662815	,	200	,	-0.966489134,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P33 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №6 ADSG'), 	'10463.17253'	,	125.01986, 28.67674	,	0	,	359.095013662815	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
);

