﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)
values 
/* точка P0 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'0'     	,	125.01986, 28.67674	,	0	,	358.960948668194	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P1 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'570'   	,	125.01986, 28.67674	,	0	,	358.960948668194	,	200	,	2.512160215,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P2 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'968.0637835'	,	125.00496, 29.39254	,	1000	,	0.867030057127593	,	272.554	,	3.10389559,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P3 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'1290.239569'	,	125.01878, 30.18196	,	2000	,	357.273375929323	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P4 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'1918.904575'	,	124.92381, 31.87559	,	2000	,	329.900006375835	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P5 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'2224.559972'	,	124.43313, 32.58787	,	2000	,	318.443342769538	,	300	,	-3.766720598,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P6 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'2689.15509'	,	123.43595, 33.52174	,	250	,	316.507827425497	,	300	,	-0.401658939,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P7 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'3062.606258'	,	122.59705, 34.24985	,	100	,	305.070929714296	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P8 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'3474.232626'	,	121.48906, 34.88298	,	100	,	109.339145206795	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P9 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'3714.520918'	,	120.74533, 34.66603	,	100	,	102.760051000812	,	300	,	4.92443769,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P10 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'3897.282902'	,	120.16145, 34.55574	,	1000	,	111.210585780271	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P11 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'4142.625669'	,	119.53865, 34.35462	,	1000	,	93.3551267037247	,	250	,	-6.283859889,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P12 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'4285.849741'	,	119.14939, 34.33516	,	100	,	304.803100699839	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P13 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'4404.081494'	,	118.83151, 34.51681	,	100	,	354.144433366385	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P14 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'4539.453823'	,	118.78609, 34.88012	,	100	,	8.07528743637928	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P15 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'4707.045042'	,	118.86394, 35.32776	,	100	,	27.5253756850774	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P16 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'4896.998503'	,	119.15588, 35.78188	,	100	,	46.1144522050599	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P17 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'5053.407903'	,	119.53216, 36.07382	,	100	,	51.9155253430073	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P18 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'5213.314627'	,	119.95371, 36.33919	,	100	,	62.7066978311643	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P19 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'5357.912017'	,	120.38508, 36.5173	,	100	,	138.770424336763	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P20 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'5508.899463'	,	120.76496, 36.24822	,	100	,	136.22093131252 	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P21 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'5779.307266'	,	121.41392, 35.74171	,	100	,	133.869169433857	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P22 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'6273.026677'	,	122.53773, 34.77618	,	100	,	133.869169433857	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P23 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'6766.746087'	,	123.43995, 33.79482	,	100	,	132.176232008564	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P24 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'7184.678992'	,	124.34216, 32.95592	,	100	,	126.553880342799	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P25 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'7665.433628'	,	125.10193, 32.08536	,	100	,	112.791971575821	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P26 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'8085.095983'	,	125.52929, 31.2148	,	100	,	93.138704556886 	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P27 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'8642.09982'	,	125.60843, 29.96436	,	100	,	178.678637935841	,	250	,	3.549566797,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'9177.376407'	,	125.57678, 28.76141	,	2000	,	98.9696479222708	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P29 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'9697.761862'	,	125.78255, 27.60594	,	2000	,	141.652426061732	,	250	,	-6.149238902,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P30 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'9860.383613'	,	125.52729, 27.31903	,	1000	,	294.32162394832 	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P31 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'9997.531575'	,	125.21072, 27.44565	,	1000	,	332.879006848254	,	250	,	-3.325360744,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P32 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'10147.8912'	,	125.03661, 27.74639	,	500	,	359.095013662815	,	200	,	-0.966489134,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P33 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'), 	'10665.22759'	,	125.01986, 28.67674	,	0	,	359.095013662815	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
);


