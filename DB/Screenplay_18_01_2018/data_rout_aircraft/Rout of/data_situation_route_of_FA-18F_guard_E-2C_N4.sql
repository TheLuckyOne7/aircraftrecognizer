﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)
values
/*Точка P0 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '0'         ,	125.01986, 28.67674	,	0	,	0.115395740752675	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P1 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '240'         ,	125.01986, 28.67674	,	0	,	0.115395740752675	,	80	,	1.801967189,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P2 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '794.9490611' ,	125.02078,29.07597	,	1000	,	17.7444669861546	,	100	,	1.73079885,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P3 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '1372.716951' ,	125.20281,29.57061	,	2000	,	19.349407793732 	,	120	,	1.933342186,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P4 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '1889.955962' ,	125.41649,30.09690	,	3000	,	17.4805210711346	,	140	,	2.135403444,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P5 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '2358.251545' ,	125.62226,30.65881	,	4000	,	31.0811683653277	,	160	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P6 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '2673.358696' ,	125.89530,31.04660	,	4000	,	16.4482365287221	,	180	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P7 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '2938.652223' ,	126.03776,31.45814	,	4000	,	335.58545150743 	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P8 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '3182.941403' ,	125.82407,31.85781	,	4000	,	291.523592825597	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P9 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '3345.557187' ,	125.50355,31.96465	,	4000	,	95.7724916981528	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P10 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '3538.978112' ,	125.09597,31.92903	,	4000	,	137.871354201487	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P11 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '3731.698831' ,	124.82293,31.67182	,	4000	,	95.607724282297 	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P12 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '3924.13639' ,	124.86250,31.32756	,	4000	,	116.727217105761	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P13 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '4150.789183' ,	125.07618,30.96350	,	4000	,	97.9381625607957	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P14 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '4384.162678' ,	125.14345,30.54801	,	4000	,	154.964377857735	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P15 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '4563.899321' ,	124.98517,30.25519	,	4000	,	112.898087478145	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P16 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '4749.927525' ,	124.62903,30.12460	,	4000	,	284.856872837952	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P17 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '4948.705144' ,	124.22937,30.21561	,	4000	,	334.010189100638	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P18 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '5152.062225' ,	124.04338,30.54405	,	4000	,	357.076264223957	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P19 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '5337.226079' ,	124.02360,30.87645	,	4000	,	35.3982914874537	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P20 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '5512.887615' ,	124.23728,31.13366	,	4000	,	65.177172582464 	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P21 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '5660.120159' ,	124.51823,31.24446	,	4000	,	154.017256057436	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P22 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '5840.454394' ,	124.85854,31.10200	,	4000	,	166.939494097615	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P23 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '5995.360405' ,	125.17511,31.03869	,	4000	,	168.965228765556	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P24 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '6166.502182' ,	125.52729,30.97933	,	4000	,	77.8654445348171	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P25 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '6345.91493' ,	125.89530,31.04660	,	4000	,	16.4482365287221	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P26 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '6584.679104' ,	126.03776,31.45814	,	4000	,	335.58545150743 	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P27 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '6828.968284' ,	125.82407,31.85781	,	4000	,	291.523592825597	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '6991.584069' ,	125.50355,31.96465	,	4000	,	95.7724916981528	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P29 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '7185.004993' ,	125.09597,31.92903	,	4000	,	137.871354201487	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P30 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '7377.725713' ,	124.82293,31.67182	,	4000	,	95.607724282297 	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P31 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '7570.163272' ,	124.86250,31.32756	,	4000	,	116.727217105761	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P32 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '7796.816065' ,	125.07618,30.96350	,	4000	,	97.9381625607957	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P33 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '8030.189559' ,	125.14345,30.54801	,	4000	,	154.964377857735	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P34 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '8209.926203' ,	124.98517,30.25519	,	4000	,	112.898087478145	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P35 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '8395.954407' ,	124.62903,30.12460	,	4000	,	123.442454335259	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P36 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '8686.597124' ,	124.12648,29.83573	,	4000	,	153.755842541168	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P37 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '8890.228849' ,	123.94050,29.50730	,	4000	,	174.315863889898	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P38 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '9162.339859' ,	123.88510,29.02057	,	4000	,	93.0082189896347	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P39 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '9420.260436' ,	123.91280,28.55759	,	4000	,	96.0160719298395	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P40 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '9623.907799' ,	123.95633,28.19354	,	4000	,	107.363124016718	,	180	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P41 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '9834.026017' ,	124.07108,27.86906	,	4000	,	140.868446699235	,	160	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P42 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '10116.93875' ,	124.42722,27.61185	,	4000	,	89.8881225267507	,	140	,	-2.941562048,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P43 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '10456.89418' ,	124.90999,27.61185	,	3000	,	25.5521269344547	,	120	,	-2.560975743,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P44 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '10847.37035' ,	125.11575,27.99173	,	2000	,	353.813601486641	,	100	,	-3.475220334,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P45 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '11135.12189' ,	125.08410,28.24894	,	1000	,	352.494998433702	,	80	,	-1.667164575,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/*Точка P46 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №4 g-d E-2C'), '11734.9427' ,	125.01986, 28.67674	,	0	,	352.494998433702	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
);

