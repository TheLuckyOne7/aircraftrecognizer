﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)
values 
/* точка P0 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'0'     	,	125.01986, 28.67674	,	0	,	358.960948668194	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P1 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'540'   	,	125.01986, 28.67674	,	0	,	358.960948668194	,	200	,	2.512160215,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P2 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'938.0637835'	,	125.00496,29.39254	,	1000	,	0.989953414387777	,	250	,	2.83975214,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P3 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'1290.207193'	,	125.02078,30.18396	,	2000	,	357.273435030041	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P4 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'1918.872168'	,	124.92581,31.87759	,	2000	,	329.900562196318	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P5 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'2224.52586'	,	124.43513,32.58987	,	2000	,	318.444004165865	,	300	,	-3.766758657,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P6 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'2689.116284'	,	123.43795,33.52374	,	250	,	316.508510553609	,	300	,	-0.401663438,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P7 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'3062.563269'	,	122.59905,34.25185	,	100	,	305.071591077945	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P8 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'3474.182954'	,	121.49106,34.88498	,	100	,	109.339572503448	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P9 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'3714.466074'	,	120.74733,34.66803	,	100	,	102.760343557157	,	300	,	4.924550391,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P10 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'3897.223875'	,	120.16345,34.55774	,	1000	,	111.211042791212	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P11 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'4142.561548'	,	119.54065,34.35662	,	1000	,	93.3552035418121	,	250	,	-6.28400923,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P12 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'4285.782216'	,	119.15139,34.33716	,	100	,	304.803746971369	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P13 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'4404.012057'	,	118.83351,34.51881	,	100	,	354.144574867938	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P14 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'4539.384352'	,	118.78809,34.88212	,	100	,	8.07509026778273	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P15 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'4706.975489'	,	118.86594,35.32976	,	100	,	27.5247844700377	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P16 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'4896.927933'	,	119.15788,35.78388	,	100	,	46.1137229020795	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P17 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'5053.33527'	,	119.53416,36.07582	,	100	,	51.9148093080533	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P18 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'5213.239454'	,	119.95571,36.34119	,	100	,	63.0184788388521	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P19 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'5356.100714'	,	120.38308,36.51530	,	100	,	138.771149772717	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P20 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'5507.090348'	,	120.76296,36.24622	,	100	,	136.221648060045	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P21 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'5777.501703'	,	121.41192,35.73971	,	100	,	133.869859443751	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P22 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'6271.226896'	,	122.53573,34.77418	,	100	,	127.476355994218	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P23 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'6728.014589'	,	123.43795,33.79282	,	100	,	132.176873812428	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P24 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'7145.951785'	,	124.34016,32.95392	,	100	,	126.554477870695	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P25 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'7546.58378'	,	125.09993,32.08336	,	100	,	112.792403807343	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P26 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'7966.247478'	,	125.52729,31.21280	,	100	,	93.1387675908787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P27 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'8523.251349'	,	125.60643,29.96236	,	100	,	178.678612629221	,	250	,	3.54956676,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'9058.527941'	,	125.57478,28.75941	,	2000	,	98.9698089262294	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'9578.91363'	,	125.78055,27.60394	,	2000	,	141.676899407043	,	250	,	-6.194452922,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'9740.348387'	,	125.52729,27.31903	,	1000	,	294.32162394832 	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'9877.496349'	,	125.21072,27.44565	,	1000	,	332.879006848254	,	250	,	-3.325360744,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'10027.85598'	,	125.03661,27.74639	,	500	,	359.095013662815	,	200	,	-0.966489134,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №7 ADSG'), 	'10545.19237'	,	125.01986, 28.67674	,	0	,	359.095013662815	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
);


