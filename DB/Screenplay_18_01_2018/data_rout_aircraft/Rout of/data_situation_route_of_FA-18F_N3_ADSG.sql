﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)
values 
/* точка P0 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'0'     	,	125.01986, 28.67674	,	0	,	358.960948668194	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P1 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'360'   	,	125.01986, 28.67674	,	0	,	358.960948668194	,	200	,	2.512160215,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P2 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'758.0637835'	,	125.00496, 29.39254	,	1000	,	0.989953414387777	,	250	,	2.83975214,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P3 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'1110.207193'	,	125.02078, 30.18396	,	2000	,	357.273435030041	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P4 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'1738.872168'	,	124.92581, 31.87759	,	2000	,	329.900562196318	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P5 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'2044.52586'	,	124.43513, 32.58987	,	2000	,	318.444004165865	,	300	,	-3.766758657,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P6 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'2509.116284'	,	123.43795, 33.52374	,	250	,	316.508510553609	,	300	,	-0.401663438,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P7 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'2882.563269'	,	122.59905, 34.25185	,	100	,	305.071591077945	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P8 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'3294.182954'	,	121.49106, 34.88498	,	100	,	292.76171623912 	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P9 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'3585.374493'	,	120.60467, 35.18572	,	100	,	269.81302399609 	,	250	,	3.814762354,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P10 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'3821.300068'	,	119.95571, 35.18572	,	1000	,	308.242376420212	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P11 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'4083.88967'	,	119.38589, 35.54977	,	1000	,	10.7649626810504	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P12 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'4327.612836'	,	119.51252, 36.08794	,	1000	,	54.5611039906763	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P13 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'4522.531682'	,	119.95571, 36.34119	,	1000	,	63.0184788388521	,	250	,	-5.249477919,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P14 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'4693.977303'	,	120.38308, 36.51530	,	100	,	138.771149772717	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P15 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'4875.164864'	,	120.76296, 36.24622	,	100	,	136.221648060045	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P16 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'5199.65849'	,	121.41192, 35.73971	,	100	,	133.869859443751	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P17 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'5792.128722'	,	122.53573, 34.77418	,	100	,	127.476355994218	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P18 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'6340.273953'	,	123.43795, 33.79282	,	100	,	132.176873812428	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P19 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'6841.798588'	,	124.34016, 32.95392	,	100	,	126.554477870695	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P20 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'7322.556983'	,	125.09993, 32.08336	,	100	,	112.792403807343	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P21 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'7742.22068'	,	125.52729, 31.21280	,	100	,	93.1387675908787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P22 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'8299.224551'	,	125.60643, 29.96236	,	100	,	178.678612629221	,	250	,	3.54956676,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P23 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'8834.501143'	,	125.57478, 28.75941	,	2000	,	98.9698089262294	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P24 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'9354.886833'	,	125.78055, 27.60394	,	2000	,	141.676899407043	,	250	,	-6.194452922,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P25 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'9516.321589'	,	125.52729, 27.31903	,	1000	,	294.32162394832 	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P26 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'9653.469551'	,	125.21072, 27.44565	,	1000	,	332.879006848254	,	250	,	-3.325360744,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P27 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'9803.829178'	,	125.03661, 27.74639	,	500	,	359.095013662815	,	200	,	-0.966489134,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
),
/* точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №3 ADSG'), 	'10321.16557'	,	125.01986, 28.67674	,	0	,	359.095013662815	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 ) 
);


