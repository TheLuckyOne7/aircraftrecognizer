﻿insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)

values
/*Точка P1 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '360' ,	129.62344, 33.33176	,	0	,	359.762737219524	,	80	,	9.562651856,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P2 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '516.8602541' ,	129.62288,33.44460	,	1500	,	0	,	100	,	15.48938854,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P3 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '613.7007456' ,	129.62288,33.53166	,	3000	,	320.234059348107	,	120	,	14.44233107,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P4 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '752.1825455' ,	129.50812,33.64641	,	5000	,	285.783314743683	,	140	,	13.61590608,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P5 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '862.3478177' ,	129.34786,33.68400	,	6500	,	270.713835836974	,	160	,	12.47060885,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P6 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '982.6306377' ,	129.14011,33.68598	,	8000	,	95.5108510865669	,	180	,	11.91658595,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P7 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '1108.505616' ,	128.89675,33.66620	,	9500	,	101.009812547814	,	200	,	7.588208819,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P8 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '1240.289022' ,	128.61778,33.62069	,	10500	,	104.997813793904	,	220	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P9 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '1394.13679' ,	128.26560,33.54155	,	10500	,	112.765321771661	,	240	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P10 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '1517.118703' ,	127.97277,33.43866	,	10500	,	110.305169982287	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '1618.384817' ,	127.71754,33.35952	,	10500	,	117.213325272936	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P12 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '1745.239745' ,	127.41482,33.22894	,	10500	,	140.569163377214	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P13 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '1907.116037' ,	127.13981,32.94799	,	10500	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '1952.654671' ,	127.08441,32.85697	,	10500	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '1999.376961' ,	127.08638,32.75211	,	10500	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2076.959403' ,	127.09034,32.57800	,	10500	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2119.488114' ,	127.15366,32.49886	,	10500	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2157.193633' ,	127.24467,32.46324	,	10500	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2203.589997' ,	127.36536,32.44148	,	10500	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2245.262725' ,	127.45637,32.49490	,	10500	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2343.600696' ,	127.65027,32.64329	,	10500	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2442.881978' ,	127.78085,32.83719	,	10500	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2543.186974' ,	127.82834,33.05878	,	10500	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2644.767239' ,	127.88176,33.28236	,	10500	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2741.086507' ,	128.06774,33.43273	,	10500	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2848.187562' ,	128.35067,33.47824	,	10500	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2899.490807' ,	128.48522,33.45251	,	10500	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '2947.549128' ,	128.58810,33.38722	,	10500	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3000.96006' ,	128.62965,33.27247	,	10500	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3059.144251' ,	128.63163,33.14188	,	10500	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3109.824983' ,	128.59206,33.03306	,	10500	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3155.802294' ,	128.50302,32.96183	,	10500	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3210.971679' ,	128.38233,32.89061	,	10500	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3267.55115' ,	128.23790,32.85301	,	10500	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3362.485266' ,	127.98860,32.89259	,	10500	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3460.548973' ,	127.74128,32.96579	,	10500	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3519.182874' ,	127.59685,33.01723	,	10500	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3578.316722' ,	127.43857,33.01921	,	10500	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3638.186705' ,	127.27830,33.01921	,	10500	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3698.893495' ,	127.13981,32.94799	,	10500	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3744.432128' ,	127.08441,32.85697	,	10500	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3791.154419' ,	127.08638,32.75211	,	10500	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3868.73686' ,	127.09034,32.57800	,	10500	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3911.265571' ,	127.15366,32.49886	,	10500	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3948.971091' ,	127.24467,32.46324	,	10500	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '3995.367455' ,	127.36536,32.44148	,	10500	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4037.040183' ,	127.45637,32.49490	,	10500	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4135.378153' ,	127.65027,32.64329	,	10500	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4234.659435' ,	127.78085,32.83719	,	10500	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4334.964431' ,	127.82834,33.05878	,	10500	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4436.544697' ,	127.88176,33.28236	,	10500	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4532.863965' ,	128.06774,33.43273	,	10500	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4639.96502' ,	128.35067,33.47824	,	10500	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4691.268265' ,	128.48522,33.45251	,	10500	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4739.326586' ,	128.58810,33.38722	,	10500	,	106.843829477024	,	250	,	0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4792.737518' ,	128.62965,33.27247	,	10500	,	90.7273544697028	,	250	,	0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4850.921708' ,	128.63163,33.14188	,	10500	,	163.045240494622	,	250	,	0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4901.60244' ,	128.59206,33.03306	,	10500	,	133.622257057787	,	250	,	0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '4947.579751' ,	128.50302,32.96183	,	10500	,	125.075727028043	,	250	,	0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5002.749137' ,	128.38233,32.89061	,	10500	,	107.182287032909	,	250	,	0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5059.328607' ,	128.23790,32.85301	,	10500	,	280.772120282361	,	250	,	0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5154.262723' ,	127.98860,32.89259	,	10500	,	289.491086329259	,	250	,	0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5252.32643' ,	127.74128,32.96579	,	10500	,	293.046829241603	,	250	,	0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5310.960331' ,	127.59685,33.01723	,	10500	,	270.897851209922	,	250	,	0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5370.094179' ,	127.43857,33.01921	,	10500	,	269.956332800396	,	250	,	0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5429.964163' ,	127.27830,33.01921	,	10500	,	121.473582729068	,	250	,	0,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5490.670952' ,	127.13981,32.94799	,	10500	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5536.172122' ,	127.08441,32.85697	,	10500	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5582.894412' ,	127.08638,32.75211	,	10500	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5660.476853' ,	127.09034,32.57800	,	10500	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5703.005564' ,	127.15366,32.49886	,	10500	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5740.711084' ,	127.24467,32.46324	,	10500	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5787.107448' ,	127.36536,32.44148	,	10500	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5828.780176' ,	127.45637,32.49490	,	10500	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '5927.118146' ,	127.65027,32.64329	,	10500	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6026.399429' ,	127.78085,32.83719	,	10500	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6126.704424' ,	127.82834,33.05878	,	10500	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6228.28469' ,	127.88176,33.28236	,	10500	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6324.603958' ,	128.06774,33.43273	,	10500	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6431.705013' ,	128.35067,33.47824	,	10500	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6483.008258' ,	128.48522,33.45251	,	10500	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6531.066579' ,	128.58810,33.38722	,	10500	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6584.477511' ,	128.62965,33.27247	,	10500	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6642.661702' ,	128.63163,33.14188	,	10500	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6693.342433' ,	128.59206,33.03306	,	10500	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6739.319745' ,	128.50302,32.96183	,	10500	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6794.48913' ,	128.38233,32.89061	,	10500	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6851.068601' ,	128.23790,32.85301	,	10500	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '6946.002717' ,	127.98860,32.89259	,	10500	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7044.066424' ,	127.74128,32.96579	,	10500	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7102.700324' ,	127.59685,33.01723	,	10500	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7161.834173' ,	127.43857,33.01921	,	10500	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7221.704156' ,	127.27830,33.01921	,	10500	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7282.410946' ,	127.13981,32.94799	,	10500	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7327.912115' ,	127.08441,32.85697	,	10500	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7374.634405' ,	127.08638,32.75211	,	10500	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7452.216847' ,	127.09034,32.57800	,	10500	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7494.745558' ,	127.15366,32.49886	,	10500	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7532.451077' ,	127.24467,32.46324	,	10500	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7578.847442' ,	127.36536,32.44148	,	10500	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7620.52017' ,	127.45637,32.49490	,	10500	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7718.85814' ,	127.65027,32.64329	,	10500	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7818.139422' ,	127.78085,32.83719	,	10500	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '7918.444418' ,	127.82834,33.05878	,	10500	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8020.024683' ,	127.88176,33.28236	,	10500	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8116.343951' ,	128.06774,33.43273	,	10500	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8223.445007' ,	128.35067,33.47824	,	10500	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8274.748251' ,	128.48522,33.45251	,	10500	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8322.806572' ,	128.58810,33.38722	,	10500	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8376.217504' ,	128.62965,33.27247	,	10500	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8434.401695' ,	128.63163,33.14188	,	10500	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8485.082427' ,	128.59206,33.03306	,	10500	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8531.059738' ,	128.50302,32.96183	,	10500	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8586.229123' ,	128.38233,32.89061	,	10500	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8642.808594' ,	128.23790,32.85301	,	10500	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8737.74271' ,	127.98860,32.89259	,	10500	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8835.806417' ,	127.74128,32.96579	,	10500	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8894.440318' ,	127.59685,33.01723	,	10500	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '8953.574166' ,	127.43857,33.01921	,	10500	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9013.444149' ,	127.27830,33.01921	,	10500	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9074.150939' ,	127.13981,32.94799	,	10500	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9119.652108' ,	127.08441,32.85697	,	10500	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9166.374399' ,	127.08638,32.75211	,	10500	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9243.95684' ,	127.09034,32.57800	,	10500	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9286.485551' ,	127.15366,32.49886	,	10500	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9324.191071' ,	127.24467,32.46324	,	10500	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9370.587435' ,	127.36536,32.44148	,	10500	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9412.260163' ,	127.45637,32.49490	,	10500	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9510.598133' ,	127.65027,32.64329	,	10500	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9609.879415' ,	127.78085,32.83719	,	10500	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9710.184411' ,	127.82834,33.05878	,	10500	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9811.764677' ,	127.88176,33.28236	,	10500	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '9908.083945' ,	128.06774,33.43273	,	10500	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10015.185' ,	128.35067,33.47824	,	10500	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10066.48824' ,	128.48522,33.45251	,	10500	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10114.54657' ,	128.58810,33.38722	,	10500	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10167.9575' ,	128.62965,33.27247	,	10500	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10226.14169' ,	128.63163,33.14188	,	10500	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10276.82242' ,	128.59206,33.03306	,	10500	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10322.79973' ,	128.50302,32.96183	,	10500	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10377.96912' ,	128.38233,32.89061	,	10500	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10434.54859' ,	128.23790,32.85301	,	10500	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10529.4827' ,	127.98860,32.89259	,	10500	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10627.54641' ,	127.74128,32.96579	,	10500	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10686.18031' ,	127.59685,33.01723	,	10500	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10745.31416' ,	127.43857,33.01921	,	10500	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10805.18414' ,	127.27830,33.01921	,	10500	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10865.89093' ,	127.13981,32.94799	,	10500	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P41 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10911.3921' ,	127.08441,32.85697	,	10500	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P42 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '10958.11439' ,	127.08638,32.75211	,	10500	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P43 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '11035.69683' ,	127.09034,32.57800	,	10500	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P44 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '11078.22554' ,	127.15366,32.49886	,	10500	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P45 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '11115.93106' ,	127.24467,32.46324	,	10500	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P46 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '11162.32743' ,	127.36536,32.44148	,	10500	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P47 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '11204.00016' ,	127.45637,32.49490	,	10500	,	81.4265113062944	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P48 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '11341.52406' ,	127.81845,32.54040	,	10500	,	77.0157447448733	,	250	,	-17.09194231,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P49 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '11487.79181' ,	128.19833,32.61361	,	8000	,	75.5878383628898	,	250	,	-12.74582278,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P50 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '11644.70597' ,	128.60393,32.70067	,	6000	,	89.5837493906398	,	240	,	-5.535188422,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P51 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '11825.36829' ,	129.06691,32.70265	,	5000	,	80.6065401118857	,	210	,	-9.095632002,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P52 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '11935.31117' ,	129.31027,32.73628	,	4000	,	54.6551620127634	,	180	,	-9.450188698,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P53 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '12041.12916' ,	129.47647,32.83521	,	3000	,	32.0600571330369	,	150	,	-6.344610836,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P54 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '12198.74324' ,	129.61101,33.01526	,	2000	,	6.36693952019   	,	100	,	-6.021665095,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P55 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '12364.81027' ,	129.63079,33.16365	,	1000	,	357.907953231846	,	80	,	-4.276488021,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
),
/*Точка P56 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №3 guard E-3C'), '12598.647' ,	129.62344, 33.33176	,	0	,	357.907953231846	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=1 )
);
