﻿insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)
values
/*Точка P1*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '270' ,	129.62344, 33.33176	,	0	,	359.762737219524	,	80	,	9.562651856,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 ) 
),
/*Точка P2*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '426.8602541' ,	129.62288,33.44460	,	1500	,	0	,	100	,	15.48938854,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 ) 
),
/*Точка P3 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '523.7007456' ,	129.62288,33.53166	,	3000	,	320.234059348107	,	120	,	14.44233107,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 ) 
),
/*Точка P4 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '662.1825455' ,	129.50812,33.64641	,	5000	,	285.783314743683	,	140	,	13.61590608,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 ) 
),
/*Точка P5 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '772.3478177' ,	129.34786,33.68400	,	6500	,	270.713835836974	,	160	,	12.47060885,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 ) 
),
/*Точка P6 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '892.6306377' ,	129.14011,33.68598	,	8000	,	95.5108510865669	,	180	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 ) 
),
/*Точка P7 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '1018.490818' ,	128.89675,33.66620	,	8000	,	101.009812547814	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 ) 
),
/*Точка P8 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '1150.232919' ,	128.61778,33.62069	,	8000	,	104.997813793904	,	220	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 ) 
),
/*Точка P9 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '1304.020417' ,	128.26560,33.54155	,	8000	,	112.765321771661	,	240	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 ) 
),
/*Точка P10 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '1426.95415' ,	127.97277,33.43866	,	8000	,	110.305169982287	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 ) 
),
/*Точка P11 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '1528.180593' ,	127.71754,33.35952	,	8000	,	117.213325272936	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 ) 
),
/*Точка P12 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '1654.985824' ,	127.41482,33.22894	,	8000	,	140.569163377214	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=40 ) 
),
/*Точка P13 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '1816.7987' ,	127.13981,32.94799	,	8000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '1862.319494' ,	127.08441,32.85697	,	8000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '1909.02348' ,	127.08638,32.75211	,	8000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '1986.575528' ,	127.09034,32.57800	,	8000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2029.087578' ,	127.15366,32.49886	,	8000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2066.778326' ,	127.24467,32.46324	,	8000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2113.156514' ,	127.36536,32.44148	,	8000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2154.812917' ,	127.45637,32.49490	,	8000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2253.112362' ,	127.65027,32.64329	,	8000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2352.354751' ,	127.78085,32.83719	,	8000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2452.620451' ,	127.82834,33.05878	,	8000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2554.160922' ,	127.88176,33.28236	,	8000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2650.442456' ,	128.06774,33.43273	,	8000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2757.501554' ,	128.35067,33.47824	,	8000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2808.7847' ,	128.48522,33.45251	,	8000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2856.824194' ,	128.58810,33.38722	,	8000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2910.214202' ,	128.62965,33.27247	,	8000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '2968.375598' ,	128.63163,33.14188	,	8000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3019.036475' ,	128.59206,33.03306	,	8000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3064.995775' ,	128.50302,32.96183	,	8000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3120.143547' ,	128.38233,32.89061	,	8000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3176.700852' ,	128.23790,32.85301	,	8000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3271.597777' ,	127.98860,32.89259	,	8000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3369.623067' ,	127.74128,32.96579	,	8000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3428.233997' ,	127.59685,33.01723	,	8000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3487.34468' ,	127.43857,33.01921	,	8000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3547.191208' ,	127.27830,33.01921	,	8000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3607.874215' ,	127.13981,32.94799	,	8000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3653.395009' ,	127.08441,32.85697	,	8000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3700.098996' ,	127.08638,32.75211	,	8000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3777.651043' ,	127.09034,32.57800	,	8000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3820.163094' ,	127.15366,32.49886	,	8000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3857.853842' ,	127.24467,32.46324	,	8000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3904.23203' ,	127.36536,32.44148	,	8000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '3945.888432' ,	127.45637,32.49490	,	8000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4044.187878' ,	127.65027,32.64329	,	8000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4143.430266' ,	127.78085,32.83719	,	8000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4243.695966' ,	127.82834,33.05878	,	8000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4345.236437' ,	127.88176,33.28236	,	8000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4441.517971' ,	128.06774,33.43273	,	8000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4548.577069' ,	128.35067,33.47824	,	8000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4599.860215' ,	128.48522,33.45251	,	8000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4647.899709' ,	128.58810,33.38722	,	8000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4701.289717' ,	128.62965,33.27247	,	8000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4759.451114' ,	128.63163,33.14188	,	8000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4810.111991' ,	128.59206,33.03306	,	8000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4856.07129' ,	128.50302,32.96183	,	8000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4911.219062' ,	128.38233,32.89061	,	8000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '4967.776367' ,	128.23790,32.85301	,	8000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5062.673292' ,	127.98860,32.89259	,	8000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5160.698582' ,	127.74128,32.96579	,	8000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5219.309513' ,	127.59685,33.01723	,	8000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5278.420195' ,	127.43857,33.01921	,	8000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5338.266723' ,	127.27830,33.01921	,	8000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5398.949731' ,	127.13981,32.94799	,	8000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5444.44198' ,	127.08441,32.85697	,	8000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5491.145967' ,	127.08638,32.75211	,	8000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5568.698015' ,	127.09034,32.57800	,	8000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5611.210065' ,	127.15366,32.49886	,	8000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5648.900813' ,	127.24467,32.46324	,	8000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5695.279001' ,	127.36536,32.44148	,	8000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5736.935403' ,	127.45637,32.49490	,	8000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5835.234849' ,	127.65027,32.64329	,	8000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '5934.477237' ,	127.78085,32.83719	,	8000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6034.742937' ,	127.82834,33.05878	,	8000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6136.283408' ,	127.88176,33.28236	,	8000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6232.564943' ,	128.06774,33.43273	,	8000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6339.62404' ,	128.35067,33.47824	,	8000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6390.907186' ,	128.48522,33.45251	,	8000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6438.94668' ,	128.58810,33.38722	,	8000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6492.336688' ,	128.62965,33.27247	,	8000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6550.498085' ,	128.63163,33.14188	,	8000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6601.158962' ,	128.59206,33.03306	,	8000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6647.118261' ,	128.50302,32.96183	,	8000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6702.266033' ,	128.38233,32.89061	,	8000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6758.823339' ,	128.23790,32.85301	,	8000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6853.720264' ,	127.98860,32.89259	,	8000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '6951.745554' ,	127.74128,32.96579	,	8000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7010.356484' ,	127.59685,33.01723	,	8000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7069.467166' ,	127.43857,33.01921	,	8000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7129.313695' ,	127.27830,33.01921	,	8000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7189.996702' ,	127.13981,32.94799	,	8000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7235.488951' ,	127.08441,32.85697	,	8000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7282.192938' ,	127.08638,32.75211	,	8000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7359.744986' ,	127.09034,32.57800	,	8000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7402.257036' ,	127.15366,32.49886	,	8000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7439.947784' ,	127.24467,32.46324	,	8000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7486.325972' ,	127.36536,32.44148	,	8000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7527.982375' ,	127.45637,32.49490	,	8000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7626.28182' ,	127.65027,32.64329	,	8000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7725.524208' ,	127.78085,32.83719	,	8000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7825.789909' ,	127.82834,33.05878	,	8000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '7927.33038' ,	127.88176,33.28236	,	8000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8023.611914' ,	128.06774,33.43273	,	8000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8130.671011' ,	128.35067,33.47824	,	8000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8181.954158' ,	128.48522,33.45251	,	8000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8229.993652' ,	128.58810,33.38722	,	8000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8283.383659' ,	128.62965,33.27247	,	8000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8341.545056' ,	128.63163,33.14188	,	8000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8392.205933' ,	128.59206,33.03306	,	8000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8438.165232' ,	128.50302,32.96183	,	8000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8493.313005' ,	128.38233,32.89061	,	8000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8549.87031' ,	128.23790,32.85301	,	8000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8644.767235' ,	127.98860,32.89259	,	8000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8742.792525' ,	127.74128,32.96579	,	8000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8801.403455' ,	127.59685,33.01723	,	8000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8860.514137' ,	127.43857,33.01921	,	8000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8920.360666' ,	127.27830,33.01921	,	8000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '8981.043673' ,	127.13981,32.94799	,	8000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9026.535923' ,	127.08441,32.85697	,	8000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9073.239909' ,	127.08638,32.75211	,	8000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9150.791957' ,	127.09034,32.57800	,	8000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9193.304007' ,	127.15366,32.49886	,	8000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9230.994755' ,	127.24467,32.46324	,	8000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9277.372944' ,	127.36536,32.44148	,	8000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9319.029346' ,	127.45637,32.49490	,	8000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9417.328791' ,	127.65027,32.64329	,	8000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9516.57118' ,	127.78085,32.83719	,	8000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9616.83688' ,	127.82834,33.05878	,	8000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9718.377351' ,	127.88176,33.28236	,	8000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9814.658885' ,	128.06774,33.43273	,	8000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9921.717983' ,	128.35067,33.47824	,	8000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '9973.001129' ,	128.48522,33.45251	,	8000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10021.04062' ,	128.58810,33.38722	,	8000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10074.43063' ,	128.62965,33.27247	,	8000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10132.59203' ,	128.63163,33.14188	,	8000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10183.2529' ,	128.59206,33.03306	,	8000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10229.2122' ,	128.50302,32.96183	,	8000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10284.35998' ,	128.38233,32.89061	,	8000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10340.91728' ,	128.23790,32.85301	,	8000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10435.81421' ,	127.98860,32.89259	,	8000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10533.8395' ,	127.74128,32.96579	,	8000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10592.45043' ,	127.59685,33.01723	,	8000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10651.56111' ,	127.43857,33.01921	,	8000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10711.40764' ,	127.27830,33.01921	,	8000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 ) 
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10772.09064' ,	127.13981,32.94799	,	8000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 ) 
),
/*Точка P41 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10817.58289' ,	127.08441,32.85697	,	8000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 ) 
),
/*Точка P42 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10864.28688' ,	127.08638,32.75211	,	8000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 ) 
),
/*Точка P43 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10941.83893' ,	127.09034,32.57800	,	8000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 ) 
),
/*Точка P44 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '10984.35098' ,	127.15366,32.49886	,	8000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 ) 
),
/*Точка P45 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '11022.04173' ,	127.24467,32.46324	,	8000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 ) 
),
/*Точка P46 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '11068.41991' ,	127.36536,32.44148	,	8000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 ) 
),
/*Точка P47 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '11110.07632' ,	127.45637,32.49490	,	8000	,	81.4265113062944	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 ) 
),
/*Точка P48 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '11247.54634' ,	127.81845,32.54040	,	8000	,	77.0157447448733	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 ) 
),
/*Точка P49 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '11393.78544' ,	128.19833,32.61361	,	8000	,	75.5878383628898	,	250	,	-12.74582278,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 ) 
),
/*Точка P50 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '11550.69959' ,	128.60393,32.70067	,	6000	,	89.5837493906398	,	240	,	-5.535188422,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 ) 
),
/*Точка P51 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '11731.36192' ,	129.06691,32.70265	,	5000	,	80.6065401118857	,	210	,	-9.095632002,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 ) 
),
/*Точка P52 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '11841.3048' ,	129.31027,32.73628	,	4000	,	54.6551620127634	,	180	,	-9.450188698,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 ) 
),
/*Точка P53 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '11947.12279' ,	129.47647,32.83521	,	3000	,	32.0600571330369	,	150	,	-6.344610836,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 ) 
),
/*Точка P54 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '12104.73687' ,	129.61101,33.01526	,	2000	,	6.36693952019   	,	100	,	-6.021665095,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 ) 
),
/*Точка P55 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '12270.8039' ,	129.63079,33.16365	,	1000	,	357.907953231846	,	80	,	-4.276488021,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 ) 
),
/*Точка P56 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №4 guard E-3C'), '12504.64063' ,	129.62344, 33.33176	,	0	,	357.907953231846	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 ) 
);
