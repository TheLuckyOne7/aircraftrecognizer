﻿delete from screenplay.route_kc_135_n1;
select setval('screenplay.route_b1_b_master_route_b1_b_master_id_seq',1);





insert into screenplay.route_kc_135_n1
(
  route_kc_135_n1_id , -- первичный ключ 
  cod_name , -- условное наименование точки на маршруте 
  geom  -- задание координат точки на маршруте 
)
values 
( default , 'KC-135 N1 P1' ,  ST_PointFromText('POINT( 131.30681 31.48096 )', 4326)),
( default , 'KC-135 N1 P2' ,  ST_PointFromText('POINT( 131.30464 31.57092 )', 4326)),
( default , 'KC-135 N1 P3' ,  ST_PointFromText('POINT( 131.21363 31.66985 )', 4326)),
( default , 'KC-135 N1 P4' ,  ST_PointFromText('POINT( 131.04743 31.68567 )', 4326)),
( default , 'KC-135 N1 P5' ,  ST_PointFromText('POINT( 130.93268 31.56696 )', 4326)),
( default , 'KC-135 N1 P6' ,  ST_PointFromText('POINT( 130.94455 31.39681 )', 4326)),
( default , 'KC-135 N1 P7' ,  ST_PointFromText('POINT( 131.05139 31.23852 )', 4326)),
( default , 'KC-135 N1 P8' ,  ST_PointFromText('POINT( 131.13449 31.02880 )', 4326)),
( default , 'KC-135 N1 P9' ,  ST_PointFromText('POINT( 131.17406 30.80324 )', 4326)),
( default , 'KC-135 N1 P10' ,  ST_PointFromText('POINT( 131.13053 30.53020 )', 4326)),
-- ( default , 'KC-135 N1 P11' ,  ST_PointFromText('POINT( 130.92872 30.44315 )', 4326)),
-- ( default , 'KC-135 N1 P12' ,  ST_PointFromText('POINT( 130.66359 30.41149 )', 4326)),
-- ( default , 'KC-135 N1 P13' ,  ST_PointFromText('POINT( 130.39055 30.47480 )', 4326)),
-- ( default , 'KC-135 N1 P14' ,  ST_PointFromText('POINT( 130.18479 30.58560 )', 4326)),
-- ( default , 'KC-135 N1 P15' ,  ST_PointFromText('POINT( 130.07399 30.80324 )', 4326)),
-- ( default , 'KC-135 N1 P16' ,  ST_PointFromText('POINT( 130.09773 31.04463 )', 4326)),
-- ( default , 'KC-135 N1 P17' ,  ST_PointFromText('POINT( 130.23227 31.19104 )', 4326)),
-- ( default , 'KC-135 N1 P18' ,  ST_PointFromText('POINT( 130.47365 31.22269 )', 4326)),
-- ( default , 'KC-135 N1 P19' ,  ST_PointFromText('POINT( 130.67151 31.11585 )', 4326)),
-- ( default , 'KC-135 N1 P20' ,  ST_PointFromText('POINT( 130.85353 30.92987 )', 4326)),
-- ( default , 'KC-135 N1 P21' ,  ST_PointFromText('POINT( 130.97620 30.69244 )', 4326)),
( default , 'KC-135 N1 P22' ,  ST_PointFromText('POINT( 131.01577 30.34026 )', 4326)),
( default , 'KC-135 N1 P23' ,  ST_PointFromText('POINT( 130.89706 30.15428 )', 4326)),
( default , 'KC-135 N1 P24' ,  ST_PointFromText('POINT( 130.65172 30.04348 )', 4326)),
( default , 'KC-135 N1 P25' ,  ST_PointFromText('POINT( 130.39055 30.00787 )', 4326)),
( default , 'KC-135 N1 P26' ,  ST_PointFromText('POINT( 130.25163 30.00985 )', 4326)),
( default , 'KC-135 N1 P27' ,  ST_PointFromText('POINT( 128.60548 30.76961 )', 4326)),
( default , 'KC-135 N1 P28' ,  ST_PointFromText('POINT( 127.07013 31.51354 )', 4326)),
( default , 'KC-135 N1 P29' ,  ST_PointFromText('POINT( 126.99537 31.66193 )', 4326)),
( default , 'KC-135 N1 P30' ,  ST_PointFromText('POINT( 127.01516 31.83209 )', 4326)),
( default , 'KC-135 N1 P31' ,  ST_PointFromText('POINT( 127.20114 31.91123 )', 4326)),
( default , 'KC-135 N1 P32' ,  ST_PointFromText('POINT( 127.49396 31.88353 )', 4326)),
( default , 'KC-135 N1 P33' ,  ST_PointFromText('POINT( 127.96882 31.72920 )', 4326)),
( default , 'KC-135 N1 P34' ,  ST_PointFromText('POINT( 128.42784 31.49178 )', 4326)),
( default , 'KC-135 N1 P35' ,  ST_PointFromText('POINT( 128.89873 31.27018 )', 4326)),
( default , 'KC-135 N1 P36' ,  ST_PointFromText('POINT( 129.37358 31.06441 )', 4326)),
( default , 'KC-135 N1 P37' ,  ST_PointFromText('POINT( 129.88800 30.83886 )', 4326)),
( default , 'KC-135 N1 P38' ,  ST_PointFromText('POINT( 130.32328 30.85468 )', 4326)),
( default , 'KC-135 N1 P39' ,  ST_PointFromText('POINT( 130.65568 30.89426 )', 4326)),
( default , 'KC-135 N1 P40' ,  ST_PointFromText('POINT( 131.02765 31.03671 )', 4326)),
( default , 'KC-135 N1 P41' ,  ST_PointFromText('POINT( 131.27298 31.21082 )', 4326)),
( default , 'KC-135 N1 P42' ,  ST_PointFromText('POINT( 131.31651 31.36515 )', 4326)),
( default , 'KC-135 N1 P43' ,  ST_PointFromText('POINT( 131.30681 31.48096 )', 4326));
  