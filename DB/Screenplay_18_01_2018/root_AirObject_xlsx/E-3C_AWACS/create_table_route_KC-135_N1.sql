﻿-- DROP SCHEMA Screenplay;

-- CREATE SCHEMA screenplay
--   AUTHORIZATION postgres;



-- Table: Screenplay.route_kc_135_n1

DROP TABLE if exists screenplay.route_kc_135_n1;

-- /*
CREATE TABLE screenplay.route_kc_135_n1
(
  route_kc_135_n1_id serial NOT NULL,
  cod_name character varying(20),
  geom geometry(Point,4326),
  CONSTRAINT screenplay_route_kc_135_n1_id_pk PRIMARY KEY (route_kc_135_n1_id),
  constraint route_kc_135_n1_cod_name_unique unique (cod_name)
)
WITH (
  OIDS=FALSE
);

-- */