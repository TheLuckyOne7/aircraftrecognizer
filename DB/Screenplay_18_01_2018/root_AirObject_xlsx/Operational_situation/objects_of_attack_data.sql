﻿delete from screenplay.objects_of_attack;
select setval('screenplay.objects_of_attack_objects_of_attack_id_seq',1);


insert into screenplay.objects_of_attack 
(
  objects_of_attack_id , -- искуственный первичный ключ
  cod_name_r , -- кодовое наименоование объекта удара на русском 
  cod_name_e , -- кодовое наименоование объекта удара  на английском 
  geom -- определение полигона объекта удара 
)

values
(
  default , -- искуственный первичный ключ
  'Объкт удара №1' , -- кодовое наименоование объекта удара на русском 
  'Object of attack №1' , -- кодовое наименоование объекта удара  на английском   
  ST_PolygonFromText('POLYGON((119.1881 34.95268,119.09906 34.95169,119.11093 34.7578,119.197 34.76076,119.1881 34.95268))', 4326) -- определение полигона объекта удара 
); 

-- */