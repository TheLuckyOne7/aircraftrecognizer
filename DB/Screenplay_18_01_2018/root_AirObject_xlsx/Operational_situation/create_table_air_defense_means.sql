﻿-- DROP SCHEMA Screenplay;

-- CREATE SCHEMA screenplay
--   AUTHORIZATION postgres;



-- Table: screenplay.air_defense_means 
-- air defense means - средства ПВО (перевод google)

DROP TABLE if exists screenplay.air_defense_means ;

-- /*
CREATE TABLE screenplay.air_defense_means 
(
  air_defense_means_id serial NOT NULL, -- искуственный первичный ключ
  type_of_adm_r character varying(20), -- тип ЗРК на русском
  type_of_adm_e character varying(20), -- тип ЗРК на английском 
  cod_name_r character varying(20), -- кодовое наименоование ЗРК на русском 
  cod_name_e character varying(20), -- кодовое наименоование ЗРК на английском 
  geom geometry(Point,4326),
  CONSTRAINT screenplay_air_defense_means_id_pk PRIMARY KEY (air_defense_means_id),
  constraint screenplay_air_defense_means_cod_name_r_unique unique (cod_name_r)
)
WITH (
  OIDS=FALSE
);

-- */