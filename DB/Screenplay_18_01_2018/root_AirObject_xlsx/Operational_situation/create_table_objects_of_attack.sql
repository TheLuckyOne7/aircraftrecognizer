﻿-- DROP SCHEMA Screenplay;

-- CREATE SCHEMA screenplay
--   AUTHORIZATION postgres;



-- Table: screenplay.objects_of_attack
-- objects of attack - объекты нападения (удара) (перевод google)

DROP TABLE if exists screenplay.objects_of_attack ;

-- /*
CREATE TABLE screenplay.objects_of_attack 
(
  objects_of_attack_id serial NOT NULL, -- искуственный первичный ключ
  cod_name_r character varying(30), -- кодовое наименоование объекта удара на русском 
  cod_name_e character varying(30), -- кодовое наименоование объекта удара на английском 
  geom geometry(polygon,4326),
  CONSTRAINT screenplay_objects_of_attack_id_pk PRIMARY KEY (objects_of_attack_id),
  constraint screenplay_objects_of_attack_cod_name_r_unique unique (cod_name_r)
)
WITH (
  OIDS=FALSE
);

-- */