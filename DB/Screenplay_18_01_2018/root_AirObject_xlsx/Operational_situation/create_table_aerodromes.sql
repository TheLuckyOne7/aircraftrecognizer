﻿-- DROP SCHEMA Screenplay;

-- CREATE SCHEMA screenplay
--   AUTHORIZATION postgres;



-- Table: screenplay.aerodromes
-- aerodromes - аэродромы 

DROP TABLE if exists screenplay.aerodromes;

-- /*
CREATE TABLE screenplay.aerodromes
(
  aerodromes_id serial NOT NULL, -- искуственный первичный ключ
  cod_name_r character varying(30), -- кодовое наименоование аэродрома на русском 
  cod_name_e character varying(30), -- кодовое наименоование аэродрома на английском 
  geom geometry(point,4326),
  CONSTRAINT screenplay_aerodromes_id_pk PRIMARY KEY (aerodromes_id),
  constraint screenplay_aerodromes_cod_name_r_unique unique (cod_name_r)
)
WITH (
  OIDS=FALSE
);

-- */