﻿delete from screenplay.warships;
select setval('screenplay.warships_warships_id_seq',1);


insert into screenplay.warships
(
  warships_id , -- искуственный первичный ключ
  cod_name_r , -- кодовое наименоование аэродрома на русском 
  cod_name_e , -- кодовое наименоование аэродрома на английском 
  geom -- определение точки, где находится аэродром
)

values
(
  default , -- искуственный первичный ключ
  'Корабль №1' , -- кодовое наименоование аэродрома на русском
  'Warship №1' , -- кодовое наименоование аэродрома на английском  
  ST_PointFromText('POINT(125.16 28.92)', 4326) -- определение точки, где находится корабль 
), 

(
  default , -- искуственный первичный ключ
  'Корабль №2' , -- кодовое наименоование аэродрома на русском
  'Warship №2' , -- кодовое наименоование аэродрома на английском  
  ST_PointFromText('POINT(124.86949 28.89438)', 4326) -- определение точки, где находится корабль 
),

(
  default , -- искуственный первичный ключ
  'Корабль №3' , -- кодовое наименоование аэродрома на русском
  'Warship №3' , -- кодовое наименоование аэродрома на английском  
  ST_PointFromText('POINT(125.24 28.6)', 4326) -- определение точки, где находится корабль 
),

(
  default , -- искуственный первичный ключ
  'Корабль №4' , -- кодовое наименоование аэродрома на русском
  'Warship №4' , -- кодовое наименоование аэродрома на английском  
  ST_PointFromText('POINT(124.73099 28.61936)', 4326) -- определение точки, где находится корабль 
), 

(
  default , -- искуственный первичный ключ
  'Корабль №5' , -- кодовое наименоование аэродрома на русском
  'Warship №5' , -- кодовое наименоование аэродрома на английском  
  ST_PointFromText('POINT(125.08 28.4)', 4326) -- определение точки, где находится корабль 
),

(
  default , -- искуственный первичный ключ
  'АВИАНОСЕЦ' , -- кодовое наименоование аэродрома на русском
  'AIRCRAFT CARRIER' , -- кодовое наименоование аэродрома на английском  
  ST_PointFromText('POINT(125.01986 28.67674)', 4326) -- определение точки, где находится корабль 
); 









-- */