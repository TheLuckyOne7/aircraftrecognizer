﻿-- DROP SCHEMA Screenplay;

-- CREATE SCHEMA screenplay
--   AUTHORIZATION postgres;



-- Table: screenplay.warships
-- warships - военные корабли

DROP TABLE if exists screenplay.warships ;

-- /*
CREATE TABLE screenplay.warships 
(
  warships_id serial NOT NULL, -- искуственный первичный ключ
  cod_name_r character varying(30), -- кодовое наименоование корабля на русском 
  cod_name_e character varying(30), -- кодовое наименоование корабля на английском 
  geom geometry(point,4326),
  CONSTRAINT screenplay_warships_id_pk PRIMARY KEY (warships_id),
  constraint screenplay_warships_cod_name_r_unique unique (cod_name_r)
)
WITH (
  OIDS=FALSE
);

-- */