﻿-- DROP SCHEMA Screenplay;

-- CREATE SCHEMA screenplay
--   AUTHORIZATION postgres;



-- Table: screenplay.route_b2_b_slave 

DROP TABLE if exists screenplay.route_b2_b_slave ;

-- /*
CREATE TABLE screenplay.route_b2_b_slave 
(
  route_b2_b_slave_id serial NOT NULL,
  cod_name character varying(20),
  geom geometry(Point,4326),
  CONSTRAINT screenplay_route_b2_b_slave_id_pk PRIMARY KEY (route_b2_b_slave_id),
  constraint route_b2_b_slave_cod_name_unique unique (cod_name)
)
WITH (
  OIDS=FALSE
);

-- */