﻿-- DROP SCHEMA Screenplay;

-- CREATE SCHEMA screenplay
--   AUTHORIZATION postgres;



-- Table: Screenplay.route_b1_b_master

DROP TABLE if exists screenplay.route_b1_b_master;

-- /*
CREATE TABLE screenplay.route_b1_b_master
(
  route_b1_b_master_id serial NOT NULL,
  cod_name character varying(20),
  geom geometry(Point,4326),
  CONSTRAINT screenplay_route_b1_b_master_id_pk PRIMARY KEY (route_b1_b_master_id),
  constraint route_b1_b_master_cod_name_unique unique (cod_name)
)
WITH (
  OIDS=FALSE
);

-- */