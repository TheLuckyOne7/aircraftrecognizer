﻿-- DROP SCHEMA Screenplay;

-- CREATE SCHEMA screenplay
--   AUTHORIZATION postgres;



-- Table: Screenplay.route_b1_b_master

DROP TABLE if exists screenplay.route_FA_18F_N1;

-- /*
CREATE TABLE screenplay.route_FA_18F_N1
(
  route_FA_18F_N1_id serial NOT NULL,
  cod_name character varying(20),
  geom geometry(Point,4326),
  CONSTRAINT screenplay_route_FA_18F_N1_id_pk PRIMARY KEY (route_FA_18F_N1_id),
  constraint route_FA_18F_N1_cod_name_unique unique (cod_name)
)
WITH (
  OIDS=FALSE
);

-- */