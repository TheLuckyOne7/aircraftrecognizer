﻿delete from re_situation.m_airborne_radar_in_air_obj;
select setval('re_situation.m_airborne_radar_in_air_obj_m_airborne_radar_in_air_obj_id_seq',1);

Insert into re_situation.m_airborne_radar_in_air_obj
(
m_airborne_radar_in_air_obj_id , -- Искуственный первичный ключ 
air_object_id , -- внешний ключ на воздушный объект 
placing_f_t_aircraft_id , -- внешний ключ на табл. размещение РЭС на типе ЛА
d_m_airborne_radar_id, -- внешний ключ на описание режима работы 
duration_puls, -- длительность зондирующего импульса, мкс 
width_spectrum_puls, -- Ширина спектра зондирующего импульса, МГц 
cycle_time_in_pack, -- Период повторения зондирующих импульсов, мкс
freq_pulses_in_a_pack, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
duration_pack, -- Длительность КГПИ, мс
period_pack, -- Мин. Период следования КГПИ, мс
q_of_carrier_freq_in_pack, -- Количество несущих ча-стот в пачке, шт
r_ch_freq_of_pack_in_overview, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
dur_of_r_irradiation_obj, -- Длительность облучения объ-екта , мс
period_of_r_irradiation_obj -- Период облу-чения объекта (цикл скани-рования), с
 )
 values 
--------------------------------------------------------------------------------- 
-- описание режимов работы БРЛС воздушного объекта  "B-1B master"
-- Ведущий удароной группы состоящей из самолетов B-1B 
--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р1"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B master'
), -- внешний ключ на воздушный объект 'B-1B master'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р1'
), -- внешний ключ на описание режима работы "Режим Р1" БРЛМ "AN/APQ-164" 

2.5, -- длительность зондирующего импульса, мкс 
0.4, -- Ширина спектра зондирующего импульса, МГц 
2000, -- Период повторения зондирующих импульсов, мкс
20.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.0, -- Длительность КГПИ, мс
1500.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
5.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20, -- Длительность облучения объ-екта , мс
1.5 -- Период облучения объекта (цикл скани-рования), с
),
--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р2"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B master'
), -- внешний ключ на воздушный объект 'B-1B master'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р2'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

2.5, -- длительность зондирующего импульса, мкс 
0.4, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.0, -- Длительность КГПИ, мс
1500.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20, -- Длительность облучения объ-екта , мс
1.5 -- Период облучения объекта (цикл сканирования), с
),
--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р3"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B master'
), -- внешний ключ на воздушный объект 'B-1B master'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р3'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

5.0, -- длительность зондирующего импульса, мкс 
0.2, -- Ширина спектра зондирующего импульса, МГц 
2500, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.0, -- Длительность КГПИ, мс
1500.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20, -- Длительность облучения объ-екта , мс
1.5 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р4"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B master'
), -- внешний ключ на воздушный объект 'B-1B master'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р4'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

0.25, -- длительность зондирующего импульса, мкс 
4.0, -- Ширина спектра зондирующего импульса, МГц 
125, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.0, -- Длительность КГПИ, мс
1000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20, -- Длительность облучения объ-екта , мс
3.5 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р5"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B master'
), -- внешний ключ на воздушный объект 'B-1B master'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р5'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

30.0, -- длительность зондирующего импульса, мкс 
1.0, -- Ширина спектра зондирующего импульса, МГц 
1010, -- Период повторения зондирующих импульсов, мкс
20.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.0, -- Длительность КГПИ, мс
250.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
5.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20, -- Длительность облучения объ-екта , мс
0.25 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р6"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B master'
), -- внешний ключ на воздушный объект 'B-1B master'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р6'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

4.0, -- длительность зондирующего импульса, мкс 
8.0, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
50.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
5.5, -- Длительность КГПИ, мс
8.0, -- Период следования КГПИ, мс
3, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
24, -- Длительность облучения объ-екта , мс
1.65 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р7"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B master'
), -- внешний ключ на воздушный объект 'B-1B master'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р7'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

30.0, -- длительность зондирующего импульса, мкс 
1.0, -- Ширина спектра зондирующего импульса, МГц 
1500, -- Период повторения зондирующих импульсов, мкс
20.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.0, -- Длительность КГПИ, мс
1500.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
5.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20, -- Длительность облучения объ-екта , мс
1.5 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р8"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B master'
), -- внешний ключ на воздушный объект 'B-1B master'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р8'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

4.0, -- длительность зондирующего импульса, мкс 
8.0, -- Ширина спектра зондирующего импульса, МГц 
500, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
5.5, -- Длительность КГПИ, мс
8.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
50.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
24.0, -- Длительность облучения объ-екта , мс
0.4 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р9"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B master'
), -- внешний ключ на воздушный объект 'B-1B master'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р9'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

2.0, -- длительность зондирующего импульса, мкс 
0.55, -- Ширина спектра зондирующего импульса, МГц 
500, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
1100.0, -- Длительность КГПИ, мс
6000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
25.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
6000, -- Длительность облучения объ-екта , мс
6.0 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------------------------------------------------------------- 
-- описание режимов работы БРЛС воздушного объекта  "B-1B slave"
-- Ведомый удароной группы состоящей из самолетов B-1B 
--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р1"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'
), -- внешний ключ на воздушный объект 'B-1B slave'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р1'
), -- внешний ключ на описание режима работы "Режим Р1" БРЛМ "AN/APQ-164" 

2.5, -- длительность зондирующего импульса, мкс 
0.4, -- Ширина спектра зондирующего импульса, МГц 
2000, -- Период повторения зондирующих импульсов, мкс
20.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.0, -- Длительность КГПИ, мс
1500.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
5.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20, -- Длительность облучения объ-екта , мс
1.5 -- Период облучения объекта (цикл скани-рования), с
),
--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р2"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'
), -- внешний ключ на воздушный объект 'B-1B slave'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р2'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

2.5, -- длительность зондирующего импульса, мкс 
0.4, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.0, -- Длительность КГПИ, мс
1500.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20, -- Длительность облучения объ-екта , мс
1.5 -- Период облучения объекта (цикл сканирования), с
),
--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р3"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'
), -- внешний ключ на воздушный объект 'B-1B slave'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р3'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

5.0, -- длительность зондирующего импульса, мкс 
0.2, -- Ширина спектра зондирующего импульса, МГц 
2500, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.0, -- Длительность КГПИ, мс
1500.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20, -- Длительность облучения объ-екта , мс
1.5 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р4"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'
), -- внешний ключ на воздушный объект 'B-1B slave'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р4'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

0.25, -- длительность зондирующего импульса, мкс 
4.0, -- Ширина спектра зондирующего импульса, МГц 
125, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.0, -- Длительность КГПИ, мс
1000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20, -- Длительность облучения объ-екта , мс
3.5 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р5"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'
), -- внешний ключ на воздушный объект 'B-1B slave'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р5'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

30.0, -- длительность зондирующего импульса, мкс 
1.0, -- Ширина спектра зондирующего импульса, МГц 
1010, -- Период повторения зондирующих импульсов, мкс
20.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.0, -- Длительность КГПИ, мс
250.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
5.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20, -- Длительность облучения объ-екта , мс
0.25 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р6"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'
), -- внешний ключ на воздушный объект 'B-1B slave'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р6'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

4.0, -- длительность зондирующего импульса, мкс 
8.0, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
50.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
5.5, -- Длительность КГПИ, мс
8.0, -- Период следования КГПИ, мс
3, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
24, -- Длительность облучения объ-екта , мс
1.65 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р7"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'
), -- внешний ключ на воздушный объект 'B-1B slave'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р7'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

30.0, -- длительность зондирующего импульса, мкс 
1.0, -- Ширина спектра зондирующего импульса, МГц 
1500, -- Период повторения зондирующих импульсов, мкс
20.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.0, -- Длительность КГПИ, мс
1500.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
5.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20, -- Длительность облучения объ-екта , мс
1.5 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р8"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'
), -- внешний ключ на воздушный объект 'B-1B slave'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р8'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

4.0, -- длительность зондирующего импульса, мкс 
8.0, -- Ширина спектра зондирующего импульса, МГц 
500, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
5.5, -- Длительность КГПИ, мс
8.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
50.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
24.0, -- Длительность облучения объ-екта , мс
0.4 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APQ-164" условное наименование "ЛА-"В-1В" БРЛС-"AN/APQ-164" №1"
-- режим работы "Режим Р9"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'
), -- внешний ключ на воздушный объект 'B-1B slave'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р9'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

2.0, -- длительность зондирующего импульса, мкс 
0.55, -- Ширина спектра зондирующего импульса, МГц 
500, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
1100.0, -- Длительность КГПИ, мс
6000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
25.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
6000, -- Длительность облучения объ-екта , мс
6.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------------------------------------------------------------- 
-- описание режимов работы БРЛС воздушного объекта  "КС-135 №1"
-- Самолет заправщик КС-135 №1 
--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р1"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р1'
), -- внешний ключ на описание режима работы "Режим Р1" БРЛМ "AN/APQ-164" 

4.5, -- длительность зондирующего импульса, мкс 
0.22, -- Ширина спектра зондирующего импульса, МГц 
2500, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
1000.0, -- Период следования КГПИ, мс
1, -- Количество несущих частот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
1.0 -- Период облучения объекта (цикл скани-рования), с
),
--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р2"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р2'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

3.6, -- длительность зондирующего импульса, мкс 
0.28, -- Ширина спектра зондирующего импульса, МГц 
2000, -- Период повторения зондирующих импульсов, мкс
10.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
2860.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
2.86 -- Период облучения объекта (цикл сканирования), с
),
--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р3"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р3'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

2.0, -- длительность зондирующего импульса, мкс 
0.5, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
10.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
6000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
6.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р4"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р4'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

0.35, -- длительность зондирующего импульса, мкс 
3.0, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
10.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
45.5, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
7.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
83.0, -- Длительность облучения объ-екта , мс
0.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р5"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р5'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

3.6, -- длительность зондирующего импульса, мкс 
0.28, -- Ширина спектра зондирующего импульса, МГц 
2500, -- Период повторения зондирующих импульсов, мкс
7.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
1725.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
1.73 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р6"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р6'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

1.0, -- длительность зондирующего импульса, мкс 
1.0, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
7.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
2850.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
2.85 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р7"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р7'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

0.35, -- длительность зондирующего импульса, мкс 
3.0, -- Ширина спектра зондирующего импульса, МГц 
500, -- Период повторения зондирующих импульсов, мкс
12.5, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
22.0, -- Длительность КГПИ, мс
24.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
2.86 -- Период облучения объекта (цикл сканирования), с
),


--------------------------------------------------------------------------------- 
-- описание режимов работы БРЛС воздушного объекта  "КС-135 №2"
-- Самолет заправщик КС-135 №1 
--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р1"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'
), -- внешний ключ на воздушный объект 'КС-135 №2'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р1'
), -- внешний ключ на описание режима работы "Режим Р1" БРЛМ "AN/APQ-164" 

4.5, -- длительность зондирующего импульса, мкс 
0.22, -- Ширина спектра зондирующего импульса, МГц 
2500, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
1000.0, -- Период следования КГПИ, мс
1, -- Количество несущих частот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
1.0 -- Период облучения объекта (цикл скани-рования), с
),
--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р2"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'
), -- внешний ключ на воздушный объект 'КС-135 №2'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р2'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

3.6, -- длительность зондирующего импульса, мкс 
0.28, -- Ширина спектра зондирующего импульса, МГц 
2000, -- Период повторения зондирующих импульсов, мкс
10.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
2860.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
2.86 -- Период облучения объекта (цикл сканирования), с
),
--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р3"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'
), -- внешний ключ на воздушный объект 'КС-135 №2'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р3'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

2.0, -- длительность зондирующего импульса, мкс 
0.5, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
10.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
6000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
6.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р4"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'
), -- внешний ключ на воздушный объект 'КС-135 №2'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р4'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

0.35, -- длительность зондирующего импульса, мкс 
3.0, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
10.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
45.5, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
7.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
83.0, -- Длительность облучения объ-екта , мс
0.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р5"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'
), -- внешний ключ на воздушный объект 'КС-135 №2'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р5'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

3.6, -- длительность зондирующего импульса, мкс 
0.28, -- Ширина спектра зондирующего импульса, МГц 
2500, -- Период повторения зондирующих импульсов, мкс
7.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
1725.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
1.73 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р6"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'
), -- внешний ключ на воздушный объект 'КС-135 №2'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р6'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

1.0, -- длительность зондирующего импульса, мкс 
1.0, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
7.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
2850.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
2.85 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р7"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'
), -- внешний ключ на воздушный объект 'КС-135 №2'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р7'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

0.35, -- длительность зондирующего импульса, мкс 
3.0, -- Ширина спектра зондирующего импульса, МГц 
500, -- Период повторения зондирующих импульсов, мкс
12.5, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
22.0, -- Длительность КГПИ, мс
24.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
2.86 -- Период облучения объекта (цикл сканирования), с
);
-- */



  