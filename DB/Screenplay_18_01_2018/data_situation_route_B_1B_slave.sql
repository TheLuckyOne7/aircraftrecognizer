﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния 
)
values
-- точка P0 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '0',		132.01058, 29.48551	,	10000	,	289.336740598774	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- точка P1 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '846.8759643',	132.01058, 29.48551	,	10000	,	289.336740598774	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=53 )
),
-- точка P2 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '1744.714547',	130.25363, 30.00785	,	10000	,	298.563143638758	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=55 )
),
-- точка P3 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '2462.135078',	128.60748, 30.76761	,	10000	,	299.909757968603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=55 )
),
-- точка P4 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '3134.831528',	127.07213, 31.51154	,	10000	,	303.124062100225	,	353.8779206	,	-22.90334352,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- точка P5 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '3567.082827',	125.71089, 32.25547	, 	100	,	298.849773724764	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- точка P6 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '4428.226715',	123.68486, 33.17352	,	100	,	322.868829678064	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- точка P7 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '5102.077431',	122.57688, 34.37647	,	100	,	313.656973569539	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=56 )
),
-- точка P8 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '5532.3124',	121.72214, 35.04126	,	100	,	286.866582923328	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=59 )
),
-- точка P9 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '5952.535506',	120.61416, 35.31035	,	100	,	259.360190901791	,	250	,	3.013968515,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=58 )
),
-- точка P10 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '6251.145129',	119.80691, 35.18372	,	1000	,	248.52166165352 	,	250	,	4.032022364,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=59 )
),
-- точка P11 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '6499.159626',	119.17378, 34.97795	,	2000	,	194.406008413948	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=54 )
),
-- точка P12 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '6615.481514',	119.09464, 34.7247	,	2000	,	126.555243698082	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=54 )
),
-- точка P13 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '6703.108634',	119.23709, 34.56641	,	2000	,	85.6412991061974	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=54 )
),
-- точка P14 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '6894.963323',	119.75943, 34.59807	,	2000	,	58.5641208412322	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=54 )
),
-- точка P15 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '7084.688984',	120.20262, 34.81967	,	2000	,	75.7850478428191	,	250	,	-8.182414714,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=54 )
),
-- точка P16 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '7316.894276',	120.81993, 34.94629	,	100	,	179.006619872839	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=54 )
),
-- точка P17 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '7720.985404',	121.70632, 34.93047	,	100	,	134.397709954873	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=54 )
),
-- точка P18 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '8419.444177',	122.76682, 34.02825	,	100	,	139.594135320029	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- точка P19 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '8959.779183',	123.6532, 33.39511	,	100	,	143.673814075018	,	200	,	9.230068262,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- точка P20 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '10032.36063',	125.48929, 32.23964	,	10000	,	149.041419423325	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- точка P21 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '10428.85419',	126.56562, 31.68565	,	10000	,	150.901781007906	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- точка P22 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '10907.02143',	127.87938, 31.05252	,	10000	,	152.139024195033	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- точка P23 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '11428.75249',	129.31976, 30.38773	,	10000	,	152.820799448201	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- точка P24 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '11760.61192',	130.2378, 29.97619	,	10000	,	163.147618383433	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- точка P25 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '12353.73754',	131.99475, 29.50134	,	10000	,	156.218312766348	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=52 )
),
-- точка P26 
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'B-1B slave'), '12426.44744',	132.20052, 29.4222	,	10000	,	156.218312766348	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=51 )
);
