﻿insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)
values
/*Точка P1 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '0'           ,	129.62344, 33.33176	,	0	,	359.762737219524	,	80	,	9.562651856,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P2 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '156.8602541' ,	129.62288,33.44460	,	1500	,	0	,	100	,	15.48938854,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P3 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '253.7007456' ,	129.62288,33.53166	,	3000	,	320.234059348107	,	120	,	14.44233107,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P4 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '392.1825455' ,	129.50812,33.64641	,	5000	,	285.783314743683	,	140	,	13.61590608,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P5 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '502.3478177' ,	129.34786,33.68400	,	6500	,	270.713835836974	,	160	,	12.47060885,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P6 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '622.6306377' ,	129.14011,33.68598	,	8000	,	95.5108510865669	,	180	,	11.91658595,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P7 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '748.5056162' ,	128.89675,33.66620	,	9500	,	101.009812547814	,	200	,	7.588208819,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P8 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '880.2890217' ,	128.61778,33.62069	,	10500	,	104.997813793904	,	220	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 )
),
/*Точка P9 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1034.13679' ,	128.26560,33.54155	,	10500	,	112.765321771661	,	240	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 )
),
/*Точка P10 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1157.118703' ,	127.97277,33.43866	,	10500	,	110.305169982287	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 )
),
/*Точка P11 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1258.384817' ,	127.71754,33.35952	,	10500	,	117.213325272936	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 )
),
/*Точка P12 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1385.239745' ,	127.41482,33.22894	,	10500	,	140.569163377214	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=40 )
),
/*Точка P13 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1547.116037' ,	127.13981,32.94799	,	10500	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1592.654671' ,	127.08441,32.85697	,	10500	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1639.376961' ,	127.08638,32.75211	,	10500	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1716.959403' ,	127.09034,32.57800	,	10500	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1759.488114' ,	127.15366,32.49886	,	10500	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1797.193633' ,	127.24467,32.46324	,	10500	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1843.589997' ,	127.36536,32.44148	,	10500	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1885.262725' ,	127.45637,32.49490	,	10500	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '1983.600696' ,	127.65027,32.64329	,	10500	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2082.881978' ,	127.78085,32.83719	,	10500	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2183.186974' ,	127.82834,33.05878	,	10500	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2284.767239' ,	127.88176,33.28236	,	10500	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2381.086507' ,	128.06774,33.43273	,	10500	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2488.187562' ,	128.35067,33.47824	,	10500	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2539.490807' ,	128.48522,33.45251	,	10500	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2587.549128' ,	128.58810,33.38722	,	10500	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2640.96006' ,	128.62965,33.27247	,	10500	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2699.144251' ,	128.63163,33.14188	,	10500	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2749.824983' ,	128.59206,33.03306	,	10500	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2795.802294' ,	128.50302,32.96183	,	10500	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2850.971679' ,	128.38233,32.89061	,	10500	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '2907.55115' ,	128.23790,32.85301	,	10500	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3002.485266' ,	127.98860,32.89259	,	10500	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3100.548973' ,	127.74128,32.96579	,	10500	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3159.182874' ,	127.59685,33.01723	,	10500	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3218.316722' ,	127.43857,33.01921	,	10500	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3278.186705' ,	127.27830,33.01921	,	10500	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3338.893495' ,	127.13981,32.94799	,	10500	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3384.432128' ,	127.08441,32.85697	,	10500	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3431.154419' ,	127.08638,32.75211	,	10500	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3508.73686' ,	127.09034,32.57800	,	10500	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3551.265571' ,	127.15366,32.49886	,	10500	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3588.971091' ,	127.24467,32.46324	,	10500	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3635.367455' ,	127.36536,32.44148	,	10500	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3677.040183' ,	127.45637,32.49490	,	10500	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3775.378153' ,	127.65027,32.64329	,	10500	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3874.659435' ,	127.78085,32.83719	,	10500	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '3974.964431' ,	127.82834,33.05878	,	10500	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4076.544697' ,	127.88176,33.28236	,	10500	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4172.863965' ,	128.06774,33.43273	,	10500	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4279.96502' ,	128.35067,33.47824	,	10500	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4331.268265' ,	128.48522,33.45251	,	10500	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4379.326586' ,	128.58810,33.38722	,	10500	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4432.737518' ,	128.62965,33.27247	,	10500	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4490.921708' ,	128.63163,33.14188	,	10500	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4541.60244' ,	128.59206,33.03306	,	10500	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4587.579751' ,	128.50302,32.96183	,	10500	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4642.749137' ,	128.38233,32.89061	,	10500	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4699.328607' ,	128.23790,32.85301	,	10500	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4794.262723' ,	127.98860,32.89259	,	10500	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4892.32643' ,	127.74128,32.96579	,	10500	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '4950.960331' ,	127.59685,33.01723	,	10500	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5010.094179' ,	127.43857,33.01921	,	10500	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5069.964163' ,	127.27830,33.01921	,	10500	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5130.670952' ,	127.13981,32.94799	,	10500	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5176.172122' ,	127.08441,32.85697	,	10500	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5222.894412' ,	127.08638,32.75211	,	10500	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5300.476853' ,	127.09034,32.57800	,	10500	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5343.005564' ,	127.15366,32.49886	,	10500	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5380.711084' ,	127.24467,32.46324	,	10500	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5427.107448' ,	127.36536,32.44148	,	10500	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5468.780176' ,	127.45637,32.49490	,	10500	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5567.118146' ,	127.65027,32.64329	,	10500	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5666.399429' ,	127.78085,32.83719	,	10500	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5766.704424' ,	127.82834,33.05878	,	10500	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5868.28469' ,	127.88176,33.28236	,	10500	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '5964.603958' ,	128.06774,33.43273	,	10500	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6071.705013' ,	128.35067,33.47824	,	10500	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6123.008258' ,	128.48522,33.45251	,	10500	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6171.066579' ,	128.58810,33.38722	,	10500	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6224.477511' ,	128.62965,33.27247	,	10500	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6282.661702' ,	128.63163,33.14188	,	10500	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6333.342433' ,	128.59206,33.03306	,	10500	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6379.319745' ,	128.50302,32.96183	,	10500	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6434.48913' ,	128.38233,32.89061	,	10500	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6491.068601' ,	128.23790,32.85301	,	10500	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6586.002717' ,	127.98860,32.89259	,	10500	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6684.066424' ,	127.74128,32.96579	,	10500	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6742.700324' ,	127.59685,33.01723	,	10500	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6801.834173' ,	127.43857,33.01921	,	10500	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6861.704156' ,	127.27830,33.01921	,	10500	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6922.410946' ,	127.13981,32.94799	,	10500	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '6967.912115' ,	127.08441,32.85697	,	10500	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7014.634405' ,	127.08638,32.75211	,	10500	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7092.216847' ,	127.09034,32.57800	,	10500	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7134.745558' ,	127.15366,32.49886	,	10500	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7172.451077' ,	127.24467,32.46324	,	10500	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7218.847442' ,	127.36536,32.44148	,	10500	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7260.52017' ,	127.45637,32.49490	,	10500	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7358.85814' ,	127.65027,32.64329	,	10500	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7458.139422' ,	127.78085,32.83719	,	10500	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7558.444418' ,	127.82834,33.05878	,	10500	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7660.024683' ,	127.88176,33.28236	,	10500	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7756.343951' ,	128.06774,33.43273	,	10500	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7863.445007' ,	128.35067,33.47824	,	10500	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7914.748251' ,	128.48522,33.45251	,	10500	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '7962.806572' ,	128.58810,33.38722	,	10500	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8016.217504' ,	128.62965,33.27247	,	10500	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8074.401695' ,	128.63163,33.14188	,	10500	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8125.082427' ,	128.59206,33.03306	,	10500	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8171.059738' ,	128.50302,32.96183	,	10500	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8226.229123' ,	128.38233,32.89061	,	10500	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8282.808594' ,	128.23790,32.85301	,	10500	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8377.74271' ,	127.98860,32.89259	,	10500	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8475.806417' ,	127.74128,32.96579	,	10500	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8534.440318' ,	127.59685,33.01723	,	10500	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8593.574166' ,	127.43857,33.01921	,	10500	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8653.444149' ,	127.27830,33.01921	,	10500	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8714.150939' ,	127.13981,32.94799	,	10500	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8759.652108' ,	127.08441,32.85697	,	10500	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8806.374399' ,	127.08638,32.75211	,	10500	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8883.95684' ,	127.09034,32.57800	,	10500	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8926.485551' ,	127.15366,32.49886	,	10500	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '8964.191071' ,	127.24467,32.46324	,	10500	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9010.587435' ,	127.36536,32.44148	,	10500	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9052.260163' ,	127.45637,32.49490	,	10500	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9150.598133' ,	127.65027,32.64329	,	10500	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9249.879415' ,	127.78085,32.83719	,	10500	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9350.184411' ,	127.82834,33.05878	,	10500	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9451.764677' ,	127.88176,33.28236	,	10500	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9548.083945' ,	128.06774,33.43273	,	10500	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9655.185' ,	128.35067,33.47824	,	10500	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9706.488245' ,	128.48522,33.45251	,	10500	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9754.546566' ,	128.58810,33.38722	,	10500	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9807.957498' ,	128.62965,33.27247	,	10500	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9866.141688' ,	128.63163,33.14188	,	10500	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9916.82242' ,	128.59206,33.03306	,	10500	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '9962.799731' ,	128.50302,32.96183	,	10500	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10017.96912' ,	128.38233,32.89061	,	10500	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10074.54859' ,	128.23790,32.85301	,	10500	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10169.4827' ,	127.98860,32.89259	,	10500	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '167.54641' ,	127.74128,32.96579	,	10500	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10326.18031' ,	127.59685,33.01723	,	10500	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10385.31416' ,	127.43857,33.01921	,	10500	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10445.18414' ,	127.27830,33.01921	,	10500	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10505.89093' ,	127.13981,32.94799	,	10500	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P41 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10551.3921' ,	127.08441,32.85697	,	10500	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P42 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10598.11439' ,	127.08638,32.75211	,	10500	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P43 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10675.69683' ,	127.09034,32.57800	,	10500	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P44 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10718.22554' ,	127.15366,32.49886	,	10500	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P45 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10755.93106' ,	127.24467,32.46324	,	10500	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P46 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10802.32743' ,	127.36536,32.44148	,	10500	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P47 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10844.00016' ,	127.45637,32.49490	,	10500	,	81.4265113062944	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P48 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '10981.52406' ,	127.81845,32.54040	,	10500	,	77.0157447448733	,	250	,	-17.09194231,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P49 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '11127.79181' ,	128.19833,32.61361	,	8000	,	75.5878383628898	,	250	,	-12.74582278,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P50 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '11284.70597' ,	128.60393,32.70067	,	6000	,	89.5837493906398	,	240	,	-5.535188422,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P51 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '11465.36829' ,	129.06691,32.70265	,	5000	,	80.6065401118857	,	210	,	-9.095632002,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P52 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '11575.31117' ,	129.317,32.73628	,	4000	,	54.6551620127634	,	180	,	-9.450188698,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P53 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '11681.12916' ,	129.47647,32.83521	,	3000	,	32.0600571330369	,	150	,	-6.344610836,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P54 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '11838.74324' ,	129.61101,33.01526	,	2000	,	6.36693952019   	,	100	,	-6.021665095,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P55 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '12004.817' ,	129.63079,33.16365	,	1000	,	357.907953231846	,	80	,	-4.276488021,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P56 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'), '12238.647' ,	129.62344, 33.33176	,	0	,	357.907953231846	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
);
