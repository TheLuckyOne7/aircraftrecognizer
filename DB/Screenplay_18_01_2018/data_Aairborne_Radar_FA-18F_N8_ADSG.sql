﻿-- Описание режимов работы БРЛС воздушного объекта  "F/A-18F №8 ADSG"
-- группа подавления системы ПВО - air defense suppression group (ADSG) - перевод google
-- Самолет F/A-18F №8  из группы подавления системы ПВО
  

--------------------------------------------------------------------------------- 
Insert into re_situation.m_airborne_radar_in_air_obj
(
m_airborne_radar_in_air_obj_id , -- Искуственный первичный ключ 
air_object_id , -- внешний ключ на воздушный объект 
placing_f_t_aircraft_id , -- внешний ключ на табл. размещение РЭС на типе ЛА
d_m_airborne_radar_id, -- внешний ключ на описание режима работы 
duration_puls, -- длительность зондирующего импульса, мкс 
width_spectrum_puls, -- Ширина спектра зондирующего импульса, МГц 
cycle_time_in_pack, -- Период повторения зондирующих импульсов, мкс
freq_pulses_in_a_pack, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
duration_pack, -- Длительность КГПИ, мс
period_pack, -- Мин. Период следования КГПИ, мс
q_of_carrier_freq_in_pack, -- Количество несущих ча-стот в пачке, шт
r_ch_freq_of_pack_in_overview, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
dur_of_r_irradiation_obj, -- Длительность облучения объ-екта , мс
period_of_r_irradiation_obj -- Период облучения объекта (цикл скани-рования), с
 )
 values 
--------------------------------------------------------------------------------- 
-- описание режимов работы БРЛС воздушного объекта  "F/A-18F №8 ADSG"
-- Самолет F/A-18F №8  из группы подавления системы ПВО

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р1"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р1'
), -- внешний ключ на описание режима работы "Режим Р1" БРЛC "AN/APG-79" 
5.0, -- длительность зондирующего импульса, мкс 
0.20, -- Ширина спектра зондирующего импульса, МГц 
2000, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
25.0, -- Длительность КГПИ, мс
1200.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
1.20 -- Период облучения объекта (цикл сканирования), с
),  

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р2"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р2'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛC "AN/APG-79" 
2.50, -- длительность зондирующего импульса, мкс 
0.38, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
12.50, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
25.0, -- Длительность КГПИ, мс
1200.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
1.20 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р3"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р3'
), -- внешний ключ на описание режима работы "Режим Р3" БРЛC "AN/APG-79" 
0.50, -- длительность зондирующего импульса, мкс 
2.1, -- Ширина спектра зондирующего импульса, МГц 
250, -- Период повторения зондирующих импульсов, мкс
7.50, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
23.0, -- Длительность КГПИ, мс
1250.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
23.0, -- Длительность облучения объ-екта , мс
1.25 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р4"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р4'
), -- внешний ключ на описание режима работы "Режим Р4" БРЛC "AN/APG-79" 
0.25, -- длительность зондирующего импульса, мкс 
4.2, -- Ширина спектра зондирующего импульса, МГц 
125, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
23.0, -- Длительность КГПИ, мс
1250.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
23.0, -- Длительность облучения объ-екта , мс
1.25 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р5"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р5'
), -- внешний ключ на описание режима работы "Режим Р5" БРЛC "AN/APG-79" 
50.0, -- длительность зондирующего импульса, мкс 
1.3, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
15.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
25.0, -- Длительность КГПИ, мс
200.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
0.20 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р6"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р6'
), -- внешний ключ на описание режима работы "Режим Р6" БРЛC "AN/APG-79" 
50.0, -- длительность зондирующего импульса, мкс 
1.3, -- Ширина спектра зондирующего импульса, МГц 
500, -- Период повторения зондирующих импульсов, мкс
15.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
25.0, -- Длительность КГПИ, мс
1200.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
1.20 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р7"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р7'
), -- внешний ключ на описание режима работы "Режим Р7" БРЛC "AN/APG-79" 
50.0, -- длительность зондирующего импульса, мкс 
1.3, -- Ширина спектра зондирующего импульса, МГц 
500, -- Период повторения зондирующих импульсов, мкс
17.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
25.0, -- Длительность КГПИ, мс
4800.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
4.80 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р8"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р8'
), -- внешний ключ на описание режима работы "Режим Р8" БРЛC "AN/APG-79" 
25.0, -- длительность зондирующего импульса, мкс 
2.45, -- Ширина спектра зондирующего импульса, МГц 
500, -- Период повторения зондирующих импульсов, мкс
8.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
38.0, -- Длительность КГПИ, мс
38.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
37.0, -- Длительность облучения объ-екта , мс
0.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р9"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р9'
), -- внешний ключ на описание режима работы "Режим Р9" БРЛC "AN/APG-79" 
1.50, -- длительность зондирующего импульса, мкс 
0.75, -- Ширина спектра зондирующего импульса, МГц 
6.0, -- Период повторения зондирующих импульсов, мкс
7.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
25.0, -- Длительность КГПИ, мс
200.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
0.20 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р10"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р10'
), -- внешний ключ на описание режима работы "Режим Р10" БРЛC "AN/APG-79" 
1.50, -- длительность зондирующего импульса, мкс 
0.75, -- Ширина спектра зондирующего импульса, МГц 
6.0, -- Период повторения зондирующих импульсов, мкс
9.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
0.80, -- Длительность КГПИ, мс
0.80, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
200.0, -- Длительность облучения объ-екта , мс
0.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р11"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р11'
), -- внешний ключ на описание режима работы "Режим Р11" БРЛC "AN/APG-79" 
3.750, -- длительность зондирующего импульса, мкс 
0.3, -- Ширина спектра зондирующего импульса, МГц 
37.0, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
25.0, -- Длительность КГПИ, мс
200.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
0.20 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р12"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р12'
), -- внешний ключ на описание режима работы "Режим Р12" БРЛC "AN/APG-79" 
3.750, -- длительность зондирующего импульса, мкс 
0.3, -- Ширина спектра зондирующего импульса, МГц 
37.0, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
25.0, -- Длительность КГПИ, мс
1200.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
1.20 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р13"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р13'
), -- внешний ключ на описание режима работы "Режим Р13" БРЛC "AN/APG-79" 
3.750, -- длительность зондирующего импульса, мкс 
0.3, -- Ширина спектра зондирующего импульса, МГц 
37.5, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
25.0, -- Длительность КГПИ, мс
4800.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
4.80 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р14"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р14'
), -- внешний ключ на описание режима работы "Режим Р14" БРЛC "AN/APG-79" 
4.0, -- длительность зондирующего импульса, мкс 
0.3, -- Ширина спектра зондирующего импульса, МГц 
40.0, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
0.45, -- Длительность КГПИ, мс
0.48, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
1850.0, -- Длительность облучения объ-екта , мс
0.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р15"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р15'
), -- внешний ключ на описание режима работы "Режим Р15" БРЛC "AN/APG-79" 
25.0, -- длительность зондирующего импульса, мкс 
2.45, -- Ширина спектра зондирующего импульса, МГц 
500.0, -- Период повторения зондирующих импульсов, мкс
10.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
25.0, -- Длительность КГПИ, мс
200.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
0.20 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р16"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р16'
), -- внешний ключ на описание режима работы "Режим Р16" БРЛC "AN/APG-79" 
25.0, -- длительность зондирующего импульса, мкс 
2.45, -- Ширина спектра зондирующего импульса, МГц 
500.0, -- Период повторения зондирующих импульсов, мкс
7.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
25.0, -- Длительность КГПИ, мс
1200.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
1.20 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р17"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р17'
), -- внешний ключ на описание режима работы "Режим Р17" БРЛC "AN/APG-79" 
15.0, -- длительность зондирующего импульса, мкс 
5.0, -- Ширина спектра зондирующего импульса, МГц 
500.0, -- Период повторения зондирующих импульсов, мкс
22.50, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
25.0, -- Длительность КГПИ, мс
25.0, -- Период следования КГПИ, мс
4, -- Количество несущих ча-стот в пачке, шт
4, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
4.80 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р18"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р18'
), -- внешний ключ на описание режима работы "Режим Р18" БРЛC "AN/APG-79" 
15.0, -- длительность зондирующего импульса, мкс 
5.0, -- Ширина спектра зондирующего импульса, МГц 
500.0, -- Период повторения зондирующих импульсов, мкс
7.50, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
3.0, -- Длительность КГПИ, мс
3.50, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
2850.0, -- Длительность облучения объ-екта , мс
0.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р19"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р19'
), -- внешний ключ на описание режима работы "Режим Р19" БРЛC "AN/APG-79" 
25.0, -- длительность зондирующего импульса, мкс 
2.45, -- Ширина спектра зондирующего импульса, МГц 
500.0, -- Период повторения зондирующих импульсов, мкс
7.50, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
3.0, -- Длительность КГПИ, мс
3.5, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
25.0, -- Длительность облучения объ-екта , мс
1.20 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р20"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р20'
), -- внешний ключ на описание режима работы "Режим Р20" БРЛC "AN/APG-79" 
15.0, -- длительность зондирующего импульса, мкс 
5.9, -- Ширина спектра зондирующего импульса, МГц 
130.0, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
19.0, -- Длительность КГПИ, мс
900.0, -- Период следования КГПИ, мс
3, -- Количество несущих ча-стот в пачке, шт
10.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
19.0, -- Длительность облучения объ-екта , мс
0.75 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-79" условное наименование "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1"
-- режим работы "Режим Р21"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №8 ADSG'
), -- внешний ключ на воздушный объект 'F/A-18F №8 ADSG'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-79'
and abr_name_mode_r like 'Режим Р21'
), -- внешний ключ на описание режима работы "Режим Р21" БРЛC "AN/APG-79" 
15.0, -- длительность зондирующего импульса, мкс 
5.9, -- Ширина спектра зондирующего импульса, МГц 
125.0, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
850.0, -- Длительность КГПИ, мс
850.0, -- Период следования КГПИ, мс
4, -- Количество несущих ча-стот в пачке, шт
20.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
850.0, -- Длительность облучения объ-екта , мс
0.85 -- Период облучения объекта (цикл сканирования), с
);
-- */

--------------------------------------------------------------------------------- 
-- Задание рабочих частот для БРЛС "ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1" 
-- воздушного объекта "F/A-18F №8 ADSG"
-- /*
insert into re_situation.oper_freq_air_objects
(
m_airborne_radar_in_air_obj_id , -- Внешний ключ 
operating_frequency  -- значение рабочей частоты 
)
values 
-- Рабочая частота для режима "Режим Р1"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р1'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.301 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р2"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р2'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01  -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р3"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р3'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01   -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р4"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р4'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р5"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р5'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р6"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р6'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р7"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р7'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р8"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р8'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р9"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р9'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р10"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р10'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р11"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р11'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р12"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р12'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р13"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р13'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р14"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р14'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р15"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р15'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р16"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р16'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р17"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р17'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р18"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р18'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р19"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р19'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р20"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р20'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р21"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F/A-18F №8 ADSG'
and pfta.code_name like 'ЛА-"F/A-18F" БРЛС-"AN/APG-79" №1'
and dmar.abr_name_mode_r like 'Режим Р21'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.01    -- значение рабочей частоты 
); 
-- */
  