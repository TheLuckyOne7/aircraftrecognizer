﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость
aircraft_condition_id -- номер состояния  
)
values
-- точка P0	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '0'          ,	131.30681, 31.48096	,	0	,	358.822644043231	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P1	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '179.358799' ,	131.30681, 31.48096	,	0	,	358.822644043231	,	80	,	7.99520677,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P2	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '304.4337381' ,	131.30464, 31.57092	,	1000	,	321.949799632081	,	100	,	7.154476592,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P3	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '444.2063668' ,	131.21363, 31.66985	,	2000	,	276.425674875807	,	120	,	7.579802898,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P4	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '576.1359187' ,	131.04743, 31.68567	,	3000	,	219.487274898466	,	140	,	8.184473652,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P5	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '698.3184857' ,	130.93268, 31.56696	,	4000	,	93.4078072912647	,	160	,	8.435856533,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P6	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '816.8600939' ,	130.94455, 31.39681	,	5000	,	119.996413843878	,	180	,	8.85171027,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P7	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '929.8326121' ,	131.05139, 31.23852	,	6000	,	108.756849525408	,	200	,	8.113681757,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P8	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '1053.081222' ,	131.13449, 31.02880	,	7000	,	98.5692414459127	,	220	,	8.66364774,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P9	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '1168.506044' ,	131.17406, 30.80324	,	8000	,	187.819439271829	,	250	,	8.147122,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P10	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '1291.248775' ,	131.13053, 30.53020	,	9000	,	207.545657604577	,	250	,	10.48226402,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P11	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '1386.648013' ,	131.01577, 30.34026	,	10000	,	208.902116389516	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P12	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '1481.258098' ,	130.89706, 30.15428	,	10000	,	242.496790616587	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=136 )
),
-- точка P13	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '1587.922212' ,	130.65172, 30.04348	,	10000	,	261.115815487687	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=136 )
),
-- точка P14	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '1689.895675' ,	130.39055, 30.00787	,	10000	,	270.0245734247289	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=138 )
),
-- точка P15	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '1742.714557' ,	130.25363, 30.00785	,	10000	,	298.563143638759	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=138 )
),
-- точка P16	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '2460.135088' ,	128.60748, 30.76761	,	10000	,	299.909757968603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=138 )
),
-- точка P17	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '3132.831538' ,	127.07213, 31.51154	,	10000	,	336.521235635308  	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P18	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '3205.885449' ,	126.99537, 31.66193	,	10000	,	5.64304402779314	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P19	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '3282.057783' ,	127.01516, 31.83209	,	10000	,	63.3366052959042	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P20	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '3360.755296' ,	127.20114, 31.91123	,	10000	,	173.719553823733	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P21	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '3472.187714' ,	127.49396, 31.88353	,	10000	,	159.196959721751	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P22	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '3664.658728' ,	127.96882, 31.72920	,	10000	,	148.848328847915	,	250	,	-4.908458631,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P23	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '3868.388672' ,	128.42784, 31.49178	,	9000	,	151.257876658535	,	240	,	-4.695658736,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P24	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '4081.351337' ,	128.89873, 31.27018	,	8000	,	153.263731199934	,	220	,	-4.339028503,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P25	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '4311.817673' ,	129.37358, 31.06441	,	7000	,	153.053748221108	,	200	,	-3.626400789,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=133 )
),
-- точка P26	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '4587.573183' ,	129.88800, 30.83886	,	6000	,	87.4643775195948	,	180	,	-4.324084612,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P27	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '4818.836003' ,	130.32328, 30.85468	,	5000	,	82.0163512322184	,	160	,	-4.992199778,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P28	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '5019.148499' ,	130.65568, 30.89426	,	4000	,	65.838202726581 	,	140	,	-3.602307352,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P29	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '5296.748355' ,	131.02765, 31.03671	,	3000	,	50.2767517481495	,	120	,	-3.95438606,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P30	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '5549.632111' ,	131.27298, 31.21082	,	2000	,	13.5406339861055	,	100	,	-5.66368732,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P31	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '5726.19553' ,	131.31651, 31.36515	,	1000	,	355.914319196458	,	80	,	-6.196099596,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
),
-- точка P32	
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №2'), '5887.587384' ,	131.30681, 31.48096	,	0	,	355.914319196458	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=132 )
);
