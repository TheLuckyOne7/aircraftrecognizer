﻿insert into re_situation.operating_frequencies
(
  operating_frequencies_id , -- первичный ключ 
  radio_network_id , -- номер радиосети в табл radio_network
  operating_frequency -- значение рабочей частоты
)
values 
(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Управления ударной группой на маршруте'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  25.38
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Взаимодействия ударной группы с группой дозаправки'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  22.40
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  54.258
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Управления ударной группой'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  238.453
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Управления ударной группой'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  245.567
),
(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Управления ударной группой'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  258.832
),
(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Управления ударной группой'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  323.453
),


(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Управления группами прорыва ПВО'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  253.453
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Управления группами прорыва ПВО'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  260.567
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Управления группами прорыва ПВО'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  273.832
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Управления группами прорыва ПВО'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  338.453
),


(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Управления группой охраны E-2C'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  271.345
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Управления группой охраны E-3C'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  282.400
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  354.654 
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Наведения ДРЛО E-2C N1'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  372.600 
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Передачи данных JTIDS'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  960.000 
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Передачи данных JTIDS'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  965.000 
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Передачи данных JTIDS'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  1210.000 
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Передачи данных JTIDS'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  1215.000 
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  121.345 
),

(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Управления воздушным движением'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  148.776 
),
(
  default, -- operating_frequencies_id , -- первичный ключ 
  (
   select radio_network_id
   from re_situation.radio_network
   where destination_r like 'Взаимодействия с кораблями УАНГ'
  ), -- radio_network_id , -- номер радиосети в табл radio_network
  161.890 
);
