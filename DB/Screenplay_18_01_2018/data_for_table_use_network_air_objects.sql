﻿-- delete from re_situation.use_networks_air_objects ;
-- select setval('re_situation.use_networks_air_objects_use_networks_air_objects_id_seq' , 1) ;

insert into re_situation.use_networks_air_objects
(
  use_networks_air_objects_id , -- первичный ключ 
  air_object_id , -- номер воздушного объекта 
  placing_f_t_aircraft_id , -- номер строки средства в размещении,
  radio_network_id -- номер радиосети 
)
values
-- B-1B master
-- B-1B
-- AN/ARC-190(V)
-- Управления ударной группой на маршруте
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B master'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/ARC-190(V)'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления ударной группой на маршруте'
  ) -- radio_network_id -- номер радиосети 
),

-- B-1B master
-- B-1B
-- AN/ARC-190(V)
-- Взаимодействия ударной группы с группой дозаправки
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B master'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/ARC-190(V)'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия ударной группы с группой дозаправки'
  ) -- radio_network_id -- номер радиосети 
),


-- B-1B master
-- B-1B
-- AN/ARC-210
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B master'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/ARC-210'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- B-1B master
-- B-1B
-- AN/ARC-210
-- Управления ударной группой
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B master'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/ARC-210'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления ударной группой'
  ) -- radio_network_id -- номер радиосети 
),

-- B-1B master
-- B-1B
-- AN/ARC-210
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B master'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/ARC-210'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- B-1B master
-- B-1B
-- AN/ARC-210
-- Наведения ДРЛО E-2C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B master'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/ARC-210'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-2C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- B-1B master
-- B-1B
-- AN/URS-107
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B master'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/URS-107'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- B-1B slave
-- B-1B
-- AN/ARC-190(V)
-- Управления ударной группой на маршруте
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B slave'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/ARC-190(V)'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления ударной группой на маршруте'
  ) -- radio_network_id -- номер радиосети 
),

-- B-1B slave
-- B-1B
-- AN/ARC-190(V)
-- Взаимодействия ударной группы с группой дозаправки
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B slave'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/ARC-190(V)'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия ударной группы с группой дозаправки'
  ) -- radio_network_id -- номер радиосети 
),


-- B-1B slave
-- B-1B
-- AN/ARC-210
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B slave'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/ARC-210'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- B-1B slave
-- B-1B
-- AN/ARC-210
-- Управления ударной группой
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B slave'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/ARC-210'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления ударной группой'
  ) -- radio_network_id -- номер радиосети 
),

-- B-1B slave
-- B-1B
-- AN/ARC-210
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B slave'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/ARC-210'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- B-1B slave
-- B-1B
-- AN/ARC-210
-- Наведения ДРЛО E-2C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B slave'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/ARC-210'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-2C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- B-1B slave
-- B-1B
-- AN/URS-107
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'B-1B slave'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'B-1B'
   and tf.name_facilities_e like 'AN/URS-107'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------

-- КС-135 №1
-- KC-135
-- AN/ARC-190(V)
-- Взаимодействия ударной группы с группой дозаправки
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'КС-135 №1'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'KC-135'
   and tf.name_facilities_e like 'AN/ARC-190(V)'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия ударной группы с группой дозаправки'
  ) -- radio_network_id -- номер радиосети 
),


-- КС-135 №1
-- KC-135
-- AN/ARC-210
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'КС-135 №1'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'KC-135'
   and tf.name_facilities_e like 'AN/ARC-210'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- КС-135 №1
-- KC-135
-- AN/ARC-210
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'КС-135 №1'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'KC-135'
   and tf.name_facilities_e like 'AN/ARC-210'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- КС-135 №1
-- KC-135
-- AN/URS-107
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'КС-135 №1'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'KC-135'
   and tf.name_facilities_e like 'AN/URS-107'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------

-- КС-135 №2
-- KC-135
-- AN/ARC-190(V)
-- Взаимодействия ударной группы с группой дозаправки
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'КС-135 №2'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'KC-135'
   and tf.name_facilities_e like 'AN/ARC-190(V)'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия ударной группы с группой дозаправки'
  ) -- radio_network_id -- номер радиосети 
),


-- КС-135 №2
-- KC-135
-- AN/ARC-210
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'КС-135 №2'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'KC-135'
   and tf.name_facilities_e like 'AN/ARC-210'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- КС-135 №2
-- KC-135
-- AN/ARC-210
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'КС-135 №2'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'KC-135'
   and tf.name_facilities_e like 'AN/ARC-210'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- КС-135 №2
-- KC-135
-- AN/URS-107
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'КС-135 №2'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'KC-135'
   and tf.name_facilities_e like 'AN/URS-107'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------

-- E-3C AWACS
-- E-3C
-- AN/ARC-194
-- р.ст. AN/ARC-194 на л.а. E-3C N1
-- Управления ударной группой на маршруте
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-3C AWACS'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-3C'
   and tf.name_facilities_e like 'AN/ARC-194'
   and code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N1'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления ударной группой на маршруте'
  ) -- radio_network_id -- номер радиосети 
),

-- E-3C AWACS
-- E-3C
-- AN/ARC-194
-- р.ст. AN/ARC-194 на л.а. E-3C N2
-- Взаимодействия ударной группы с группой дозаправки
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-3C AWACS'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-3C'
   and tf.name_facilities_e like 'AN/ARC-194'
   and code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N2'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия ударной группы с группой дозаправки'
  ) -- radio_network_id -- номер радиосети 
),

-- E-3C AWACS
-- E-3C
-- AN/ARC-194
-- р.ст. AN/ARC-194 на л.а. E-3C N3
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-3C AWACS'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-3C'
   and tf.name_facilities_e like 'AN/ARC-194'
   and code_name like 'р.ст. AN/ARC-194 на л.а. E-3C N3'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- E-3C AWACS
-- E-3C
-- AN/ARC-192
-- р.ст. AN/ARC-192 на л.а. E-3С N1
-- Управления ударной группой
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-3C AWACS'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-3C'
   and tf.name_facilities_e like 'AN/ARC-192'
   and code_name like 'р.ст. AN/ARC-192 на л.а. E-3С N1'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления ударной группой'
  ) -- radio_network_id -- номер радиосети 
),

-- E-3C AWACS
-- E-3C
-- AN/ARC-186
-- 'р.ст. AN/ARC-186 на л.а. E-3С N1'
-- Управления группами прорыва ПВО
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-3C AWACS'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-3C'
   and tf.name_facilities_e like 'AN/ARC-186'
   and code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N1'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группами прорыва ПВО'
  ) -- radio_network_id -- номер радиосети 
),

-- E-3C AWACS
-- E-3C
-- AN/ARC-186
-- 'р.ст. AN/ARC-186 на л.а. E-3С N1'
-- Управления группой охраны E-3C
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-3C AWACS'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-3C'
   and tf.name_facilities_e like 'AN/ARC-186'
   and code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N2'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группой охраны E-3C'
  ) -- radio_network_id -- номер радиосети 
),

-- E-3C AWACS
-- E-3C
-- AN/ARC-186
-- 'р.ст. AN/ARC-186 на л.а. E-3С N1'
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-3C AWACS'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-3C'
   and tf.name_facilities_e like 'AN/ARC-186'
   and code_name like 'р.ст. AN/ARC-186 на л.а. E-3С N3'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- E-3C AWACS
-- E-3C
-- AN/URS-107
-- 'р.ст. AN/URS-107 на л.а. E-3С'
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-3C AWACS'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-3C'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. E-3С'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- E-3C AWACS
-- E-3C
-- AN/ARC-193
-- 'р.ст. AN/ARC-193 на л.а. E-3C'
-- Управления воздушным движением
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-3C AWACS'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-3C'
   and tf.name_facilities_e like 'AN/ARC-193'
   and code_name like 'р.ст. AN/ARC-193 на л.а. E-3C'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления воздушным движением'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F-16D №1 guard E-3C
-- F-16D
-- AN/ARC-164
-- р.ст. AN/ARC-164 на л.а. F-16D
-- Управления группой охраны E-3C
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №1 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/ARC-164'
   and code_name like 'р.ст. AN/ARC-164 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группой охраны E-3C'
  ) -- radio_network_id -- номер радиосети 
),

-- F-16D №1 guard E-3C
-- F-16D
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F-16D
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №1 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F-16D №1 guard E-3C
-- F-16D
-- AN/ARC-222
-- р.ст. AN/ARC-222 на л.а. F-16D
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №1 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/ARC-222'
   and code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

-- F-16D №1 guard E-3C
-- F-16D
-- AN/ARC-222
-- р.ст. AN/ARC-222 на л.а. F-16D
-- Управления воздушным движением
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №1 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/ARC-222'
   and code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления воздушным движением'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F-16D №2 guard E-3C
-- F-16D
-- AN/ARC-164
-- р.ст. AN/ARC-164 на л.а. F-16D
-- Управления группой охраны E-3C
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №2 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/ARC-164'
   and code_name like 'р.ст. AN/ARC-164 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группой охраны E-3C'
  ) -- radio_network_id -- номер радиосети 
),

-- F-16D №2 guard E-3C
-- F-16D
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F-16D
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №2 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F-16D №2 guard E-3C
-- F-16D
-- AN/ARC-222
-- р.ст. AN/ARC-222 на л.а. F-16D
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №2 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/ARC-222'
   and code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

-- F-16D №2 guard E-3C
-- F-16D
-- AN/ARC-222
-- р.ст. AN/ARC-222 на л.а. F-16D
-- Управления воздушным движением
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №2 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/ARC-222'
   and code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления воздушным движением'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F-16D №3 guard E-3C
-- F-16D
-- AN/ARC-164
-- р.ст. AN/ARC-164 на л.а. F-16D
-- Управления группой охраны E-3C
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №3 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/ARC-164'
   and code_name like 'р.ст. AN/ARC-164 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группой охраны E-3C'
  ) -- radio_network_id -- номер радиосети 
),

-- F-16D №3 guard E-3C
-- F-16D
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F-16D
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №3 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F-16D №3 guard E-3C
-- F-16D
-- AN/ARC-222
-- р.ст. AN/ARC-222 на л.а. F-16D
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №3 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/ARC-222'
   and code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

-- F-16D №3 guard E-3C
-- F-16D
-- AN/ARC-222
-- р.ст. AN/ARC-222 на л.а. F-16D
-- Управления воздушным движением
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №3 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/ARC-222'
   and code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления воздушным движением'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F-16D №4 guard E-3C
-- F-16D
-- AN/ARC-164
-- р.ст. AN/ARC-164 на л.а. F-16D
-- Управления группой охраны E-3C
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №4 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/ARC-164'
   and code_name like 'р.ст. AN/ARC-164 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группой охраны E-3C'
  ) -- radio_network_id -- номер радиосети 
),

-- F-16D №4 guard E-3C
-- F-16D
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F-16D
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №4 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F-16D №4 guard E-3C
-- F-16D
-- AN/ARC-222
-- р.ст. AN/ARC-222 на л.а. F-16D
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №4 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/ARC-222'
   and code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

-- F-16D №4 guard E-3C
-- F-16D
-- AN/ARC-222
-- р.ст. AN/ARC-222 на л.а. F-16D
-- Управления воздушным движением
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F-16D №4 guard E-3C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F-16D'
   and tf.name_facilities_e like 'AN/ARC-222'
   and code_name like 'р.ст. AN/ARC-222 на л.а. F-16D'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления воздушным движением'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------

-- E-2C Hawkeye	
-- E-2C 
-- AN/ARC-182
-- р.ст. AN/ARC-182 на л.а. E-2C
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-2C Hawkeye'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-2C'
   and tf.name_facilities_e like 'AN/ARC-182'
   and code_name like 'р.ст. AN/ARC-182 на л.а. E-2C'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- E-2C Hawkeye	
-- E-2C
-- AN/ARC-164
-- р.ст. AN/ARC-164 на л.а. E-2C
-- Управления ударной группой
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-2C Hawkeye'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-2C'
   and tf.name_facilities_e like 'AN/ARC-164'
   and code_name like 'р.ст. AN/ARC-164 на л.а. E-2C'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления ударной группой'
  ) -- radio_network_id -- номер радиосети 
),

-- E-2C Hawkeye	
-- E-2C
-- AN/ARC-164
-- р.ст. AN/ARC-164 на л.а. E-2C
-- Управления группой охраны E-2C
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-2C Hawkeye'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-2C'
   and tf.name_facilities_e like 'AN/ARC-164'
   and code_name like 'р.ст. AN/ARC-164 на л.а. E-2C'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группой охраны E-2C'
  ) -- radio_network_id -- номер радиосети 
),

-- E-2C Hawkeye	
-- E-2C
-- AN/ARC-164
-- р.ст. AN/ARC-164 на л.а. E-2C
-- Наведения ДРЛО E-2C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-2C Hawkeye'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-2C'
   and tf.name_facilities_e like 'AN/ARC-164'
   and code_name like 'р.ст. AN/ARC-164 на л.а. E-2C'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-2C N1'
  ) -- radio_network_id -- номер радиосети 
),


-- E-2C Hawkeye	
-- E-2C
-- AN/ARC-181
-- р.ст. AN/ARC-181 на л.а. E-2C
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-2C Hawkeye'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-2C'
   and tf.name_facilities_e like 'AN/ARC-181'
   and code_name like 'р.ст. AN/ARC-181 на л.а. E-2C'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- E-2C Hawkeye	
-- E-2C
-- AN/ARC-182
-- р.ст. AN/ARC-182 на л.а. E-2C
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-2C Hawkeye'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-2C'
   and tf.name_facilities_e like 'AN/ARC-182'
   and code_name like 'р.ст. AN/ARC-182 на л.а. E-2C'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

-- E-2C Hawkeye	
-- E-2C
-- AN/ARC-193
-- р.ст. AN/ARC-193 на л.а. E-2C
-- Управления воздушным движением
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-2C Hawkeye'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-2C'
   and tf.name_facilities_e like 'AN/ARC-193'
   and code_name like 'р.ст. AN/ARC-193 на л.а. E-2C'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления воздушным движением'
  ) -- radio_network_id -- номер радиосети 
),

-- E-2C Hawkeye	
-- E-2C
-- AN/ARC-201
-- р.ст. AN/ARC-201 на л.а. E-2C
-- Взаимодействия с кораблями УАНГ
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'E-2C Hawkeye'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'E-2C'
   and tf.name_facilities_e like 'AN/ARC-201'
   and code_name like 'р.ст. AN/ARC-201 на л.а. E-2C'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с кораблями УАНГ'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F/A-18F №1 g-d E-2C
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Управления группой охраны E-3C
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №1 g-d E-2C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группой охраны E-2C'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №1 g-d E-2C
-- F/A-18F
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F/A-18F
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №1 g-d E-2C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №1 g-d E-2C
-- F/A-18F
-- AN/ARC-210
-- р.ст. AN/ARC-210 на л.а. F/A-18F
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №1 g-d E-2C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F/A-18F №2 g-d E-2C
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Управления группой охраны E-3C
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №2 g-d E-2C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группой охраны E-2C'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №2 g-d E-2C
-- F/A-18F
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F/A-18F
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №2 g-d E-2C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №2 g-d E-2C
-- F/A-18F
-- AN/ARC-210
-- р.ст. AN/ARC-210 на л.а. F/A-18F
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №2 g-d E-2C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F/A-18F №3 g-d E-2C
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Управления группой охраны E-3C
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №3 g-d E-2C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группой охраны E-2C'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №3 g-d E-2C
-- F/A-18F
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F/A-18F
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №3 g-d E-2C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №3 g-d E-2C
-- F/A-18F
-- AN/ARC-210
-- р.ст. AN/ARC-210 на л.а. F/A-18F
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №3 g-d E-2C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F/A-18F №4 g-d E-2C
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Управления группой охраны E-3C
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №4 g-d E-2C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группой охраны E-2C'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №4 g-d E-2C
-- F/A-18F
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F/A-18F
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №4 g-d E-2C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №4 g-d E-2C
-- F/A-18F
-- AN/ARC-210
-- р.ст. AN/ARC-210 на л.а. F/A-18F
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №4 g-d E-2C'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F/A-18F №1 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №1 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №1 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Управления группами прорыва ПВО
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №1 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группами прорыва ПВО'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №1 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №1 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №1 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-2C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №1 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-2C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №1 ADSG
-- F/A-18F
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F/A-18F
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №1 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №1 ADSG
-- F/A-18F
-- AN/ARC-210
-- р.ст. AN/ARC-210 на л.а. F/A-18F
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №1 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),


----------------------------------------------------------------------------
-- F/A-18F №2 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №2 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №2 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Управления группами прорыва ПВО
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №2 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группами прорыва ПВО'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №2 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №2 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №2 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-2C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №2 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-2C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №2 ADSG
-- F/A-18F
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F/A-18F
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №2 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №2 ADSG
-- F/A-18F
-- AN/ARC-210
-- р.ст. AN/ARC-210 на л.а. F/A-18F
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №2 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F/A-18F №3 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №3 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №3 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Управления группами прорыва ПВО
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №3 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группами прорыва ПВО'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №3 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №3 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №3 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-2C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №3 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-2C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №3 ADSG
-- F/A-18F
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F/A-18F
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №3 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №3 ADSG
-- F/A-18F
-- AN/ARC-210
-- р.ст. AN/ARC-210 на л.а. F/A-18F
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №3 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F/A-18F №4 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №4 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №4 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Управления группами прорыва ПВО
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №4 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группами прорыва ПВО'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №4 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №4 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №4 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-2C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №4 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-2C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №4 ADSG
-- F/A-18F
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F/A-18F
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №4 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №4 ADSG
-- F/A-18F
-- AN/ARC-210
-- р.ст. AN/ARC-210 на л.а. F/A-18F
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №4 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F/A-18F №5 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №5 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №5 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Управления группами прорыва ПВО
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №5 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группами прорыва ПВО'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №5 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №5 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №5 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-2C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №5 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-2C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №5 ADSG
-- F/A-18F
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F/A-18F
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №5 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №5 ADSG
-- F/A-18F
-- AN/ARC-210
-- р.ст. AN/ARC-210 на л.а. F/A-18F
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №5 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F/A-18F №6 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №6 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №6 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Управления группами прорыва ПВО
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №6 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группами прорыва ПВО'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №6 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №6 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №6 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-2C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №6 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-2C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №6 ADSG
-- F/A-18F
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F/A-18F
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №6 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №6 ADSG
-- F/A-18F
-- AN/ARC-210
-- р.ст. AN/ARC-210 на л.а. F/A-18F
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №6 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F/A-18F №7 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №7 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №7 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Управления группами прорыва ПВО
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №7 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группами прорыва ПВО'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №7 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №7 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №7 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-2C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №7 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-2C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №7 ADSG
-- F/A-18F
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F/A-18F
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №7 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №7 ADSG
-- F/A-18F
-- AN/ARC-210
-- р.ст. AN/ARC-210 на л.а. F/A-18F
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №7 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
),

----------------------------------------------------------------------------
-- F/A-18F №8 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Взаимодействия с сухопутными войсками
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №8 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с сухопутными войсками'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №8 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Управления группами прорыва ПВО
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №8 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Управления группами прорыва ПВО'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №8 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-3C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №8 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-3C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №8 ADSG
-- F/A-18F 
-- AN/ARC-210
-- р.ст. AN/ARC-164 на л.а. F/A-18F
-- Наведения ДРЛО E-2C N1
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №8 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Наведения ДРЛО E-2C N1'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №8 ADSG
-- F/A-18F
-- AN/URS-107
-- р.ст. AN/URS-107 на л.а. F/A-18F
-- Передачи данных JTIDS
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №8 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/URS-107'
   and code_name like 'р.ст. AN/URS-107 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Передачи данных JTIDS'
  ) -- radio_network_id -- номер радиосети 
),

-- F/A-18F №8 ADSG
-- F/A-18F
-- AN/ARC-210
-- р.ст. AN/ARC-210 на л.а. F/A-18F
-- Взаимодействия с воздушными маяками УВД
(
  default, -- use_networks_air_objects_id , -- первичный ключ 
  (
   select air_object_id
   from re_situation.air_object
   where air_object_name like 'F/A-18F №8 ADSG'
  ), -- air_object_id , -- номер воздушного объекта 

  (
   select pa.placing_f_t_aircraft_id -- , pa.code_name
   from re_situation.placing_f_t_aircraft as pa, 
   re_situation.type_of_aircraft as ta, re_situation.types_of_re_facilities as tf 
   where pa.types_of_re_facilities_id=tf.types_of_re_facilities_id
   and ta.type_of_aircraft_id=pa.type_of_aircraft_id
   and ta.code_name_e like 'F/A-18F'
   and tf.name_facilities_e like 'AN/ARC-210'
   and code_name like 'р.ст. AN/ARC-210 на л.а. F/A-18F'
  ), -- placing_f_t_aircraft_id , -- номер строки средства в размещении,

  (
   select radio_network_id from re_situation.radio_network 
   where destination_r like 'Взаимодействия с воздушными маяками УВД'
  ) -- radio_network_id -- номер радиосети 
);

-- */