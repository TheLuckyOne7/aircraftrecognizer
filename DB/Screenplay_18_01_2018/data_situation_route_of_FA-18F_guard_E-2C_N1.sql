﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)

values
/*Точка P1 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '0'           ,	125.01986, 28.67674	,	0	,	0.115395740752675	,	80	,	1.801967189,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/*Точка P2 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '554.9708358' ,	125.02078,29.07597	,	1500	,	17.7444669861546	,	100	,	1.73079885,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/*Точка P3 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '1132.806725' ,	125.20281,29.57061	,	3000	,	19.349407793732 	,	120	,	1.933342186,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
),
/*Точка P4 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '1650.14718' ,	125.41649,30.09690	,	4500	,	17.4805210711346	,	140	,	2.135403444,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=9 ) 
),
/*Точка P5 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '2118.571324' ,	125.62226,30.65881	,	6000	,	31.0811683653277	,	160	,	3.17327478,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=9 ) 
),
/*Точка P6 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '2433.777332' ,	125.89530,31.04660	,	6000	,	16.4482365287221	,	180	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=11 ) 
),
/*Точка P7 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '2699.154088' ,	126.03776,31.45814	,	6000	,	335.58545150743 	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=12 ) 
),
/*Точка P8 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '2943.519908' ,	125.82407,31.85781	,	6000	,	291.523592825597	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=13 ) 
),
/*Точка P9 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '3106.186709' ,	125.50355,31.96465	,	6000	,	95.7724916981528	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=14 ) 
),
/*Точка P10 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '3299.668315' ,	125.09597,31.92903	,	6000	,	137.871354201487	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=15 ) 
),
/*Точка P11 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '3492.449496' ,	124.82293,31.67182	,	6000	,	95.607724282297 	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=16 ) 
),
/*Точка P12 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '3684.947427' ,	124.86250,31.32756	,	6000	,	116.727217105761	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=17 ) 
),
/*Точка P13 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '3911.671327' ,	125.07618,30.96350	,	6000	,	97.9381625607957	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=18 ) 
),
/*Точка P14 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '4145.118037' ,	125.14345,30.54801	,	6000	,	154.964377857735	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=19 ) 
),
/*Точка P15 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '4324.911068' ,	124.98517,30.25519	,	6000	,	112.898087478145	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=9 ) 
),
/*Точка P16 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '4510.997634' ,	124.62903,30.12460	,	6000	,	284.856872837952	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=10 ) 
),
/*Точка P17 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '4709.837614' ,	124.22937,30.21561	,	6000	,	334.010189100638	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=11 ) 
),
/*Точка P18 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '4913.258493' ,	124.04338,30.54405	,	6000	,	357.076264223957	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=12 ) 
),
/*Точка P19 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '5098.480438' ,	124.02360,30.87645	,	6000	,	35.3982914874537	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=13 ) 
),
/*Точка P20 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '5274.197084' ,	124.23728,31.13366	,	6000	,	65.177172582464 	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=14 ) 
),
/*Точка P21 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '5421.475818' ,	124.51823,31.24446	,	6000	,	154.017256057436	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=15 ) 
),
/*Точка P22 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '5601.866629' ,	124.85854,31.10200	,	6000	,	166.939494097615	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=16 ) 
),
/*Точка P23 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '5756.821238' ,	125.17511,31.03869	,	6000	,	168.965228765556	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=17 ) 
),
/*Точка P24 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '5928.016707' ,	125.52729,30.97933	,	6000	,	77.8654445348171	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=18 ) 
),
/*Точка P25 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '6107.485741' ,	125.89530,31.04660	,	6000	,	16.4482365287221	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=11 ) 
),
/*Точка P26 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '6346.324821' ,	126.03776,31.45814	,	6000	,	335.58545150743 	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=12 ) 
),
/*Точка P27 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '6590.690641' ,	125.82407,31.85781	,	6000	,	291.523592825597	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=13 ) 
),
/*Точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '6753.357443' ,	125.50355,31.96465	,	6000	,	95.7724916981528	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=14 ) 
),
/*Точка P29 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '6946.839048' ,	125.09597,31.92903	,	6000	,	137.871354201487	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=15 ) 
),
/*Точка P30 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '7139.620229' ,	124.82293,31.67182	,	6000	,	95.607724282297 	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=16 ) 
),
/*Точка P31 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '7332.11816' ,	124.86250,31.32756	,	6000	,	116.727217105761	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=17 ) 
),
/*Точка P32 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '7558.842061' ,	125.07618,30.96350	,	6000	,	97.9381625607957	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=18 ) 
),
/*Точка P33 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '7792.28877' ,	125.14345,30.54801	,	6000	,	154.964377857735	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=19 ) 
),
/*Точка P34 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '7972.081801' ,	124.98517,30.25519	,	6000	,	112.898087478145	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=9 ) 
),
/*Точка P35 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '8158.168367' ,	124.62903,30.12460	,	6000	,	123.442454335259	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=10 ) 
),
/*Точка P36 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '8448.902267' ,	124.12648,29.83573	,	6000	,	153.755842541168	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=9 ) 
),
/*Точка P37 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '8652.597876' ,	123.94050,29.50730	,	6000	,	174.315863889898	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=9 ) 
),
/*Точка P38 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '8924.794254' ,	123.88510,29.02057	,	6000	,	93.0082189896347	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=9 ) 
),
/*Точка P39 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '9182.795748' ,	123.91280,28.55759	,	6000	,	96.0160719298395	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=11 ) 
),
/*Точка P40 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '9386.506999' ,	123.95633,28.19354	,	6000	,	107.363124016718	,	180	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=12 ) 
),
/*Точка P41 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '9596.691137' ,	124.07108,27.86906	,	6000	,	140.868446699235	,	160	,	-3.53438164,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=13 ) 
),
/*Точка P42 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '9879.692628' ,	124.42722,27.61185	,	6000	,	89.8881225267507	,	140	,	-2.941562048,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=14 ) 
),
/*Точка P43 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '10219.74139' ,	124.90999,27.61185	,	4500	,	25.5521269344547	,	120	,	-2.560975743,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=15 ) 
),
/*Точка P44 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '10610.29414' ,	125.11575,27.99173	,	3000	,	353.813601486641	,	100	,	-3.475220334,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
),
/*Точка P45 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '10898.07955' ,	125.08410,28.24894	,	1500	,	352.494998433702	,	80	,	-1.667164575,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
),
/*Точка P46 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №1 g-d E-2C'), '11497.92389' ,	125.01986, 28.67674	,	0	,	352.494998433702	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
);

