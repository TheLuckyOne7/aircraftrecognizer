﻿insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)
values
/*Точка P1 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '90' ,	129.62344, 33.33176	,	0	,	359.762737219524	,	80	,	9.562651856,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P2 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '246.8602541' ,	129.62288,33.44460	,	1500	,	0	,	100	,	15.48938854,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P3 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '343.7007456' ,	129.62288,33.53166	,	3000	,	320.234059348107	,	120	,	14.44233107,
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P4 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '482.1825455' ,	129.50812,33.64641	,	5000	,	285.783314743683	,	140	,	13.61590608,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P5 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '592.3478177' ,	129.34786,33.68400	,	6500	,	270.713835836974	,	160	,	12.47060885,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P6 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '712.6306377' ,	129.14011,33.68598	,	8000	,	95.5108510865669	,	180	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 )
),
/*Точка P7 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '838.4908184' ,	128.89675,33.66620	,	8000	,	101.009812547814	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 )
),
/*Точка P8 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '970.232919' ,	128.61778,33.62069	,	8000	,	104.997813793904	,	220	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 )
),
/*Точка P9 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '1124.020417' ,	128.26560,33.54155	,	8000	,	112.765321771661	,	240	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 )
),
/*Точка P10 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '1246.95415' ,	127.97277,33.43866	,	8000	,	110.305169982287	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 )
),
/*Точка P11 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '1348.180593' ,	127.71754,33.35952	,	8000	,	117.213325272936	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=39 )
),
/*Точка P12 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '1474.985824' ,	127.41482,33.22894	,	8000	,	140.569163377214	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=40 )
),
/*Точка P13 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '1636.7987' ,	127.13981,32.94799	,	8000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '1682.319494' ,	127.08441,32.85697	,	8000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '1729.02348' ,	127.08638,32.75211	,	8000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '1806.575528' ,	127.09034,32.57800	,	8000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '1849.087578' ,	127.15366,32.49886	,	8000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '1886.778326' ,	127.24467,32.46324	,	8000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '1933.156514' ,	127.36536,32.44148	,	8000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '1974.812917' ,	127.45637,32.49490	,	8000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2073.112362' ,	127.65027,32.64329	,	8000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2172.354751' ,	127.78085,32.83719	,	8000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2272.620451' ,	127.82834,33.05878	,	8000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2374.160922' ,	127.88176,33.28236	,	8000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2470.442456' ,	128.06774,33.43273	,	8000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2577.501554' ,	128.35067,33.47824	,	8000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2628.7847' ,	128.48522,33.45251	,	8000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2676.824194' ,	128.58810,33.38722	,	8000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2730.214202' ,	128.62965,33.27247	,	8000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2788.375598' ,	128.63163,33.14188	,	8000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2839.036475' ,	128.59206,33.03306	,	8000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2884.995775' ,	128.50302,32.96183	,	8000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2940.143547' ,	128.38233,32.89061	,	8000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '2996.700852' ,	128.23790,32.85301	,	8000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3091.597777' ,	127.98860,32.89259	,	8000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3189.623067' ,	127.74128,32.96579	,	8000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3248.233997' ,	127.59685,33.01723	,	8000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3307.34468' ,	127.43857,33.01921	,	8000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3367.191208' ,	127.27830,33.01921	,	8000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3427.874215' ,	127.13981,32.94799	,	8000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3473.395009' ,	127.08441,32.85697	,	8000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3520.098996' ,	127.08638,32.75211	,	8000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3597.651043' ,	127.09034,32.57800	,	8000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3640.163094' ,	127.15366,32.49886	,	8000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3677.853842' ,	127.24467,32.46324	,	8000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3724.23203' ,	127.36536,32.44148	,	8000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3765.888432' ,	127.45637,32.49490	,	8000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3864.187878' ,	127.65027,32.64329	,	8000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '3963.430266' ,	127.78085,32.83719	,	8000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4063.695966' ,	127.82834,33.05878	,	8000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4165.236437' ,	127.88176,33.28236	,	8000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4261.517971' ,	128.06774,33.43273	,	8000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4368.577069' ,	128.35067,33.47824	,	8000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4419.860215' ,	128.48522,33.45251	,	8000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4467.899709' ,	128.58810,33.38722	,	8000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4521.289717' ,	128.62965,33.27247	,	8000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4579.451114' ,	128.63163,33.14188	,	8000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4630.111991' ,	128.59206,33.03306	,	8000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4676.07129' ,	128.50302,32.96183	,	8000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4731.219062' ,	128.38233,32.89061	,	8000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4787.776367' ,	128.23790,32.85301	,	8000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4882.673292' ,	127.98860,32.89259	,	8000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '4980.698582' ,	127.74128,32.96579	,	8000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5039.309513' ,	127.59685,33.01723	,	8000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5098.420195' ,	127.43857,33.01921	,	8000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5158.266723' ,	127.27830,33.01921	,	8000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5218.949731' ,	127.13981,32.94799	,	8000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5264.44198' ,	127.08441,32.85697	,	8000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5311.145967' ,	127.08638,32.75211	,	8000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5388.698015' ,	127.09034,32.57800	,	8000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5431.210065' ,	127.15366,32.49886	,	8000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5468.900813' ,	127.24467,32.46324	,	8000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5515.279001' ,	127.36536,32.44148	,	8000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5556.935403' ,	127.45637,32.49490	,	8000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5655.234849' ,	127.65027,32.64329	,	8000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5754.477237' ,	127.78085,32.83719	,	8000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5854.742937' ,	127.82834,33.05878	,	8000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '5956.283408' ,	127.88176,33.28236	,	8000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6052.564943' ,	128.06774,33.43273	,	8000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6159.62404' ,	128.35067,33.47824	,	8000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6210.907186' ,	128.48522,33.45251	,	8000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6258.94668' ,	128.58810,33.38722	,	8000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6312.336688' ,	128.62965,33.27247	,	8000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6370.498085' ,	128.63163,33.14188	,	8000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6421.158962' ,	128.59206,33.03306	,	8000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6467.118261' ,	128.50302,32.96183	,	8000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6522.266033' ,	128.38233,32.89061	,	8000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6578.823339' ,	128.23790,32.85301	,	8000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6673.720264' ,	127.98860,32.89259	,	8000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6771.745554' ,	127.74128,32.96579	,	8000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6830.356484' ,	127.59685,33.01723	,	8000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6889.467166' ,	127.43857,33.01921	,	8000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '6949.313695' ,	127.27830,33.01921	,	8000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7009.996702' ,	127.13981,32.94799	,	8000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7055.488951' ,	127.08441,32.85697	,	8000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7102.192938' ,	127.08638,32.75211	,	8000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7179.744986' ,	127.09034,32.57800	,	8000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7222.257036' ,	127.15366,32.49886	,	8000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7259.947784' ,	127.24467,32.46324	,	8000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7306.325972' ,	127.36536,32.44148	,	8000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7347.982375' ,	127.45637,32.49490	,	8000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7446.28182' ,	127.65027,32.64329	,	8000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7545.524208' ,	127.78085,32.83719	,	8000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7645.789909' ,	127.82834,33.05878	,	8000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7747.33038' ,	127.88176,33.28236	,	8000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7843.611914' ,	128.06774,33.43273	,	8000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '7950.671011' ,	128.35067,33.47824	,	8000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8001.954158' ,	128.48522,33.45251	,	8000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8049.993652' ,	128.58810,33.38722	,	8000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8103.383659' ,	128.62965,33.27247	,	8000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8161.545056' ,	128.63163,33.14188	,	8000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8212.205933' ,	128.59206,33.03306	,	8000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8258.165232' ,	128.50302,32.96183	,	8000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8313.313005' ,	128.38233,32.89061	,	8000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8369.87031' ,	128.23790,32.85301	,	8000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8464.767235' ,	127.98860,32.89259	,	8000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8562.792525' ,	127.74128,32.96579	,	8000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8621.403455' ,	127.59685,33.01723	,	8000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8680.514137' ,	127.43857,33.01921	,	8000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8740.360666' ,	127.27830,33.01921	,	8000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8801.043673' ,	127.13981,32.94799	,	8000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8846.535923' ,	127.08441,32.85697	,	8000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8893.239909' ,	127.08638,32.75211	,	8000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '8970.791957' ,	127.09034,32.57800	,	8000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9013.304007' ,	127.15366,32.49886	,	8000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9050.994755' ,	127.24467,32.46324	,	8000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9097.372944' ,	127.36536,32.44148	,	8000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9139.029346' ,	127.45637,32.49490	,	8000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9237.328791' ,	127.65027,32.64329	,	8000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9336.57118' ,	127.78085,32.83719	,	8000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9436.83688' ,	127.82834,33.05878	,	8000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9538.377351' ,	127.88176,33.28236	,	8000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9634.658885' ,	128.06774,33.43273	,	8000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9741.717983' ,	128.35067,33.47824	,	8000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9793.001129' ,	128.48522,33.45251	,	8000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9841.040623' ,	128.58810,33.38722	,	8000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9894.430631' ,	128.62965,33.27247	,	8000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '9952.592027' ,	128.63163,33.14188	,	8000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10003.2529' ,	128.59206,33.03306	,	8000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10049.2122' ,	128.50302,32.96183	,	8000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10104.35998' ,	128.38233,32.89061	,	8000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10160.91728' ,	128.23790,32.85301	,	8000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10255.81421' ,	127.98860,32.89259	,	8000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10353.8395' ,	127.74128,32.96579	,	8000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10412.45043' ,	127.59685,33.01723	,	8000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10471.56111' ,	127.43857,33.01921	,	8000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10531.40764' ,	127.27830,33.01921	,	8000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10592.09064' ,	127.13981,32.94799	,	8000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=41 )
),
/*Точка P41 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10637.58289' ,	127.08441,32.85697	,	8000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=42 )
),
/*Точка P42 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10684.28688' ,	127.08638,32.75211	,	8000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=43 )
),
/*Точка P43 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10761.83893' ,	127.09034,32.57800	,	8000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=44 )
),
/*Точка P44 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10804.35098' ,	127.15366,32.49886	,	8000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=45 )
),
/*Точка P45 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10842.04173' ,	127.24467,32.46324	,	8000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P46 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10888.41991' ,	127.36536,32.44148	,	8000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P47 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '10930.07632' ,	127.45637,32.49490	,	8000	,	81.4265113062944	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P48 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '11067.54634' ,	127.81845,32.54040	,	8000	,	77.0157447448733	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P49 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '11213.78544' ,	128.19833,32.61361	,	8000	,	75.5878383628898	,	250	,	-12.74582278,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P50 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '11370.69959' ,	128.60393,32.70067	,	6000	,	89.5837493906398	,	240	,	-5.535188422,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P51 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '11551.36192' ,	129.06691,32.70265	,	5000	,	80.6065401118857	,	210	,	-9.095632002,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P52 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '11661.3048' ,	129.31027,32.73628	,	4000	,	54.6551620127634	,	180	,	-9.450188698,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P53 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '11767.12279' ,	129.47647,32.83521	,	3000	,	32.0600571330369	,	150	,	-6.344610836,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=34 )
),
/*Точка P54 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '11924.73687' ,	129.61101,33.01526	,	2000	,	6.36693952019   	,	100	,	-6.021665095,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P55 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '12090.8039' ,	129.63079,33.16365	,	1000	,	357.907953231846	,	80	,	-4.276488021,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
),
/*Точка P56 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'F-16D №2 guard E-3C'), '12324.64063' ,	129.62344, 33.33176	,	0	,	357.907953231846	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=32 )
);