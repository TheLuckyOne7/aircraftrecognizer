﻿-- select air_object_id from re_situation.air_object where air_object_name like 'B-1B master'

/*
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
-- */

/*
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APQ-164'
and abr_name_mode_r like 'Режим Р2'
-- */

/*
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'B-1B master'
and pfta.code_name like 'ЛА-"В-1В" БРЛС-"AN/APQ-164" №1'
and dmar.abr_name_mode_r like 'Режим Р2'
-- */