﻿insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)
values
/*Точка P0*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '0'        ,	129.62344, 33.33176	,	0	,	359.762737219524	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=102 )
),
/*Точка P1*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '180'        ,	129.62344, 33.33176	,	0	,	359.762737219524	,	80	,	6.375351379,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=102 )
),
/*Точка P2*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '336.8540996',	129.62288,33.44460	,	1000	,	0               	,	100	,	15.49060383,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=101 )
),
/*Точка P3*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '433.6869937',	129.62288,33.53166	,	2500	,	320.234059348107	,	120	,	14.44346389,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=102 )
),
/*Точка P4*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '572.1579322',	129.50812,33.64641	,	4500	,	285.783314743683	,	140	,	13.61697379,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=107 )
),
/*Точка P5*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '682.3145664',	129.34786,33.68400	,	6000	,	270.713835836974	,	160	,	12.47158652,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=107 )
),
/*Точка P6*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '802.5879573',	129.14011,33.68598	,	7500	,	95.5108510865669	,	180	,	11.91751996,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=108 )
),
/*Точка P7*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '928.4530706',	128.89675,33.66620	,	9000	,	101.009812547814	,	200	,	7.58880346,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=107 )
),
/*Точка P8*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '1060.22615',	128.61778,33.62069	,	10000	,	104.997813793904	,	220	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=108 )
),
/*Точка P9*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '1214.061864',	128.26560,33.54155	,	10000	,	112.765321771661	,	240	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=108 )
),
/*Точка P10*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '1337.034141',	127.97277,33.43866	,	10000	,	110.305169982287	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=107 )
),
/*Точка P11*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '1438.292321',	127.71754,33.35952	,	10000	,	117.213325272936	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=108 )
),
/*Точка P12*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '1565.13731',	127.41482,33.22894	,	10000	,	140.569163377214	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=109 )
),
/*Точка P13*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '1727.000919',	127.13981,32.94799	,	10000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=110 )
),
/*Точка P14*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '1772.535984',	127.08441,32.85697	,	10000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P15*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '1819.254614',	127.08638,32.75211	,	10000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P16*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '1896.830976',	127.09034,32.57800	,	10000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P17*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '1939.356355',	127.15366,32.49886	,	10000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P18*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '1977.05892',	127.24467,32.46324	,	10000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P19*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2023.45165',	127.36536,32.44148	,	10000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=118 )
),
/*Точка P20*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2065.121112',	127.45637,32.49490	,	10000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=119 )
),
/*Точка P21*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2163.451378',	127.65027,32.64329	,	10000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P22*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2262.724881',	127.78085,32.83719	,	10000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P23*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2363.022018',	127.82834,33.05878	,	10000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=116 )
),
/*Точка P24*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2464.594324',	127.88176,33.28236	,	10000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=117 )
),
/*Точка P25*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2560.906046',	128.06774,33.43273	,	10000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=115 )
),
/*Точка P26*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2667.998709',	128.35067,33.47824	,	10000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P27*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2719.297934',	128.48522,33.45251	,	10000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P28*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2767.35249',	128.58810,33.38722	,	10000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=112 )
),
/*Точка P29*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2820.759237',	128.62965,33.27247	,	10000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=111 )
),
/*Точка P30*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2878.938869',	128.63163,33.14188	,	10000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=117 )
),
/*Точка P31*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2929.61563',	128.59206,33.03306	,	10000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=118 )
),
/*Точка P32*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '2975.589339',	128.50302,32.96183	,	10000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=119 )
),
/*Точка P33*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3030.754401',	128.38233,32.89061	,	10000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P34*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3087.329439',	128.23790,32.85301	,	10000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P35*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3182.256117',	127.98860,32.89259	,	10000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P36*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3280.31214',	127.74128,32.96579	,	10000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P37*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3338.941447',	127.59685,33.01723	,	10000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=110 )
),
/*Точка P38*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3398.070662',	127.43857,33.01921	,	10000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P39*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3457.935954',	127.27830,33.01921	,	10000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P40*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3518.637988',	127.13981,32.94799	,	10000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P14*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3564.173053',	127.08441,32.85697	,	10000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P15*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3610.891683',	127.08638,32.75211	,	10000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P16*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3688.468045',	127.09034,32.57800	,	10000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P17*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3730.993424',	127.15366,32.49886	,	10000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P18*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3768.695989',	127.24467,32.46324	,	10000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=118 )
),
/*Точка P19*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3815.088719',	127.36536,32.44148	,	10000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=119 )
),
/*Точка P20*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3856.758181',	127.45637,32.49490	,	10000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P21*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '3955.088447',	127.65027,32.64329	,	10000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P22*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4054.36195',	127.78085,32.83719	,	10000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=116 )
),
/*Точка P23*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4154.659087',	127.82834,33.05878	,	10000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=117 )
),
/*Точка P24*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4256.231393',	127.88176,33.28236	,	10000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=115 )
),
/*Точка P25*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4352.543115',	128.06774,33.43273	,	10000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P26*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4459.635778',	128.35067,33.47824	,	10000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P27*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4510.935003',	128.48522,33.45251	,	10000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=112 )
),
/*Точка P28*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4558.989559',	128.58810,33.38722	,	10000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=111 )
),
/*Точка P29*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4612.396306',	128.62965,33.27247	,	10000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=117 )
),
/*Точка P30*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4670.575938',	128.63163,33.14188	,	10000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=118 )
),
/*Точка P31*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4721.252699',	128.59206,33.03306	,	10000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=119 )
),
/*Точка P32*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4767.226408',	128.50302,32.96183	,	10000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P33*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4822.39147',	128.38233,32.89061	,	10000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P34*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4878.966508',	128.23790,32.85301	,	10000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P35*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '4973.893186',	127.98860,32.89259	,	10000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P36*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5071.94921',	127.74128,32.96579	,	10000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P37*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5130.578516',	127.59685,33.01723	,	10000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P38*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5189.707731',	127.43857,33.01921	,	10000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P39*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5249.573023',	127.27830,33.01921	,	10000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P40*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5310.275057',	127.13981,32.94799	,	10000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P14*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5355.774442',	127.08441,32.85697	,	10000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P15*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5402.493072',	127.08638,32.75211	,	10000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=118 )
),
/*Точка P16*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5480.069434',	127.09034,32.57800	,	10000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=119 )
),
/*Точка P17*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5522.594813',	127.15366,32.49886	,	10000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P18*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5560.297378',	127.24467,32.46324	,	10000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P19*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5606.690108',	127.36536,32.44148	,	10000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=116 )
),
/*Точка P20*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5648.35957',	127.45637,32.49490	,	10000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=117 )
),
/*Точка P21*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5746.689836',	127.65027,32.64329	,	10000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=115 )
),
/*Точка P22*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5845.963339',	127.78085,32.83719	,	10000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P23*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '5946.260476',	127.82834,33.05878	,	10000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P24*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6047.832782',	127.88176,33.28236	,	10000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=112 )
),
/*Точка P25*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6144.144504',	128.06774,33.43273	,	10000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=111 )
),
/*Точка P26*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6251.237167',	128.35067,33.47824	,	10000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=117 )
),
/*Точка P27*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6302.536392',	128.48522,33.45251	,	10000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=118 )
),
/*Точка P28*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6350.590948',	128.58810,33.38722	,	10000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=119 )
),
/*Точка P29*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6403.997695',	128.62965,33.27247	,	10000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P30*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6462.177327',	128.63163,33.14188	,	10000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P31*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6512.854088',	128.59206,33.03306	,	10000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P32*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6558.827797',	128.50302,32.96183	,	10000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P33*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6613.992859',	128.38233,32.89061	,	10000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P34*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6670.567897',	128.23790,32.85301	,	10000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P35*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6765.494575',	127.98860,32.89259	,	10000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P36*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6863.550598',	127.74128,32.96579	,	10000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P37*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6922.179905',	127.59685,33.01723	,	10000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P38*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '6981.30912',	127.43857,33.01921	,	10000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P39*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7041.174412',	127.27830,33.01921	,	10000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P40*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7101.876446',	127.13981,32.94799	,	10000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P14*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7147.375831',	127.08441,32.85697	,	10000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P15*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7194.094461',	127.08638,32.75211	,	10000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P16*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7271.670823',	127.09034,32.57800	,	10000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=118 )
),
/*Точка P17*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7314.196202',	127.15366,32.49886	,	10000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=119 )
),
/*Точка P18*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7351.898767',	127.24467,32.46324	,	10000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P19*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7398.291496',	127.36536,32.44148	,	10000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P20*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7439.960959',	127.45637,32.49490	,	10000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P21*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7538.291224',	127.65027,32.64329	,	10000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=116 )
),
/*Точка P22*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7637.564728',	127.78085,32.83719	,	10000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=117 )
),
/*Точка P23*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7737.861865',	127.82834,33.05878	,	10000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=115 )
),
/*Точка P24*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7839.434171',	127.88176,33.28236	,	10000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P25*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '7935.745893',	128.06774,33.43273	,	10000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P26*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8042.838556',	128.35067,33.47824	,	10000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=112 )
),
/*Точка P27*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8094.137781',	128.48522,33.45251	,	10000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=111 )
),
/*Точка P28*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8142.192337',	128.58810,33.38722	,	10000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=117 )
),
/*Точка P29*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8195.599084',	128.62965,33.27247	,	10000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=118 )
),
/*Точка P30*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8253.778716',	128.63163,33.14188	,	10000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=119 )
),
/*Точка P31*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8304.455477',	128.59206,33.03306	,	10000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P32*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8350.429186',	128.50302,32.96183	,	10000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P33*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8405.594248',	128.38233,32.89061	,	10000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P34*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8462.169286',	128.23790,32.85301	,	10000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P35*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8557.095964',	127.98860,32.89259	,	10000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P36*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8655.151987',	127.74128,32.96579	,	10000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P37*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8713.781294',	127.59685,33.01723	,	10000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P38*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8772.910509',	127.43857,33.01921	,	10000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P39*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8832.775801',	127.27830,33.01921	,	10000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=101 )
),
/*Точка P40*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8893.477834',	127.13981,32.94799	,	10000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P14*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8938.97722',	127.08441,32.85697	,	10000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P15*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '8985.69585',	127.08638,32.75211	,	10000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P16*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9063.272212',	127.09034,32.57800	,	10000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P17*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9105.797591',	127.15366,32.49886	,	10000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=118 )
),
/*Точка P18*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9143.500156',	127.24467,32.46324	,	10000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=119 )
),
/*Точка P19*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9189.892885',	127.36536,32.44148	,	10000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P20*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9231.562348',	127.45637,32.49490	,	10000	,	47.7053842228357	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P21*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9329.892613',	127.65027,32.64329	,	10000	,	29.4941522462325	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P22*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9429.166117',	127.78085,32.83719	,	10000	,	10.1824828667633	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=116 )
),
/*Точка P23*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9529.463253',	127.82834,33.05878	,	10000	,	11.2950721135822	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=117 )
),
/*Точка P24*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9631.03556',	127.88176,33.28236	,	10000	,	45.8804423982172	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=115 )
),
/*Точка P25*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9727.347282',	128.06774,33.43273	,	10000	,	79.0095268822724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P26*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9834.439945',	128.35067,33.47824	,	10000	,	167.126119638998	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P27*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9885.73917',	128.48522,33.45251	,	10000	,	142.781169415632	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=112 )
),
/*Точка P28*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9933.793726',	128.58810,33.38722	,	10000	,	106.843829477024	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=111 )
),
/*Точка P29*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '9987.200473',	128.62965,33.27247	,	10000	,	90.7273544697028	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=117 )
),
/*Точка P30*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10045.3801',	128.63163,33.14188	,	10000	,	163.045240494622	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=118 )
),
/*Точка P31*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10096.05687',	128.59206,33.03306	,	10000	,	133.622257057787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=119 )
),
/*Точка P32*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10142.03057',	128.50302,32.96183	,	10000	,	125.075727028043	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P33*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10197.19564',	128.38233,32.89061	,	10000	,	107.182287032909	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=121 )
),
/*Точка P34*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10253.77067',	128.23790,32.85301	,	10000	,	280.772120282361	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=109 )
),
/*Точка P35*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10348.69735',	127.98860,32.89259	,	10000	,	289.491086329259	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=120 )
),
/*Точка P36*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10446.75338',	127.74128,32.96579	,	10000	,	293.046829241603	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=110 )
),
/*Точка P37*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10505.38268',	127.59685,33.01723	,	10000	,	270.897851209922	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P38*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10564.5119',	127.43857,33.01921	,	10000	,	269.956332800396	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P39*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10624.37719',	127.27830,33.01921	,	10000	,	121.473582729068	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P40*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10685.07922',	127.13981,32.94799	,	10000	,	152.916709485569	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P41*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10730.57861',	127.08441,32.85697	,	10000	,	90.9052093462724	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=113 )
),
/*Точка P42*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10777.29724',	127.08638,32.75211	,	10000	,	91.0979782324967	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=114 )
),
/*Точка P43*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10854.8736',	127.09034,32.57800	,	10000	,	124.017088797199	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=112 )
),
/*Точка P44*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10897.39898',	127.15366,32.49886	,	10000	,	155.134846747253	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=102 )
),
/*Точка P45*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10935.10155',	127.24467,32.46324	,	10000	,	167.971765377418	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=102 )
),
/*Точка P46*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '10981.49427',	127.36536,32.44148	,	10000	,	55.1486201547755	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=101 )
),
/*Точка P47*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '11023.16374',	127.45637,32.49490	,	10000	,	81.4265113062944	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=102 )
),
/*Точка P48*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '11160.67686',	127.81845,32.54040	,	10000	,	77.0157447448733	,	250	,	-13.67408965,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=103 )
),
/*Точка P49*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '11306.93889',	128.19833,32.61361	,	8000	,	75.5878383628898	,	250	,	-12.74582278,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=102 )
),
/*Точка P50*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '11463.85304',	128.60393,32.70067	,	6000	,	89.5837493906398	,	240	,	-5.535188422,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=103 )
),
/*Точка P51*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '11644.51536',	129.06691,32.70265	,	5000	,	80.6065401118857	,	210	,	-9.095632002,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=104 )
),
/*Точка P52*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '11754.45825',	129.31027,32.73628	,	4000	,	54.6551620127634	,	180	,	-9.450188698,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=105 )
),
/*Точка P53*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '11860.27624',	129.47647,32.83521	,	3000	,	32.0600571330369	,	150	,	-6.344610836,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=106 )
),
/*Точка P54*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '12017.89032',	129.61101,33.01526	,	2000	,	6.36693952019   	,	100	,	-6.021665095,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=106 )
),
/*Точка P55*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '12183.95734',	129.63079,33.16365	,	1000	,	357.907953231846	,	80	,	-4.276488021,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=104 )
),
/*Точка P56*/
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-3C AWACS'),  '12417.79408',	129.62344, 33.33176	,	0	,	357.907953231846	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=101 )
);
