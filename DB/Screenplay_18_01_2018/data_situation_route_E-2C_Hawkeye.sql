﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)

values
/*Точка P0 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '0' ,	125.01986, 28.67674	,	0	,	0.115395740752675	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=71 ) 
),
/*Точка P1 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '120' ,	125.01986, 28.67674	,	0	,	0.115395740752675	,	80	,	1.801967189,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=71 ) 
),
/*Точка P2 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '674.9490611' ,	125.02078,29.07597	,	1000	,	17.7444669861546	,	100	,	1.73079885,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=71 ) 
),
/*Точка P3 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '1252.716951' ,	125.20281,29.57061	,	2000	,	19.349407793732	,	120	,	1.933342186,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=72 ) 
),
/*Точка P4 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '1769.955962' ,	125.41649,30.09690	,	3000	,	17.4805210711346	,	140	,	2.135403444,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=71 ) 
),
/*Точка P5 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '2238.251545' ,	125.62226,30.65881	,	4000	,	31.0811683653277	,	160	,	3.17327478,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=72 ) 
),
/*Точка P6 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '2553.38341' ,	125.89530,31.04660	,	5000	,	16.4482365287221	,	180	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=74 ) 
),
/*Точка P7 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '2818.718552' ,	126.03776,31.45814	,	5000	,	335.58545150743	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=75 ) 
),
/*Точка P8 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '3063.046052' ,	125.82407,31.85781	,	5000	,	291.523592825597	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=76 ) 
),
/*Точка P9 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '3225.687345' ,	125.50355,31.96465	,	5000	,	95.7724916981528	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=77 ) 
),
/*Точка P10 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '3419.138609' ,	125.09597,31.92903	,	5000	,	137.871354201487	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=79 ) 
),
/*Точка P11 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '3611.889559' ,	124.82293,31.67182	,	5000	,	95.607724282297	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=80 ) 
),
/*Точка P12 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '3804.357305' ,	124.86250,31.32756	,	5000	,	116.727217105761	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P13 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '4031.045652' ,	125.07618,30.96350	,	5000	,	97.9381625607957	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P14 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '4264.455753' ,	125.14345,30.54801	,	5000	,	154.964377857735	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P15 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '4444.220591' ,	124.98517,30.25519	,	5000	,	112.898087478145	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P16 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '4630.277976' ,	124.62903,30.12460	,	5000	,	284.856872837952	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P17 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '4829.086775' ,	124.22937,30.21561	,	5000	,	334.010189100638	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P18 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '5032.475755' ,	124.04338,30.54405	,	5000	,	357.076264223957	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P19 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '5217.668655' ,	124.02360,30.87645	,	5000	,	35.3982914874537	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P20 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '5393.357746' ,	124.23728,31.13366	,	5000	,	65.177172582464	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P21 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '5540.613385' ,	124.51823,31.24446	,	5000	,	154.017256057436	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P22 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '5720.975908' ,	124.85854,31.10200	,	5000	,	166.939494097615	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P23 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '5875.906218' ,	125.17511,31.03869	,	5000	,	168.965228765556	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P24 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '6047.074841' ,	125.52729,30.97933	,	5000	,	77.8654445348171	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P25 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '6226.515732' ,	125.89530,31.04660	,	5000	,	16.4482365287221	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=79 ) 
),
/*Точка P26 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '6465.317359' ,	126.03776,31.45814	,	5000	,	335.58545150743	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=79 ) 
),
/*Точка P27 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '6709.644859' ,	125.82407,31.85781	,	5000	,	291.523592825597	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=79 ) 
),
/*Точка P28 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '6872.286152' ,	125.50355,31.96465	,	5000	,	95.7724916981528	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=79 ) 
),
/*Точка P29 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '7065.737417' ,	125.09597,31.92903	,	5000	,	137.871354201487	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=80 ) 
),
/*Точка P30 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '7258.488367' ,	124.82293,31.67182	,	5000	,	95.607724282297	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P31 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '7450.956112' ,	124.86250,31.32756	,	5000	,	116.727217105761	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P32 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '7677.644459' ,	125.07618,30.96350	,	5000	,	97.9381625607957	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P33 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '7911.054561' ,	125.14345,30.54801	,	5000	,	154.964377857735	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P34 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '8090.819398' ,	124.98517,30.25519	,	5000	,	112.898087478145	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P35 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '8276.876783' ,	124.62903,30.12460	,	5000	,	123.442454335259	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=81 ) 
),
/*Точка P36 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '8567.565091' ,	124.12648,29.83573	,	5000	,	153.755842541168	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=72 ) 
),
/*Точка P37 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '8771.228759' ,	123.94050,29.50730	,	5000	,	174.315863889898	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=71 ) 
),
/*Точка P38 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '9043.382453' ,	123.88510,29.02057	,	5000	,	93.0082189896347	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=72 ) 
),
/*Точка P39 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '9301.343488' ,	123.91280,28.55759	,	5000	,	96.0160719298395	,	200	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=71 ) 
),
/*Точка P40 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '9505.022795' ,	123.95633,28.19354	,	5000	,	107.363124016718	,	180	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=72 ) 
),
/*Точка P41 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '9715.173973' ,	124.07108,27.86906	,	5000	,	140.868446699235	,	160	,	-3.53438164,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=71 ) 
),
/*Точка P42 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '9998.108897' ,	124.42722,27.61185	,	4000	,	89.8881225267507	,	140	,	-2.941562048,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=72 ) 
),
/*Точка P43 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '10338.06433' ,	124.90999,27.61185	,	3000	,	25.5521269344547	,	120	,	-2.560975743,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=71 ) 
),
/*Точка P44 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '10728.5405' ,	125.11575,27.99173	,	2000	,	353.813601486641	,	100	,	-3.475220334,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=71 ) 
),
/*Точка P45 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '11016.29204' ,	125.08410,28.24894	,	1000	,	352.494998433702	,	80	,	-1.667164575,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=71 ) 
),
/*Точка P46 */
( default,	(select air_object_id from re_situation.air_object where air_object_name like 'E-2C Hawkeye'), '11616.11285' ,	125.01986, 28.67674	,	0	,	352.494998433702	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=71 ) 
);

