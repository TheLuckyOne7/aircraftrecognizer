﻿--------------------------------------------------------------------------------- 
-- Описание режимов работы БРЛС воздушного объекта  "КС-135 №1"
-- Самолет заправщик КС-135 №1 

Insert into re_situation.m_airborne_radar_in_air_obj
(
m_airborne_radar_in_air_obj_id , -- Искуственный первичный ключ 
air_object_id , -- внешний ключ на воздушный объект 
placing_f_t_aircraft_id , -- внешний ключ на табл. размещение РЭС на типе ЛА
d_m_airborne_radar_id, -- внешний ключ на описание режима работы 
duration_puls, -- длительность зондирующего импульса, мкс 
width_spectrum_puls, -- Ширина спектра зондирующего импульса, МГц 
cycle_time_in_pack, -- Период повторения зондирующих импульсов, мкс
freq_pulses_in_a_pack, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
duration_pack, -- Длительность КГПИ, мс
period_pack, -- Мин. Период следования КГПИ, мс
q_of_carrier_freq_in_pack, -- Количество несущих ча-стот в пачке, шт
r_ch_freq_of_pack_in_overview, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
dur_of_r_irradiation_obj, -- Длительность облучения объ-екта , мс
period_of_r_irradiation_obj -- Период облу-чения объекта (цикл скани-рования), с
 )
 values 
-------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р1"

(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р1'
), -- внешний ключ на описание режима работы "Режим Р1" БРЛМ "AN/APQ-164" 

4.5, -- длительность зондирующего импульса, мкс 
0.22, -- Ширина спектра зондирующего импульса, МГц 
2500, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
1000.0, -- Период следования КГПИ, мс
1, -- Количество несущих частот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
1.0 -- Период облучения объекта (цикл скани-рования), с
),
--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р2"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р2'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

3.6, -- длительность зондирующего импульса, мкс 
0.28, -- Ширина спектра зондирующего импульса, МГц 
2000, -- Период повторения зондирующих импульсов, мкс
10.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
2860.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
2.86 -- Период облучения объекта (цикл сканирования), с
),
--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р3"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р3'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

2.0, -- длительность зондирующего импульса, мкс 
0.5, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
10.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
6000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
6.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р4"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р4'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

0.35, -- длительность зондирующего импульса, мкс 
3.0, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
10.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
45.5, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
7.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
83.0, -- Длительность облучения объ-екта , мс
0.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р5"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р5'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

3.6, -- длительность зондирующего импульса, мкс 
0.28, -- Ширина спектра зондирующего импульса, МГц 
2500, -- Период повторения зондирующих импульсов, мкс
7.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
1725.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
1.73 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р6"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р6'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

1.0, -- длительность зондирующего импульса, мкс 
1.0, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
7.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
43.0, -- Длительность КГПИ, мс
2850.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
2.85 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APN-59" условное наименование "ЛА-"KС-135" БРЛС-"AN/APN-59" №1"
-- режим работы "Режим Р7"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'КС-135 №1'
), -- внешний ключ на воздушный объект 'КС-135 №1'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"KС-135" БРЛС-"AN/APN-59" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APN-59'
and abr_name_mode_r like 'Режим Р7'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛМ "AN/APQ-164" 

0.35, -- длительность зондирующего импульса, мкс 
3.0, -- Ширина спектра зондирующего импульса, МГц 
500, -- Период повторения зондирующих импульсов, мкс
12.5, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
22.0, -- Длительность КГПИ, мс
24.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
43.0, -- Длительность облучения объ-екта , мс
2.86 -- Период облучения объекта (цикл сканирования), с
);


--------------------------------------------------------------------------------- 
-- Задание рабочих частот для БРЛС "ЛА-"KС-135" БРЛС-"AN/APN-59" №1" 
-- воздушного объекта "КС-135 №1"

insert into re_situation.oper_freq_air_objects
(
m_airborne_radar_in_air_obj_id , -- Внешний ключ 
operating_frequency  -- значение рабочей частоты 
)
values 
-- Рабочая частота для режима "Режим Р1"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'КС-135 №1'
and pfta.code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
and dmar.abr_name_mode_r like 'Режим Р1'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.350 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р2"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'КС-135 №1'
and pfta.code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
and dmar.abr_name_mode_r like 'Режим Р2'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.380 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р3"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'КС-135 №1'
and pfta.code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
and dmar.abr_name_mode_r like 'Режим Р3'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.380 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р4"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'КС-135 №1'
and pfta.code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
and dmar.abr_name_mode_r like 'Режим Р4'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.380 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р5"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'КС-135 №1'
and pfta.code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
and dmar.abr_name_mode_r like 'Режим Р5'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.380 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р6"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'КС-135 №1'
and pfta.code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
and dmar.abr_name_mode_r like 'Режим Р6'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.380 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р7"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'КС-135 №1'
and pfta.code_name like 'ЛА-"KС-135" БРЛС-"AN/APN-59" №1'
and dmar.abr_name_mode_r like 'Режим Р7'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.380 -- значение рабочей частоты 
);







  