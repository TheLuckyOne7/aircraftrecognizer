﻿-- delete from re_situation.situation ;
-- select setval('re_situation.situation_situation_id_seq', 1);


insert into re_situation.situation
(
situation_id ,  -- искуственній первичный ключ 
air_object_id , -- id - номер воздушного объекта 
operative_time , -- оперативное время, секунд
x_longitude , -- географическая долгота, градусов 
y_latitude , -- географическая широта, градусов  
h_height , -- высота, меторов 
course , -- курс, градусов в перделах от 0 до 360
horizontal_velocity , -- горизонтальная скорость 
vertical_velocity, -- вертикальная скорость 
aircraft_condition_id -- номер состояния
)
values 
/* точка P0 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'0'		,	125.01986, 28.67674	,	0	,	358.960948668194	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P1 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'480'		,	125.01986, 28.67674	,	0	,	358.960948668194	,	200	,	2.512160215,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P2 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'878.0637835'	,	125.00496,29.39254	,	1000	,	0.989953414387777	,	250	,	2.83975214,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P3 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'1230.207193'	,	125.02078,30.18396	,	2000	,	357.273435030041	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
),
/* точка P4 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'1858.872168'	,	124.92581,31.87759	,	2000	,	329.900562196318	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=11 ) 
),
/* точка P5 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'2164.52586'	,	124.43513,32.58987	,	2000	,	318.444004165865	,	300	,	-3.766758657,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=12 ) 
),
/* точка P6 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'2629.116284'	,	123.43795,33.52374	,	250	,	316.508510553609	,	300	,	-0.401663438,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=13 ) 
),
/* точка P7 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'3002.563269'	,	122.59905,34.25185	,	100	,	305.071591077945	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=14 ) 
),
/* точка P8 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'3414.182954'	,	121.49106,34.88498	,	100	,	292.76171623912 	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=15 ) 
),
/* точка P9 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'3705.374493'	,	120.60467,35.18572	,	100	,	116.106957897537	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=15 ) 
),
/* точка P10 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'3845.754001'	,	120.18940,35.01835	,	100	,	127.014001147334	,	300	,	5.510747072,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=6 ) 
),
/* точка P11 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'4009.07124'	,	119.76122,34.75236	,	1000	,	120.750943949375	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=7 ) 
),
/* точка P12 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'4172.227516'	,	119.37846,34.56423	,	1000	,	269.928227034626	,	250	,	-9.71061219,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=3 ) 
),
/* точка P13 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'4264.909623'	,	119.12544,34.56423	,	100	,	322.925742253345	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=3 ) 
),
/* точка P14 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'4379.531789'	,	118.89838,34.81075	,	100	,	359.376282865257	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=3 ) 
),
/* точка P15 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'4559.892421'	,	118.89189,35.29732	,	100	,	26.6821362652991	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=3 ) 
),
/* точка P16 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'4751.106229'	,	119.17735,35.75793	,	100	,	44.6462975305933	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=3 ) 
),
/* точка P17 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'4917.046725'	,	119.56660,36.07582	,	100	,	49.6797369537677	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=3 ) 
),
/* точка P18 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'5069.41859'	,	119.95571,36.34119	,	100	,	63.0184788388521	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=3 ) 
),
/* точка P19 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'5212.27985'	,	120.38308,36.51530	,	100	,	138.771149772717	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=3 ) 
),
/* точка P20 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'5363.269485'	,	120.76296,36.24622	,	100	,	136.221648060045	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P21 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'5633.680839'	,	121.41192,35.73971	,	100	,	133.869859443751	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=4 ) 
),
/* точка P22 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'6127.406033'	,	122.53573,34.77418	,	100	,	127.476355994218	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=11 ) 
),
/* точка P23 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'6584.193725'	,	123.43795,33.79282	,	100	,	132.176873812428	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=12 ) 
),
/* точка P24 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'7002.130921'	,	124.34016,32.95392	,	100	,	126.554477870695	,	300	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=13 ) 
),
/* точка P25 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'7402.762917'	,	125.09993,32.08336	,	100	,	112.792403807343	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=14 ) 
),
/* точка P26 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'7822.426614'	,	125.52729,31.21280	,	100	,	93.1387675908787	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=15 ) 
),
/* точка P27 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'8379.430485'	,	125.60643,29.96236	,	100	,	178.678612629221	,	250	,	3.54956676,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=16 ) 
),
/* точка P28 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'8914.707077'	,	125.57478,28.75941	,	2000	,	98.9698089262294	,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=17 ) 
),
/* точка P29 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'9435.092767'	,	125.78055,27.60394	,	2000	,	141.676899407043	,	250	,	-6.194452922,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=18 ) 
),
/* точка P30 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'9596.527523'	,	125.52729,27.31903	,	1000	,	294.32162394832		,	250	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P31 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'9733.675485'	,	125.21072,27.44565	,	1000	,	332.879006848254	,	250	,	-3.325360744,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P32 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'9884.035112'	,	125.03661,27.74639	,	500	,	359.095013662815	,	200	,	-0.966489134,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
),
/* точка P33 */
(default,	(select air_object_id from re_situation.air_object where air_object_name like 'F/A-18F №5 ADSG'), 	'10401.3715'	,	125.01986, 28.67674	,	0	,	359.095013662815	,	0	,	0,	
( select aircraft_condition_id from re_situation.aircraft_condition where nsr_esrc_id=2 ) 
);

