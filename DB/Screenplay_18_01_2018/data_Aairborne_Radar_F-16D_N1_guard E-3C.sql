﻿-- Описание режимов работы БРЛС воздушного объекта  "F-16D №1 guard E-3C" 
-- Самолет F-16D №1 из группы охраны самоета ДРЛО E-3C 

--------------------------------------------------------------------------------- 
Insert into re_situation.m_airborne_radar_in_air_obj
(
m_airborne_radar_in_air_obj_id , -- Искуственный первичный ключ 
air_object_id , -- внешний ключ на воздушный объект 
placing_f_t_aircraft_id , -- внешний ключ на табл. размещение РЭС на типе ЛА
d_m_airborne_radar_id, -- внешний ключ на описание режима работы 
duration_puls, -- длительность зондирующего импульса, мкс 
width_spectrum_puls, -- Ширина спектра зондирующего импульса, МГц 
cycle_time_in_pack, -- Период повторения зондирующих импульсов, мкс
freq_pulses_in_a_pack, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
duration_pack, -- Длительность КГПИ, мс
period_pack, -- Мин. Период следования КГПИ, мс
q_of_carrier_freq_in_pack, -- Количество несущих ча-стот в пачке, шт
r_ch_freq_of_pack_in_overview, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
dur_of_r_irradiation_obj, -- Длительность облучения объ-екта , мс
period_of_r_irradiation_obj -- Период облу-чения объекта (цикл скани-рования), с
 )
 values 
--------------------------------------------------------------------------------- 
-- описание режимов работы БРЛС воздушного объекта  "F-16D №1 guard E-3C"
-- Самолет F-16D №1 из группы охраны самоета ДРЛО E-3C 
--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р1"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р1'
), -- внешний ключ на описание режима работы "Режим Р1" БРЛC "AN/APG-68" 
5.0, -- длительность зондирующего импульса, мкс 
0.20, -- Ширина спектра зондирующего импульса, МГц 
2000, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.5, -- Длительность КГПИ, мс
1150.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20.5, -- Длительность облучения объ-екта , мс
1.15 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р2"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р2'
), -- внешний ключ на описание режима работы "Режим Р2" БРЛC "AN/APG-68" 

2.5, -- длительность зондирующего импульса, мкс 
0.45, -- Ширина спектра зондирующего импульса, МГц 
1000, -- Период повторения зондирующих импульсов, мкс
15.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.5, -- Длительность КГПИ, мс
1150.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20.5, -- Длительность облучения объ-екта , мс
1.15 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р3"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р3'
), -- внешний ключ на описание режима работы "Режим Р3" БРЛC "AN/APG-68" 

0.4, -- длительность зондирующего импульса, мкс 
2.6, -- Ширина спектра зондирующего импульса, МГц 
250, -- Период повторения зондирующих импульсов, мкс
7.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
24.3, -- Длительность КГПИ, мс
1300.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
24.8, -- Длительность облучения объ-екта , мс
1.3 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р4"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р4'
), -- внешний ключ на описание режима работы "Режим Р4" БРЛC "AN/APG-68" 

0.20, -- длительность зондирующего импульса, мкс 
5.4, -- Ширина спектра зондирующего импульса, МГц 
125, -- Период повторения зондирующих импульсов, мкс
8.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
24.3, -- Длительность КГПИ, мс
1300.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
24.8, -- Длительность облучения объ-екта , мс
1.3 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р5"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р5'
), -- внешний ключ на описание режима работы "Режим Р5" БРЛC "AN/APG-68" 

30.0, -- длительность зондирующего импульса, мкс 
1.2, -- Ширина спектра зондирующего импульса, МГц 
500.0, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.5, -- Длительность КГПИ, мс
165.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20.5, -- Длительность облучения объ-екта , мс
0.165 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р6"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р6'
), -- внешний ключ на описание режима работы "Режим Р6" БРЛC "AN/APG-68" 

30.0, -- длительность зондирующего импульса, мкс 
1.2, -- Ширина спектра зондирующего импульса, МГц 
500.0, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.5, -- Длительность КГПИ, мс
950.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20.5, -- Длительность облучения объ-екта , мс
1.0 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р7"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р7'
), -- внешний ключ на описание режима работы "Режим Р7" БРЛC "AN/APG-68" 

30.0, -- длительность зондирующего импульса, мкс 
1.2, -- Ширина спектра зондирующего импульса, МГц 
500.0, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.5, -- Длительность КГПИ, мс
4000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20.5, -- Длительность облучения объ-екта , мс
4.0 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р8"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р8'
), -- внешний ключ на описание режима работы "Режим Р8" БРЛC "AN/APG-68" 

15.0, -- длительность зондирующего импульса, мкс 
1.2, -- Ширина спектра зондирующего импульса, МГц 
500.0, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
1670.5, -- Длительность КГПИ, мс
2000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
140.5, -- Длительность облучения объ-екта , мс
0.0 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р9"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р9'
), -- внешний ключ на описание режима работы "Режим Р9" БРЛC "AN/APG-68" 

1.30, -- длительность зондирующего импульса, мкс 
0.85, -- Ширина спектра зондирующего импульса, МГц 
6.50, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.5, -- Длительность КГПИ, мс
165.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20.5, -- Длительность облучения объ-екта , мс
0.165 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р10"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р10'
), -- внешний ключ на описание режима работы "Режим Р10" БРЛC "AN/APG-68" 

1.30, -- длительность зондирующего импульса, мкс 
0.85, -- Ширина спектра зондирующего импульса, МГц 
6.50, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
0.83, -- Длительность КГПИ, мс
0.875, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
165.0, -- Длительность облучения объ-екта , мс
0.0 -- Период облучения объекта (цикл сканирования), с
), 

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р11"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р11'
), -- внешний ключ на описание режима работы "Режим Р11" БРЛC "AN/APG-68" 

8.0, -- длительность зондирующего импульса, мкс 
0.15, -- Ширина спектра зондирующего импульса, МГц 
75.0, -- Период повторения зондирующих импульсов, мкс
16.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
0.83, -- Длительность КГПИ, мс
0.875, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20.5 , -- Длительность облучения объ-екта , мс
2.9 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р12"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р12'
), -- внешний ключ на описание режима работы "Режим Р12" БРЛC "AN/APG-68" 

6.0, -- длительность зондирующего импульса, мкс 
0.15, -- Ширина спектра зондирующего импульса, МГц 
75.0, -- Период повторения зондирующих импульсов, мкс
14.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
0.83, -- Длительность КГПИ, мс
0.875, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
2900.0 , -- Длительность облучения объ-екта , мс
0.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р13"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р13'
), -- внешний ключ на описание режима работы "Режим Р13" БРЛC "AN/APG-68" 

15.0, -- длительность зондирующего импульса, мкс 
1.20, -- Ширина спектра зондирующего импульса, МГц 
500.0, -- Период повторения зондирующих импульсов, мкс
17.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
1.75, -- Длительность КГПИ, мс
2.25, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20.5 , -- Длительность облучения объ-екта , мс
0.90 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р14"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р14'
), -- внешний ключ на описание режима работы "Режим Р14" БРЛC "AN/APG-68" 

15.0, -- длительность зондирующего импульса, мкс 
1.20, -- Ширина спектра зондирующего импульса, МГц 
500.0, -- Период повторения зондирующих импульсов, мкс
18.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
1.75, -- Длительность КГПИ, мс
2.25, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
2900.0 , -- Длительность облучения объ-екта , мс
0.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р15"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р15'
), -- внешний ключ на описание режима работы "Режим Р15" БРЛC "AN/APG-68" 

14.8, -- длительность зондирующего импульса, мкс 
5.90, -- Ширина спектра зондирующего импульса, МГц 
250.0, -- Период повторения зондирующих импульсов, мкс
9.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.50, -- Длительность КГПИ, мс
2000.0, -- Период следования КГПИ, мс
1, -- Количество несущих ча-стот в пачке, шт
0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20.5 , -- Длительность облучения объ-екта , мс
2.0 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р16"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р16'
), -- внешний ключ на описание режима работы "Режим Р16" БРЛC "AN/APG-68" 

14.8, -- длительность зондирующего импульса, мкс 
5.90, -- Ширина спектра зондирующего импульса, МГц 
125.0, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
20.50, -- Длительность КГПИ, мс
850.0, -- Период следования КГПИ, мс
2, -- Количество несущих ча-стот в пачке, шт
7.5, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
20.5 , -- Длительность облучения объ-екта , мс
0.85 -- Период облучения объекта (цикл сканирования), с
),

--------------------------
-- БРЛС типа "AN/APG-68" условное наименование "ЛА-"F-16D" БРЛС-"AN/APG-68" №1"
-- режим работы "Режим Р17"
(
default, -- Искуственный первичный ключ 
(
select air_object_id from re_situation.air_object where air_object_name like 'F-16D №1 guard E-3C'
), -- внешний ключ на воздушный объект 'F-16D №1 guard E-3C'
	
(
select placing_f_t_aircraft_id from re_situation.placing_f_t_aircraft 
where code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
), -- внешний ключ на табл. размещение РЭС на типе ЛА code_name='ЛА-"F-16D" БРЛС-"AN/APG-68" №1'

(
select d_m_airborne_radar_id
from re_situation.d_m_airborne_radar as dm, re_situation.types_of_re_facilities as tf
where dm.types_of_re_facilities_id = tf.types_of_re_facilities_id
and name_facilities_e like 'AN/APG-68'
and abr_name_mode_r like 'Режим Р17'
), -- внешний ключ на описание режима работы "Режим Р17" БРЛC "AN/APG-68" 

14.8, -- длительность зондирующего импульса, мкс 
5.90, -- Ширина спектра зондирующего импульса, МГц 
125.0, -- Период повторения зондирующих импульсов, мкс
0.0, -- Ширина диапазона перестройки несущей частоты в пачке,МГц
850.0, -- Длительность КГПИ, мс
850.0, -- Период следования КГПИ, мс
2, -- Количество несущих ча-стот в пачке, шт
13.0, -- Величина диапазона перестройки частоты повторения КГПИ в об-зоре, шт
850.5 , -- Длительность облучения объ-екта , мс
0.0 -- Период облучения объекта (цикл сканирования), с
);


--------------------------------------------------------------------------------- 
-- Задание рабочих частот для БРЛС "ЛА-"F-16D" БРЛС-"AN/APG-68" №1" 
-- воздушного объекта "F-16D №1 guard E-3C"
-- /*
insert into re_situation.oper_freq_air_objects
(
m_airborne_radar_in_air_obj_id , -- Внешний ключ 
operating_frequency  -- значение рабочей частоты 
)
values 
-- Рабочая частота для режима "Режим Р1"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р1'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.35 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р2"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р2'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р3"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р3'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р4"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р4'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р5"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р5'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р6"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р6'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р7"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р7'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р8"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р8'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р9"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р9'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р10"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р10'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р11"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р11'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р12"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р12'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р13"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р13'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р14"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р14'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р15"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р15'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р16"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р16'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
),

-- Рабочая частота для режима "Режим Р17"
( 
(
select m_airborne_radar_in_air_obj_id
from re_situation.m_airborne_radar_in_air_obj as mar, re_situation.air_object as ao,
re_situation.placing_f_t_aircraft as pfta, re_situation.d_m_airborne_radar as dmar
where mar.placing_f_t_aircraft_id = pfta.placing_f_t_aircraft_id
and ao.air_object_id = mar.air_object_id
and dmar.d_m_airborne_radar_id = mar.d_m_airborne_radar_id
and ao.air_object_name like 'F-16D №1 guard E-3C'
and pfta.code_name like 'ЛА-"F-16D" БРЛС-"AN/APG-68" №1'
and dmar.abr_name_mode_r like 'Режим Р17'
),  -- Внешний ключ на поле m_airborne_radar_in_air_obj_id описание режима работы  
9.10 -- значение рабочей частоты 
);
-- */
  