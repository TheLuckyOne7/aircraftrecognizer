const express = require('express');
const bodyParser = require('body-parser');
const brain = require('brain.js');
const fs = require('fs');
const { Client } = require('pg')
const trainedNetwork = require('./trained_network/data.json')

const app = express();
const R = require('ramda')
const client = new Client({
	user: 'maximrepey',
	host: 'localhost',
	database: 'Simulator'
})
client.connect()

const net = new brain.NeuralNetwork({hiddenLayers: [18]});
const port = process.env.PORT || 2000;

let normalize = (value, divider = 20500) => value / divider

if(!Object.keys(trainedNetwork).length){
	let trainData = []
	client.query(
		`SELECT d_m_airborne_radar_id,
			(r.frequency_min + r.frequency_max)/2 as carrierFrequency,
			(r.duration_puls_min + r.duration_puls_max)/2 as durationPuls,
			(r.width_spectrum_puls_min + r.width_spectrum_puls_max)/2 as widthSpectrumPuls,
			r.modulation_radar_s_id as modulationRadar,
			(r.cycle_time_in_pack_min + r.cycle_time_in_pack_max)/2 as cycleTimeInPack,
			(r.freq_pulses_in_a_pack_min + r.freq_pulses_in_a_pack_max)/2 as freqPulsesInAPack,
			(r.duration_pack_min + r.duration_pack_max)/2 as durationPack,
			(r.period_pack_min + r.period_pack_max)/2 as periodPack,
			(r.c_frequency_number_min + r.c_frequency_number_max)/2 as frequencyNumber,
			(r.freq_pulses_in_a_pack_min + r.freq_pulses_in_a_pack_max)/2 as freqPulsesInPack,
			(r.dur_to_radio_waves_min + r.dur_to_radio_waves_max)/2 as durRadioWaves,
			(r.period_obj_r_waves_min + r.period_obj_r_waves_max)/2 as periodObjRWaves,
			name_facilities_r
		FROM re_situation.d_m_airborne_radar r left join re_situation.types_of_re_facilities f on r.types_of_re_facilities_id = f.types_of_re_facilities_id
		GROUP BY d_m_airborne_radar_id, name_facilities_r
		ORDER BY d_m_airborne_radar_id;`,
		(err, resp) => {
			trainData = R.map(x => {
				return {
					input: {
						carrierFrequency: normalize(x['carrierFrequency'.toLowerCase()]),
						durationPuls: normalize(x['durationPuls'.toLowerCase()]),
						widthSpectrumPuls: normalize(x['widthSpectrumPuls'.toLowerCase()]),
						modulationRadar: normalize(x['modulationRadar'.toLowerCase()]),
						cycleTimeInPack: normalize(x['cycleTimeInPack'.toLowerCase()]),
						freqPulsesInAPack: normalize(x['freqPulsesInAPack'.toLowerCase()]),
						durationPack: normalize(x['durationPack'.toLowerCase()]),
						periodPack: normalize(x['periodPack'.toLowerCase()]),
						frequencyNumber: normalize(x['frequencyNumber'.toLowerCase()]),
						freqPulsesInPack: normalize(x['freqPulsesInPack'.toLowerCase()]),
						durRadioWaves: normalize(x['durRadioWaves'.toLowerCase()]),
						periodObjRWaves: normalize(x['periodObjRWaves'.toLowerCase()])
					},
					output: {
						[x.name_facilities_r]: 1
					}
				}
			})(resp.rows)
			net.train(trainData,
				{
					errorThresh: 0.005,
					iterations: 20000,
					log: true,
					logPeriod: 1,
					learningRate: 0.3
				}
			)
			let wstream = fs.createWriteStream('./trained_network/data.json')
			wstream.write(JSON.stringify(net.toJSON(),null,4))
			wstream.end()
	})
} else {
	net.fromJSON(trainedNetwork);
}

app.use(bodyParser.json())
let lastLog = 0
app.get('/simulaton-log', (req,res) => {
	debugger
	client.query(
		`SELECT *
		FROM simulation_log.freq_band_air_radars
		WHERE freq_band_air_radars_id > ${lastLog}
		LIMIT 1;`,
		(err, resp) => {
			lastLog = resp.rows[0].freq_band_air_radars_id + 500
			let row = resp.rows[0]
			let input = {
				carrierFrequency: row.carrier_frequency,
				durationPuls: row.duration_puls,
				widthSpectrumPuls: row.width_spectrum_puls,
				modulationRadar: row.modulation_radar_s_id,
				cycleTimeInPack: row.cycle_time_in_pack,
				freqPulsesInAPack: row.freq_pulses_in_a_pack,
				durationPack: row.duration_pack,
				periodPack: row.period_pack,
				frequencyNumber: row.q_of_carrier_freq_in_pack,
				freqPulsesInPack: row.r_ch_freq_of_pack_in_overview,
				durRadioWaves: row.dur_of_r_irradiation_obj,
				periodObjRWaves:row.period_of_r_irradiation_obj
			}
			res.send(input)
	})
})

app.post('/recognize', (req, res) => {
	let inputArr = req.body
	let outputArr = []
	R.forEach(input => {
		let normalizedInput = {
			carrierFrequency: normalize(input.carrierFrequency),
			durationPuls: normalize(input.durationPuls),
			widthSpectrumPuls: normalize(input.widthSpectrumPuls),
			modulationRadar: normalize(input.modulationRadar),
			cycleTimeInPack: normalize(input.cycleTimeInPack),
			freqPulsesInAPack: normalize(input.freqPulsesInAPack),
			durationPack: normalize(input.durationPack),
			periodPack: normalize(input.periodPack),
			frequencyNumber: normalize(input.frequencyNumber),
			freqPulsesInPack: normalize(input.freqPulsesInPack),
			durRadioWaves: normalize(input.durRadioWaves),
			periodObjRWaves: normalize(input.periodObjRWaves)
		}
		let output = net.run(normalizedInput)
		let outputName = Object.keys(output).reduce((a, b) => output[a] > output[b] ? a : b)
		outputArr.push({id: input.id, result: outputName})
	})(inputArr)
	res.send(outputArr)
});

app.listen(port, () => console.log(`Listening on port ${port}`));
